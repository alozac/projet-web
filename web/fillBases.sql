/*
apostrophe dans les VALUES, altGr + 7 sinon
mysql -u root -p Projet < fillBases.sql
*/
DELETE FROM `mission`;
ALTER TABLE `mission` AUTO_INCREMENT = 1;
INSERT INTO `mission`
(`name`, `image`, `descr`, `gold`, `reputation`, `duration`, `endDate`, `pension_id`, `pdv`, `hasEaten`)
VALUES 
('Chaussou', 'game_icon_noir.jpg',
'Chaussou est un adorable petit chat de 3 ans. Sa maîtresse part en vacances et a besoin que vous le gardiez pendant quelques jours. N\'est-il pas adorable ?'
,100, 100, 24, null, null, 100, 3),
('Chaussette', 'game_icon_noir.jpg',
'Chaussette vient tout juste d\'avoir 8 mois ! C\'est un chat noir à pattes blanches et il a besoin que vous vous occupiez de lui quelques temps. Accepterez-vous ?
',100, 100, 24, null, null, 50, 3),
('Carmen', 'game_icon.jpg',
'Carmen est une chatte de 4 ans, noire à poil long. Elle est très affectueuse et joueuse, accepterez-vous de vous occupez d\'elle pour quelques jours ?',
40, 70, 48, null, null, 100, 3),
('Amour', 'game_icon_roux.jpg',
'Amour est une petite chatonne de 5 mois, très peureuse mais adorable, qui a besoin d\'être gardée quelques heures avant le retour de sa maîtresse. Si vous êtes patients, elle vous accordera peut-être un câlin !',
150, 150, 36, null, null, 100, 3),
('Bubulle', 'game_icon_marron.jpg',
'Bubulle n\'est pas un chat comme les autres : il adore l\'eau. Son maître par en vacances et aurait besoin que vous le gardiez quelques jours, n\'oubliez pas de lui faire prendre un bain régulièrement !',
250, 400, 60, null, null, 100, 3),
('Loki', 'game_icon.jpg',
'Loki est un chat tout blanc comme3neige qui a besoin d\'être gardé pendant que sa maîtresse fait ses courses. Ce devrait-être une mission simple pour vous, non?
',	25, 100, 6, null, null, 100, 3),
('Charly', 'game_icon_gris.jpg',
'Charly est un chat très sauvage et actif, il lui faudra beaucoup de place dans votre pension pour pouvoir le garder. Acceptez-vous de garder ce petit monstre ?',
45, 150, 2, null, null, 100, 3),
('Nouille', 'game_icon_gris.jpg',
'Nouille est chat très affectueux et très attaché à son maître. Ce dernier vous fait confiance pour que Nouille se sente à l\'aise avec vous, alors ne le décevez pas !',
175, 125, 1, null, null, 100, 3),
('Yoda', 'game_icon.jpg',
'Yoda est une chatte très gentille mais très gourmande, vous devez la garder pendant quelques heures et faire attention à ce qu\'elle ne mange pas toutes vos réserves de croquettes !',
25, 100, 4, null, null, 100, 3),
('Ficelle', 'game_icon_blanc.jpg',
'Ficelle est sans doute le chat le plus joueur qu\'on ai jamais vu ! Si vous acceptez de le garder, il faudra lui accorder beaucoup de temps de jeu ! Et bien sûr le nourrir en conséquence.',
45, 150, 36, null, null, 100, 3),
('Lumiere', 'game_icon_blanc.jpg',
'Lumiere est une vraie princesse et a besoin que l\'on s\'occupe d\'elle en conséquence. Ne tarrissez pas de câlins ni de croquettes ou elle le fera savoir !
',50,100,24,null,null,100, 3),
('Pitchou', 'game_icon_gris.jpg',
'Pitchou est un petit chat tout gris, que des gens peu recommandables ont abandonné sur le bord d\'une route. Heureusement il a trouvé un nouveau maître, qui l\'a trouvé alors qu\'il partait en vacances. Il vous demande de vous en occuper le temps qu\'il revienne.',
250,400,60,null,null,100, 3),
('K\'rote', 'game_icon.jpg',
'K\'rote est matou tout roux, un peu peureux mais très câlin. Son maître part en vacances et vous fait confiance pour le garder !',
500,500,72,null,null,100,3);


