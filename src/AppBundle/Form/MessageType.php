<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
      $var=$options["data"]->getReceiver();
        $builder->add('receiver', TextType::class, array('label' => "Pour",
        												'data' => $var == null ? "" : $var->getName(),
        												'mapped' => false))
                ->add('toAllPensions', CheckboxType::class, array('mapped'    => false,
                                                                  'label'     => "Toutes les pensions",
                                                                  'required'  => false,
                                                                  'data'      => false))
                ->add('object',   TextType::class, array('label' => "Objet"))
				        ->add('message',  TextareaType::class, array('attr' => array('rows' => '7')))
            	  ->add('send',     SubmitType::class, array('label' => "Envoyer"))
        ;
    }
}