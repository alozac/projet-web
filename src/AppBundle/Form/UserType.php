<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username')
                ->add('email',    EmailType::class)
				->add('password', RepeatedType::class, array(
                                    'type' => PasswordType::class,
                                    'first_options'  => array('label' => 'Mot de passe'),
                                    'second_options' => array('label' => 'Confirmation du mot de passe')))
                ->add('pensionName', TextType::class, array('mapped' => false ,
                										   'label' => "Nom"))
                ->add('pensionAvatar', FileType::class, array('mapped' => false ,
                                                           'label' => "Avatar"))
            	->add('signup', SubmitType::class, array('label' => "S'inscrire"))
        ;
    }
}