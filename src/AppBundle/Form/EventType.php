<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        date_default_timezone_set('Europe/Paris');

        $builder->add('name',            TextType::class,     array('label' => "Nom"))
                ->add('description',     TextareaType::class, array('attr' => array('rows' => "7")))
                ->add('bonusGold',       PercentType::class,  array('label' => "Or",
                                                                     'type' => 'integer',
                                                                     'data' => 0))
            	->add('bonusReputation', PercentType::class,  array('label' => "Réputation",
                                                                     'type' => 'integer',
                                                                     'data' => 0))
                ->add('salesGold',       PercentType::class,  array('label' => "Or",
                                                                     'type' => 'integer',
                                                                     'data' => 0))
                ->add('start',  DateTimeType::class, array('data' => date_create("tomorrow 00:00")))
                ->add('end',    DateTimeType::class, array('data' => date_create("tomorrow 00:00 +1week")))
                ->add('create', SubmitType::class,   array('label' => "Créer"))
        ;
    }
}