<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="bonusGold", type="integer", nullable=true)
     */
    private $bonusGold;

    /**
     * @var int
     *
     * @ORM\Column(name="bonusReputation", type="integer", nullable=true)
     */
    private $bonusReputation;

    /**
     * @var int
     *
     * @ORM\Column(name="salesGold", type="integer", nullable=true)
     */
    private $salesGold;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start", type="datetime", nullable=false)
     */
    private $start;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end", type="datetime", nullable=false)
     */
    private $end;


    public function toArray()
    {
        return ["id"             => $this->id,             "name"      => $this->name,
                "description"    => $this->description,    "bonusGold" => $this->bonusGold,       
                "bonusReputation"=> $this->bonusReputation,"salesGold" => $this->salesGold,
                "start"          => $this->start,          "end"         => $this->end];
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     *
     * @return Event
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get gold
     *
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * Set reputation
     *
     * @param integer $reputation
     *
     * @return Event
     */
    public function setReputation($reputation)
    {
        $this->reputation = $reputation;

        return $this;
    }

    /**
     * Get reputation
     *
     * @return int
     */
    public function getReputation()
    {
        return $this->reputation;
    }

    /**
     * Set start
     *
     * @param \DateTime $start
     *
     * @return Event
     */
    public function setStart($start)
    {
        $this->start = $start;

        return $this;
    }

    /**
     * Get start
     *
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * Set end
     *
     * @param \DateTime $end
     *
     * @return Event
     */
    public function setEnd($end)
    {
        $this->end = $end;

        return $this;
    }

    /**
     * Get end
     *
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * Set bonusGold
     *
     * @param integer $bonusGold
     *
     * @return Event
     */
    public function setBonusGold($bonusGold)
    {
        $this->bonusGold = $bonusGold;

        return $this;
    }

    /**
     * Get bonusGold
     *
     * @return integer
     */
    public function getBonusGold()
    {
        return $this->bonusGold;
    }

    /**
     * Set bonusReputation
     *
     * @param integer $bonusReputation
     *
     * @return Event
     */
    public function setBonusReputation($bonusReputation)
    {
        $this->bonusReputation = $bonusReputation;

        return $this;
    }

    /**
     * Get bonusReputation
     *
     * @return integer
     */
    public function getBonusReputation()
    {
        return $this->bonusReputation;
    }

    /**
     * Set bonusPaws
     *
     * @param integer $bonusPaws
     *
     * @return Event
     */
    public function setBonusPaws($bonusPaws)
    {
        $this->bonusPaws = $bonusPaws;

        return $this;
    }

    /**
     * Get bonusPaws
     *
     * @return integer
     */
    public function getBonusPaws()
    {
        return $this->bonusPaws;
    }

    /**
     * Set salesGold
     *
     * @param integer $salesGold
     *
     * @return Event
     */
    public function setSalesGold($salesGold)
    {
        $this->salesGold = $salesGold;

        return $this;
    }

    /**
     * Get salesGold
     *
     * @return integer
     */
    public function getSalesGold()
    {
        return $this->salesGold;
    }
}
