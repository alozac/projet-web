<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mission
 *
 * @ORM\Table(name="mission")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MissionRepository")
 */
class Mission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="text")
     */
    private $descr;

    /**
     * @var int
     *
     * @ORM\Column(name="gold", type="integer")
     */
    private $gold;

    /**
     * @var int
     *
     * @ORM\Column(name="reputation", type="integer")
     */
    private $reputation;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     */
    private $endDate;


    /**
     * @ORM\ManyToOne(targetEntity="Pension", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $pension;


    /**
     * @ORM\Column(name="pdv", type="integer")
     */
    private $pdv;

    /**
     * @var int
     *
     * @ORM\Column(name="hasEaten", type="integer")
     */
    private $hasEaten;

    private static $costsFood = array(1 => 50, 2 => 100, 3 => 200);
    private static $pdvLoss   = array(50, 30, 20, 10);


    public function __construct(Mission $mission,
                                Pension $pension,
                                \DateTime $endtime){

        $this->name = $mission->getName();
        $this->image = $mission->getImage();
        $this->descr = $mission->getDescr();
        $this->gold = $mission->getGold();
        $this->reputation = $mission->getReputation();
        $this->duration = $mission->getDuration();

        $this->endDate = $endtime;
        $this->pension = $pension;
        $this->pdv = $mission->getPdv();
        $this->hasEaten = 3;
    }

    public function updateDailyRessources($nbDaysPassed){
        $typeEat = $this->hasEaten;
        $pdvLoss = self::$pdvLoss;
        $diffPdv = rand($typeEat == 3 ? 0 : $pdvLoss[$typeEat + 1],
                        $pdvLoss[$typeEat]) / 100;

        $this->pdv -= floor($diffPdv * $this->pdv);
        $this->hasEaten = 0;
        
        for ($i=0; $i < $nbDaysPassed - 1; $i++) { 
            $diffPdv = rand($pdvLoss[0], $pdvLoss[1]) / 100;
            $this->pdv -= floor($diffPdv * $this->pdv);
        }
    }


    public function getCostHeal($events){
        $cost = (100 - $this->pdv)*5;
        for ($i=0; $i < count($events); $i++) { 
            $cost -= floor($cost * $events[$i]->getSalesGold()/100);
        }
        return $cost;
    }

    public static function getCostsFood($events){
        $costs = self::$costsFood;
        for ($i=0; $i < count($events); $i++) { 
            for ($j=1; $j <= 3; $j++) { 
                $costs[$j] -= floor($costs[$j] * $events[$i]->getSalesGold()/100);
            }
        }
        return $costs;
    }

    public function applyBonusEvent($events){
        for ($i=0; $i < count($events); $i++) { 
            $this->gold       += floor($this->gold       * $events[$i]->getBonusGold()/100);
            $this->reputation += floor($this->reputation * $events[$i]->getBonusReputation()/100);
        }
    }

    public function applyMalusPdv(){
        $this->gold       = floor($this->pdv / 100 * $this->gold);
        $pensReput = $this->pension->getReputation();
        if ($this->pdv < 50){
            $malus = - 10 * (50 - $this->pdv);
            if ($pensReput + $malus < 0)
                $this->reputation = - $pensReput;
            else 
                $this->reputation = $malus;
        } else {
            $this->reputation = floor($this->pdv / 100 * $this->reputation);
        }
    }


    public function toArray()
    {
        return ["id"        => $this->id,         "name"    => $this->name,
                "image"     => $this->image,      "descr"   => $this->descr,
                "gold"      => $this->gold,       "pdv"     => $this->pdv,
                "reputation"=> $this->reputation, "duration"=> $this->duration,
                "endDate"   => $this->endDate,    "pension" =>$this->pension,
                "hasEaten"  => $this->hasEaten];
    }

    /**
     * Get pdv
     *
     * @return int
     */
    public function getPdv()
    {
        return $this->pdv;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Mission
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Mission
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return Mission
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     *
     * @return Mission
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get gold
     *
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }


    /**
     * Set reputation
     *
     * @param integer $reputation
     *
     * @return Mission
     */
    public function setReputation($reputation)
    {
        $this->reputation = $reputation;

        return $this;
    }

    /**
     * Get reputation
     *
     * @return int
     */
    public function getReputation()
    {
        return $this->reputation;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     *
     * @return Mission
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return Mission
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    public function setPdv ($pdv){
        $this->pdv = $pdv;
        return $this;
    }

    /**
     * Set pension
     *
     * @param \AppBundle\Entity\Pension $pension
     *
     * @return Mission
     */
    public function setPension(\AppBundle\Entity\Pension $pension = null)
    {
        $this->pension = $pension;

        return $this;
    }

    /**
     * Get pension
     *
     * @return \AppBundle\Entity\Pension
     */
    public function getPension()
    {
        return $this->pension;
    }

    /**
     * Set hasEaten
     *
     * @param integer $hasEaten
     *
     * @return Mission
     */
    public function setHasEaten($hasEaten)
    {
        $this->hasEaten = $hasEaten;

        return $this;
    }

    /**
     * Get hasEaten
     *
     * @return integer
     */
    public function getHasEaten()
    {
        return $this->hasEaten;
    }
}
