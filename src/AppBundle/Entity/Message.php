<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Message
 *
 * @ORM\Table(name="message")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 */
class Message
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Pension", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="Pension", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $receiver;

    /**
     * @var string
     *
     * @ORM\Column(name="object", type="string", length=255,nullable=true)
     */
    private $object;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime",nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text",nullable=true)
     */
    private $message;

    /**
     * @var boolean
     *
     * @ORM\Column(name="sentSide", type="boolean")
     */
    private $sentSide;


    public function __construct(Pension $sender = null,
                                Message $prevMess = null,
                                $message = null){
        if ($sender != null)
            $this->sender = $sender;
        if ($prevMess != null) {
            $this->receiver = $prevMess->getSender();
            if($prevMess->getObject() != null)
                $this->object   = "Re : " . $prevMess->getObject();
        }
        if ($message != null)
            $this->message = $message;
        $this->sentSide = true;
    }


    public function toArray(){
        $receiver = $this->receiver != null ? $this->receiver->toArray()
                                            : null;
        $sender = $this->sender != null ? $this->sender->toArray()
                                        : null;

        return ["id"       => $this->id,      "sender" => $sender,
                "receiver" => $receiver,      "object" => $this->object,
                "message"  => $this->message, "date"   => $this->date,
                "sentSide" => $this->sentSide ];
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set object
     *
     * @param string $object
     *
     * @return Message
     */
    public function setObject($object)
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Get object
     *
     * @return string
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Message
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Message
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set sender
     *
     * @param \AppBundle\Entity\Pension $sender
     *
     * @return Message
     */
    public function setSender(\AppBundle\Entity\Pension $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \AppBundle\Entity\Pension
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set receiver
     *
     * @param \AppBundle\Entity\Pension $receiver
     *
     * @return Message
     */
    public function setReceiver(\AppBundle\Entity\Pension $receiver)
    {
        $this->receiver = $receiver;

        return $this;
    }

    /**
     * Get receiver
     *
     * @return \AppBundle\Entity\Pension
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Set sentSide
     *
     * @param boolean $sentSide
     *
     * @return Message
     */
    public function setSentSide($sentSide)
    {
        $this->sentSide = $sentSide;

        return $this;
    }

    /**
     * Get sentSide
     *
     * @return boolean
     */
    public function getSentSide()
    {
        return $this->sentSide;
    }
}
