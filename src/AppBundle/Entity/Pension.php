<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Pension
 *
 * @ORM\Table(name="pension")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PensionRepository")
 * @UniqueEntity("name", message="Une pension du même nom existe déjà!")
 */
class Pension
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, unique=true)
     */
    private $name;

    /**
     * @var string 
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\File(
     *     maxSize = "1M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png"},
     *     mimeTypesMessage = "Le fichier choisi ne correspond pas à un fichier valide",
     *     notFoundMessage = "Le fichier n'a pas été trouvé sur le disque",
     *     uploadErrorMessage = "Erreur dans l'upload du fichier"
     * )
     */
    private $avatar;

    /**
     * @var int
     *
     * @ORM\Column(name="reputation", type="integer")
     */
    private $reputation;

    /**
     * @var int
     *
     * @ORM\Column(name="gold", type="integer")
     */
    private $gold;

    /**
     * @var int
     *
     * @ORM\Column(name="paws", type="integer")
     */
    private $paws;

    /**
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var int
     *
     * @ORM\Column(name="nbMissPossible", type="integer")
     */
    private $nbMissPossible;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastConnected", type="datetime")
     */
    private $lastConnected;

    private static $mimeTypes = array("image/jpeg", "image/gif", "image/png");


    public function __construct($owner, $name = null, $avatar = null)
    {
        $this->owner = $owner;
        $this->reputation = 0;
        $this->gold = 100;
        $this->reinitPaws();
        $this->nbMissPossible = 10;
        $this->lastConnected = date_create("yesterday");
        if ($name != null)
            $this->name = $name;
        if ($avatar != null)
            $this->setAvatar($avatar);
    }

    public function addRewards($mission){
        $this->reputation += $mission->getReputation();
        $this->gold +=       $mission->getGold();
    }

    public function reinitPaws(){
        $this->paws = 10;
    }

    public function decrementPaws(){
        $this->setPaws($this->paws - 1);
    }

    public static function isValidAvatar($avatar){
        return in_array($avatar->getMimeType(), self::$mimeTypes);
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pension
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set avatar
     *
     * @return Pension
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set reputation
     *
     * @param integer $reputation
     *
     * @return Pension
     */
    public function setReputation($reputation)
    {
        $this->reputation = $reputation;

        return $this;
    }

    /**
     * Get reputation
     *
     * @return int
     */
    public function getReputation()
    {
        return $this->reputation;
    }

    /**
     * Set gold
     *
     * @param integer $gold
     *
     * @return Pension
     */
    public function setGold($gold)
    {
        $this->gold = $gold;

        return $this;
    }

    /**
     * Get gold
     *
     * @return int
     */
    public function getGold()
    {
        return $this->gold;
    }

    /**
     * Set paws
     *
     * @param integer $paws
     *
     * @return Pension
     */
    public function setPaws($paws)
    {
        $this->paws = $paws;

        return $this;
    }

    /**
     * Get paws
     *
     * @return int
     */
    public function getPaws()
    {
        return $this->paws;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\User $owner
     *
     * @return Pension
     */
    public function setOwner(\AppBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set nbMissPossible
     *
     * @param integer $nbMissPossible
     *
     * @return Pension
     */
    public function setNbMissPossible($nbMissPossible)
    {
        $this->nbMissPossible = $nbMissPossible;

        return $this;
    }

    /**
     * Get nbMissPossible
     *
     * @return integer
     */
    public function getNbMissPossible()
    {
        return $this->nbMissPossible;
    }

    /**
     * Set lastConnected
     *
     * @param \DateTime $lastConnected
     *
     * @return Mission
     */
    public function setLastConnected($lastConnected)
    {
        $this->lastConnected = $lastConnected;

        return $this;
    }

    /**
     * Get lastConnected
     *
     * @return \DateTime
     */
    public function getLastConnected()
    {
        return $this->lastConnected;
    }


    public function toArray()
    {
        return ["id"        => $this->id,         "name"      => $this->name,
                "avatar"    => $this->avatar,     "gold"      => $this->gold,       
                "paws"      => $this->paws,       "reputation"=> $this->reputation,
                "owner"     => $this->owner,      "lastConnected" => $this->lastConnected];
    }
}
