<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pension;
use AppBundle\Repository\PensionRepository;
use AppBundle\Entity\Message;
use AppBundle\Repository\MessageRepository;

use AppBundle\Form\MessageType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PostController extends Controller {

    /**
     * Page de consultation des messages reçus et envoyés
     *
     * @Route("/post", name="post_consult")
     */
    public function consultAction (Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }
        $user = $this->getUser();

        $messagesRec = $this->getDoctrine()
                            ->getRepository('AppBundle:Message')
                            ->findBy( array ( "receiver" => $user->getLastPension(),
                                              "sentSide" => false));

        $messagesSent = $this->getDoctrine()
                         ->getRepository('AppBundle:Message')
                         ->findBy( array ( "sender" => $user->getLastPension(),
                                           "sentSide" => true));

        for ($i = 0 ; $i < count($messagesRec) ; $i++) { 
            $messagesRec[$i] = $messagesRec[$i]->toArray();
        }
        for ($i = 0 ; $i < count($messagesSent) ; $i++) { 
            $messagesSent[$i] = $messagesSent[$i]->toArray();
        }

        return $this->render("post/post_consult.html.twig", array( 'messagesRec'  => $messagesRec,
                                                                   'messagesSent' => $messagesSent));
    }


     /**
     * Route utilisée lorsque le joueur envoit un message à une pension depuis le classement
     *
     * @Route("/post/write_to/{id}", name="post_write_to")
     */
     public function writeFromRankAction (Request $request, Pension $pension){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        //On remplit seulement un message temporaire pour pouvoir utiliser la route de création de message
        $user = $this->getUser();
        $prefillMess = new Message($pension);

        $em = $this->getDoctrine()->getManager();
        $em->persist($prefillMess);
        $em->flush(); 

        return $this->redirectToRoute("post_write" ,array("id"=>$prefillMess->getId()));
     }

    /**
     * @Route("/post/write/{id}", name="post_write", defaults={"id"="new"})
     */
    public function writeMessageAction (Request $request, $id, Message $message = null) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        // Si l'identifiant donné n'existe pas
        if ($message == null && $id != "new"){
            return $this->redirectToRoute("post_write");
        }

        $user = $this->getUser();

        $newMess = new Message($user->getLastPension(), $message);

        $form = $this->createForm(MessageType::class, $newMess);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $isGranted = $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN');

            $recEntered = $form["receiver"]->getData();
            $recExists = $this->getDoctrine()
                              ->getRepository('AppBundle:Pension')
                              ->findOneByName($recEntered);

            // Si l'on vient de la route précédente, on supprime le message temporaire évoqué plus haut
            if($message != null && $message->getObject()==null){
                $em = $this->getDoctrine()->getManager();
                $em->remove($message);
                $em->flush(); 
            }

            // On teste si le joueur est administrateur et si oui, 
            // s'il souhaite envoyer son message à toutes les pensions
            if ($isGranted && $form["toAllPensions"]->getData())
                $this->sendToAll($newMess);
            else if ($recExists)
                $this->sendToDest($newMess, $recExists);
            else {
                $this->get('session')->getFlashBag()->add('error', "Destinataire inexistant");
                return $this->render("post/post_write.html.twig", array("formNew" => $form->createView()));
            }

            $this->get('session')->getFlashBag()->add('ok', 'Message envoyé!');
            return $this->redirectToRoute('post_consult');
        }

        return $this->render("post/post_write.html.twig", array("formNew" => $form->createView()));
    }

    /**
     * Supprime un message
     *
     * @Route("/post/remove/{id}", name="post_remove")
     */
    public function removeAction (Request $request, Message $message) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($message);
        $em->flush(); 
        
        return $this->redirectToRoute('post_consult');
    }



    public function sendToDest(Message $newMess, Pension $dest){
            $newMess->setReceiver($dest);
            date_default_timezone_set('Europe/Paris');
            $newMess->setDate(date_create(date("d-m-Y H:i:s")));

            $newMessClone = clone $newMess;
            $newMessClone->setSentSide(false);

            $em = $this->getDoctrine()->getManager();
            $em->persist($newMess);
            $em->persist($newMessClone);
            $em->flush(); 
    }
    

    public function sendToAll(Message $newMess){
            date_default_timezone_set('Europe/Paris');
            $newMess->setDate(date_create(date("d-m-Y H:i:s")));

            $em = $this->getDoctrine()->getManager();

            $allPensions = $this->getDoctrine()
                                ->getRepository('AppBundle:Pension')
                                ->findAll();

            for ($i=0; $i < count($allPensions); $i++) { 
                $newMess->setReceiver($allPensions[$i]);
                $newMessClone = clone $newMess;
                $newMessClone->setSentSide(false);
                $em->persist($newMessClone);
            }

            $newMess->setReceiver($newMess->getSender());
            $em->persist($newMess);
            $em->flush(); 
    }

}