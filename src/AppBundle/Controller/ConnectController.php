<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Pension;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\PensionRepository;

use AppBundle\Form\UserType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;


class ConnectController extends Controller {
    /**
     * @Route("/login", name="login")
     */
    public function connectAction (Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('/connect/connect.html.twig', array(
                                        'last_username' => $lastUsername,
                                        'error'         => $error ));
    }


     /**
     * @Route("/signup", name="signup")
     */
    public function registerAction (Request $request) {

    	$user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $pensionExist = $this->getDoctrine()
                                 ->getRepository('AppBundle:Pension')
                                 ->findOneByName($form["pensionName"]->getData());
            $avatarFile = $form["pensionAvatar"]->getData();

            if($pensionExist){
                $this->get('session')->getFlashBag()->add('error', 'Une pension du même nom existe déjà');
            } else if(!Pension::isValidAvatar($avatarFile)){
                $this->get('session')->getFlashBag()->add('error', 'Type de fichier incorrect ' . $avatarFile->getMimeType());
            } else {

                // Encodage du mot de passe
                $password = $this->get('security.password_encoder')
                                 ->encodePassword($user, $user->getPassword());
                $user->setPassword($password);


                // Ligne à décommenter pour céer un utilisateur adminisrateur
                // $user->setRoles(array("ROLE_ADMIN"));


                // On change le nom du fichier pour qu'il soit unique,
                // et on le déplace dans le dossier contenant tous les avatars de pension
                $fileName = md5(uniqid()).'.'.$avatarFile->guessExtension();
                $avatarFile->move( $this->getParameter('avatar_pensions_dir'),
                                    $fileName );


                $fstPension = new Pension($user, $form["pensionName"]->getData(),
                                                 $fileName);
                $user->setLastPension($fstPension);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->persist($fstPension);
                $em->flush(); 

                // Permet de se connecter directement une fois l'inscription terminée
                $token = new UsernamePasswordToken($user, null, 'main', array('ROLE_USER'));
                $this->get('security.token_storage')->setToken($token);
                
            	return $this->redirectToRoute('homepage');
            }
      	}

     	return $this->render('/connect/register.html.twig', array('form' => $form->createView()));
    }


    /**
    * Avant de déconnecter un utilisateur, on met à jour la date de sa dernière connexion
    *
    * @Route("/logout_user", name="logout_user")
    */
    public function disconnectAction(Request $request){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $user = $this->getUser();
        date_default_timezone_set('Europe/Paris');
        $user->getLastPension()->setLastConnected(date_create()); 

        $em = $this->getDoctrine()->getManager();
        $em->flush(); 

        return $this->redirectToRoute('logout');
    }


    /**
    * @Route("/logout", name="logout")
    */
    public function logoutAction(Request $request){
        // Déconnexion automatique de l'utilisateur
    }
}
