<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pension;
use AppBundle\Repository\PensionRepository;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RankingController extends Controller {

	/**
     * Cherche toutes les pensions, classées par réputation
     *
	 * @Route("/ranking", name="ranking")
	 */
	public function rankingAction (Request $request){
		 if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }
        $allUser = $this->getDoctrine()
                         ->getRepository('AppBundle:Pension')
                         ->findByReput();

        for($i=0; $i<count($allUser); $i++){
            // Récupère le nombre de pensionnaires de la pension
        	$nbM = $this->getDoctrine()
        				->getRepository('AppBundle:Mission')
        				->findByPension($allUser[$i]);

        	$allUser[$i]=$allUser[$i]->toArray();
        	$allUser[$i]["nbMission"] = count($nbM);
        }

       	return $this->render("ranking.html.twig", array("allPension"=>$allUser,
       													"nbTot"=>count($allUser)));
	}

}