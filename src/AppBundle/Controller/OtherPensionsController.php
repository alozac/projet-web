<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pension;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\PensionRepository;

use AppBundle\Form\PensionType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class OtherPensionsController extends Controller {

    /**
     * Page d'accueil de la page affichant les pensions du joueur
     *
     * @Route("/persos", name="pensions")
     */
    public function pensionsAction (Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }
        $user = $this->getUser();

        $form = $this->formNewPension( $user , $request);

        $pensions = $this->getDoctrine()
                         ->getRepository('AppBundle:Pension')
                         ->findByOwner($user->getId());

        for ($i = 0 ; $i < count($pensions) ; $i++) { 
            $nbM = $this->getDoctrine()
                        ->getRepository('AppBundle:Mission')
                        ->findByPension($pensions[$i]);
            $pensions[$i] = $pensions[$i]->toArray();
            $pensions[$i]["nbMission"] = count($nbM);
        }

        return $this->render('persos.html.twig', array('allPensions' => $pensions, 
                                                       'formNew'  => $form->createView()));
    }


    /**
     * Supprime une pension, et toutes ses missions associées
     *
     * @Route("/remove/{id}", name="removePension")
     */
    public function removeAction(Request $request, Pension $toRemove){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $flash = $this->get('session')->getFlashBag();
        
        $user = $this->getUser();

        if ($toRemove->getOwner()->getId() == $user->getId()){
            $usrConnOn = $user->getLastPension();

            if ($usrConnOn->getId() == $toRemove->getId()){
                $flash->add('error', "Impossible de supprimer '" . $toRemove->getName() . "' : cette pension est en cours d'utilisation");
                return $this->redirectToRoute('pensions');
            } else {
                
                $em = $this->getDoctrine()->getManager();
                $em->remove($toRemove);
                $em->flush(); 

                $flash->add('ok', "La pension '" . $toRemove->getName() . "' a été supprimée!");
                return $this->redirectToRoute('pensions');
            }
        }
        $flash->add('error', "Cette pension ne vous appartient pas.");
        return $this->redirectToRoute('pensions');
    }


    /**
     * Permet de se connecter sur une autre pension
     *
     * @Route("/change/{id}", name="changePension")
     */
    public function changeAction(Request $request, Pension $toConnect){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }
        $flash = $this->get('session')->getFlashBag();
                    
        $user = $this->getUser();

        if ($toConnect->getOwner()->getId() == $user->getId()){
            $user->setLastPension($toConnect);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush(); 

            return $this->redirectToRoute('homepage');
        }

        $flash->add('error', "Cette pension ne vous appartient pas.");
        return $this->redirectToRoute('pensions');
    }


    // Crée le formulaire de création d'une nouvelle pension
    public function formNewPension( $user , $request){

        $newPension = new Pension($user);
        $form = $this->createForm(PensionType::class, $newPension);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // Comme à l'inscription, on rend l'avatar de la nouvelle pension unique
            // et on le déplace dans le bon répertoire
            $avatarFile = $newPension->getAvatar();
            $fileName = md5(uniqid()).'.'.$avatarFile->guessExtension();
            $avatarFile->move( $this->getParameter('avatar_pensions_dir'),
                               $fileName );

            $newPension->setAvatar($fileName);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($newPension);
            $em->flush(); 

            $this->get('session')->getFlashBag()
                 ->add('ok', "La pension '" . $newPension->getName() . "' a bien été créée!");
        }

        return $form;
    }

}