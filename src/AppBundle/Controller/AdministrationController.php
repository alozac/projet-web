<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use AppBundle\Entity\Pension;
use AppBundle\Repository\EventRepository;

use AppBundle\Repository\UserRepository;
use AppBundle\Repository\PensionRepository;


use AppBundle\Form\EventType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdministrationController extends Controller {


    /**
     * @Route("/administration", name="administration")
     */
    public function adminAction (Request $request){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }
        return $this->redirectToRoute("events_list");
    }


    /**
     * Supprimer un utilisateur, et donc ses pensions
     *
     * @Route("/administration/remove_user/{id}", name="remove_user")
     */
    public function removeAction (Request $request, Pension $id){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }
        $user=$id->getOwner();

        if($this->getUser()->getId() == $user->getId()){
            $this->get('session')->getFlashBag()->add('error', 'Vous ne pouvez pas vous supprimer');
            return $this->redirectToRoute("ranking");
        }

        // Tous les messages envoyés et reçus par cet utilisateur ne seront
        // plus utilisés, donc on les supprime de la bdd
        $messagesRec = $this->getDoctrine()
                            ->getRepository('AppBundle:Message')
                            ->findUselessMessages($user->getId());

        $em = $this->getDoctrine()->getManager();

        for ($i=0; $i < count($messagesRec); $i++) { 
            $em->remove($messagesRec[$i]);
        }

        $em->remove($user);
        $em->flush();

        $this->get('session')->getFlashBag()->add('ok', 'Utilisateur supprimé');

        return $this->redirectToRoute("ranking");
    }


	/**
     * Créer un nouvel événement ou modifier un événement existant
     *
	 * @Route("/administration/event/modify/{id}", name="modifyEvent", defaults={"id"="new"})
	 */
	public function modifyEventAction (Request $request, Event $toModify = null){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        // Si on crée un nouvel événement, $toModify est null
        if ($toModify == null){
            $toModify = new Event();
        }

        $form = $this->createForm(EventType::class, $toModify);


        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($toModify);
            $em->flush(); 

            $this->get('session')->getFlashBag()->add('ok', 'Evènement créé');
            return $this->redirectToRoute('administration');
        }

        return $this->render("administration/modify_event.html.twig",
                                array("form" => $form->createView()));
	}


    /**
     * Affiche la liste des événements globaux
     *
     * @Route("/administration/events/", name="events_list")
     */
    public function listEventsAction (Request $request){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        $events = $this->getDoctrine()
                       ->getRepository('AppBundle:Event')
                       ->findAll();

        for ($i=0; $i < count($events); $i++) { 
            $events[$i] = $events[$i]->toArray();
        }

        return $this->render("administration/events_list.html.twig",
                                array('events' => $events));
    }

    /**
     * Supprime un événement global
     *
     * @Route("/administration/event/delete/{id}", name="deleteEvent")
     */
    public function deleteEventAction (Request $request, Event $toDelete){
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('homepage');
        }

        $em = $this->getDoctrine()->getManager();
        $em->remove($toDelete);
        $em->flush(); 

        return $this->redirectToRoute('events_list');
    }

}