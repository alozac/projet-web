<?php

namespace AppBundle\Controller;
use AppBundle\Entity\User;
use AppBundle\Entity\Pension;
use AppBundle\Entity\Mission;
use AppBundle\Repository\MissionRepository;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class VeterinaryController extends Controller {

	/**
	* Page d'accueil du vétérinaire
	*
	* @Route("/veterinary", name="veterinary")
	*/
	public function vetoAction(Request $request){
	    if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $user = $this->getUser();

        // Récupère les événements influant sur le coût des soins et de la nourriture s'il y en a
        $salesEvents = $this->getSalesEvents();

        $missUser =  $this->getDoctrine()
                          ->getRepository('AppBundle:Mission')
                          ->findByPension($user->getLastPension());

        for($i=0; $i<count($missUser); $i++){
        	$cost = $missUser[$i]->getCostHeal($salesEvents);
        	$missUser[$i] = $missUser[$i]->toArray();
        	$missUser[$i]["costHeal"] = $cost;
        }
		
		 return $this->render('veterinaire.html.twig',
		 		array('animal_list' => $missUser,
		 			  'costsFood'   => Mission::getCostsFood($salesEvents)));
	}


	/**
	* Soigner un animal si possible
	*
	* @Route("/veterinary/cure/{id}", name="cure")
	*/
	public function cureAction(Request $request, Mission $id){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $flash = $this->get('session')->getFlashBag();

        $pension = $this->getUser()
        				->getLastPension();

		$pdvsick = $id->getPdv();

		$salesEvents = $this->getSalesEvents();

		$actugold = $pension->getGold();
        $cost = $id->getCostHeal($salesEvents);

		if($actugold < $cost || $id->getPdv() == 100 || $id->getPension()->getId() != $pension->getId()){
			$flash->add('error',"Action indisponible.");
			return $this->redirectToRoute("veterinary");
		}

		if ($pension->getPaws() == 0){
            $flash->add('error', "Vous n'avez plus de Paws, revenez demain!");
            return $this->redirectToRoute('veterinary');
        }

		$id->setPdv(100);
		$pension->setGold($actugold - $cost);
		$pension->decrementPaws();

		$em = $this->getDoctrine()->getManager();
		$em->flush();

		$flash->add('ok', "Vous avez soigné ".$id->getName()." !");
		return $this->redirectToRoute("veterinary");
	}


	/**
	* Permet de nourrir $toFeed avec le repas $typeFood choisi par l'utilisateur
	*
	* @Route("/veterinary/feed/{id}/{typeFood}", name="feed")
	*/
	public function feedAction(Request $request, Mission $toFeed, $typeFood){
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $pension = $this->getUser()
        				->getLastPension();

        $flash = $this->get('session')->getFlashBag();

        if($typeFood < 1 || $typeFood > 3 || $toFeed->getHasEaten() != 0 || $toFeed->getPension()->getId() != $pension->getId()){
            $flash->add('error',"Action indisponible");
			return $this->redirectToRoute("veterinary");
		}

		if ($pension->getPaws() == 0){
            $flash->add('error', "Vous n'avez plus de Paws, revenez demain!");
            return $this->redirectToRoute('veterinary');
        }
        
        $events = $this->getSalesEvents();
        $cout = Mission::getCostsFood($events)[$typeFood];

		$actugold = $pension->getGold();

		if($actugold < $cout){
			$flash->add('error',"Vous n'avez pas assez d'argent.");
			return $this->redirectToRoute("veterinary");
		}

		$toFeed->setHasEaten($typeFood);
		$pension->setGold($actugold - $cout);
		$pension->decrementPaws();

		$em = $this->getDoctrine()->getManager();
		$em->flush();
		
		$flash->add('ok', "Vous avez nourri ". $toFeed->getName()." !");
		return $this->redirectToRoute("veterinary");
	}


	public function getSalesEvents(){
		return $this->getDoctrine()
             		->getRepository('AppBundle:Event')
             		->findActiveSalesEvents();
	}
}