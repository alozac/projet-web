<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Pension;
use AppBundle\Entity\Mission;
use AppBundle\Repository\MissionRepository;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MissionController extends Controller {

    /**
     * Choisis aléatoirement 3 missions que le joueur n'a pas déjà acceptées
     *
     * @Route("/mission", name="chooseMission")
     */
    public function chooseAction (Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }

        $lastPension = $this->getUser()->getLastPension();
        
        $nbMiss =  $this->getDoctrine()
                        ->getRepository('AppBundle:Mission')
                        ->findByPension($lastPension);


        // 0 si possibilité d'accepter une nouvelle mission
        // 1 si nombre de Paws insuffisant
        // 2 si nombre de missions max atteint
        $newPossible = 0;

        if($lastPension->getPaws() == 0)
            $newPossible = 1;
        if (count($nbMiss) >= $lastPension->getNbMissPossible())
            $newPossible = 2;

        // Récupère les missions non encore acceptées par le joueur
        $freeMiss = $this->getDoctrine()
                     ->getRepository('AppBundle:Mission')
                     ->findFreeMissions($lastPension->getId());

        // Choisis aléatoirement 3 missions parmi $freeMiss
        $chosenMiss = $this->chooseAvailableMiss($freeMiss);
        
        return $this->render('missions.html.twig', array("missions"     => $chosenMiss,
                                                         "new_possible" => $newPossible));
    }


    /**
     * Si possible, ajoute $missAccepted aux missions en cours du joueur
     *
     * @Route("/accepted/{id}", name="acceptedMiss")
     */
    public function acceptedAction (Request $request, Mission $missAccepted) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }
        $flash = $this->get('session')->getFlashBag();

        $lastPension = $this->getUser()->getLastPension();

        if ($lastPension->getPaws() == 0){
            $flash->add('error', "Action incorrecte.");
            return $this->redirectToRoute('chooseMission');
        }

        $freeMiss = $this->getDoctrine()
                         ->getRepository('AppBundle:Mission')
                         ->findFreeMissions($lastPension->getId());

        // On vérifie que $missAccepted n'est pas déjà acceptée par le joueur
        if (in_array($missAccepted, $freeMiss)){

            $this->acceptMission($missAccepted, $lastPension);

            $flash->add('ok', "Vous vous occupez désormais de " . $missAccepted->getName() . " !");
        } else {
            $flash->add('error', "Mission incorrecte");
        }
        return $this->redirectToRoute('chooseMission');
    }



    
    public function chooseAvailableMiss($freeMiss){
        $chosenMiss;
        $indexTaken = [];
        for ($i = 0 ; $i < 3 ; $i++){
            do{
                $rd = rand(0, count($freeMiss) - 1);
            }
            while (in_array($rd, $indexTaken));
            array_push($indexTaken, $rd);
            $chosenMiss[$i] = $freeMiss[$rd]->toArray();
        }
        return $chosenMiss;
    }


    public function acceptMission(Mission $toAccept, Pension $lastPension){
            date_default_timezone_set('Europe/Paris');
            $end = date_create()->modify('+' . $toAccept->getDuration() . ' hour')
                                ->format('Y-m-d H:i:s');

            $newMiss = new Mission($toAccept, $lastPension, date_create($end));
            
            $lastPension->decrementPaws();

            $em = $this->getDoctrine()->getManager();
            $em->persist($newMiss);
            $em->flush(); 
    }
}