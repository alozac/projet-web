<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Pension;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PensionController extends Controller {

    /**
     * @Route("/", name="initpage")
     */
    public function indexAction () {
      return $this->redirectToRoute('homepage');
    }

    /**
     * Page d'accueil de la pension
     *
     * @Route("/pension", name="homepage")
     */
    public function homeAction (Request $request) {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('login');
        }
        
        date_default_timezone_set('Europe/Paris');
        $user = $this->getUser();
        $lastPens = $user->getLastPension();

        // On met à jour les paws et les missions si la dernière connexion
        // ne date pas d'aujourd'hui
        if ($lastPens->getLastConnected() <= date_create("today 00:00")){
          $this->updateDailyRessources($lastPens);
        }

        $lastPens->setLastConnected(date_create('now')); 

        $missUser = $this->getDoctrine()
                         ->getRepository('AppBundle:Mission')
                         ->findByPension($lastPens);

        $events = $this->getDoctrine()
                       ->getRepository('AppBundle:Event')
                       ->findAllActiveEvents();

        $missOver    = [];
        $missNotOver = [];

        $now = date_create(date("d-m-Y H:i:s"));
        $nbSecNow = strtotime("now");

        $em = $this->getDoctrine()->getManager();
        for ($i=0; $i < count($missUser); $i++) {
            $endDate       = $missUser[$i]->getEndDate(); 
            $nbSecDuration = $missUser[$i]->getDuration()  * 60 * 60;
            $nbSecDone     = $nbSecNow - ( $endDate->getTimestamp() -  $nbSecDuration); 
            // On détermine si la mission est terminée ou non
            if ($endDate > $now){
                $missUser[$i] = $missUser[$i]->toArray();
                $finishedAt = $nbSecDone / $nbSecDuration * 100;
                $missUser[$i]["finishedAt"] = $finishedAt;
                array_push($missNotOver, $missUser[$i]);

            } else {
                // Si la mission est terminée on calcule et on attribue 
                // les récompenses à la pension
                array_push($missOver, $missUser[$i]);
                if ($events)
                    $missUser[$i]->applyBonusEvent($events);
                $missUser[$i]->applyMalusPdv($events);
                $lastPens->addRewards($missUser[$i]);
                $em->remove($missUser[$i]);   
            }
        }
        $em->flush();   
        
        return $this->render('pension.html.twig', array('missOver'    => $missOver,
                                                        'missNotOver' => $missNotOver,
                                                        'events'      => $events));
    }



    public function updateDailyRessources(Pension $lastPens){
        date_default_timezone_set('Europe/Paris');
        $nbDays = date_diff(date_create('now'), $lastPens->getLastConnected())
                  ->format('%a');

        $lastPens->reinitPaws();

        $missPens =  $this->getDoctrine()
                          ->getRepository('AppBundle:Mission')
                          ->findByPension($lastPens);

        for ($i=0; $i < count($missPens); $i++) { 
            $missPens[$i]->updateDailyRessources($nbDays);
        }

        $em = $this->getDoctrine()->getManager();
        $em->flush(); 
    }

}