<?php

namespace Proxies\__CG__\AppBundle\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Pension extends \AppBundle\Entity\Pension implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'id', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'name', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'avatar', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'reputation', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'gold', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'paws', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'owner', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'nbMissPossible', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'lastConnected'];
        }

        return ['__isInitialized__', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'id', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'name', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'avatar', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'reputation', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'gold', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'paws', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'owner', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'nbMissPossible', '' . "\0" . 'AppBundle\\Entity\\Pension' . "\0" . 'lastConnected'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Pension $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function addRewards($mission)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addRewards', [$mission]);

        return parent::addRewards($mission);
    }

    /**
     * {@inheritDoc}
     */
    public function reinitPaws()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'reinitPaws', []);

        return parent::reinitPaws();
    }

    /**
     * {@inheritDoc}
     */
    public function decrementPaws()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'decrementPaws', []);

        return parent::decrementPaws();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setName($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setName', [$name]);

        return parent::setName($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getName', []);

        return parent::getName();
    }

    /**
     * {@inheritDoc}
     */
    public function setAvatar($avatar)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAvatar', [$avatar]);

        return parent::setAvatar($avatar);
    }

    /**
     * {@inheritDoc}
     */
    public function getAvatar()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAvatar', []);

        return parent::getAvatar();
    }

    /**
     * {@inheritDoc}
     */
    public function setReputation($reputation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReputation', [$reputation]);

        return parent::setReputation($reputation);
    }

    /**
     * {@inheritDoc}
     */
    public function getReputation()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getReputation', []);

        return parent::getReputation();
    }

    /**
     * {@inheritDoc}
     */
    public function setGold($gold)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setGold', [$gold]);

        return parent::setGold($gold);
    }

    /**
     * {@inheritDoc}
     */
    public function getGold()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getGold', []);

        return parent::getGold();
    }

    /**
     * {@inheritDoc}
     */
    public function setPaws($paws)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPaws', [$paws]);

        return parent::setPaws($paws);
    }

    /**
     * {@inheritDoc}
     */
    public function getPaws()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPaws', []);

        return parent::getPaws();
    }

    /**
     * {@inheritDoc}
     */
    public function setOwner(\AppBundle\Entity\User $owner = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOwner', [$owner]);

        return parent::setOwner($owner);
    }

    /**
     * {@inheritDoc}
     */
    public function getOwner()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOwner', []);

        return parent::getOwner();
    }

    /**
     * {@inheritDoc}
     */
    public function setNbMissPossible($nbMissPossible)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNbMissPossible', [$nbMissPossible]);

        return parent::setNbMissPossible($nbMissPossible);
    }

    /**
     * {@inheritDoc}
     */
    public function getNbMissPossible()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNbMissPossible', []);

        return parent::getNbMissPossible();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastConnected($lastConnected)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastConnected', [$lastConnected]);

        return parent::setLastConnected($lastConnected);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastConnected()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastConnected', []);

        return parent::getLastConnected();
    }

    /**
     * {@inheritDoc}
     */
    public function toArray()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toArray', []);

        return parent::toArray();
    }

}
