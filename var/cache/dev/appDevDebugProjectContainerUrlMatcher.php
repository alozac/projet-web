<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ($pathinfo === '/_profiler/open') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        if (0 === strpos($pathinfo, '/administration')) {
            // administration
            if ($pathinfo === '/administration') {
                return array (  '_controller' => 'AppBundle\\Controller\\AdministrationController::adminAction',  '_route' => 'administration',);
            }

            // remove_user
            if (0 === strpos($pathinfo, '/administration/remove_user') && preg_match('#^/administration/remove_user/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'remove_user')), array (  '_controller' => 'AppBundle\\Controller\\AdministrationController::removeAction',));
            }

            if (0 === strpos($pathinfo, '/administration/event')) {
                // modifyEvent
                if (0 === strpos($pathinfo, '/administration/event/modify') && preg_match('#^/administration/event/modify(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'modifyEvent')), array (  'id' => 'new',  '_controller' => 'AppBundle\\Controller\\AdministrationController::modifyEventAction',));
                }

                // events_list
                if (rtrim($pathinfo, '/') === '/administration/events') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'events_list');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\AdministrationController::listEventsAction',  '_route' => 'events_list',);
                }

                // deleteEvent
                if (0 === strpos($pathinfo, '/administration/event/delete') && preg_match('#^/administration/event/delete/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'deleteEvent')), array (  '_controller' => 'AppBundle\\Controller\\AdministrationController::deleteEventAction',));
                }

            }

        }

        // login
        if ($pathinfo === '/login') {
            return array (  '_controller' => 'AppBundle\\Controller\\ConnectController::connectAction',  '_route' => 'login',);
        }

        // signup
        if ($pathinfo === '/signup') {
            return array (  '_controller' => 'AppBundle\\Controller\\ConnectController::registerAction',  '_route' => 'signup',);
        }

        if (0 === strpos($pathinfo, '/logout')) {
            // logout_user
            if ($pathinfo === '/logout_user') {
                return array (  '_controller' => 'AppBundle\\Controller\\ConnectController::disconnectAction',  '_route' => 'logout_user',);
            }

            // logout
            if ($pathinfo === '/logout') {
                return array (  '_controller' => 'AppBundle\\Controller\\ConnectController::logoutAction',  '_route' => 'logout',);
            }

        }

        // chooseMission
        if ($pathinfo === '/mission') {
            return array (  '_controller' => 'AppBundle\\Controller\\MissionController::chooseAction',  '_route' => 'chooseMission',);
        }

        // acceptedMiss
        if (0 === strpos($pathinfo, '/accepted') && preg_match('#^/accepted/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'acceptedMiss')), array (  '_controller' => 'AppBundle\\Controller\\MissionController::acceptedAction',));
        }

        // pensions
        if ($pathinfo === '/persos') {
            return array (  '_controller' => 'AppBundle\\Controller\\OtherPensionsController::pensionsAction',  '_route' => 'pensions',);
        }

        // removePension
        if (0 === strpos($pathinfo, '/remove') && preg_match('#^/remove/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'removePension')), array (  '_controller' => 'AppBundle\\Controller\\OtherPensionsController::removeAction',));
        }

        // changePension
        if (0 === strpos($pathinfo, '/change') && preg_match('#^/change/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'changePension')), array (  '_controller' => 'AppBundle\\Controller\\OtherPensionsController::changeAction',));
        }

        // initpage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'initpage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\PensionController::indexAction',  '_route' => 'initpage',);
        }

        if (0 === strpos($pathinfo, '/p')) {
            // homepage
            if ($pathinfo === '/pension') {
                return array (  '_controller' => 'AppBundle\\Controller\\PensionController::homeAction',  '_route' => 'homepage',);
            }

            if (0 === strpos($pathinfo, '/post')) {
                // post_consult
                if ($pathinfo === '/post') {
                    return array (  '_controller' => 'AppBundle\\Controller\\PostController::consultAction',  '_route' => 'post_consult',);
                }

                if (0 === strpos($pathinfo, '/post/write')) {
                    // post_write_to
                    if (0 === strpos($pathinfo, '/post/write_to') && preg_match('#^/post/write_to/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_write_to')), array (  '_controller' => 'AppBundle\\Controller\\PostController::writeFromRankAction',));
                    }

                    // post_write
                    if (preg_match('#^/post/write(?:/(?P<id>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_write')), array (  'id' => 'new',  '_controller' => 'AppBundle\\Controller\\PostController::writeMessageAction',));
                    }

                }

                // post_remove
                if (0 === strpos($pathinfo, '/post/remove') && preg_match('#^/post/remove/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_remove')), array (  '_controller' => 'AppBundle\\Controller\\PostController::removeAction',));
                }

            }

        }

        // ranking
        if ($pathinfo === '/ranking') {
            return array (  '_controller' => 'AppBundle\\Controller\\RankingController::rankingAction',  '_route' => 'ranking',);
        }

        if (0 === strpos($pathinfo, '/veterinary')) {
            // veterinary
            if ($pathinfo === '/veterinary') {
                return array (  '_controller' => 'AppBundle\\Controller\\VeterinaryController::vetoAction',  '_route' => 'veterinary',);
            }

            // cure
            if (0 === strpos($pathinfo, '/veterinary/cure') && preg_match('#^/veterinary/cure/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'cure')), array (  '_controller' => 'AppBundle\\Controller\\VeterinaryController::cureAction',));
            }

            // feed
            if (0 === strpos($pathinfo, '/veterinary/feed') && preg_match('#^/veterinary/feed/(?P<id>[^/]++)/(?P<typeFood>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'feed')), array (  '_controller' => 'AppBundle\\Controller\\VeterinaryController::feedAction',));
            }

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
