<?php

/* @Framework/Form/button_widget.html.php */
class __TwigTemplate_7ca8dfbbe728fd176031c48ed7e706e7315b8589df5ac36a7edee596af173624 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c3fe33f219a1f803045ddf678603c804eca1a9a92176dc59e8c1d2dc64f8d5ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3fe33f219a1f803045ddf678603c804eca1a9a92176dc59e8c1d2dc64f8d5ec->enter($__internal_c3fe33f219a1f803045ddf678603c804eca1a9a92176dc59e8c1d2dc64f8d5ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        $__internal_81080a752fac6b80adf14857d2b3aa0410ebe1b4b71cba1111492fcde6026e59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_81080a752fac6b80adf14857d2b3aa0410ebe1b4b71cba1111492fcde6026e59->enter($__internal_81080a752fac6b80adf14857d2b3aa0410ebe1b4b71cba1111492fcde6026e59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_widget.html.php"));

        // line 1
        echo "<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
";
        
        $__internal_c3fe33f219a1f803045ddf678603c804eca1a9a92176dc59e8c1d2dc64f8d5ec->leave($__internal_c3fe33f219a1f803045ddf678603c804eca1a9a92176dc59e8c1d2dc64f8d5ec_prof);

        
        $__internal_81080a752fac6b80adf14857d2b3aa0410ebe1b4b71cba1111492fcde6026e59->leave($__internal_81080a752fac6b80adf14857d2b3aa0410ebe1b4b71cba1111492fcde6026e59_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!\$label) { \$label = isset(\$label_format)
    ? strtr(\$label_format, array('%name%' => \$name, '%id%' => \$id))
    : \$view['form']->humanize(\$name); } ?>
<button type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'button' ?>\" <?php echo \$view['form']->block(\$form, 'button_attributes') ?>><?php echo \$view->escape(false !== \$translation_domain ? \$view['translator']->trans(\$label, array(), \$translation_domain) : \$label) ?></button>
", "@Framework/Form/button_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_widget.html.php");
    }
}
