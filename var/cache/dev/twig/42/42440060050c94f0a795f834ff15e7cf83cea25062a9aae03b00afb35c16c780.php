<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_8b1291777c8ca9ef38a0e7c25b249115e16f0df4a95e24256122fb75a1a5c404 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_dcb17df7b87086ed475166c1a8b27513a2f3b3ea4ebe287db7e342ec8adc188b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dcb17df7b87086ed475166c1a8b27513a2f3b3ea4ebe287db7e342ec8adc188b->enter($__internal_dcb17df7b87086ed475166c1a8b27513a2f3b3ea4ebe287db7e342ec8adc188b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_dd2d546cf41555f1e5c4c95d0f97195f6b1c3633922e3245ee34abb8f6e7f726 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd2d546cf41555f1e5c4c95d0f97195f6b1c3633922e3245ee34abb8f6e7f726->enter($__internal_dd2d546cf41555f1e5c4c95d0f97195f6b1c3633922e3245ee34abb8f6e7f726_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_dcb17df7b87086ed475166c1a8b27513a2f3b3ea4ebe287db7e342ec8adc188b->leave($__internal_dcb17df7b87086ed475166c1a8b27513a2f3b3ea4ebe287db7e342ec8adc188b_prof);

        
        $__internal_dd2d546cf41555f1e5c4c95d0f97195f6b1c3633922e3245ee34abb8f6e7f726->leave($__internal_dd2d546cf41555f1e5c4c95d0f97195f6b1c3633922e3245ee34abb8f6e7f726_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
