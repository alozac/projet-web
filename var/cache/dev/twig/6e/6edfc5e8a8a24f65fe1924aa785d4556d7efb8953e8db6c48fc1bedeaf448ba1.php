<?php

/* base.html.twig */
class __TwigTemplate_fcffa5aebd625df35071cf0e2558e4a48d8b1ae903883d65b1ca0b6c803773ab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'main_screen' => array($this, 'block_main_screen'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b897d0f43cc1c69a567363128c84fc7b4036ac990b722cabe0eea8f95968041 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b897d0f43cc1c69a567363128c84fc7b4036ac990b722cabe0eea8f95968041->enter($__internal_5b897d0f43cc1c69a567363128c84fc7b4036ac990b722cabe0eea8f95968041_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_9828fd983acaa13b45ee1eda42b50288b826aca01883c530c9f44835b099923c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9828fd983acaa13b45ee1eda42b50288b826aca01883c530c9f44835b099923c->enter($__internal_9828fd983acaa13b45ee1eda42b50288b826aca01883c530c9f44835b099923c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
      <meta charset=\"utf-8\">
      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
      <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/js/jquery-3.2.0.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "      <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
      <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/game_icon.jpg"), "html", null, true);
        echo "\" />
    </head>

    ";
        // line 18
        $this->displayBlock('body', $context, $blocks);
        // line 74
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 81
        echo "\t</div>
    </body>
</html>
";
        
        $__internal_5b897d0f43cc1c69a567363128c84fc7b4036ac990b722cabe0eea8f95968041->leave($__internal_5b897d0f43cc1c69a567363128c84fc7b4036ac990b722cabe0eea8f95968041_prof);

        
        $__internal_9828fd983acaa13b45ee1eda42b50288b826aca01883c530c9f44835b099923c->leave($__internal_9828fd983acaa13b45ee1eda42b50288b826aca01883c530c9f44835b099923c_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_43affca797ea26014a431870d1db9e03ba163b4ba618fcaadb3eda219033d037 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_43affca797ea26014a431870d1db9e03ba163b4ba618fcaadb3eda219033d037->enter($__internal_43affca797ea26014a431870d1db9e03ba163b4ba618fcaadb3eda219033d037_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_1c58706649f6e0d9285f79210255ea757abfe6f2e95f76abcbda839a8d780035 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c58706649f6e0d9285f79210255ea757abfe6f2e95f76abcbda839a8d780035->enter($__internal_1c58706649f6e0d9285f79210255ea757abfe6f2e95f76abcbda839a8d780035_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "         <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
         <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/css/bootstrap-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
         <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/base.css"), "html", null, true);
        echo "\"/>
        ";
        
        $__internal_1c58706649f6e0d9285f79210255ea757abfe6f2e95f76abcbda839a8d780035->leave($__internal_1c58706649f6e0d9285f79210255ea757abfe6f2e95f76abcbda839a8d780035_prof);

        
        $__internal_43affca797ea26014a431870d1db9e03ba163b4ba618fcaadb3eda219033d037->leave($__internal_43affca797ea26014a431870d1db9e03ba163b4ba618fcaadb3eda219033d037_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_f00ae821e927f8e039651217d45a0494c59d29ee7adc60630c7e8dbc4eece955 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f00ae821e927f8e039651217d45a0494c59d29ee7adc60630c7e8dbc4eece955->enter($__internal_f00ae821e927f8e039651217d45a0494c59d29ee7adc60630c7e8dbc4eece955_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_18912431a9e38b0e5605f6f535ac74de8f3f9d9a1eecb2eb5d4944ddad3078cb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_18912431a9e38b0e5605f6f535ac74de8f3f9d9a1eecb2eb5d4944ddad3078cb->enter($__internal_18912431a9e38b0e5605f6f535ac74de8f3f9d9a1eecb2eb5d4944ddad3078cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_18912431a9e38b0e5605f6f535ac74de8f3f9d9a1eecb2eb5d4944ddad3078cb->leave($__internal_18912431a9e38b0e5605f6f535ac74de8f3f9d9a1eecb2eb5d4944ddad3078cb_prof);

        
        $__internal_f00ae821e927f8e039651217d45a0494c59d29ee7adc60630c7e8dbc4eece955->leave($__internal_f00ae821e927f8e039651217d45a0494c59d29ee7adc60630c7e8dbc4eece955_prof);

    }

    // line 18
    public function block_body($context, array $blocks = array())
    {
        $__internal_d05612c403f0de5e10a4668a3b74f643edc11293dddf8bfcb27811e61dae82a0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d05612c403f0de5e10a4668a3b74f643edc11293dddf8bfcb27811e61dae82a0->enter($__internal_d05612c403f0de5e10a4668a3b74f643edc11293dddf8bfcb27811e61dae82a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6d2c7604b6154f254ea82986e71e989af40238ce205881d383ef616fd0c65347 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d2c7604b6154f254ea82986e71e989af40238ce205881d383ef616fd0c65347->enter($__internal_6d2c7604b6154f254ea82986e71e989af40238ce205881d383ef616fd0c65347_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 19
        echo "    <body>
\t<div class=\"container-fluid\">
    <div class=\"row\">
      <div id=\"left_menu\" class = \"col-sm-2 col-sm-pull-0\">
        <div id=\"infosPension\" class=\"menu\">
        ";
        // line 24
        $context["auth"] = $this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array());
        // line 25
        echo "          ";
        if ( !(null === ($context["auth"] ?? $this->getContext($context, "auth")))) {
            // line 26
            echo "             <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
            echo "\"> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "gold", array()), "html", null, true);
            echo " &nbsp&nbsp
             <img src=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/paws.png"), "html", null, true);
            echo "\"> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "paws", array()), "html", null, true);
            echo "<br>
             Réputation : ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "reputation", array()), "html", null, true);
            echo "<br>
             <img src=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/avatar/" . $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "avatar", array()))), "html", null, true);
            echo "\"
                  class=\"img-circle\" id=\"imgPension\"/>
             ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "name", array()), "html", null, true);
            echo "
          ";
        }
        // line 33
        echo "        </div>

      \t<div id=\"liens\" class=\"menu\">
        ";
        // line 36
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 37
            echo "          <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration");
            echo "\" ><input type=\"button\" value=\"Administration\" class=\"butt\"></a>
          <br>
        ";
        }
        // line 40
        echo "          <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\"><input type=\"button\" value=\"Votre pension\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("chooseMission");
        echo "\" ><input type=\"button\" value=\"Missions\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("veterinary");
        echo "\"><input type=\"button\" value=\"Vétérinaire\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_consult");
        echo "\"><input type=\"button\" value=\"Poste\" class=\"butt\"></a>
          <br>
          <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pensions");
        echo "\"><input type=\"button\" value=\"Vos pensions\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ranking");
        echo "\"><input type=\"button\" value=\"Classement\" class=\"butt\"></a>
      \t\t<br>
      \t\t<a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout_user");
        echo "\"><input type=\"button\" value=\"Deconnexion\" class=\"butt\"></a>
      \t</div>
      </div>

    \t<div id=\"ecran_princip\" class=\"col-sm-9 col-sm-push-1\">
          ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "ok"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["msg"]) {
            // line 58
            echo "            <div class=\"alert alert-success\">
                 ";
            // line 59
            echo twig_escape_filter($this->env, $context["msg"], "html", null, true);
            echo "
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['msg'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "          ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 63
            echo "            <div class=\"alert alert-danger\">
               ";
            // line 64
            echo twig_escape_filter($this->env, $context["info"], "html", null, true);
            echo "
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "
          ";
        // line 68
        $this->displayBlock('main_screen', $context, $blocks);
        // line 70
        echo "    \t</div>
    </div>

    ";
        
        $__internal_6d2c7604b6154f254ea82986e71e989af40238ce205881d383ef616fd0c65347->leave($__internal_6d2c7604b6154f254ea82986e71e989af40238ce205881d383ef616fd0c65347_prof);

        
        $__internal_d05612c403f0de5e10a4668a3b74f643edc11293dddf8bfcb27811e61dae82a0->leave($__internal_d05612c403f0de5e10a4668a3b74f643edc11293dddf8bfcb27811e61dae82a0_prof);

    }

    // line 68
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_f5b5a830ea51e6beee3ec013bba8d04339e94a57e8bbdc6e6b2d293514b829cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5b5a830ea51e6beee3ec013bba8d04339e94a57e8bbdc6e6b2d293514b829cd->enter($__internal_f5b5a830ea51e6beee3ec013bba8d04339e94a57e8bbdc6e6b2d293514b829cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_cd36684524a69c02b645c67d57a679d11f3406637a79c20cb43e44034095e745 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd36684524a69c02b645c67d57a679d11f3406637a79c20cb43e44034095e745->enter($__internal_cd36684524a69c02b645c67d57a679d11f3406637a79c20cb43e44034095e745_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 69
        echo "          ";
        
        $__internal_cd36684524a69c02b645c67d57a679d11f3406637a79c20cb43e44034095e745->leave($__internal_cd36684524a69c02b645c67d57a679d11f3406637a79c20cb43e44034095e745_prof);

        
        $__internal_f5b5a830ea51e6beee3ec013bba8d04339e94a57e8bbdc6e6b2d293514b829cd->leave($__internal_f5b5a830ea51e6beee3ec013bba8d04339e94a57e8bbdc6e6b2d293514b829cd_prof);

    }

    // line 74
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_59aaa1ae0bdf74a47420d12396b99c4995a939690a04b4ca68cedfee16457431 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59aaa1ae0bdf74a47420d12396b99c4995a939690a04b4ca68cedfee16457431->enter($__internal_59aaa1ae0bdf74a47420d12396b99c4995a939690a04b4ca68cedfee16457431_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_9c2c59856c2eb2ca92db805fbff52fd4450570209af50a6b8b91c37aa5133799 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c2c59856c2eb2ca92db805fbff52fd4450570209af50a6b8b91c37aa5133799->enter($__internal_9c2c59856c2eb2ca92db805fbff52fd4450570209af50a6b8b91c37aa5133799_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 75
        echo "       ";
        if ((null === $this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()))) {
            // line 76
            echo "      <script>
        \$(\"#left_menu\").addClass(\"disabled\");
      </script>
      ";
        }
        // line 80
        echo "    ";
        
        $__internal_9c2c59856c2eb2ca92db805fbff52fd4450570209af50a6b8b91c37aa5133799->leave($__internal_9c2c59856c2eb2ca92db805fbff52fd4450570209af50a6b8b91c37aa5133799_prof);

        
        $__internal_59aaa1ae0bdf74a47420d12396b99c4995a939690a04b4ca68cedfee16457431->leave($__internal_59aaa1ae0bdf74a47420d12396b99c4995a939690a04b4ca68cedfee16457431_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  314 => 80,  308 => 76,  305 => 75,  296 => 74,  286 => 69,  277 => 68,  264 => 70,  262 => 68,  259 => 67,  250 => 64,  247 => 63,  242 => 62,  233 => 59,  230 => 58,  226 => 57,  218 => 52,  213 => 50,  208 => 48,  203 => 46,  198 => 44,  193 => 42,  187 => 40,  180 => 37,  178 => 36,  173 => 33,  168 => 31,  163 => 29,  159 => 28,  153 => 27,  146 => 26,  143 => 25,  141 => 24,  134 => 19,  125 => 18,  107 => 14,  95 => 12,  91 => 11,  86 => 10,  77 => 9,  64 => 81,  61 => 74,  59 => 18,  53 => 15,  48 => 14,  46 => 9,  42 => 8,  38 => 7,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
      <meta charset=\"utf-8\">
      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
      <script type=\"text/javascript\" src=\"{{ asset('bootstrap/js/jquery-3.2.0.js') }}\"></script>
      <script type=\"text/javascript\" src=\"{{ asset('bootstrap/js/bootstrap.min.js') }}\"></script>
        {% block stylesheets %}
         <link href=\"{{asset('bootstrap/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
         <link href=\"{{asset('bootstrap/css/bootstrap-theme.min.css')}}\" rel=\"stylesheet\">
         <link rel=\"stylesheet\" href=\"{{asset('css/base.css')}}\"/>
        {% endblock %}
      <title>{% block title %}Welcome!{% endblock %}</title>
      <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('imgs/game_icon.jpg') }}\" />
    </head>

    {% block body %}
    <body>
\t<div class=\"container-fluid\">
    <div class=\"row\">
      <div id=\"left_menu\" class = \"col-sm-2 col-sm-pull-0\">
        <div id=\"infosPension\" class=\"menu\">
        {% set auth = app.user %}
          {% if auth is not null %}
             <img src=\"{{ asset('imgs/po.png') }}\"> : {{ auth.lastPension.gold }} &nbsp&nbsp
             <img src=\"{{ asset('imgs/paws.png') }}\"> : {{ auth.lastPension.paws }}<br>
             Réputation : {{ auth.lastPension.reputation }}<br>
             <img src=\"{{ asset('uploads/avatar/' ~ auth.lastPension.avatar) }}\"
                  class=\"img-circle\" id=\"imgPension\"/>
             {{ auth.lastPension.name }}
          {% endif %}
        </div>

      \t<div id=\"liens\" class=\"menu\">
        {% if is_granted(\"ROLE_ADMIN\") %}
          <a href=\"{{ path('administration') }}\" ><input type=\"button\" value=\"Administration\" class=\"butt\"></a>
          <br>
        {% endif %}
          <a href=\"{{ path('homepage') }}\"><input type=\"button\" value=\"Votre pension\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"{{ path('chooseMission') }}\" ><input type=\"button\" value=\"Missions\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"{{ path('veterinary') }}\"><input type=\"button\" value=\"Vétérinaire\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"{{ path('post_consult') }}\"><input type=\"button\" value=\"Poste\" class=\"butt\"></a>
          <br>
          <a href=\"{{ path('pensions') }}\"><input type=\"button\" value=\"Vos pensions\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"{{ path('ranking') }}\"><input type=\"button\" value=\"Classement\" class=\"butt\"></a>
      \t\t<br>
      \t\t<a href=\"{{ path('logout_user') }}\"><input type=\"button\" value=\"Deconnexion\" class=\"butt\"></a>
      \t</div>
      </div>

    \t<div id=\"ecran_princip\" class=\"col-sm-9 col-sm-push-1\">
          {% for msg in app.session.flashbag.get('ok') %}
            <div class=\"alert alert-success\">
                 {{ msg }}
            </div>
          {% endfor %}
          {% for info in app.session.flashbag.get('error') %}
            <div class=\"alert alert-danger\">
               {{ info }}
            </div>
          {% endfor %}

          {% block main_screen %}
          {% endblock %}
    \t</div>
    </div>

    {% endblock %}
    {% block javascripts %}
       {% if app.user is null %}
      <script>
        \$(\"#left_menu\").addClass(\"disabled\");
      </script>
      {% endif %}
    {% endblock %}
\t</div>
    </body>
</html>
", "base.html.twig", "/var/www/projet-web/app/Resources/views/base.html.twig");
    }
}
