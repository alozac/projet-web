<?php

/* administration/homeAdmin.html.twig */
class __TwigTemplate_e7eb969026c7331eb40ac886eb09198fcf87e690c525f61c467a88f965cc7eea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "administration/homeAdmin.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b02badb4b4b7401e323306df44d96a76882adc995feea14bf09dbd3439d60779 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b02badb4b4b7401e323306df44d96a76882adc995feea14bf09dbd3439d60779->enter($__internal_b02badb4b4b7401e323306df44d96a76882adc995feea14bf09dbd3439d60779_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/homeAdmin.html.twig"));

        $__internal_a42b64ef5434deb7c8c1c89f3c891301b2c93138fe894ddfb0046a751a813ca7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a42b64ef5434deb7c8c1c89f3c891301b2c93138fe894ddfb0046a751a813ca7->enter($__internal_a42b64ef5434deb7c8c1c89f3c891301b2c93138fe894ddfb0046a751a813ca7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/homeAdmin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b02badb4b4b7401e323306df44d96a76882adc995feea14bf09dbd3439d60779->leave($__internal_b02badb4b4b7401e323306df44d96a76882adc995feea14bf09dbd3439d60779_prof);

        
        $__internal_a42b64ef5434deb7c8c1c89f3c891301b2c93138fe894ddfb0046a751a813ca7->leave($__internal_a42b64ef5434deb7c8c1c89f3c891301b2c93138fe894ddfb0046a751a813ca7_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0cb674153f683cb5257944efa5aadd4fcb43013a16a78cf7188def90384ce670 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cb674153f683cb5257944efa5aadd4fcb43013a16a78cf7188def90384ce670->enter($__internal_0cb674153f683cb5257944efa5aadd4fcb43013a16a78cf7188def90384ce670_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_e95de5d71ccd5b58ac049b615bebd0cf59db1bb40540d03b7dfd2b7d23d7a3a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e95de5d71ccd5b58ac049b615bebd0cf59db1bb40540d03b7dfd2b7d23d7a3a9->enter($__internal_e95de5d71ccd5b58ac049b615bebd0cf59db1bb40540d03b7dfd2b7d23d7a3a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_consult.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_e95de5d71ccd5b58ac049b615bebd0cf59db1bb40540d03b7dfd2b7d23d7a3a9->leave($__internal_e95de5d71ccd5b58ac049b615bebd0cf59db1bb40540d03b7dfd2b7d23d7a3a9_prof);

        
        $__internal_0cb674153f683cb5257944efa5aadd4fcb43013a16a78cf7188def90384ce670->leave($__internal_0cb674153f683cb5257944efa5aadd4fcb43013a16a78cf7188def90384ce670_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_120faec828436c0ee7520aaa3d30f969379c83d6598717b73c3f3d037b841430 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_120faec828436c0ee7520aaa3d30f969379c83d6598717b73c3f3d037b841430->enter($__internal_120faec828436c0ee7520aaa3d30f969379c83d6598717b73c3f3d037b841430_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_6e7646448a4cd7ee7341e48cfe0eac30577dd3c2cc5217566ef6f07b9ea23415 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e7646448a4cd7ee7341e48cfe0eac30577dd3c2cc5217566ef6f07b9ea23415->enter($__internal_6e7646448a4cd7ee7341e48cfe0eac30577dd3c2cc5217566ef6f07b9ea23415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
";
        // line 10
        $context["route"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method");
        // line 11
        echo "\t
<nav class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <ul class=\"nav navbar-nav\">

      <li class=\"";
        // line 16
        if (((((($context["route"] ?? $this->getContext($context, "route")) == "administration") || (        // line 17
($context["route"] ?? $this->getContext($context, "route")) == "modifyEvent")) || (        // line 18
($context["route"] ?? $this->getContext($context, "route")) == "events_list")) || (        // line 19
($context["route"] ?? $this->getContext($context, "route")) == "deleteEvent"))) {
            echo " active ";
        }
        echo "\">
      \t<a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("events_list");
        echo "\">Evènements</a>
      </li>

    </ul>
  </div>
</nav>

";
        
        $__internal_6e7646448a4cd7ee7341e48cfe0eac30577dd3c2cc5217566ef6f07b9ea23415->leave($__internal_6e7646448a4cd7ee7341e48cfe0eac30577dd3c2cc5217566ef6f07b9ea23415_prof);

        
        $__internal_120faec828436c0ee7520aaa3d30f969379c83d6598717b73c3f3d037b841430->leave($__internal_120faec828436c0ee7520aaa3d30f969379c83d6598717b73c3f3d037b841430_prof);

    }

    public function getTemplateName()
    {
        return "administration/homeAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 20,  91 => 19,  90 => 18,  89 => 17,  88 => 16,  81 => 11,  79 => 10,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/post_consult.css')}}\"/>
{% endblock %}

{% block main_screen %}

{% set route = app.request.attributes.get('_route') %}
\t
<nav class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <ul class=\"nav navbar-nav\">

      <li class=\"{% if route == 'administration' 
      \t\t\t\tor route == 'modifyEvent'
      \t\t\t\tor route == 'events_list'
      \t\t\t\tor route == 'deleteEvent' %} active {% endif %}\">
      \t<a href=\"{{ path('events_list') }}\">Evènements</a>
      </li>

    </ul>
  </div>
</nav>

{% endblock %}", "administration/homeAdmin.html.twig", "/var/www/projet-web/app/Resources/views/administration/homeAdmin.html.twig");
    }
}
