<?php

/* administration/events_list.html.twig */
class __TwigTemplate_415f0afecca6acf1b2dbb1bd6e81c7339d9082353b5b65a3a94d97ebfe2b1585 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("administration/homeAdmin.html.twig", "administration/events_list.html.twig", 1);
        $this->blocks = array(
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "administration/homeAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2fd798b689799c957c9b764ffa92312741d7e6d2de61979cd9a983e539b0b434 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2fd798b689799c957c9b764ffa92312741d7e6d2de61979cd9a983e539b0b434->enter($__internal_2fd798b689799c957c9b764ffa92312741d7e6d2de61979cd9a983e539b0b434_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/events_list.html.twig"));

        $__internal_af39cec793a1421c28599f6e28a956876e88d5b006123645170bce3013e019ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af39cec793a1421c28599f6e28a956876e88d5b006123645170bce3013e019ee->enter($__internal_af39cec793a1421c28599f6e28a956876e88d5b006123645170bce3013e019ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/events_list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2fd798b689799c957c9b764ffa92312741d7e6d2de61979cd9a983e539b0b434->leave($__internal_2fd798b689799c957c9b764ffa92312741d7e6d2de61979cd9a983e539b0b434_prof);

        
        $__internal_af39cec793a1421c28599f6e28a956876e88d5b006123645170bce3013e019ee->leave($__internal_af39cec793a1421c28599f6e28a956876e88d5b006123645170bce3013e019ee_prof);

    }

    // line 3
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_3577c2efcd39211000a8ccbd609a10978e860f5db0310de14b9736aa41a26fc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3577c2efcd39211000a8ccbd609a10978e860f5db0310de14b9736aa41a26fc3->enter($__internal_3577c2efcd39211000a8ccbd609a10978e860f5db0310de14b9736aa41a26fc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_10efab3a0f916a3d68e67bcb8d111f931c018b5c11aeb72a60c6dd992cdf5a70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10efab3a0f916a3d68e67bcb8d111f931c018b5c11aeb72a60c6dd992cdf5a70->enter($__internal_10efab3a0f916a3d68e67bcb8d111f931c018b5c11aeb72a60c6dd992cdf5a70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 4
        echo "\t";
        $this->displayParentBlock("main_screen", $context, $blocks);
        echo "

\t<div class=\"row\">

\t\t<div id=\"list_events\" class=\"col-sm-4\">
\t\t";
        // line 9
        if (twig_test_empty(($context["events"] ?? $this->getContext($context, "events")))) {
            // line 10
            echo "\t\t\t<p>Aucun évènement à afficher</p>
\t\t";
        } else {
            // line 12
            echo "\t\t\t<div class=\"list-group\">
\t\t\t\t";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["events"] ?? $this->getContext($context, "events"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 14
                echo "\t\t\t\t<a id=\"event-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"event list-group-item\" href='#'>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["events"] ?? $this->getContext($context, "events")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</a>
\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "\t\t\t</div>
\t\t";
        }
        // line 18
        echo "\t\t</div>

\t\t<div id=\"event_view\" class=\"col-sm-5 col-sm-push-1\">
\t\t</div>

\t</div>

\t<a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modifyEvent", array("id" => 0));
        echo "\" role=\"button\" id=\"modify\" class=\"chgBut btn btn-info btn-sm disabled\">
\t\tModifier
\t</a>
\t<a href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("deleteEvent", array("id" => 0));
        echo "\" role=\"button\" id=\"remove\" class=\"chgBut btn btn-info btn-sm disabled\">
\t\tSupprimer
\t</a>
\t<a href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modifyEvent");
        echo "\" role=\"button\" class=\"btn btn-info btn-sm\">
\t\tNouveau
\t</a>

\t<script>
\tvar events = ";
        // line 36
        echo twig_jsonencode_filter(($context["events"] ?? $this->getContext($context, "events")));
        echo ";

\t\$(\".event\").click(function(){
\t\t\$(\".event\").removeClass(\"active\");
\t\t\$(this).addClass(\"active\");
\t 
\t\tvar newEvent = events[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefMod = \$(\"#modify\").attr(\"href\");
\t\t\$(\"#modify\").attr(\"href\", hrefMod.replace(/[0-9]+/, newEvent.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newEvent.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#event_view').html(\"<h4>\" + newEvent.name + \"</h4>\\
\t\t\t\t\t\t\t\t<p>Du <strong>\"+ newEvent.start.date.split('.')[0] +\"</strong>\\
\t\t\t\t\t\t\t\t au <strong>\" + newEvent.end.date.split('.')[0] +\"</strong></p>\\
\t\t\t\t\t\t\t   <p>\" + newEvent.description + \"</p>\");
\t});
</script>
    

";
        
        $__internal_10efab3a0f916a3d68e67bcb8d111f931c018b5c11aeb72a60c6dd992cdf5a70->leave($__internal_10efab3a0f916a3d68e67bcb8d111f931c018b5c11aeb72a60c6dd992cdf5a70_prof);

        
        $__internal_3577c2efcd39211000a8ccbd609a10978e860f5db0310de14b9736aa41a26fc3->leave($__internal_3577c2efcd39211000a8ccbd609a10978e860f5db0310de14b9736aa41a26fc3_prof);

    }

    public function getTemplateName()
    {
        return "administration/events_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 36,  107 => 31,  101 => 28,  95 => 25,  86 => 18,  82 => 16,  71 => 14,  67 => 13,  64 => 12,  60 => 10,  58 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'administration/homeAdmin.html.twig' %}

{% block main_screen %}
\t{{ parent() }}

\t<div class=\"row\">

\t\t<div id=\"list_events\" class=\"col-sm-4\">
\t\t{% if events is empty %}
\t\t\t<p>Aucun évènement à afficher</p>
\t\t{% else %}
\t\t\t<div class=\"list-group\">
\t\t\t\t{% for i in 0..events|length-1 %}
\t\t\t\t<a id=\"event-{{ i }}\" class=\"event list-group-item\" href='#'>{{ events[i].name }}</a>
\t\t\t\t{% endfor %}
\t\t\t</div>
\t\t{% endif %}
\t\t</div>

\t\t<div id=\"event_view\" class=\"col-sm-5 col-sm-push-1\">
\t\t</div>

\t</div>

\t<a href=\"{{ path('modifyEvent',  { 'id' : 0 }) }}\" role=\"button\" id=\"modify\" class=\"chgBut btn btn-info btn-sm disabled\">
\t\tModifier
\t</a>
\t<a href=\"{{ path('deleteEvent', { 'id' : 0 }) }}\" role=\"button\" id=\"remove\" class=\"chgBut btn btn-info btn-sm disabled\">
\t\tSupprimer
\t</a>
\t<a href=\"{{ path('modifyEvent') }}\" role=\"button\" class=\"btn btn-info btn-sm\">
\t\tNouveau
\t</a>

\t<script>
\tvar events = {{ events|json_encode|raw }};

\t\$(\".event\").click(function(){
\t\t\$(\".event\").removeClass(\"active\");
\t\t\$(this).addClass(\"active\");
\t 
\t\tvar newEvent = events[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefMod = \$(\"#modify\").attr(\"href\");
\t\t\$(\"#modify\").attr(\"href\", hrefMod.replace(/[0-9]+/, newEvent.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newEvent.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#event_view').html(\"<h4>\" + newEvent.name + \"</h4>\\
\t\t\t\t\t\t\t\t<p>Du <strong>\"+ newEvent.start.date.split('.')[0] +\"</strong>\\
\t\t\t\t\t\t\t\t au <strong>\" + newEvent.end.date.split('.')[0] +\"</strong></p>\\
\t\t\t\t\t\t\t   <p>\" + newEvent.description + \"</p>\");
\t});
</script>
    

{% endblock %}", "administration/events_list.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\administration\\events_list.html.twig");
    }
}
