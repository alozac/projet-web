<?php

/* /connect/connect.html.twig */
class __TwigTemplate_3c185f2b0a0da568224598f1c40986d0ecfdf759a939a3cb79579b986e48ac74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/connect/connect.html.twig", 1);
        $this->blocks = array(
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_415f56e2ce450a1462990f0aba2fc12c334d49d87cfd5ca665a1a93ba6f9ee17 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_415f56e2ce450a1462990f0aba2fc12c334d49d87cfd5ca665a1a93ba6f9ee17->enter($__internal_415f56e2ce450a1462990f0aba2fc12c334d49d87cfd5ca665a1a93ba6f9ee17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/connect/connect.html.twig"));

        $__internal_6a3fce1ef27268bcaf463443ec9606560548015a2a7d7bd3604d837eaf272a87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a3fce1ef27268bcaf463443ec9606560548015a2a7d7bd3604d837eaf272a87->enter($__internal_6a3fce1ef27268bcaf463443ec9606560548015a2a7d7bd3604d837eaf272a87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/connect/connect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_415f56e2ce450a1462990f0aba2fc12c334d49d87cfd5ca665a1a93ba6f9ee17->leave($__internal_415f56e2ce450a1462990f0aba2fc12c334d49d87cfd5ca665a1a93ba6f9ee17_prof);

        
        $__internal_6a3fce1ef27268bcaf463443ec9606560548015a2a7d7bd3604d837eaf272a87->leave($__internal_6a3fce1ef27268bcaf463443ec9606560548015a2a7d7bd3604d837eaf272a87_prof);

    }

    // line 2
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_261f5c16c3303a3d91ee64dcdff04433d28e9c27b4f314db23870c560eae2b70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_261f5c16c3303a3d91ee64dcdff04433d28e9c27b4f314db23870c560eae2b70->enter($__internal_261f5c16c3303a3d91ee64dcdff04433d28e9c27b4f314db23870c560eae2b70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_db0f656606ae97316d99bbba32a71b032edaaa6f88db60e6a362849d6ff5ebfb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db0f656606ae97316d99bbba32a71b032edaaa6f88db60e6a362849d6ff5ebfb->enter($__internal_db0f656606ae97316d99bbba32a71b032edaaa6f88db60e6a362849d6ff5ebfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 3
        echo "    <h1>Connexion</h1>

    ";
        // line 5
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 6
            echo "\t\t<div class=\"alert alert-danger error_connect\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "message", array()), "html", null, true);
            echo "</div>
\t";
        }
        // line 8
        echo "

  <form action=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
  \t<div class=\"form-group\">
    \t<label for=\"username\">Pseudo :</label>
    \t<input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" value=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
    </div>
    
    <div class=\"form-group\">
    \t<label for=\"password\">Mot de passe :</label>
    \t<input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\"  />
    </div>
    <button type=\"submit\" class=\"btn btn-primary\">Connexion</button>
  </form>


\t<button type=\"button\" class=\"btn btn-link\">
\t\t<a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("signup");
        echo "\">Inscription</a>
\t</button>

";
        
        $__internal_db0f656606ae97316d99bbba32a71b032edaaa6f88db60e6a362849d6ff5ebfb->leave($__internal_db0f656606ae97316d99bbba32a71b032edaaa6f88db60e6a362849d6ff5ebfb_prof);

        
        $__internal_261f5c16c3303a3d91ee64dcdff04433d28e9c27b4f314db23870c560eae2b70->leave($__internal_261f5c16c3303a3d91ee64dcdff04433d28e9c27b4f314db23870c560eae2b70_prof);

    }

    public function getTemplateName()
    {
        return "/connect/connect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 25,  71 => 13,  65 => 10,  61 => 8,  55 => 6,  53 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block main_screen %}
    <h1>Connexion</h1>

    {% if error %}
\t\t<div class=\"alert alert-danger error_connect\">{{ error.message }}</div>
\t{% endif %}


  <form action=\"{{ path('login') }}\" method=\"post\">
  \t<div class=\"form-group\">
    \t<label for=\"username\">Pseudo :</label>
    \t<input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" value=\"{{ last_username }}\" />
    </div>
    
    <div class=\"form-group\">
    \t<label for=\"password\">Mot de passe :</label>
    \t<input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\"  />
    </div>
    <button type=\"submit\" class=\"btn btn-primary\">Connexion</button>
  </form>


\t<button type=\"button\" class=\"btn btn-link\">
\t\t<a href=\"{{ path('signup') }}\">Inscription</a>
\t</button>

{% endblock %}", "/connect/connect.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\connect\\connect.html.twig");
    }
}
