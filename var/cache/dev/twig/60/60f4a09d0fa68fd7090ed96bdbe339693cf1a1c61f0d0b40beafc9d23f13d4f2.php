<?php

/* administration/homeAdmin.html.twig */
class __TwigTemplate_9dd3611c7d8fb5bc68bb1487b42381f1239f47fcdd2f38212fd238e7980b5a1c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "administration/homeAdmin.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e3d78acacd4aceb318711ab5c7d206d295958428bed696281eda53a38a2c0413 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e3d78acacd4aceb318711ab5c7d206d295958428bed696281eda53a38a2c0413->enter($__internal_e3d78acacd4aceb318711ab5c7d206d295958428bed696281eda53a38a2c0413_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/homeAdmin.html.twig"));

        $__internal_137562c15439dee0c79ef5667e0a0bc8f757b3e48593fa1d71a47468162d762f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_137562c15439dee0c79ef5667e0a0bc8f757b3e48593fa1d71a47468162d762f->enter($__internal_137562c15439dee0c79ef5667e0a0bc8f757b3e48593fa1d71a47468162d762f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/homeAdmin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e3d78acacd4aceb318711ab5c7d206d295958428bed696281eda53a38a2c0413->leave($__internal_e3d78acacd4aceb318711ab5c7d206d295958428bed696281eda53a38a2c0413_prof);

        
        $__internal_137562c15439dee0c79ef5667e0a0bc8f757b3e48593fa1d71a47468162d762f->leave($__internal_137562c15439dee0c79ef5667e0a0bc8f757b3e48593fa1d71a47468162d762f_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_89182257b27072e3d79872a87cc522ac6ae9676908c6d51bf5aa75af2e4b21bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89182257b27072e3d79872a87cc522ac6ae9676908c6d51bf5aa75af2e4b21bb->enter($__internal_89182257b27072e3d79872a87cc522ac6ae9676908c6d51bf5aa75af2e4b21bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_beab7452f51292f22354b7750c8a261849bab959ca1dc4819a8d643d423cc83b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_beab7452f51292f22354b7750c8a261849bab959ca1dc4819a8d643d423cc83b->enter($__internal_beab7452f51292f22354b7750c8a261849bab959ca1dc4819a8d643d423cc83b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_consult.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_beab7452f51292f22354b7750c8a261849bab959ca1dc4819a8d643d423cc83b->leave($__internal_beab7452f51292f22354b7750c8a261849bab959ca1dc4819a8d643d423cc83b_prof);

        
        $__internal_89182257b27072e3d79872a87cc522ac6ae9676908c6d51bf5aa75af2e4b21bb->leave($__internal_89182257b27072e3d79872a87cc522ac6ae9676908c6d51bf5aa75af2e4b21bb_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_1ca700a12f8227cd199eb1e6fb7a5efa9eb2f788614e996b92eac79f103383a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1ca700a12f8227cd199eb1e6fb7a5efa9eb2f788614e996b92eac79f103383a2->enter($__internal_1ca700a12f8227cd199eb1e6fb7a5efa9eb2f788614e996b92eac79f103383a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_b8230a779105e96cd27707be4efe7e65de78eb0d3e912bec64fd0dad7851ec89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8230a779105e96cd27707be4efe7e65de78eb0d3e912bec64fd0dad7851ec89->enter($__internal_b8230a779105e96cd27707be4efe7e65de78eb0d3e912bec64fd0dad7851ec89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
";
        // line 10
        $context["route"] = $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "request", array()), "attributes", array()), "get", array(0 => "_route"), "method");
        // line 11
        echo "\t
<nav class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <ul class=\"nav navbar-nav\">

      <li class=\"";
        // line 16
        if (((((($context["route"] ?? $this->getContext($context, "route")) == "administration") || (        // line 17
($context["route"] ?? $this->getContext($context, "route")) == "modifyEvent")) || (        // line 18
($context["route"] ?? $this->getContext($context, "route")) == "events_list")) || (        // line 19
($context["route"] ?? $this->getContext($context, "route")) == "deleteEvent"))) {
            echo " active ";
        }
        echo "\">
      \t<a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("events_list");
        echo "\">Evènements</a>
      </li>

    </ul>
  </div>
</nav>

";
        
        $__internal_b8230a779105e96cd27707be4efe7e65de78eb0d3e912bec64fd0dad7851ec89->leave($__internal_b8230a779105e96cd27707be4efe7e65de78eb0d3e912bec64fd0dad7851ec89_prof);

        
        $__internal_1ca700a12f8227cd199eb1e6fb7a5efa9eb2f788614e996b92eac79f103383a2->leave($__internal_1ca700a12f8227cd199eb1e6fb7a5efa9eb2f788614e996b92eac79f103383a2_prof);

    }

    public function getTemplateName()
    {
        return "administration/homeAdmin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 20,  91 => 19,  90 => 18,  89 => 17,  88 => 16,  81 => 11,  79 => 10,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/post_consult.css')}}\"/>
{% endblock %}

{% block main_screen %}

{% set route = app.request.attributes.get('_route') %}
\t
<nav class=\"navbar navbar-default\">
  <div class=\"container-fluid\">
    <ul class=\"nav navbar-nav\">

      <li class=\"{% if route == 'administration' 
      \t\t\t\tor route == 'modifyEvent'
      \t\t\t\tor route == 'events_list'
      \t\t\t\tor route == 'deleteEvent' %} active {% endif %}\">
      \t<a href=\"{{ path('events_list') }}\">Evènements</a>
      </li>

    </ul>
  </div>
</nav>

{% endblock %}", "administration/homeAdmin.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\administration\\homeAdmin.html.twig");
    }
}
