<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_ebdbe9cd64ec8db08e2dd0e53213f72dab1de75d5aaf4bbf1145a48b6190709a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25e7fcf8e0951cd9d366268ce3fe3e66211437029ff87360a04b36f4b615ff96 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_25e7fcf8e0951cd9d366268ce3fe3e66211437029ff87360a04b36f4b615ff96->enter($__internal_25e7fcf8e0951cd9d366268ce3fe3e66211437029ff87360a04b36f4b615ff96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_1d28abacdc79f0fbec95e18030cd4f9fca13b76ef39b747625fb458da875cf2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d28abacdc79f0fbec95e18030cd4f9fca13b76ef39b747625fb458da875cf2a->enter($__internal_1d28abacdc79f0fbec95e18030cd4f9fca13b76ef39b747625fb458da875cf2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_25e7fcf8e0951cd9d366268ce3fe3e66211437029ff87360a04b36f4b615ff96->leave($__internal_25e7fcf8e0951cd9d366268ce3fe3e66211437029ff87360a04b36f4b615ff96_prof);

        
        $__internal_1d28abacdc79f0fbec95e18030cd4f9fca13b76ef39b747625fb458da875cf2a->leave($__internal_1d28abacdc79f0fbec95e18030cd4f9fca13b76ef39b747625fb458da875cf2a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
