<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_c297e43fac4a682c526ef34eab883ab2669f5a7c6cff364a060e8af62121635c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8650fc8fd302634a0a7a717e8c585f34472b51916eea5a5c29d1c66d50d91363 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8650fc8fd302634a0a7a717e8c585f34472b51916eea5a5c29d1c66d50d91363->enter($__internal_8650fc8fd302634a0a7a717e8c585f34472b51916eea5a5c29d1c66d50d91363_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_6b32751f1ff00ecd1f4cf4d681e3ec0f92f6d2057fa6c5150708a8745ddcb886 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b32751f1ff00ecd1f4cf4d681e3ec0f92f6d2057fa6c5150708a8745ddcb886->enter($__internal_6b32751f1ff00ecd1f4cf4d681e3ec0f92f6d2057fa6c5150708a8745ddcb886_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_8650fc8fd302634a0a7a717e8c585f34472b51916eea5a5c29d1c66d50d91363->leave($__internal_8650fc8fd302634a0a7a717e8c585f34472b51916eea5a5c29d1c66d50d91363_prof);

        
        $__internal_6b32751f1ff00ecd1f4cf4d681e3ec0f92f6d2057fa6c5150708a8745ddcb886->leave($__internal_6b32751f1ff00ecd1f4cf4d681e3ec0f92f6d2057fa6c5150708a8745ddcb886_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
