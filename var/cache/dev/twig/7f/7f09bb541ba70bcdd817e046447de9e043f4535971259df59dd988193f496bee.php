<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_3727925fe25ad85e355d269386936e62c33e54e041b0f38e4dfe9235c1e03e34 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_90c0173e26bafe3cff514dab4ab7a06ff7244917f6004d5d396ea72ff9443d35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90c0173e26bafe3cff514dab4ab7a06ff7244917f6004d5d396ea72ff9443d35->enter($__internal_90c0173e26bafe3cff514dab4ab7a06ff7244917f6004d5d396ea72ff9443d35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_ea8d62436e23354e59387e5eb970655a783db590fe4ca47b8763a7966ee32e48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea8d62436e23354e59387e5eb970655a783db590fe4ca47b8763a7966ee32e48->enter($__internal_ea8d62436e23354e59387e5eb970655a783db590fe4ca47b8763a7966ee32e48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.atom.twig", 1)->display($context);
        
        $__internal_90c0173e26bafe3cff514dab4ab7a06ff7244917f6004d5d396ea72ff9443d35->leave($__internal_90c0173e26bafe3cff514dab4ab7a06ff7244917f6004d5d396ea72ff9443d35_prof);

        
        $__internal_ea8d62436e23354e59387e5eb970655a783db590fe4ca47b8763a7966ee32e48->leave($__internal_ea8d62436e23354e59387e5eb970655a783db590fe4ca47b8763a7966ee32e48_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.atom.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
