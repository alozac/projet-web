<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_b0e5da753aa1cdffcb94211a859eaaa328c0d426f6af727a6f7e69857a03f794 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b057a7406a0ca59c80b5653ff60cb42f82f105b11d1f5806f08257b95c35300 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b057a7406a0ca59c80b5653ff60cb42f82f105b11d1f5806f08257b95c35300->enter($__internal_3b057a7406a0ca59c80b5653ff60cb42f82f105b11d1f5806f08257b95c35300_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_97b19abe280f228d21988360b9fcbb2362f9c9f36039e41d75136ca2d461a75b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97b19abe280f228d21988360b9fcbb2362f9c9f36039e41d75136ca2d461a75b->enter($__internal_97b19abe280f228d21988360b9fcbb2362f9c9f36039e41d75136ca2d461a75b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_3b057a7406a0ca59c80b5653ff60cb42f82f105b11d1f5806f08257b95c35300->leave($__internal_3b057a7406a0ca59c80b5653ff60cb42f82f105b11d1f5806f08257b95c35300_prof);

        
        $__internal_97b19abe280f228d21988360b9fcbb2362f9c9f36039e41d75136ca2d461a75b->leave($__internal_97b19abe280f228d21988360b9fcbb2362f9c9f36039e41d75136ca2d461a75b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
