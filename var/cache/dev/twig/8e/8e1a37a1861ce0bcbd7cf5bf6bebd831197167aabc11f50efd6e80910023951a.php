<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_279ecc404c752ab2fc4021ac71321b6e56c3b962c2568c8ac7c3a035275de6eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02c4ed645e3aebed109c425c7cbf6f641abfa8e5d6f647f9dd66308aecec803d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02c4ed645e3aebed109c425c7cbf6f641abfa8e5d6f647f9dd66308aecec803d->enter($__internal_02c4ed645e3aebed109c425c7cbf6f641abfa8e5d6f647f9dd66308aecec803d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_31fa369f14dac450387f494359e66e09251676b72fe45cca63204fb97be229a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_31fa369f14dac450387f494359e66e09251676b72fe45cca63204fb97be229a9->enter($__internal_31fa369f14dac450387f494359e66e09251676b72fe45cca63204fb97be229a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_02c4ed645e3aebed109c425c7cbf6f641abfa8e5d6f647f9dd66308aecec803d->leave($__internal_02c4ed645e3aebed109c425c7cbf6f641abfa8e5d6f647f9dd66308aecec803d_prof);

        
        $__internal_31fa369f14dac450387f494359e66e09251676b72fe45cca63204fb97be229a9->leave($__internal_31fa369f14dac450387f494359e66e09251676b72fe45cca63204fb97be229a9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
