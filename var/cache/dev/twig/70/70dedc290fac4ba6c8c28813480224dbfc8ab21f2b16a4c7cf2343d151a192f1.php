<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_c376111ae31b56cd3f7b6ec128f883f91fa3e9a3fbd636276d22e7ed4072eba7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba3bddc30353c8997d72d9f413acf99a7afd88d990cd0ab739bbb4f746115abf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba3bddc30353c8997d72d9f413acf99a7afd88d990cd0ab739bbb4f746115abf->enter($__internal_ba3bddc30353c8997d72d9f413acf99a7afd88d990cd0ab739bbb4f746115abf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_5825c4f6fbbd1244c7066484a8cec2e71884540fa25a8e0063ace000d113537a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5825c4f6fbbd1244c7066484a8cec2e71884540fa25a8e0063ace000d113537a->enter($__internal_5825c4f6fbbd1244c7066484a8cec2e71884540fa25a8e0063ace000d113537a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.js.twig", 2)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_ba3bddc30353c8997d72d9f413acf99a7afd88d990cd0ab739bbb4f746115abf->leave($__internal_ba3bddc30353c8997d72d9f413acf99a7afd88d990cd0ab739bbb4f746115abf_prof);

        
        $__internal_5825c4f6fbbd1244c7066484a8cec2e71884540fa25a8e0063ace000d113537a->leave($__internal_5825c4f6fbbd1244c7066484a8cec2e71884540fa25a8e0063ace000d113537a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.js.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
