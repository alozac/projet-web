<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_e3ca1d0e0ce71a37affe3741d2db354569881cae5850814e2d81f06fdcddc9f5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3b954eb505c91ba7d0d2a7e03e20f096941b1e81e1494b8d649395fffa711ed5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3b954eb505c91ba7d0d2a7e03e20f096941b1e81e1494b8d649395fffa711ed5->enter($__internal_3b954eb505c91ba7d0d2a7e03e20f096941b1e81e1494b8d649395fffa711ed5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_d4f264fa18ba8dd26f5c13dddf25373bbc35c4eb5bd8333e670e69f8ed20ca61 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4f264fa18ba8dd26f5c13dddf25373bbc35c4eb5bd8333e670e69f8ed20ca61->enter($__internal_d4f264fa18ba8dd26f5c13dddf25373bbc35c4eb5bd8333e670e69f8ed20ca61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_3b954eb505c91ba7d0d2a7e03e20f096941b1e81e1494b8d649395fffa711ed5->leave($__internal_3b954eb505c91ba7d0d2a7e03e20f096941b1e81e1494b8d649395fffa711ed5_prof);

        
        $__internal_d4f264fa18ba8dd26f5c13dddf25373bbc35c4eb5bd8333e670e69f8ed20ca61->leave($__internal_d4f264fa18ba8dd26f5c13dddf25373bbc35c4eb5bd8333e670e69f8ed20ca61_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
