<?php

/* @WebProfiler/Profiler/header.html.twig */
class __TwigTemplate_ae9e20ab4219b89761e32ea05980796a9a6e7c91ace9ff3795a2dfca63e24522 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_60072cd108a15d3cd0b8a6a7e7a145429a07c14b83d50c911fd8d7ce059e53d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60072cd108a15d3cd0b8a6a7e7a145429a07c14b83d50c911fd8d7ce059e53d2->enter($__internal_60072cd108a15d3cd0b8a6a7e7a145429a07c14b83d50c911fd8d7ce059e53d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        $__internal_a3e7e2e522245fea9396a5c98a7a3710a1062feffc3e46574eb84a67bc1c7d90 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3e7e2e522245fea9396a5c98a7a3710a1062feffc3e46574eb84a67bc1c7d90->enter($__internal_a3e7e2e522245fea9396a5c98a7a3710a1062feffc3e46574eb84a67bc1c7d90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_60072cd108a15d3cd0b8a6a7e7a145429a07c14b83d50c911fd8d7ce059e53d2->leave($__internal_60072cd108a15d3cd0b8a6a7e7a145429a07c14b83d50c911fd8d7ce059e53d2_prof);

        
        $__internal_a3e7e2e522245fea9396a5c98a7a3710a1062feffc3e46574eb84a67bc1c7d90->leave($__internal_a3e7e2e522245fea9396a5c98a7a3710a1062feffc3e46574eb84a67bc1c7d90_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "@WebProfiler/Profiler/header.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}
