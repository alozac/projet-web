<?php

/* /connect/register.html.twig */
class __TwigTemplate_f1c0b5925dd57086591b480cf5d02f5c5200cbe346aba873def8c36cb61f5e62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/connect/register.html.twig", 1);
        $this->blocks = array(
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_48c41eeef619f5cbcdab3e6212cbf7d22bdd77d6ead9101531445df8aef3df3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_48c41eeef619f5cbcdab3e6212cbf7d22bdd77d6ead9101531445df8aef3df3c->enter($__internal_48c41eeef619f5cbcdab3e6212cbf7d22bdd77d6ead9101531445df8aef3df3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/connect/register.html.twig"));

        $__internal_3c85ddaf163cd169ed46228bd5ce53f31c5088fbfeecc6568a7af89d07c597b6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3c85ddaf163cd169ed46228bd5ce53f31c5088fbfeecc6568a7af89d07c597b6->enter($__internal_3c85ddaf163cd169ed46228bd5ce53f31c5088fbfeecc6568a7af89d07c597b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/connect/register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_48c41eeef619f5cbcdab3e6212cbf7d22bdd77d6ead9101531445df8aef3df3c->leave($__internal_48c41eeef619f5cbcdab3e6212cbf7d22bdd77d6ead9101531445df8aef3df3c_prof);

        
        $__internal_3c85ddaf163cd169ed46228bd5ce53f31c5088fbfeecc6568a7af89d07c597b6->leave($__internal_3c85ddaf163cd169ed46228bd5ce53f31c5088fbfeecc6568a7af89d07c597b6_prof);

    }

    // line 3
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_ae3b4ee6e08f9ab97c732291d7dc20649becd4701283ccc5b2e26ca2bbd59415 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae3b4ee6e08f9ab97c732291d7dc20649becd4701283ccc5b2e26ca2bbd59415->enter($__internal_ae3b4ee6e08f9ab97c732291d7dc20649becd4701283ccc5b2e26ca2bbd59415_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_c0b502d8a2e05b3ebb1bba38e1a7d8a6989946745b3c7aa4059fcc7074822e28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0b502d8a2e05b3ebb1bba38e1a7d8a6989946745b3c7aa4059fcc7074822e28->enter($__internal_c0b502d8a2e05b3ebb1bba38e1a7d8a6989946745b3c7aa4059fcc7074822e28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 4
        echo "    <h1>Inscription</h1>

\t";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "username", array()), 'row');
        echo "
\t";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "email", array()), 'row');
        echo "
\t";
        // line 9
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "password", array()), 'row');
        echo "
\t<div>
\t\t<h3>Votre Première pension:</h3>
\t\t";
        // line 12
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "pensionName", array()), 'row');
        echo "
\t\t";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "pensionAvatar", array()), 'row');
        echo "
\t</div>
\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "signup", array()), 'row', array("attr" => array("class" => "btn-primary")));
        echo "
\t";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "_token", array()), 'row');
        echo "
\t";
        // line 17
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

\t
\t<button type=\"button\" class=\"btn btn-link\">
\t\t<a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\">Connexion</a>
\t</button>

";
        
        $__internal_c0b502d8a2e05b3ebb1bba38e1a7d8a6989946745b3c7aa4059fcc7074822e28->leave($__internal_c0b502d8a2e05b3ebb1bba38e1a7d8a6989946745b3c7aa4059fcc7074822e28_prof);

        
        $__internal_ae3b4ee6e08f9ab97c732291d7dc20649becd4701283ccc5b2e26ca2bbd59415->leave($__internal_ae3b4ee6e08f9ab97c732291d7dc20649becd4701283ccc5b2e26ca2bbd59415_prof);

    }

    public function getTemplateName()
    {
        return "/connect/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 21,  88 => 17,  84 => 16,  80 => 15,  75 => 13,  71 => 12,  65 => 9,  61 => 8,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block main_screen %}
    <h1>Inscription</h1>

\t{{ form_start(form) }}
\t{{ form_row(form.username) }}
\t{{ form_row(form.email) }}
\t{{ form_row(form.password) }}
\t<div>
\t\t<h3>Votre Première pension:</h3>
\t\t{{ form_row(form.pensionName) }}
\t\t{{ form_row(form.pensionAvatar) }}
\t</div>
\t{{ form_row(form.signup, { 'attr': {'class': 'btn-primary'} }) }}
\t{{ form_row(form._token) }}
\t{{ form_end(form) }}

\t
\t<button type=\"button\" class=\"btn btn-link\">
\t\t<a href=\"{{ path('login') }}\">Connexion</a>
\t</button>

{% endblock %}", "/connect/register.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\connect\\register.html.twig");
    }
}
