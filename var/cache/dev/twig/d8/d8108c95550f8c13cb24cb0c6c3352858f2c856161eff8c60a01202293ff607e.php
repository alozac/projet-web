<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_b29a285a91cc3c706e790891c543ae7f1f62d88fc843f54c8ae1c93070b8d365 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_29451d322fbf7a5da329eb82a01164e5a301b5234c2eeb9c10a7af1419c89e07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29451d322fbf7a5da329eb82a01164e5a301b5234c2eeb9c10a7af1419c89e07->enter($__internal_29451d322fbf7a5da329eb82a01164e5a301b5234c2eeb9c10a7af1419c89e07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_e475b5a710134d2c6da262f282600deedc9b9330b02d3ffc8f7a3fae4366775e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e475b5a710134d2c6da262f282600deedc9b9330b02d3ffc8f7a3fae4366775e->enter($__internal_e475b5a710134d2c6da262f282600deedc9b9330b02d3ffc8f7a3fae4366775e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_29451d322fbf7a5da329eb82a01164e5a301b5234c2eeb9c10a7af1419c89e07->leave($__internal_29451d322fbf7a5da329eb82a01164e5a301b5234c2eeb9c10a7af1419c89e07_prof);

        
        $__internal_e475b5a710134d2c6da262f282600deedc9b9330b02d3ffc8f7a3fae4366775e->leave($__internal_e475b5a710134d2c6da262f282600deedc9b9330b02d3ffc8f7a3fae4366775e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
