<?php

/* persos.html.twig */
class __TwigTemplate_3fa908f39e3ff2b3222a2b555791657ad1b2e12c44bf1417187947a36667e3b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "persos.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a04ed69f4c8c0473282b74314718406f91c0f6886780070926f1cdc22e93fef5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a04ed69f4c8c0473282b74314718406f91c0f6886780070926f1cdc22e93fef5->enter($__internal_a04ed69f4c8c0473282b74314718406f91c0f6886780070926f1cdc22e93fef5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "persos.html.twig"));

        $__internal_ceee8dfac6dd044947e3711c91a6fc606fa5f9468e586d0b5ab8da623847fd47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ceee8dfac6dd044947e3711c91a6fc606fa5f9468e586d0b5ab8da623847fd47->enter($__internal_ceee8dfac6dd044947e3711c91a6fc606fa5f9468e586d0b5ab8da623847fd47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "persos.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a04ed69f4c8c0473282b74314718406f91c0f6886780070926f1cdc22e93fef5->leave($__internal_a04ed69f4c8c0473282b74314718406f91c0f6886780070926f1cdc22e93fef5_prof);

        
        $__internal_ceee8dfac6dd044947e3711c91a6fc606fa5f9468e586d0b5ab8da623847fd47->leave($__internal_ceee8dfac6dd044947e3711c91a6fc606fa5f9468e586d0b5ab8da623847fd47_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9cd9bd266cdfbaa42aad604bac52465e89a052849b53d638d0065c6903c59bb1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9cd9bd266cdfbaa42aad604bac52465e89a052849b53d638d0065c6903c59bb1->enter($__internal_9cd9bd266cdfbaa42aad604bac52465e89a052849b53d638d0065c6903c59bb1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_00b7dc2a04a41f7578fcd5b64d4775b3675add0b92dbe648ea8f582c013db892 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_00b7dc2a04a41f7578fcd5b64d4775b3675add0b92dbe648ea8f582c013db892->enter($__internal_00b7dc2a04a41f7578fcd5b64d4775b3675add0b92dbe648ea8f582c013db892_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
";
        
        $__internal_00b7dc2a04a41f7578fcd5b64d4775b3675add0b92dbe648ea8f582c013db892->leave($__internal_00b7dc2a04a41f7578fcd5b64d4775b3675add0b92dbe648ea8f582c013db892_prof);

        
        $__internal_9cd9bd266cdfbaa42aad604bac52465e89a052849b53d638d0065c6903c59bb1->leave($__internal_9cd9bd266cdfbaa42aad604bac52465e89a052849b53d638d0065c6903c59bb1_prof);

    }

    // line 7
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_a61b61fbe3a81969a565c7355be9811f3b6889a8d3f3ae021ba97b641900ea5e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a61b61fbe3a81969a565c7355be9811f3b6889a8d3f3ae021ba97b641900ea5e->enter($__internal_a61b61fbe3a81969a565c7355be9811f3b6889a8d3f3ae021ba97b641900ea5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_a734096e2eb7417886949a472ad616faa1b1a95ec32427a157be676851458b73 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a734096e2eb7417886949a472ad616faa1b1a95ec32427a157be676851458b73->enter($__internal_a734096e2eb7417886949a472ad616faa1b1a95ec32427a157be676851458b73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 8
        echo "
<div class=\"page-header\">
\t<h3>Vos pensions</h3>
</div>

\t";
        // line 13
        $context["indexConn"] =  -1;
        // line 14
        echo "
\t<div class=\"row\">
\t\t<div id=\"otherPensions\" class=\"col-sm-4\">
\t\t\t<ul id=\"list_pensions\" class=\"list-group\">
\t\t\t\t";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["allPensions"] ?? $this->getContext($context, "allPensions"))) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 19
            echo "\t\t\t\t\t<li id=\"pension-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"pension list-group-item
\t\t\t\t\t\t\t";
            // line 20
            if (($this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), $context["i"], array(), "array"), "name", array()) == $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "name", array()))) {
                // line 21
                echo "\t\t\t\t\t\t\t\t";
                $context["indexConn"] = $context["i"];
                // line 22
                echo "\t\t\t\t\t\t\t\tactive
\t\t\t\t\t\t\t";
            }
            // line 23
            echo "\"
\t\t\t\t\t>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), $context["i"], array(), "array"), "name", array()), "html", null, true);
            echo "</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "\t\t\t</ul>
\t\t</div>

\t\t<div id=\"currPension\" class=\"col-sm-7 col-sm-push-1\">
\t\t\t<div id=\"infoPension\">
\t\t\t\t<img id=\"avatar\" src=\"uploads/avatar/";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), ($context["indexConn"] ?? $this->getContext($context, "indexConn")), array(), "array"), "avatar", array()), "html", null, true);
        echo "\" heigth=\"150\" width=\"150\"><br>
\t\t\t\t<img src='/imgs/game_icon.jpg' height='20' width='20'> :
\t\t\t\t\t<span id=\"nbMiss\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), ($context["indexConn"] ?? $this->getContext($context, "indexConn")), array(), "array"), "nbMission", array()), "html", null, true);
        echo "</span><br>
\t\t\t\t<img src='/imgs/po.png'> : 
\t\t\t\t\t<span id=\"gold\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), ($context["indexConn"] ?? $this->getContext($context, "indexConn")), array(), "array"), "gold", array()), "html", null, true);
        echo "</span>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t<span id=\"changePension\">
\t\t\t\t\t<a href='";
        // line 39
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("changePension", array("id" => $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "LastPension", array()), "id", array()))), "html", null, true);
        echo "'>
\t\t\t\t\t\t<button type=\"button\" id=\"changePensionBut\" disabled class=\"upBut btn btn-default\">Changer de pension</button>
\t\t\t\t\t</a>
\t\t\t\t</span>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t<div>
\t\t<button data-toggle=\"collapse\" data-target=\"#createPension\" class=\"btn btn-default\">Nouvelle pension</button>
\t\t<span id=\"removePension\">
\t\t\t<a href='";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("removePension", array("id" => $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "LastPension", array()), "id", array()))), "html", null, true);
        echo "'>
\t\t\t\t<button type=\"button\" id=\"rmPensionBut\" disabled class=\" upBut btn btn-default\">Supprimer</button>
\t\t\t</a>
\t\t</span>
\t</div>
\t<div id=\"div_create_pension\" class=\"col-sm-4\">

\t\t<div id=\"createPension\" class=\"collapse\">
\t\t\t";
        // line 58
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_start');
        echo "
\t\t\t";
        // line 59
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'widget');
        echo "
\t\t\t";
        // line 60
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_end');
        echo "
\t\t</div>
\t</div>

<script>
\tvar pensions = ";
        // line 65
        echo twig_jsonencode_filter(($context["allPensions"] ?? $this->getContext($context, "allPensions")));
        echo ";

\t\$(\".pension\").click(function(){
\t\t\$(\".pension\").removeClass(\"active\");
\t\t\$(this).addClass(\"active\");
\t 
\t\tvar newPension = pensions[\$(this).attr(\"id\").split(\"-\")[1]];

\t\t\$(\"#avatar\").attr(\"src\", \"uploads/avatar/\"+newPension.avatar);
\t\t\$(\"#nbMiss\").html(newPension.nbMission);
\t\t\$(\"#gold\").html(newPension.gold);


\t\tvar hrefRm = \$(\"#removePension > a\").attr(\"href\");
\t\t\$(\"#removePension > a\").attr(\"href\", hrefRm.replace(/[0-9]+/, newPension.id));

\t\tvar hrefChg = \$(\"#changePension > a\").attr(\"href\");
\t\t\$(\"#changePension > a\").attr(\"href\", hrefChg.replace(/[0-9]+/, newPension.id));

\t\tif (newPension.id == ";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "lastPension", array()), "id", array()), "html", null, true);
        echo ")
\t\t\t\$(\".upBut\").prop(\"disabled\", true);
\t\telse \$(\".upBut\").prop(\"disabled\", false);
\t});

</script>

";
        
        $__internal_a734096e2eb7417886949a472ad616faa1b1a95ec32427a157be676851458b73->leave($__internal_a734096e2eb7417886949a472ad616faa1b1a95ec32427a157be676851458b73_prof);

        
        $__internal_a61b61fbe3a81969a565c7355be9811f3b6889a8d3f3ae021ba97b641900ea5e->leave($__internal_a61b61fbe3a81969a565c7355be9811f3b6889a8d3f3ae021ba97b641900ea5e_prof);

    }

    public function getTemplateName()
    {
        return "persos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 84,  181 => 65,  173 => 60,  169 => 59,  165 => 58,  154 => 50,  140 => 39,  133 => 35,  128 => 33,  123 => 31,  116 => 26,  108 => 24,  105 => 23,  101 => 22,  98 => 21,  96 => 20,  91 => 19,  87 => 18,  81 => 14,  79 => 13,  72 => 8,  63 => 7,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Vos pensions</h3>
</div>

\t{% set indexConn = -1 %}

\t<div class=\"row\">
\t\t<div id=\"otherPensions\" class=\"col-sm-4\">
\t\t\t<ul id=\"list_pensions\" class=\"list-group\">
\t\t\t\t{% for i in 0..allPensions|length -1 %}
\t\t\t\t\t<li id=\"pension-{{ i }}\" class=\"pension list-group-item
\t\t\t\t\t\t\t{% if allPensions[i].name == auth.lastPension.name %}
\t\t\t\t\t\t\t\t{% set indexConn = i %}
\t\t\t\t\t\t\t\tactive
\t\t\t\t\t\t\t{% endif %}\"
\t\t\t\t\t>{{ allPensions[i].name }}</li>
\t\t\t\t{% endfor %}
\t\t\t</ul>
\t\t</div>

\t\t<div id=\"currPension\" class=\"col-sm-7 col-sm-push-1\">
\t\t\t<div id=\"infoPension\">
\t\t\t\t<img id=\"avatar\" src=\"uploads/avatar/{{ allPensions[indexConn].avatar }}\" heigth=\"150\" width=\"150\"><br>
\t\t\t\t<img src='/imgs/game_icon.jpg' height='20' width='20'> :
\t\t\t\t\t<span id=\"nbMiss\">{{ allPensions[indexConn].nbMission }}</span><br>
\t\t\t\t<img src='/imgs/po.png'> : 
\t\t\t\t\t<span id=\"gold\">{{ allPensions[indexConn].gold }}</span>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t<span id=\"changePension\">
\t\t\t\t\t<a href='{{ path(\"changePension\", { \"id\" : auth.LastPension.id }) }}'>
\t\t\t\t\t\t<button type=\"button\" id=\"changePensionBut\" disabled class=\"upBut btn btn-default\">Changer de pension</button>
\t\t\t\t\t</a>
\t\t\t\t</span>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t<div>
\t\t<button data-toggle=\"collapse\" data-target=\"#createPension\" class=\"btn btn-default\">Nouvelle pension</button>
\t\t<span id=\"removePension\">
\t\t\t<a href='{{ path(\"removePension\", { \"id\" : auth.LastPension.id }) }}'>
\t\t\t\t<button type=\"button\" id=\"rmPensionBut\" disabled class=\" upBut btn btn-default\">Supprimer</button>
\t\t\t</a>
\t\t</span>
\t</div>
\t<div id=\"div_create_pension\" class=\"col-sm-4\">

\t\t<div id=\"createPension\" class=\"collapse\">
\t\t\t{{ form_start(formNew) }}
\t\t\t{{ form_widget(formNew) }}
\t\t\t{{ form_end(formNew) }}
\t\t</div>
\t</div>

<script>
\tvar pensions = {{ allPensions|json_encode|raw }};

\t\$(\".pension\").click(function(){
\t\t\$(\".pension\").removeClass(\"active\");
\t\t\$(this).addClass(\"active\");
\t 
\t\tvar newPension = pensions[\$(this).attr(\"id\").split(\"-\")[1]];

\t\t\$(\"#avatar\").attr(\"src\", \"uploads/avatar/\"+newPension.avatar);
\t\t\$(\"#nbMiss\").html(newPension.nbMission);
\t\t\$(\"#gold\").html(newPension.gold);


\t\tvar hrefRm = \$(\"#removePension > a\").attr(\"href\");
\t\t\$(\"#removePension > a\").attr(\"href\", hrefRm.replace(/[0-9]+/, newPension.id));

\t\tvar hrefChg = \$(\"#changePension > a\").attr(\"href\");
\t\t\$(\"#changePension > a\").attr(\"href\", hrefChg.replace(/[0-9]+/, newPension.id));

\t\tif (newPension.id == {{ app.user.lastPension.id }})
\t\t\t\$(\".upBut\").prop(\"disabled\", true);
\t\telse \$(\".upBut\").prop(\"disabled\", false);
\t});

</script>

{% endblock %}", "persos.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\persos.html.twig");
    }
}
