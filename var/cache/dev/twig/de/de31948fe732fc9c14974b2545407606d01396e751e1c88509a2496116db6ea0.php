<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_a79edb70d9f2980463380c5e39ce6bc21a352c15e3338d734fcb4c78d1e65b3c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_46ad00e386f58ecbdcc66c24ed9563101bef327d4487b84c1ceed34c685fee5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_46ad00e386f58ecbdcc66c24ed9563101bef327d4487b84c1ceed34c685fee5d->enter($__internal_46ad00e386f58ecbdcc66c24ed9563101bef327d4487b84c1ceed34c685fee5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_80cdf34e12d9b929532609868617c9e0937738174e4fdf29b40d6a4fdd10c85c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80cdf34e12d9b929532609868617c9e0937738174e4fdf29b40d6a4fdd10c85c->enter($__internal_80cdf34e12d9b929532609868617c9e0937738174e4fdf29b40d6a4fdd10c85c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_46ad00e386f58ecbdcc66c24ed9563101bef327d4487b84c1ceed34c685fee5d->leave($__internal_46ad00e386f58ecbdcc66c24ed9563101bef327d4487b84c1ceed34c685fee5d_prof);

        
        $__internal_80cdf34e12d9b929532609868617c9e0937738174e4fdf29b40d6a4fdd10c85c->leave($__internal_80cdf34e12d9b929532609868617c9e0937738174e4fdf29b40d6a4fdd10c85c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
