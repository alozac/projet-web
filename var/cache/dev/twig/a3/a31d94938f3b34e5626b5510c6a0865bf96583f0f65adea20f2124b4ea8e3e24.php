<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_57455bc33aecfac6544ea92ce7609362e910188355485e171a00baa0113b4cdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ada4af481c28835d2081c9caf395cec543047632092365960cfe8139cf8cea62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ada4af481c28835d2081c9caf395cec543047632092365960cfe8139cf8cea62->enter($__internal_ada4af481c28835d2081c9caf395cec543047632092365960cfe8139cf8cea62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_46b69e95c3374046549b0dec42a19e6c1e73cad706a268a6412900d498a67631 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_46b69e95c3374046549b0dec42a19e6c1e73cad706a268a6412900d498a67631->enter($__internal_46b69e95c3374046549b0dec42a19e6c1e73cad706a268a6412900d498a67631_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ada4af481c28835d2081c9caf395cec543047632092365960cfe8139cf8cea62->leave($__internal_ada4af481c28835d2081c9caf395cec543047632092365960cfe8139cf8cea62_prof);

        
        $__internal_46b69e95c3374046549b0dec42a19e6c1e73cad706a268a6412900d498a67631->leave($__internal_46b69e95c3374046549b0dec42a19e6c1e73cad706a268a6412900d498a67631_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_1bebb435965258603fc22713312761b6569e239cb3f93326eea37dad023b8ec3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bebb435965258603fc22713312761b6569e239cb3f93326eea37dad023b8ec3->enter($__internal_1bebb435965258603fc22713312761b6569e239cb3f93326eea37dad023b8ec3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_7c90245c4d103177266b016acbce4ad77a5f1b3d7d5271f40fc39338beaec89e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c90245c4d103177266b016acbce4ad77a5f1b3d7d5271f40fc39338beaec89e->enter($__internal_7c90245c4d103177266b016acbce4ad77a5f1b3d7d5271f40fc39338beaec89e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_7c90245c4d103177266b016acbce4ad77a5f1b3d7d5271f40fc39338beaec89e->leave($__internal_7c90245c4d103177266b016acbce4ad77a5f1b3d7d5271f40fc39338beaec89e_prof);

        
        $__internal_1bebb435965258603fc22713312761b6569e239cb3f93326eea37dad023b8ec3->leave($__internal_1bebb435965258603fc22713312761b6569e239cb3f93326eea37dad023b8ec3_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_9cd944a5c1b7e1e21847c0a5aaf8ef1a771c11cfe03e767c34dd9044be3c5144 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9cd944a5c1b7e1e21847c0a5aaf8ef1a771c11cfe03e767c34dd9044be3c5144->enter($__internal_9cd944a5c1b7e1e21847c0a5aaf8ef1a771c11cfe03e767c34dd9044be3c5144_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_078787456f80934093918c2176d9c7783fbee7e4bca7584000c3153c2e69f85f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_078787456f80934093918c2176d9c7783fbee7e4bca7584000c3153c2e69f85f->enter($__internal_078787456f80934093918c2176d9c7783fbee7e4bca7584000c3153c2e69f85f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_078787456f80934093918c2176d9c7783fbee7e4bca7584000c3153c2e69f85f->leave($__internal_078787456f80934093918c2176d9c7783fbee7e4bca7584000c3153c2e69f85f_prof);

        
        $__internal_9cd944a5c1b7e1e21847c0a5aaf8ef1a771c11cfe03e767c34dd9044be3c5144->leave($__internal_9cd944a5c1b7e1e21847c0a5aaf8ef1a771c11cfe03e767c34dd9044be3c5144_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0926c210b3e3e7bd672d5923cd27d8959f2fb00ebc4fb58dad6ebba00c803fd8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0926c210b3e3e7bd672d5923cd27d8959f2fb00ebc4fb58dad6ebba00c803fd8->enter($__internal_0926c210b3e3e7bd672d5923cd27d8959f2fb00ebc4fb58dad6ebba00c803fd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c169b0dc0e0a6d7c2f5f6841704ded93418b97e38196ec28a7ee7c6a7d58a53c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c169b0dc0e0a6d7c2f5f6841704ded93418b97e38196ec28a7ee7c6a7d58a53c->enter($__internal_c169b0dc0e0a6d7c2f5f6841704ded93418b97e38196ec28a7ee7c6a7d58a53c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_c169b0dc0e0a6d7c2f5f6841704ded93418b97e38196ec28a7ee7c6a7d58a53c->leave($__internal_c169b0dc0e0a6d7c2f5f6841704ded93418b97e38196ec28a7ee7c6a7d58a53c_prof);

        
        $__internal_0926c210b3e3e7bd672d5923cd27d8959f2fb00ebc4fb58dad6ebba00c803fd8->leave($__internal_0926c210b3e3e7bd672d5923cd27d8959f2fb00ebc4fb58dad6ebba00c803fd8_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\wamp64\\www\\projet-web\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
