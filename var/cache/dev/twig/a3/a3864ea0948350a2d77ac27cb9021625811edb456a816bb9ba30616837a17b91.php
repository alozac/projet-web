<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_2306b6cd3134615afe006532fa4a7d4af130e420a8cc45a2cb3f38eb643a9fa3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d18f7d6c08dc687d7bb738bf0639be008fd1662cd7a0ece7b65cb99639cc566f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d18f7d6c08dc687d7bb738bf0639be008fd1662cd7a0ece7b65cb99639cc566f->enter($__internal_d18f7d6c08dc687d7bb738bf0639be008fd1662cd7a0ece7b65cb99639cc566f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_4a2f31acf62c1f263c85cfad7b5a87b57b0db60510fa1c63a6adf65fc10f40ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a2f31acf62c1f263c85cfad7b5a87b57b0db60510fa1c63a6adf65fc10f40ac->enter($__internal_4a2f31acf62c1f263c85cfad7b5a87b57b0db60510fa1c63a6adf65fc10f40ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => ($context["status_code"] ?? $this->getContext($context, "status_code")), "message" => ($context["status_text"] ?? $this->getContext($context, "status_text")), "exception" => $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_d18f7d6c08dc687d7bb738bf0639be008fd1662cd7a0ece7b65cb99639cc566f->leave($__internal_d18f7d6c08dc687d7bb738bf0639be008fd1662cd7a0ece7b65cb99639cc566f_prof);

        
        $__internal_4a2f31acf62c1f263c85cfad7b5a87b57b0db60510fa1c63a6adf65fc10f40ac->leave($__internal_4a2f31acf62c1f263c85cfad7b5a87b57b0db60510fa1c63a6adf65fc10f40ac_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
