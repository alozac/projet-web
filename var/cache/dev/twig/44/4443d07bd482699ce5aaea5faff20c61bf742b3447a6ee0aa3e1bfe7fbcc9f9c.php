<?php

/* post/post_consult.html.twig */
class __TwigTemplate_44e7180f0e5daed66bb2ce2a2d554bc9b4fd3a98d09dda9fbcfbde9a6ff3b772 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "post/post_consult.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b805083c88d37f10f424b1cad18f33c36191c474b7ffccd5dbcf74fc00c227e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b805083c88d37f10f424b1cad18f33c36191c474b7ffccd5dbcf74fc00c227e4->enter($__internal_b805083c88d37f10f424b1cad18f33c36191c474b7ffccd5dbcf74fc00c227e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_consult.html.twig"));

        $__internal_da0aaa6f6a0db7f6eeb8d51a6559d6e412ba9357e0ea29f3b7a85a64e0fb8217 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da0aaa6f6a0db7f6eeb8d51a6559d6e412ba9357e0ea29f3b7a85a64e0fb8217->enter($__internal_da0aaa6f6a0db7f6eeb8d51a6559d6e412ba9357e0ea29f3b7a85a64e0fb8217_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_consult.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b805083c88d37f10f424b1cad18f33c36191c474b7ffccd5dbcf74fc00c227e4->leave($__internal_b805083c88d37f10f424b1cad18f33c36191c474b7ffccd5dbcf74fc00c227e4_prof);

        
        $__internal_da0aaa6f6a0db7f6eeb8d51a6559d6e412ba9357e0ea29f3b7a85a64e0fb8217->leave($__internal_da0aaa6f6a0db7f6eeb8d51a6559d6e412ba9357e0ea29f3b7a85a64e0fb8217_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_5438b7e02388875e7871517b1e0a50c56b8c3673fc73b115db8d3c377283615b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5438b7e02388875e7871517b1e0a50c56b8c3673fc73b115db8d3c377283615b->enter($__internal_5438b7e02388875e7871517b1e0a50c56b8c3673fc73b115db8d3c377283615b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_9659a1d0ab955044e3a34daba5298337b6877cb93a8e0ea496b5e14b8ef24191 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9659a1d0ab955044e3a34daba5298337b6877cb93a8e0ea496b5e14b8ef24191->enter($__internal_9659a1d0ab955044e3a34daba5298337b6877cb93a8e0ea496b5e14b8ef24191_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_consult.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_9659a1d0ab955044e3a34daba5298337b6877cb93a8e0ea496b5e14b8ef24191->leave($__internal_9659a1d0ab955044e3a34daba5298337b6877cb93a8e0ea496b5e14b8ef24191_prof);

        
        $__internal_5438b7e02388875e7871517b1e0a50c56b8c3673fc73b115db8d3c377283615b->leave($__internal_5438b7e02388875e7871517b1e0a50c56b8c3673fc73b115db8d3c377283615b_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_00a8b7ea95a2c9beab7fdd356c9e070a43dae1514bece545be5e00deb30c24be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00a8b7ea95a2c9beab7fdd356c9e070a43dae1514bece545be5e00deb30c24be->enter($__internal_00a8b7ea95a2c9beab7fdd356c9e070a43dae1514bece545be5e00deb30c24be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_1c141b99957d0bd53542ce13e0ea2e12f3759689dbc32a76a688ddd9b4717927 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1c141b99957d0bd53542ce13e0ea2e12f3759689dbc32a76a688ddd9b4717927->enter($__internal_1c141b99957d0bd53542ce13e0ea2e12f3759689dbc32a76a688ddd9b4717927_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h3>Messagerie</h3>
</div>


\t<div class=\"row\">
\t\t<div class=\"list_mess  col-sm-6\">
\t\t\t<ul id=\"mess_rec_pill\" class=\"nav nav-pills\">
\t\t  \t\t<li class=\"active mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_rec\">Recus</a>
\t\t  \t\t</li>
\t\t  \t\t<li id=\"mess_sent_pill\" class=\"mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_sent\">Envoyés</a>
\t\t  \t\t</li>
\t\t\t</ul>
\t\t\t
\t\t\t<div class=\"tab-content\">
\t\t\t\t<div id=\"list_mess_rec\" class=\"tab-pane fade in active\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Expéditeur</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    ";
        // line 37
        if ( !twig_test_empty(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")))) {
            // line 38
            echo "\t\t\t\t\t     ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["messagesRec"] ?? $this->getContext($context, "messagesRec"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 39
                echo "\t\t\t\t\t\t\t<tr id=\"mess_rec-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"message_rec link active\">
\t\t\t\t\t\t\t\t<td>";
                // line 40
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "sender", array()), "name", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 41
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "object", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 42
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "date", array()), "d/m/Y à H:i"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 45
            echo "\t\t\t\t\t\t";
        }
        // line 46
        echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t\t<div id=\"list_mess_sent\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Destinataire</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    ";
        // line 59
        if ( !twig_test_empty(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")))) {
            // line 60
            echo "\t\t\t\t\t     ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["messagesSent"] ?? $this->getContext($context, "messagesSent"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 61
                echo "\t\t\t\t\t     ";
                $context["toAll"] = (($this->getAttribute($this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "receiver", array()), "name", array()) == $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "lastPension", array()), "name", array())) && $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN"));
                // line 63
                echo "\t\t\t\t\t\t\t<tr id=\"mess_sent-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"message_sent link active\">
\t\t\t\t\t\t\t\t";
                // line 64
                if (($context["toAll"] ?? $this->getContext($context, "toAll"))) {
                    // line 65
                    echo "\t\t\t\t\t\t\t\t<td>Toutes les pensions</td>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 67
                    echo "\t\t\t\t\t\t\t\t<td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "receiver", array()), "name", array()), "html", null, true);
                    echo "</td>
\t\t\t\t\t\t\t\t";
                }
                // line 69
                echo "\t\t\t\t\t\t\t\t<td>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "object", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 70
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "date", array()), "d/m/Y à H:i"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "\t\t\t\t\t\t";
        }
        // line 74
        echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"mess_view\" class=\"col-sm-6\">
\t\t\t<p>Sélectionnez un message à lire</p>
\t\t</div>
\t</div>

\t<a href=\"";
        // line 85
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_write", array("id" => 0));
        echo "\" role=\"button\" id=\"answer\" class=\"chgBut btn btn-info btn-sm disabled\">Répondre</a>
\t<a href=\"";
        // line 86
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_remove", array("id" => 0));
        echo "\" role=\"button\" id=\"remove\" class=\"chgBut btn btn-info btn-sm disabled\">Supprimer</a>
\t<a href=\"";
        // line 87
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_write");
        echo "\" role=\"button\" class=\"btn btn-info btn-sm\">Nouveau</a>

\t<script>

 \tvar messagesRec  = ";
        // line 91
        echo twig_jsonencode_filter(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")));
        echo ";
  \tvar messagesSent = ";
        // line 92
        echo twig_jsonencode_filter(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")));
        echo ";

\t\$(\".message_rec\").click(function(){
\t\t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t 
\t\tvar newMess = messagesRec[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefAns = \$(\"#answer\").attr(\"href\");
\t\t\$(\"#answer\").attr(\"href\", hrefAns.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".message_sent\").click(function(){
\t \t\$(\".message_sent\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t\t
\t\tvar newMess = messagesSent[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".mess_pill\").click(function(){
\t\t\$(\".chgBut\").addClass(\"disabled\");
\t \t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(\".message_sent\").removeClass(\"success\");
\t});
</script>
";
        
        $__internal_1c141b99957d0bd53542ce13e0ea2e12f3759689dbc32a76a688ddd9b4717927->leave($__internal_1c141b99957d0bd53542ce13e0ea2e12f3759689dbc32a76a688ddd9b4717927_prof);

        
        $__internal_00a8b7ea95a2c9beab7fdd356c9e070a43dae1514bece545be5e00deb30c24be->leave($__internal_00a8b7ea95a2c9beab7fdd356c9e070a43dae1514bece545be5e00deb30c24be_prof);

    }

    public function getTemplateName()
    {
        return "post/post_consult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  229 => 92,  225 => 91,  218 => 87,  214 => 86,  210 => 85,  197 => 74,  194 => 73,  185 => 70,  180 => 69,  174 => 67,  170 => 65,  168 => 64,  163 => 63,  160 => 61,  155 => 60,  153 => 59,  138 => 46,  135 => 45,  126 => 42,  122 => 41,  118 => 40,  113 => 39,  108 => 38,  106 => 37,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/post_consult.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Messagerie</h3>
</div>


\t<div class=\"row\">
\t\t<div class=\"list_mess  col-sm-6\">
\t\t\t<ul id=\"mess_rec_pill\" class=\"nav nav-pills\">
\t\t  \t\t<li class=\"active mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_rec\">Recus</a>
\t\t  \t\t</li>
\t\t  \t\t<li id=\"mess_sent_pill\" class=\"mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_sent\">Envoyés</a>
\t\t  \t\t</li>
\t\t\t</ul>
\t\t\t
\t\t\t<div class=\"tab-content\">
\t\t\t\t<div id=\"list_mess_rec\" class=\"tab-pane fade in active\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Expéditeur</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    {% if messagesRec is not empty %}
\t\t\t\t\t     {% for i in 0..messagesRec|length -1 %}
\t\t\t\t\t\t\t<tr id=\"mess_rec-{{ i }}\" class=\"message_rec link active\">
\t\t\t\t\t\t\t\t<td>{{ messagesRec[i].sender.name }}</td>
\t\t\t\t\t\t\t\t<td>{{ messagesRec[i].object }}</td>
\t\t\t\t\t\t\t\t<td>{{ messagesRec[i].date|date(\"d/m/Y à H:i\") }}</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t\t<div id=\"list_mess_sent\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Destinataire</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    {% if messagesSent is not empty %}
\t\t\t\t\t     {% for i in 0..messagesSent|length -1 %}
\t\t\t\t\t     {% set toAll = messagesSent[i].receiver.name == app.user.lastPension.name
\t\t\t\t\t     \t\t\t\t and is_granted('ROLE_ADMIN') %}
\t\t\t\t\t\t\t<tr id=\"mess_sent-{{ i }}\" class=\"message_sent link active\">
\t\t\t\t\t\t\t\t{% if toAll %}
\t\t\t\t\t\t\t\t<td>Toutes les pensions</td>
\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t<td>{{ messagesSent[i].receiver.name }}</td>
\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t<td>{{ messagesSent[i].object }}</td>
\t\t\t\t\t\t\t\t<td>{{ messagesSent[i].date|date(\"d/m/Y à H:i\") }}</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"mess_view\" class=\"col-sm-6\">
\t\t\t<p>Sélectionnez un message à lire</p>
\t\t</div>
\t</div>

\t<a href=\"{{ path('post_write',  { 'id' : 0 }) }}\" role=\"button\" id=\"answer\" class=\"chgBut btn btn-info btn-sm disabled\">Répondre</a>
\t<a href=\"{{ path('post_remove', { 'id' : 0 }) }}\" role=\"button\" id=\"remove\" class=\"chgBut btn btn-info btn-sm disabled\">Supprimer</a>
\t<a href=\"{{ path('post_write') }}\" role=\"button\" class=\"btn btn-info btn-sm\">Nouveau</a>

\t<script>

 \tvar messagesRec  = {{ messagesRec|json_encode|raw }};
  \tvar messagesSent = {{ messagesSent|json_encode|raw }};

\t\$(\".message_rec\").click(function(){
\t\t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t 
\t\tvar newMess = messagesRec[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefAns = \$(\"#answer\").attr(\"href\");
\t\t\$(\"#answer\").attr(\"href\", hrefAns.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".message_sent\").click(function(){
\t \t\$(\".message_sent\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t\t
\t\tvar newMess = messagesSent[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".mess_pill\").click(function(){
\t\t\$(\".chgBut\").addClass(\"disabled\");
\t \t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(\".message_sent\").removeClass(\"success\");
\t});
</script>
{% endblock %}", "post/post_consult.html.twig", "/var/www/projet-web/app/Resources/views/post/post_consult.html.twig");
    }
}
