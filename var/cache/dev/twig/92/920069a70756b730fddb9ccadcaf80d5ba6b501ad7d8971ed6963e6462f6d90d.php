<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_1dc24dabedba78fe92a2c5900a99430792c008ba777cc5401563b246ed0149f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f7326029949df4b516c3bd93ef51e520a0d43ec719dedb4d6aaa1f638ca89fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f7326029949df4b516c3bd93ef51e520a0d43ec719dedb4d6aaa1f638ca89fb->enter($__internal_8f7326029949df4b516c3bd93ef51e520a0d43ec719dedb4d6aaa1f638ca89fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_9045a75642534c73f4186ab4fadceed676bb6bcfd7b26d5570641713f6d3dd46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9045a75642534c73f4186ab4fadceed676bb6bcfd7b26d5570641713f6d3dd46->enter($__internal_9045a75642534c73f4186ab4fadceed676bb6bcfd7b26d5570641713f6d3dd46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_8f7326029949df4b516c3bd93ef51e520a0d43ec719dedb4d6aaa1f638ca89fb->leave($__internal_8f7326029949df4b516c3bd93ef51e520a0d43ec719dedb4d6aaa1f638ca89fb_prof);

        
        $__internal_9045a75642534c73f4186ab4fadceed676bb6bcfd7b26d5570641713f6d3dd46->leave($__internal_9045a75642534c73f4186ab4fadceed676bb6bcfd7b26d5570641713f6d3dd46_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_5697449308607c67c86a0f681c535477dd0001d9bbd00538a4bca1c09b015884 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5697449308607c67c86a0f681c535477dd0001d9bbd00538a4bca1c09b015884->enter($__internal_5697449308607c67c86a0f681c535477dd0001d9bbd00538a4bca1c09b015884_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_86731146d56de2c748067268130f3e73a75ac015e7a5ea1f176c04207d55aaa7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86731146d56de2c748067268130f3e73a75ac015e7a5ea1f176c04207d55aaa7->enter($__internal_86731146d56de2c748067268130f3e73a75ac015e7a5ea1f176c04207d55aaa7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_86731146d56de2c748067268130f3e73a75ac015e7a5ea1f176c04207d55aaa7->leave($__internal_86731146d56de2c748067268130f3e73a75ac015e7a5ea1f176c04207d55aaa7_prof);

        
        $__internal_5697449308607c67c86a0f681c535477dd0001d9bbd00538a4bca1c09b015884->leave($__internal_5697449308607c67c86a0f681c535477dd0001d9bbd00538a4bca1c09b015884_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
