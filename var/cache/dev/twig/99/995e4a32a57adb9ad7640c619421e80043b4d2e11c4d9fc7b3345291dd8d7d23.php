<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_042f9d521d9aaa435ed89e2ede737a06e10959b040bc487f0bb605490acce6d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1991ec7eb88581c98055250d61aefd124579f4023cdfdc2e8ff7c5676b377736 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1991ec7eb88581c98055250d61aefd124579f4023cdfdc2e8ff7c5676b377736->enter($__internal_1991ec7eb88581c98055250d61aefd124579f4023cdfdc2e8ff7c5676b377736_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_2d0dc9d847fc798f0e0af74b1f900309ad69084cffa7938f6ef174f8493e9bda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d0dc9d847fc798f0e0af74b1f900309ad69084cffa7938f6ef174f8493e9bda->enter($__internal_2d0dc9d847fc798f0e0af74b1f900309ad69084cffa7938f6ef174f8493e9bda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_1991ec7eb88581c98055250d61aefd124579f4023cdfdc2e8ff7c5676b377736->leave($__internal_1991ec7eb88581c98055250d61aefd124579f4023cdfdc2e8ff7c5676b377736_prof);

        
        $__internal_2d0dc9d847fc798f0e0af74b1f900309ad69084cffa7938f6ef174f8493e9bda->leave($__internal_2d0dc9d847fc798f0e0af74b1f900309ad69084cffa7938f6ef174f8493e9bda_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
