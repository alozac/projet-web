<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_285e9b6a9ae8d4f4f3486d8dae3627dfe3bf29b84536ec47742fc5c0c34ea928 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a67e94c12481bade2e37f20ba9ed09aaf18938d61773f610b4df78ebb6b48108 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a67e94c12481bade2e37f20ba9ed09aaf18938d61773f610b4df78ebb6b48108->enter($__internal_a67e94c12481bade2e37f20ba9ed09aaf18938d61773f610b4df78ebb6b48108_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_a00b10057d6f276d79ee03c4d3dd8af4c98b94789dbae83bffea45ee005ad0db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a00b10057d6f276d79ee03c4d3dd8af4c98b94789dbae83bffea45ee005ad0db->enter($__internal_a00b10057d6f276d79ee03c4d3dd8af4c98b94789dbae83bffea45ee005ad0db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter(($context["token"] ?? $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a67e94c12481bade2e37f20ba9ed09aaf18938d61773f610b4df78ebb6b48108->leave($__internal_a67e94c12481bade2e37f20ba9ed09aaf18938d61773f610b4df78ebb6b48108_prof);

        
        $__internal_a00b10057d6f276d79ee03c4d3dd8af4c98b94789dbae83bffea45ee005ad0db->leave($__internal_a00b10057d6f276d79ee03c4d3dd8af4c98b94789dbae83bffea45ee005ad0db_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_9d91708f2847dafbb4654d496102d82df01ef215a879846fe276b415c2c3a346 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d91708f2847dafbb4654d496102d82df01ef215a879846fe276b415c2c3a346->enter($__internal_9d91708f2847dafbb4654d496102d82df01ef215a879846fe276b415c2c3a346_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_e93e291797a523e6ec040a45e5d7af1b0b5511bea9d90ec6fedbe065ef6806e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e93e291797a523e6ec040a45e5d7af1b0b5511bea9d90ec6fedbe065ef6806e8->enter($__internal_e93e291797a523e6ec040a45e5d7af1b0b5511bea9d90ec6fedbe065ef6806e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_e93e291797a523e6ec040a45e5d7af1b0b5511bea9d90ec6fedbe065ef6806e8->leave($__internal_e93e291797a523e6ec040a45e5d7af1b0b5511bea9d90ec6fedbe065ef6806e8_prof);

        
        $__internal_9d91708f2847dafbb4654d496102d82df01ef215a879846fe276b415c2c3a346->leave($__internal_9d91708f2847dafbb4654d496102d82df01ef215a879846fe276b415c2c3a346_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_6b88a253bdeb003539b0ad1e4c119c1bbc4dcb4b033668199eb8af165f8a680f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b88a253bdeb003539b0ad1e4c119c1bbc4dcb4b033668199eb8af165f8a680f->enter($__internal_6b88a253bdeb003539b0ad1e4c119c1bbc4dcb4b033668199eb8af165f8a680f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_be7bf687a8e760afeb8938eb13ef5a1a15f6bdd6031d9d9e1032bc757eefd404 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be7bf687a8e760afeb8938eb13ef5a1a15f6bdd6031d9d9e1032bc757eefd404->enter($__internal_be7bf687a8e760afeb8938eb13ef5a1a15f6bdd6031d9d9e1032bc757eefd404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messages"] ?? $this->getContext($context, "messages")), ($context["about"] ?? $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_be7bf687a8e760afeb8938eb13ef5a1a15f6bdd6031d9d9e1032bc757eefd404->leave($__internal_be7bf687a8e760afeb8938eb13ef5a1a15f6bdd6031d9d9e1032bc757eefd404_prof);

        
        $__internal_6b88a253bdeb003539b0ad1e4c119c1bbc4dcb4b033668199eb8af165f8a680f->leave($__internal_6b88a253bdeb003539b0ad1e4c119c1bbc4dcb4b033668199eb8af165f8a680f_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 36,  84 => 35,  75 => 34,  61 => 29,  55 => 27,  46 => 26,  36 => 1,  34 => 12,  33 => 11,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'purge' : {
        status:  'success',
        title:   'The profiler database was purged successfully',
        message: 'Now you need to browse some pages with the Symfony Profiler enabled to collect data.'
    },
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    },
    'upload_error' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'No file given or the file was not uploaded successfully.'
    },
    'already_exists' : {
        status:  'error',
        title:   'A problem occurred when uploading the data',
        message: 'The token already exists in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
