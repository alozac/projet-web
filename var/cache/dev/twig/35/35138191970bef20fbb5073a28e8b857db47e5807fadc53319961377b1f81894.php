<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_d29aebd12c035a31cbf2ab55f99d88da7f375f76f62295319bb3c15e223025f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4929de79e7e1aa4f20f399f1ad0073f12e6432225715b1830faf209a99d13859 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4929de79e7e1aa4f20f399f1ad0073f12e6432225715b1830faf209a99d13859->enter($__internal_4929de79e7e1aa4f20f399f1ad0073f12e6432225715b1830faf209a99d13859_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_7bd15aa59cf118c568e675e3bed4b207354ca5be14283791ad1d555510c6c572 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7bd15aa59cf118c568e675e3bed4b207354ca5be14283791ad1d555510c6c572->enter($__internal_7bd15aa59cf118c568e675e3bed4b207354ca5be14283791ad1d555510c6c572_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_4929de79e7e1aa4f20f399f1ad0073f12e6432225715b1830faf209a99d13859->leave($__internal_4929de79e7e1aa4f20f399f1ad0073f12e6432225715b1830faf209a99d13859_prof);

        
        $__internal_7bd15aa59cf118c568e675e3bed4b207354ca5be14283791ad1d555510c6c572->leave($__internal_7bd15aa59cf118c568e675e3bed4b207354ca5be14283791ad1d555510c6c572_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}
