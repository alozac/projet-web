<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_5820a311979741ad663c066c1b542ca5f41abdf4e1811da178862af887b4aeef extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_08d550263f768fe3f0be89d73ed76d3662b46e667774e136c4eeed3b3be3f41d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08d550263f768fe3f0be89d73ed76d3662b46e667774e136c4eeed3b3be3f41d->enter($__internal_08d550263f768fe3f0be89d73ed76d3662b46e667774e136c4eeed3b3be3f41d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_a5b8e534bd79e9b65a5f2279441072ba81ff39d832a60f6096c2ea213e462436 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5b8e534bd79e9b65a5f2279441072ba81ff39d832a60f6096c2ea213e462436->enter($__internal_a5b8e534bd79e9b65a5f2279441072ba81ff39d832a60f6096c2ea213e462436_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_08d550263f768fe3f0be89d73ed76d3662b46e667774e136c4eeed3b3be3f41d->leave($__internal_08d550263f768fe3f0be89d73ed76d3662b46e667774e136c4eeed3b3be3f41d_prof);

        
        $__internal_a5b8e534bd79e9b65a5f2279441072ba81ff39d832a60f6096c2ea213e462436->leave($__internal_a5b8e534bd79e9b65a5f2279441072ba81ff39d832a60f6096c2ea213e462436_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
