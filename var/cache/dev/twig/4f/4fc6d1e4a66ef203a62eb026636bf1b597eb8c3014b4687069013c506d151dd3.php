<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_18976dec639552ae196e4c6e02b171dd9a709e461c7da7d7270a7c23ddca9e59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_adcc20dcaf79e9f3819c58820434b02de6725fc74cb4d7a162a42354066e77ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_adcc20dcaf79e9f3819c58820434b02de6725fc74cb4d7a162a42354066e77ce->enter($__internal_adcc20dcaf79e9f3819c58820434b02de6725fc74cb4d7a162a42354066e77ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_08369bfaaf34fd1c8d78eebc327e52df891935120ff4f2bb96cb17230b29e906 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_08369bfaaf34fd1c8d78eebc327e52df891935120ff4f2bb96cb17230b29e906->enter($__internal_08369bfaaf34fd1c8d78eebc327e52df891935120ff4f2bb96cb17230b29e906_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_adcc20dcaf79e9f3819c58820434b02de6725fc74cb4d7a162a42354066e77ce->leave($__internal_adcc20dcaf79e9f3819c58820434b02de6725fc74cb4d7a162a42354066e77ce_prof);

        
        $__internal_08369bfaaf34fd1c8d78eebc327e52df891935120ff4f2bb96cb17230b29e906->leave($__internal_08369bfaaf34fd1c8d78eebc327e52df891935120ff4f2bb96cb17230b29e906_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
