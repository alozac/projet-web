<?php

/* post/post_write.html.twig */
class __TwigTemplate_0a786896db649b307b4c76a4918ee83a3ffc8c667e2ab274de35cc1cfe6a9b1b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "post/post_write.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f518c0a61fb77ae0af7a9c56ef82f73355680f5ddd5e42f100a245825ca4146 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f518c0a61fb77ae0af7a9c56ef82f73355680f5ddd5e42f100a245825ca4146->enter($__internal_7f518c0a61fb77ae0af7a9c56ef82f73355680f5ddd5e42f100a245825ca4146_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_write.html.twig"));

        $__internal_0877c2e4a79e8bd7f75eb755d63e3c94757cb67f4944b30410544ecc3e6ccfce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0877c2e4a79e8bd7f75eb755d63e3c94757cb67f4944b30410544ecc3e6ccfce->enter($__internal_0877c2e4a79e8bd7f75eb755d63e3c94757cb67f4944b30410544ecc3e6ccfce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_write.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7f518c0a61fb77ae0af7a9c56ef82f73355680f5ddd5e42f100a245825ca4146->leave($__internal_7f518c0a61fb77ae0af7a9c56ef82f73355680f5ddd5e42f100a245825ca4146_prof);

        
        $__internal_0877c2e4a79e8bd7f75eb755d63e3c94757cb67f4944b30410544ecc3e6ccfce->leave($__internal_0877c2e4a79e8bd7f75eb755d63e3c94757cb67f4944b30410544ecc3e6ccfce_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_cacf31ea36e93e727cc984463b68b789dec386477427458340054edcb64be6fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cacf31ea36e93e727cc984463b68b789dec386477427458340054edcb64be6fb->enter($__internal_cacf31ea36e93e727cc984463b68b789dec386477427458340054edcb64be6fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_ca81978f8971422d59fdad3c1faca1a8c4317b0b7eb337b2083c50f6656e1079 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ca81978f8971422d59fdad3c1faca1a8c4317b0b7eb337b2083c50f6656e1079->enter($__internal_ca81978f8971422d59fdad3c1faca1a8c4317b0b7eb337b2083c50f6656e1079_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_new.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_ca81978f8971422d59fdad3c1faca1a8c4317b0b7eb337b2083c50f6656e1079->leave($__internal_ca81978f8971422d59fdad3c1faca1a8c4317b0b7eb337b2083c50f6656e1079_prof);

        
        $__internal_cacf31ea36e93e727cc984463b68b789dec386477427458340054edcb64be6fb->leave($__internal_cacf31ea36e93e727cc984463b68b789dec386477427458340054edcb64be6fb_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_9934ff5911febf2704c203169d78b1c1bc5ab120b5f057ea17ba117434b1b20b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9934ff5911febf2704c203169d78b1c1bc5ab120b5f057ea17ba117434b1b20b->enter($__internal_9934ff5911febf2704c203169d78b1c1bc5ab120b5f057ea17ba117434b1b20b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_3bd9f57f0b3e04dd879a567afaa131996680f06be315da3aa86dcc4990d63dad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bd9f57f0b3e04dd879a567afaa131996680f06be315da3aa86dcc4990d63dad->enter($__internal_3bd9f57f0b3e04dd879a567afaa131996680f06be315da3aa86dcc4990d63dad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h3>Nouveau message</h3>
</div>

\t";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_start');
        echo "
\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "receiver", array()), 'row');
        echo "
\t";
        // line 16
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 17
            echo "\t\t";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "toAllPensions", array()), 'row');
            echo "\t";
        }
        // line 18
        echo "\t";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "object", array()), 'row');
        echo "
\t";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "message", array()), 'row');
        echo "
\t";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "send", array()), 'row', array("attr" => array("class" => "btn-primary")));
        echo "
\t";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "_token", array()), 'row');
        echo "
\t";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_end', array("render_rest" => false));
        echo "
<script>
\t\$(\"#message_toAllPensions\").click(function(){
\t\tif (\$(this).is(\":checked\"))
\t\t\t\$(\"#message_receiver\").prop('disabled', true);
\t\telse 
\t\t\t\$(\"#message_receiver\").prop('disabled', false);
\t});
</script>
";
        
        $__internal_3bd9f57f0b3e04dd879a567afaa131996680f06be315da3aa86dcc4990d63dad->leave($__internal_3bd9f57f0b3e04dd879a567afaa131996680f06be315da3aa86dcc4990d63dad_prof);

        
        $__internal_9934ff5911febf2704c203169d78b1c1bc5ab120b5f057ea17ba117434b1b20b->leave($__internal_9934ff5911febf2704c203169d78b1c1bc5ab120b5f057ea17ba117434b1b20b_prof);

    }

    public function getTemplateName()
    {
        return "post/post_write.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 22,  111 => 21,  107 => 20,  103 => 19,  98 => 18,  93 => 17,  91 => 16,  87 => 15,  83 => 14,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/post_new.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Nouveau message</h3>
</div>

\t{{ form_start(formNew) }}
\t{{ form_row(formNew.receiver)  }}
\t{% if is_granted('ROLE_ADMIN') %}
\t\t{{ form_row(formNew.toAllPensions) }}\t{% endif %}
\t{{ form_row(formNew.object) }}
\t{{ form_row(formNew.message) }}
\t{{ form_row(formNew.send, { 'attr': {'class': 'btn-primary'} }) }}
\t{{ form_row(formNew._token) }}
\t{{ form_end(formNew, {'render_rest': false}) }}
<script>
\t\$(\"#message_toAllPensions\").click(function(){
\t\tif (\$(this).is(\":checked\"))
\t\t\t\$(\"#message_receiver\").prop('disabled', true);
\t\telse 
\t\t\t\$(\"#message_receiver\").prop('disabled', false);
\t});
</script>
{% endblock %}", "post/post_write.html.twig", "/var/www/projet-web/app/Resources/views/post/post_write.html.twig");
    }
}
