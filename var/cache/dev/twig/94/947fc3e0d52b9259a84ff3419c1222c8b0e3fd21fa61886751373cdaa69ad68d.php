<?php

/* ranking.html.twig */
class __TwigTemplate_05b44152bdecc140c871df852329c4470442a67b8ee020cb135ea841a9346eb3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "ranking.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c0fceb90af05debf05bb4bd39659061968fcee4a0354f5eb9c475e2dc58a606 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c0fceb90af05debf05bb4bd39659061968fcee4a0354f5eb9c475e2dc58a606->enter($__internal_1c0fceb90af05debf05bb4bd39659061968fcee4a0354f5eb9c475e2dc58a606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ranking.html.twig"));

        $__internal_1a8ac2891f9b76a8a438ab8e9108cedf2e46ab706eff965569c7c7dd8f5d0b44 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a8ac2891f9b76a8a438ab8e9108cedf2e46ab706eff965569c7c7dd8f5d0b44->enter($__internal_1a8ac2891f9b76a8a438ab8e9108cedf2e46ab706eff965569c7c7dd8f5d0b44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ranking.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1c0fceb90af05debf05bb4bd39659061968fcee4a0354f5eb9c475e2dc58a606->leave($__internal_1c0fceb90af05debf05bb4bd39659061968fcee4a0354f5eb9c475e2dc58a606_prof);

        
        $__internal_1a8ac2891f9b76a8a438ab8e9108cedf2e46ab706eff965569c7c7dd8f5d0b44->leave($__internal_1a8ac2891f9b76a8a438ab8e9108cedf2e46ab706eff965569c7c7dd8f5d0b44_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0219b1db053dba6d152bacabfc99401cf441f505bfa1757bbb042f9f67e6a4ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0219b1db053dba6d152bacabfc99401cf441f505bfa1757bbb042f9f67e6a4ae->enter($__internal_0219b1db053dba6d152bacabfc99401cf441f505bfa1757bbb042f9f67e6a4ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_efd5c8767f8f7a352a1d36023a8ee0b7faca96cb3d53b05c5f631f094c4dc943 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efd5c8767f8f7a352a1d36023a8ee0b7faca96cb3d53b05c5f631f094c4dc943->enter($__internal_efd5c8767f8f7a352a1d36023a8ee0b7faca96cb3d53b05c5f631f094c4dc943_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_consult.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_efd5c8767f8f7a352a1d36023a8ee0b7faca96cb3d53b05c5f631f094c4dc943->leave($__internal_efd5c8767f8f7a352a1d36023a8ee0b7faca96cb3d53b05c5f631f094c4dc943_prof);

        
        $__internal_0219b1db053dba6d152bacabfc99401cf441f505bfa1757bbb042f9f67e6a4ae->leave($__internal_0219b1db053dba6d152bacabfc99401cf441f505bfa1757bbb042f9f67e6a4ae_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_ba177a0f9e181da88aa7f0556bf57d4ce660138063945671bead7f51e7c484c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba177a0f9e181da88aa7f0556bf57d4ce660138063945671bead7f51e7c484c4->enter($__internal_ba177a0f9e181da88aa7f0556bf57d4ce660138063945671bead7f51e7c484c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_1eb1a55a77f7890a83d7ba5ec1559bcad3f67d08f22661d898b3d300d2abbf58 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1eb1a55a77f7890a83d7ba5ec1559bcad3f67d08f22661d898b3d300d2abbf58->enter($__internal_1eb1a55a77f7890a83d7ba5ec1559bcad3f67d08f22661d898b3d300d2abbf58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h3>Classement</h3>
\t<p>Cliquez sur une pension pour afficher les informations la concernant</p>
</div>

\t
\t<div class=\"row\">
\t\t<div class=\"list_user  col-sm-5\">
\t\t\t<div class=\"col-sm-6\">
\t\t\t\tNombre de pensions : ";
        // line 19
        echo twig_escape_filter($this->env, ($context["nbTot"] ?? $this->getContext($context, "nbTot")), "html", null, true);
        echo "
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t \t<input type=\"text\" id=\"search\" onkeyup=\"search();\" placeholder=\"Rechercher une pension...\">
\t\t\t</div>
\t\t\t\t\t<table id=\"tableRank\" class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th>Rang</th>
\t\t\t\t\t        <th>Nom</th>
\t\t\t\t\t        <th>Réputation</th>
\t\t\t\t\t        <th>Nombre de pensionnaires</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t     ";
        // line 34
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["allPension"] ?? $this->getContext($context, "allPension"))) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 35
            echo "\t\t\t\t\t\t\t<tr id=\"pension-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"pension link active\">
\t\t\t\t\t\t\t\t<td>";
            // line 36
            echo twig_escape_filter($this->env, ($context["i"] + 1), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPension"] ?? $this->getContext($context, "allPension")), $context["i"], array(), "array"), "name", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 38
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPension"] ?? $this->getContext($context, "allPension")), $context["i"], array(), "array"), "reputation", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPension"] ?? $this->getContext($context, "allPension")), $context["i"], array(), "array"), "nbMission", array()), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t</div>
\t\t<div id=\"pension_view\" class=\"col-sm-6 col-sm-push-1\">
\t\t\t<div id=\"info_pension\">
\t\t\t</div>
\t\t\t<a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_write_to", array("id" => 0));
        echo "\" class=\"btn btn-default hidden\" id=\"write_mess\">Ecrire un message</a>
\t\t\t";
        // line 49
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            echo " <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("remove_user", array("id" => 0));
            echo "\" class=\"btn btn-default hidden\" id=\"remove_user\">Supprimer cet utilisateur</a> ";
        }
        // line 50
        echo "\t\t</div>
\t</div>
<script>

var allPension  = ";
        // line 54
        echo twig_jsonencode_filter(($context["allPension"] ?? $this->getContext($context, "allPension")));
        echo ";

\$(\".pension\").click(function(){
\t\$(\".pension\").removeClass(\"success\");
\t\$(this).addClass(\"success\");

\tvar selPen = allPension[\$(this).attr(\"id\").split(\"-\")[1]];
\t\$(\"#info_pension\").html(\"<h3>\"+selPen.name+\"</h3><br><img src=\\\"uploads/avatar/\"+selPen.avatar+\"\\\" heigth=\\\"150\\\" width=\\\"150\\\"><br>\");
\tvar href= \$(\"#write_mess\").attr(\"href\");
\t\$(\"#write_mess\").removeClass(\"hidden\");
\t\$(\"#write_mess\").attr(\"href\", href.replace(/[0-9]+/,selPen.id));

\t\$(\"#remove_user\").removeClass(\"hidden\");
\thref = \$(\"#remove_user\").attr(\"href\");
\t\$(\"#remove_user\").attr(\"href\", href.replace(/[0-9]+/, selPen.id));
});

\tfunction search() {

\t  var input, filter, table, tr, td, i;
\t  input = \$(\"#search\");
\t  filter = input.val().toUpperCase();
\t  table = \$(\"#tableRank\");
\t  tr = \$(\"#tableRank\").find(\"tr\");

\t  for (i = 1; i < tr.length; i++) {
\t    td = tr[i].getElementsByTagName(\"td\")[1];
\t    if (td) {
\t      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
\t        tr[i].style.display = \"\";
\t      } else {
\t        tr[i].style.display = \"none\";
\t      }
\t    } 
\t  }
\t}

</script>
";
        
        $__internal_1eb1a55a77f7890a83d7ba5ec1559bcad3f67d08f22661d898b3d300d2abbf58->leave($__internal_1eb1a55a77f7890a83d7ba5ec1559bcad3f67d08f22661d898b3d300d2abbf58_prof);

        
        $__internal_ba177a0f9e181da88aa7f0556bf57d4ce660138063945671bead7f51e7c484c4->leave($__internal_ba177a0f9e181da88aa7f0556bf57d4ce660138063945671bead7f51e7c484c4_prof);

    }

    public function getTemplateName()
    {
        return "ranking.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 54,  154 => 50,  148 => 49,  144 => 48,  136 => 42,  127 => 39,  123 => 38,  119 => 37,  115 => 36,  110 => 35,  106 => 34,  88 => 19,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{ asset('css/post_consult.css') }}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Classement</h3>
\t<p>Cliquez sur une pension pour afficher les informations la concernant</p>
</div>

\t
\t<div class=\"row\">
\t\t<div class=\"list_user  col-sm-5\">
\t\t\t<div class=\"col-sm-6\">
\t\t\t\tNombre de pensions : {{ nbTot }}
\t\t\t</div>
\t\t\t<div class=\"col-sm-6\">
\t\t\t \t<input type=\"text\" id=\"search\" onkeyup=\"search();\" placeholder=\"Rechercher une pension...\">
\t\t\t</div>
\t\t\t\t\t<table id=\"tableRank\" class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th>Rang</th>
\t\t\t\t\t        <th>Nom</th>
\t\t\t\t\t        <th>Réputation</th>
\t\t\t\t\t        <th>Nombre de pensionnaires</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t     {% for i in 0..allPension|length -1 %}
\t\t\t\t\t\t\t<tr id=\"pension-{{ i }}\" class=\"pension link active\">
\t\t\t\t\t\t\t\t<td>{{ i+1 }}</td>
\t\t\t\t\t\t\t\t<td>{{ allPension[i].name }}</td>
\t\t\t\t\t\t\t\t<td>{{ allPension[i].reputation }}</td>
\t\t\t\t\t\t\t\t<td>{{ allPension[i].nbMission }}</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t</div>
\t\t<div id=\"pension_view\" class=\"col-sm-6 col-sm-push-1\">
\t\t\t<div id=\"info_pension\">
\t\t\t</div>
\t\t\t<a href=\"{{ path('post_write_to', {'id':0}) }}\" class=\"btn btn-default hidden\" id=\"write_mess\">Ecrire un message</a>
\t\t\t{% if is_granted('ROLE_ADMIN') %} <a href=\"{{ path('remove_user', {'id':0}) }}\" class=\"btn btn-default hidden\" id=\"remove_user\">Supprimer cet utilisateur</a> {% endif %}
\t\t</div>
\t</div>
<script>

var allPension  = {{ allPension|json_encode|raw }};

\$(\".pension\").click(function(){
\t\$(\".pension\").removeClass(\"success\");
\t\$(this).addClass(\"success\");

\tvar selPen = allPension[\$(this).attr(\"id\").split(\"-\")[1]];
\t\$(\"#info_pension\").html(\"<h3>\"+selPen.name+\"</h3><br><img src=\\\"uploads/avatar/\"+selPen.avatar+\"\\\" heigth=\\\"150\\\" width=\\\"150\\\"><br>\");
\tvar href= \$(\"#write_mess\").attr(\"href\");
\t\$(\"#write_mess\").removeClass(\"hidden\");
\t\$(\"#write_mess\").attr(\"href\", href.replace(/[0-9]+/,selPen.id));

\t\$(\"#remove_user\").removeClass(\"hidden\");
\thref = \$(\"#remove_user\").attr(\"href\");
\t\$(\"#remove_user\").attr(\"href\", href.replace(/[0-9]+/, selPen.id));
});

\tfunction search() {

\t  var input, filter, table, tr, td, i;
\t  input = \$(\"#search\");
\t  filter = input.val().toUpperCase();
\t  table = \$(\"#tableRank\");
\t  tr = \$(\"#tableRank\").find(\"tr\");

\t  for (i = 1; i < tr.length; i++) {
\t    td = tr[i].getElementsByTagName(\"td\")[1];
\t    if (td) {
\t      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
\t        tr[i].style.display = \"\";
\t      } else {
\t        tr[i].style.display = \"none\";
\t      }
\t    } 
\t  }
\t}

</script>
{% endblock %}", "ranking.html.twig", "/var/www/projet-web/app/Resources/views/ranking.html.twig");
    }
}
