<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_55f5598e44180353eb6404e850f26acce07faa4c887b21e4feb7cb081b322a8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d6ea66a5ba805e9ce406c69124b83b90fc095784514cf80a899300d27bcc9a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d6ea66a5ba805e9ce406c69124b83b90fc095784514cf80a899300d27bcc9a4->enter($__internal_7d6ea66a5ba805e9ce406c69124b83b90fc095784514cf80a899300d27bcc9a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_b07f4b9d58d446d1c1ce79cbc750a9ae60768e44c180e03ce63010dd9218c5cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b07f4b9d58d446d1c1ce79cbc750a9ae60768e44c180e03ce63010dd9218c5cc->enter($__internal_b07f4b9d58d446d1c1ce79cbc750a9ae60768e44c180e03ce63010dd9218c5cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_7d6ea66a5ba805e9ce406c69124b83b90fc095784514cf80a899300d27bcc9a4->leave($__internal_7d6ea66a5ba805e9ce406c69124b83b90fc095784514cf80a899300d27bcc9a4_prof);

        
        $__internal_b07f4b9d58d446d1c1ce79cbc750a9ae60768e44c180e03ce63010dd9218c5cc->leave($__internal_b07f4b9d58d446d1c1ce79cbc750a9ae60768e44c180e03ce63010dd9218c5cc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
