<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_13d02a4d034894dbd000025ac1763fd690e96c831029426ecd0806d4e6ee13e4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_defb23d3f8189b49cc16ea951bc20d100ea71945e6a54e76c064aaf61c9c83ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_defb23d3f8189b49cc16ea951bc20d100ea71945e6a54e76c064aaf61c9c83ec->enter($__internal_defb23d3f8189b49cc16ea951bc20d100ea71945e6a54e76c064aaf61c9c83ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_83ed468cc1c3fa0384064c200344f9eb2c92b5eced5d151dc3acbbc3bc8d95a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_83ed468cc1c3fa0384064c200344f9eb2c92b5eced5d151dc3acbbc3bc8d95a0->enter($__internal_83ed468cc1c3fa0384064c200344f9eb2c92b5eced5d151dc3acbbc3bc8d95a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_defb23d3f8189b49cc16ea951bc20d100ea71945e6a54e76c064aaf61c9c83ec->leave($__internal_defb23d3f8189b49cc16ea951bc20d100ea71945e6a54e76c064aaf61c9c83ec_prof);

        
        $__internal_83ed468cc1c3fa0384064c200344f9eb2c92b5eced5d151dc3acbbc3bc8d95a0->leave($__internal_83ed468cc1c3fa0384064c200344f9eb2c92b5eced5d151dc3acbbc3bc8d95a0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
