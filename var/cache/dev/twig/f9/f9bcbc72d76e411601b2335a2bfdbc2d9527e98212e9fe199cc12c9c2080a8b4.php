<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_99d1858e2fc49d723712c037e7020326a777a5925900c289a2cdba320a5c9b92 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_918281006f30e7c7b47a4861daa5ccca2f045dd3397b0da1a0a6a439e6c0d924 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_918281006f30e7c7b47a4861daa5ccca2f045dd3397b0da1a0a6a439e6c0d924->enter($__internal_918281006f30e7c7b47a4861daa5ccca2f045dd3397b0da1a0a6a439e6c0d924_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_425277399648d3e012c883420f29fcdce510fd5436bca6db5e88507eb71496b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_425277399648d3e012c883420f29fcdce510fd5436bca6db5e88507eb71496b9->enter($__internal_425277399648d3e012c883420f29fcdce510fd5436bca6db5e88507eb71496b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_918281006f30e7c7b47a4861daa5ccca2f045dd3397b0da1a0a6a439e6c0d924->leave($__internal_918281006f30e7c7b47a4861daa5ccca2f045dd3397b0da1a0a6a439e6c0d924_prof);

        
        $__internal_425277399648d3e012c883420f29fcdce510fd5436bca6db5e88507eb71496b9->leave($__internal_425277399648d3e012c883420f29fcdce510fd5436bca6db5e88507eb71496b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
