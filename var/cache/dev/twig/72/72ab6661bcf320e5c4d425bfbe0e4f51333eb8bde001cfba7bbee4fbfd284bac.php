<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_14a6ae2f5811878dfde77c13333aeec9f7ef72165018b1d182beb24ac36b0ff1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_511fe0dcc947b163f422f1e72f66fbb30d8672a26fe44914cadc36dad133509b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_511fe0dcc947b163f422f1e72f66fbb30d8672a26fe44914cadc36dad133509b->enter($__internal_511fe0dcc947b163f422f1e72f66fbb30d8672a26fe44914cadc36dad133509b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_63e47d806eb49c8e670508d94e0015366c59ce791302e4993d7751d5f5fe9400 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63e47d806eb49c8e670508d94e0015366c59ce791302e4993d7751d5f5fe9400->enter($__internal_63e47d806eb49c8e670508d94e0015366c59ce791302e4993d7751d5f5fe9400_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_511fe0dcc947b163f422f1e72f66fbb30d8672a26fe44914cadc36dad133509b->leave($__internal_511fe0dcc947b163f422f1e72f66fbb30d8672a26fe44914cadc36dad133509b_prof);

        
        $__internal_63e47d806eb49c8e670508d94e0015366c59ce791302e4993d7751d5f5fe9400->leave($__internal_63e47d806eb49c8e670508d94e0015366c59ce791302e4993d7751d5f5fe9400_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
