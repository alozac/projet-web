<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_ff27cc852a27d51d3bcef17830012c8f528d8f68fff0338326ac29cb170694e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6809deb0bea692dda5b923b0a1adde2075c1b8d1ff5c603e767234d69b63575 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6809deb0bea692dda5b923b0a1adde2075c1b8d1ff5c603e767234d69b63575->enter($__internal_e6809deb0bea692dda5b923b0a1adde2075c1b8d1ff5c603e767234d69b63575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_94ba163a3e4480d7f838ac9e11287302ae898e697a8e3f439297e150e848fcec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_94ba163a3e4480d7f838ac9e11287302ae898e697a8e3f439297e150e848fcec->enter($__internal_94ba163a3e4480d7f838ac9e11287302ae898e697a8e3f439297e150e848fcec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_e6809deb0bea692dda5b923b0a1adde2075c1b8d1ff5c603e767234d69b63575->leave($__internal_e6809deb0bea692dda5b923b0a1adde2075c1b8d1ff5c603e767234d69b63575_prof);

        
        $__internal_94ba163a3e4480d7f838ac9e11287302ae898e697a8e3f439297e150e848fcec->leave($__internal_94ba163a3e4480d7f838ac9e11287302ae898e697a8e3f439297e150e848fcec_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
