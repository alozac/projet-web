<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_e45fecf8a55de68004bbee1cdc147ba94016a8c7a2a9b83d3492ccd8c5bcce5a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df6b2651f4b8c2ba722a97687ec0c6e3a42b92344e90ca1b9ed28cea834360c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df6b2651f4b8c2ba722a97687ec0c6e3a42b92344e90ca1b9ed28cea834360c2->enter($__internal_df6b2651f4b8c2ba722a97687ec0c6e3a42b92344e90ca1b9ed28cea834360c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_749f6d245056652ecf832ae9de9bc4e863035ba1da1f9ad3afb9532eda13092d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_749f6d245056652ecf832ae9de9bc4e863035ba1da1f9ad3afb9532eda13092d->enter($__internal_749f6d245056652ecf832ae9de9bc4e863035ba1da1f9ad3afb9532eda13092d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_df6b2651f4b8c2ba722a97687ec0c6e3a42b92344e90ca1b9ed28cea834360c2->leave($__internal_df6b2651f4b8c2ba722a97687ec0c6e3a42b92344e90ca1b9ed28cea834360c2_prof);

        
        $__internal_749f6d245056652ecf832ae9de9bc4e863035ba1da1f9ad3afb9532eda13092d->leave($__internal_749f6d245056652ecf832ae9de9bc4e863035ba1da1f9ad3afb9532eda13092d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
