<?php

/* base.html.twig */
class __TwigTemplate_e461d95e1d2892cdd0d1b7cffb9be3b485dee7efde8f1ac74c8c1d941ca29aa8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
            'main_screen' => array($this, 'block_main_screen'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae2022cca2857ae896f43297ae29817d430c91481c12d4255dcfa5f21f5f5966 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae2022cca2857ae896f43297ae29817d430c91481c12d4255dcfa5f21f5f5966->enter($__internal_ae2022cca2857ae896f43297ae29817d430c91481c12d4255dcfa5f21f5f5966_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_c99d2c7fa0e12d3c7a3d5c018fa5aa544192fb467a4fbf56b2f4b76c8ba5514d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c99d2c7fa0e12d3c7a3d5c018fa5aa544192fb467a4fbf56b2f4b76c8ba5514d->enter($__internal_c99d2c7fa0e12d3c7a3d5c018fa5aa544192fb467a4fbf56b2f4b76c8ba5514d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
      <meta charset=\"utf-8\">
      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
      <script type=\"text/javascript\" src=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/js/jquery-3.2.0.js"), "html", null, true);
        echo "\"></script>
      <script type=\"text/javascript\" src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
        ";
        // line 9
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 14
        echo "      <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
      <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/game_icon.jpg"), "html", null, true);
        echo "\" />
    </head>

    ";
        // line 18
        $this->displayBlock('body', $context, $blocks);
        // line 74
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 81
        echo "\t</div>
    </body>
</html>
";
        
        $__internal_ae2022cca2857ae896f43297ae29817d430c91481c12d4255dcfa5f21f5f5966->leave($__internal_ae2022cca2857ae896f43297ae29817d430c91481c12d4255dcfa5f21f5f5966_prof);

        
        $__internal_c99d2c7fa0e12d3c7a3d5c018fa5aa544192fb467a4fbf56b2f4b76c8ba5514d->leave($__internal_c99d2c7fa0e12d3c7a3d5c018fa5aa544192fb467a4fbf56b2f4b76c8ba5514d_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_89f22cd04e3767ee65a2c0d1f972c3734d3155891aa04a935a0e42cbe1689eab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_89f22cd04e3767ee65a2c0d1f972c3734d3155891aa04a935a0e42cbe1689eab->enter($__internal_89f22cd04e3767ee65a2c0d1f972c3734d3155891aa04a935a0e42cbe1689eab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_61786a22a606382133d890687d779afd42719b96a5d039f327ce910738f37e8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_61786a22a606382133d890687d779afd42719b96a5d039f327ce910738f37e8c->enter($__internal_61786a22a606382133d890687d779afd42719b96a5d039f327ce910738f37e8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "         <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/css/bootstrap.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
         <link href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bootstrap/css/bootstrap-theme.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
         <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/base.css"), "html", null, true);
        echo "\"/>
        ";
        
        $__internal_61786a22a606382133d890687d779afd42719b96a5d039f327ce910738f37e8c->leave($__internal_61786a22a606382133d890687d779afd42719b96a5d039f327ce910738f37e8c_prof);

        
        $__internal_89f22cd04e3767ee65a2c0d1f972c3734d3155891aa04a935a0e42cbe1689eab->leave($__internal_89f22cd04e3767ee65a2c0d1f972c3734d3155891aa04a935a0e42cbe1689eab_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_b2822951cf4b61afe0027d42205b9aa34a2a6d1c0bc2c4b4780bb28f8b53cb7c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2822951cf4b61afe0027d42205b9aa34a2a6d1c0bc2c4b4780bb28f8b53cb7c->enter($__internal_b2822951cf4b61afe0027d42205b9aa34a2a6d1c0bc2c4b4780bb28f8b53cb7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_60278da4e77f8cbe24a30f9324863b0c7df5958dc199f9b062339a419f847b70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60278da4e77f8cbe24a30f9324863b0c7df5958dc199f9b062339a419f847b70->enter($__internal_60278da4e77f8cbe24a30f9324863b0c7df5958dc199f9b062339a419f847b70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_60278da4e77f8cbe24a30f9324863b0c7df5958dc199f9b062339a419f847b70->leave($__internal_60278da4e77f8cbe24a30f9324863b0c7df5958dc199f9b062339a419f847b70_prof);

        
        $__internal_b2822951cf4b61afe0027d42205b9aa34a2a6d1c0bc2c4b4780bb28f8b53cb7c->leave($__internal_b2822951cf4b61afe0027d42205b9aa34a2a6d1c0bc2c4b4780bb28f8b53cb7c_prof);

    }

    // line 18
    public function block_body($context, array $blocks = array())
    {
        $__internal_d79dcb9750e7cd55c3cea70a8755b4437036b3daf4b84ac802c421dc9098dbdc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d79dcb9750e7cd55c3cea70a8755b4437036b3daf4b84ac802c421dc9098dbdc->enter($__internal_d79dcb9750e7cd55c3cea70a8755b4437036b3daf4b84ac802c421dc9098dbdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6b08be069487919cc01cd59e4eecc13fca5b0c24a292ae38863d804954b5343b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b08be069487919cc01cd59e4eecc13fca5b0c24a292ae38863d804954b5343b->enter($__internal_6b08be069487919cc01cd59e4eecc13fca5b0c24a292ae38863d804954b5343b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 19
        echo "    <body>
\t<div class=\"container-fluid\">
    <div class=\"row\">
      <div id=\"left_menu\" class = \"col-sm-2 col-sm-pull-0\">
        <div id=\"infosPension\" class=\"menu\">
        ";
        // line 24
        $context["auth"] = $this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array());
        // line 25
        echo "          ";
        if ( !(null === ($context["auth"] ?? $this->getContext($context, "auth")))) {
            // line 26
            echo "             <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
            echo "\"> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "gold", array()), "html", null, true);
            echo " &nbsp&nbsp
             <img src=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/paws.png"), "html", null, true);
            echo "\"> : ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "paws", array()), "html", null, true);
            echo "<br>
             Réputation : ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "reputation", array()), "html", null, true);
            echo "<br>
             <img src=\"";
            // line 29
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("uploads/avatar/" . $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "avatar", array()))), "html", null, true);
            echo "\"
                  class=\"img-circle\" id=\"imgPension\"/>
             ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "name", array()), "html", null, true);
            echo "
          ";
        }
        // line 33
        echo "        </div>

      \t<div id=\"liens\" class=\"menu\">
        ";
        // line 36
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 37
            echo "          <a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("administration");
            echo "\" ><input type=\"button\" value=\"Administration\" class=\"butt\"></a>
          <br>
        ";
        }
        // line 40
        echo "          <a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\"><input type=\"button\" value=\"Votre pension\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("chooseMission");
        echo "\" ><input type=\"button\" value=\"Missions\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("veterinary");
        echo "\"><input type=\"button\" value=\"Vétérinaire\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"";
        // line 46
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_consult");
        echo "\"><input type=\"button\" value=\"Poste\" class=\"butt\"></a>
          <br>
          <a href=\"";
        // line 48
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pensions");
        echo "\"><input type=\"button\" value=\"Vos pensions\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("ranking");
        echo "\"><input type=\"button\" value=\"Classement\" class=\"butt\"></a>
      \t\t<br>
      \t\t<a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("logout_user");
        echo "\"><input type=\"button\" value=\"Deconnexion\" class=\"butt\"></a>
      \t</div>
      </div>

    \t<div id=\"ecran_princip\" class=\"col-sm-9 col-sm-push-1\">
          ";
        // line 57
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "ok"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["msg"]) {
            // line 58
            echo "            <div class=\"alert alert-success\">
                 ";
            // line 59
            echo twig_escape_filter($this->env, $context["msg"], "html", null, true);
            echo "
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['msg'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "          ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
            // line 63
            echo "            <div class=\"alert alert-danger\">
               ";
            // line 64
            echo twig_escape_filter($this->env, $context["info"], "html", null, true);
            echo "
            </div>
          ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "
          ";
        // line 68
        $this->displayBlock('main_screen', $context, $blocks);
        // line 70
        echo "    \t</div>
    </div>

    ";
        
        $__internal_6b08be069487919cc01cd59e4eecc13fca5b0c24a292ae38863d804954b5343b->leave($__internal_6b08be069487919cc01cd59e4eecc13fca5b0c24a292ae38863d804954b5343b_prof);

        
        $__internal_d79dcb9750e7cd55c3cea70a8755b4437036b3daf4b84ac802c421dc9098dbdc->leave($__internal_d79dcb9750e7cd55c3cea70a8755b4437036b3daf4b84ac802c421dc9098dbdc_prof);

    }

    // line 68
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_ed5ec36e4f48113b1b3a21ba0e09669d451ddc1729b3eea95ceb6daa5349a9db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed5ec36e4f48113b1b3a21ba0e09669d451ddc1729b3eea95ceb6daa5349a9db->enter($__internal_ed5ec36e4f48113b1b3a21ba0e09669d451ddc1729b3eea95ceb6daa5349a9db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_2501669c44ccca2c104132d260959352fe2dc55d0b4b202e9267678460586141 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2501669c44ccca2c104132d260959352fe2dc55d0b4b202e9267678460586141->enter($__internal_2501669c44ccca2c104132d260959352fe2dc55d0b4b202e9267678460586141_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 69
        echo "          ";
        
        $__internal_2501669c44ccca2c104132d260959352fe2dc55d0b4b202e9267678460586141->leave($__internal_2501669c44ccca2c104132d260959352fe2dc55d0b4b202e9267678460586141_prof);

        
        $__internal_ed5ec36e4f48113b1b3a21ba0e09669d451ddc1729b3eea95ceb6daa5349a9db->leave($__internal_ed5ec36e4f48113b1b3a21ba0e09669d451ddc1729b3eea95ceb6daa5349a9db_prof);

    }

    // line 74
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c086e2d180ba3e8f748ad2c5e13a57653a8e5744dc43bcff08cc6554146c6b9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c086e2d180ba3e8f748ad2c5e13a57653a8e5744dc43bcff08cc6554146c6b9d->enter($__internal_c086e2d180ba3e8f748ad2c5e13a57653a8e5744dc43bcff08cc6554146c6b9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_7d90df903f1d0c9bf6fa9fcb2352f6acab83cbb434eac6f53fdaeb82bfd9f94a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d90df903f1d0c9bf6fa9fcb2352f6acab83cbb434eac6f53fdaeb82bfd9f94a->enter($__internal_7d90df903f1d0c9bf6fa9fcb2352f6acab83cbb434eac6f53fdaeb82bfd9f94a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 75
        echo "       ";
        if ((null === $this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()))) {
            // line 76
            echo "      <script>
        \$(\"#left_menu\").addClass(\"disabled\");
      </script>
      ";
        }
        // line 80
        echo "    ";
        
        $__internal_7d90df903f1d0c9bf6fa9fcb2352f6acab83cbb434eac6f53fdaeb82bfd9f94a->leave($__internal_7d90df903f1d0c9bf6fa9fcb2352f6acab83cbb434eac6f53fdaeb82bfd9f94a_prof);

        
        $__internal_c086e2d180ba3e8f748ad2c5e13a57653a8e5744dc43bcff08cc6554146c6b9d->leave($__internal_c086e2d180ba3e8f748ad2c5e13a57653a8e5744dc43bcff08cc6554146c6b9d_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  314 => 80,  308 => 76,  305 => 75,  296 => 74,  286 => 69,  277 => 68,  264 => 70,  262 => 68,  259 => 67,  250 => 64,  247 => 63,  242 => 62,  233 => 59,  230 => 58,  226 => 57,  218 => 52,  213 => 50,  208 => 48,  203 => 46,  198 => 44,  193 => 42,  187 => 40,  180 => 37,  178 => 36,  173 => 33,  168 => 31,  163 => 29,  159 => 28,  153 => 27,  146 => 26,  143 => 25,  141 => 24,  134 => 19,  125 => 18,  107 => 14,  95 => 12,  91 => 11,  86 => 10,  77 => 9,  64 => 81,  61 => 74,  59 => 18,  53 => 15,  48 => 14,  46 => 9,  42 => 8,  38 => 7,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
      <meta charset=\"utf-8\">
      <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
      <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
      <script type=\"text/javascript\" src=\"{{ asset('bootstrap/js/jquery-3.2.0.js') }}\"></script>
      <script type=\"text/javascript\" src=\"{{ asset('bootstrap/js/bootstrap.min.js') }}\"></script>
        {% block stylesheets %}
         <link href=\"{{asset('bootstrap/css/bootstrap.min.css')}}\" rel=\"stylesheet\">
         <link href=\"{{asset('bootstrap/css/bootstrap-theme.min.css')}}\" rel=\"stylesheet\">
         <link rel=\"stylesheet\" href=\"{{asset('css/base.css')}}\"/>
        {% endblock %}
      <title>{% block title %}Welcome!{% endblock %}</title>
      <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('imgs/game_icon.jpg') }}\" />
    </head>

    {% block body %}
    <body>
\t<div class=\"container-fluid\">
    <div class=\"row\">
      <div id=\"left_menu\" class = \"col-sm-2 col-sm-pull-0\">
        <div id=\"infosPension\" class=\"menu\">
        {% set auth = app.user %}
          {% if auth is not null %}
             <img src=\"{{ asset('imgs/po.png') }}\"> : {{ auth.lastPension.gold }} &nbsp&nbsp
             <img src=\"{{ asset('imgs/paws.png') }}\"> : {{ auth.lastPension.paws }}<br>
             Réputation : {{ auth.lastPension.reputation }}<br>
             <img src=\"{{ asset('uploads/avatar/' ~ auth.lastPension.avatar) }}\"
                  class=\"img-circle\" id=\"imgPension\"/>
             {{ auth.lastPension.name }}
          {% endif %}
        </div>

      \t<div id=\"liens\" class=\"menu\">
        {% if is_granted(\"ROLE_ADMIN\") %}
          <a href=\"{{ path('administration') }}\" ><input type=\"button\" value=\"Administration\" class=\"butt\"></a>
          <br>
        {% endif %}
          <a href=\"{{ path('homepage') }}\"><input type=\"button\" value=\"Votre pension\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"{{ path('chooseMission') }}\" ><input type=\"button\" value=\"Missions\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"{{ path('veterinary') }}\"><input type=\"button\" value=\"Vétérinaire\" class=\"butt\"></a>
      \t\t<br>
          <a href=\"{{ path('post_consult') }}\"><input type=\"button\" value=\"Poste\" class=\"butt\"></a>
          <br>
          <a href=\"{{ path('pensions') }}\"><input type=\"button\" value=\"Vos pensions\" class=\"butt\"></a>
          <br>
      \t\t<a href=\"{{ path('ranking') }}\"><input type=\"button\" value=\"Classement\" class=\"butt\"></a>
      \t\t<br>
      \t\t<a href=\"{{ path('logout_user') }}\"><input type=\"button\" value=\"Deconnexion\" class=\"butt\"></a>
      \t</div>
      </div>

    \t<div id=\"ecran_princip\" class=\"col-sm-9 col-sm-push-1\">
          {% for msg in app.session.flashbag.get('ok') %}
            <div class=\"alert alert-success\">
                 {{ msg }}
            </div>
          {% endfor %}
          {% for info in app.session.flashbag.get('error') %}
            <div class=\"alert alert-danger\">
               {{ info }}
            </div>
          {% endfor %}

          {% block main_screen %}
          {% endblock %}
    \t</div>
    </div>

    {% endblock %}
    {% block javascripts %}
       {% if app.user is null %}
      <script>
        \$(\"#left_menu\").addClass(\"disabled\");
      </script>
      {% endif %}
    {% endblock %}
\t</div>
    </body>
</html>
", "base.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\base.html.twig");
    }
}
