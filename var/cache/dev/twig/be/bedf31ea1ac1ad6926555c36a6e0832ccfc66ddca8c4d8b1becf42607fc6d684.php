<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_2fda6349c19790506d3e6aacc1acc415a64924b0d17de84d657dec2a8bb6abe6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b85f6d91a2acbd077c149314820464b71288573d30e93291660e89bd287f644b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b85f6d91a2acbd077c149314820464b71288573d30e93291660e89bd287f644b->enter($__internal_b85f6d91a2acbd077c149314820464b71288573d30e93291660e89bd287f644b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_15720fc65f523ceb8f9e1c825100c42e1c9c86949c7b513725e0c607792b52a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15720fc65f523ceb8f9e1c825100c42e1c9c86949c7b513725e0c607792b52a4->enter($__internal_15720fc65f523ceb8f9e1c825100c42e1c9c86949c7b513725e0c607792b52a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_b85f6d91a2acbd077c149314820464b71288573d30e93291660e89bd287f644b->leave($__internal_b85f6d91a2acbd077c149314820464b71288573d30e93291660e89bd287f644b_prof);

        
        $__internal_15720fc65f523ceb8f9e1c825100c42e1c9c86949c7b513725e0c607792b52a4->leave($__internal_15720fc65f523ceb8f9e1c825100c42e1c9c86949c7b513725e0c607792b52a4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
