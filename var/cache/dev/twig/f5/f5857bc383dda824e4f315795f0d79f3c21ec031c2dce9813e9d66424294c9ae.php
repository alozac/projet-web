<?php

/* pension.html.twig */
class __TwigTemplate_19fe923a0c7c19438cdf478edbdeaeb31fab8dc58f669a1dc53de966c8b5506c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "pension.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_caed8ac64df4bde8dd629c8fe031eabdf0489ce9673898edc65c0d84635b3bad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_caed8ac64df4bde8dd629c8fe031eabdf0489ce9673898edc65c0d84635b3bad->enter($__internal_caed8ac64df4bde8dd629c8fe031eabdf0489ce9673898edc65c0d84635b3bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "pension.html.twig"));

        $__internal_901f66f35ec64169ad039e614462b177a54b2a79c6f6e17af4825bc8acffe4b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_901f66f35ec64169ad039e614462b177a54b2a79c6f6e17af4825bc8acffe4b7->enter($__internal_901f66f35ec64169ad039e614462b177a54b2a79c6f6e17af4825bc8acffe4b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "pension.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_caed8ac64df4bde8dd629c8fe031eabdf0489ce9673898edc65c0d84635b3bad->leave($__internal_caed8ac64df4bde8dd629c8fe031eabdf0489ce9673898edc65c0d84635b3bad_prof);

        
        $__internal_901f66f35ec64169ad039e614462b177a54b2a79c6f6e17af4825bc8acffe4b7->leave($__internal_901f66f35ec64169ad039e614462b177a54b2a79c6f6e17af4825bc8acffe4b7_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_7225e9883985b97ff83c8ffd7b1534297d8d5c57f231dcaf81d63740b731154b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7225e9883985b97ff83c8ffd7b1534297d8d5c57f231dcaf81d63740b731154b->enter($__internal_7225e9883985b97ff83c8ffd7b1534297d8d5c57f231dcaf81d63740b731154b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_a8be085f78b431e96110abb52e3b11dda6c0d8bdd162a8ee8894170fa446468c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8be085f78b431e96110abb52e3b11dda6c0d8bdd162a8ee8894170fa446468c->enter($__internal_a8be085f78b431e96110abb52e3b11dda6c0d8bdd162a8ee8894170fa446468c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/pension.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_a8be085f78b431e96110abb52e3b11dda6c0d8bdd162a8ee8894170fa446468c->leave($__internal_a8be085f78b431e96110abb52e3b11dda6c0d8bdd162a8ee8894170fa446468c_prof);

        
        $__internal_7225e9883985b97ff83c8ffd7b1534297d8d5c57f231dcaf81d63740b731154b->leave($__internal_7225e9883985b97ff83c8ffd7b1534297d8d5c57f231dcaf81d63740b731154b_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_725b8da6310452311eff34456cc81215d453cba9b6af6dbcf0d45b38ff874419 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_725b8da6310452311eff34456cc81215d453cba9b6af6dbcf0d45b38ff874419->enter($__internal_725b8da6310452311eff34456cc81215d453cba9b6af6dbcf0d45b38ff874419_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_8ba3de45e516629b05c326fad594bd7e1346e9974e348fb1be997dc2a9815384 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8ba3de45e516629b05c326fad594bd7e1346e9974e348fb1be997dc2a9815384->enter($__internal_8ba3de45e516629b05c326fad594bd7e1346e9974e348fb1be997dc2a9815384_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h2>Résumé de votre pension</h2>
\t<h4>
\t\t";
        // line 13
        if ( !twig_test_empty(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")))) {
            // line 14
            echo "\t\t\tVous avez actuellement ";
            echo twig_escape_filter($this->env, twig_length_filter($this->env, ($context["missNotOver"] ?? $this->getContext($context, "missNotOver"))), "html", null, true);
            echo " 
\t\t\tpensionnaire";
            // line 15
            if ((twig_length_filter($this->env, ($context["missNotOver"] ?? $this->getContext($context, "missNotOver"))) > 1)) {
                echo "s";
            }
            // line 16
            echo "\t\t";
        } else {
            // line 17
            echo "\t\t\tVous n'avez aucun pensionnaire pour le moment!
\t\t";
        }
        // line 19
        echo "\t</h4>
</div>

<div class=\"row\">
\t\t<div class=\"list_miss  col-sm-10 col-sm-push-1\">
\t\t\t<ul class=\"nav nav-pills\">
\t\t  \t\t<li id=\"miss_not_over_pill\" class=\"miss_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#miss_not_over\">Missions en cours</a>
\t\t  \t\t</li>
\t\t  \t\t";
        // line 28
        if ( !twig_test_empty(($context["missOver"] ?? $this->getContext($context, "missOver")))) {
            // line 29
            echo "\t\t  \t\t<li id=\"miss_over_pill\" class=\"miss_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#miss_over\">Missions terminées</a>
\t\t  \t\t</li>
\t\t  \t\t";
        }
        // line 33
        echo "\t\t\t</ul>
\t\t\t
\t\t\t<div class=\"tab-content\">
\t\t\t\t<div id=\"miss_not_over\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th class=\"img_th\"></th>
\t\t\t\t\t        <th>Nom</th>
\t\t\t\t\t        <th>Santé</th>
\t\t\t\t\t        <th>Repas</th>
\t\t\t\t\t        <th class='date_th'>Termine le</th>
\t\t\t\t\t        <th></th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    ";
        // line 49
        if ( !twig_test_empty(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")))) {
            // line 50
            echo "\t\t\t\t\t     \t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["missNotOver"] ?? $this->getContext($context, "missNotOver"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 51
                echo "\t\t\t\t\t\t\t<tr id=\"miss_not_over-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"mission_not_over active\">
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<img class=\"imgMission\" src=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("imgs/missions/" . $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "image", array()))), "html", null, true);
                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\talt=\"Image de ";
                // line 54
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "\"/>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>";
                // line 56
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<div class=\"progress\">
  \t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar
  \t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 60
                if (($this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "pdv", array()) >= 50)) {
                    // line 61
                    echo "  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-success 
  \t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 63
                    echo "  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-danger
  \t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 64
                echo "\"
  \t\t\t\t\t\t\t\t\t\t\t role=\"progressbar\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuenow=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "pdv", array()), "html", null, true);
                echo "\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "pdv", array()), "html", null, true);
                echo "%\"> 
  \t\t\t\t\t\t\t\t\t\t</div>
     \t\t\t\t\t\t\t\t\t<div class=\"progress-bar-title\">";
                // line 69
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "pdv", array()), "html", null, true);
                echo "</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t";
                // line 73
                $context["eaten"] = $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "hasEaten", array());
                // line 74
                echo "\t\t\t\t\t\t\t\t\t";
                if ((($context["eaten"] ?? $this->getContext($context, "eaten")) == 0)) {
                    // line 75
                    echo "\t\t\t\t\t\t\t\t\t\tAucun<br><a href=";
                    echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("veterinary");
                    echo " class=\"btn btn-link\">Nourrir</a>
\t\t\t\t\t\t\t\t\t";
                } elseif ((                // line 76
($context["eaten"] ?? $this->getContext($context, "eaten")) == 1)) {
                    // line 77
                    echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_bronze.png"), "html", null, true);
                    echo "\" height=\"30\" width=\"30\">
\t\t\t\t\t\t\t\t\t";
                } elseif ((                // line 78
($context["eaten"] ?? $this->getContext($context, "eaten")) == 2)) {
                    // line 79
                    echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_argent.png"), "html", null, true);
                    echo "\" height=\"30\" width=\"30\">
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 81
                    echo "\t\t\t\t\t\t\t\t\t\t<img src=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_or.png"), "html", null, true);
                    echo "\" height=\"30\" width=\"30\">
\t\t\t\t\t\t\t\t\t";
                }
                // line 82
                echo "\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>";
                // line 84
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "endDate", array()), "d/m/Y à H:i"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"progress_td\">
\t\t\t\t\t\t\t\t\t";
                // line 86
                $context["finishedAt"] = twig_number_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["missNotOver"] ?? $this->getContext($context, "missNotOver")), $context["i"], array(), "array"), "finishedAt", array()));
                // line 87
                echo "\t\t\t\t\t\t\t\t\t<div class=\"progress\">
  \t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-info progress-bar-striped active\" role=\"progressbar\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuenow=\"";
                // line 89
                echo twig_escape_filter($this->env, ($context["finishedAt"] ?? $this->getContext($context, "finishedAt")), "html", null, true);
                echo "\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:";
                // line 90
                echo twig_escape_filter($this->env, ($context["finishedAt"] ?? $this->getContext($context, "finishedAt")), "html", null, true);
                echo "%\"> 
  \t\t\t\t\t\t\t\t\t\t</div>
  \t\t\t\t\t\t\t\t\t\t <div class=\"progress-bar-title\">Terminée à ";
                // line 92
                echo twig_escape_filter($this->env, ($context["finishedAt"] ?? $this->getContext($context, "finishedAt")), "html", null, true);
                echo "%</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 97
            echo "\t\t\t\t\t\t";
        }
        // line 98
        echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>

\t\t\t\t";
        // line 102
        if ( !twig_test_empty(($context["missOver"] ?? $this->getContext($context, "missOver")))) {
            // line 103
            echo "\t\t\t\t<div id=\"miss_over\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th rowspan=\"2\"></th>
\t\t\t\t\t        <th rowspan=\"2\">Nom</th>
\t\t\t\t\t        <th rowspan=\"2\">Santé</th>
\t\t\t\t\t      \t<th colspan=\"2\">Récompenses</th>
\t\t\t\t\t      \t<th rowspan=\"2\" class=\"date_th\">Terminée le</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th class=\"rewards_th\">Or</th>
\t\t\t\t\t        <th class=\"rewards_th\">Reputation</th>
\t\t\t\t\t      </tr>
\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t     \t";
            // line 119
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["missOver"] ?? $this->getContext($context, "missOver"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 120
                echo "\t\t\t\t\t\t\t<tr id=\"miss_over-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"mission_over active\">
\t\t\t\t\t\t\t\t<td class=\"img_th\"><img class=\"imgMission\" src=\"";
                // line 121
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl(("imgs/missions/" . $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "image", array()))), "html", null, true);
                echo "\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\talt=\"Image de ";
                // line 122
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "\"</img></td>
\t\t\t\t\t\t\t\t<td>";
                // line 123
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"progress_td\">
\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar
  \t\t\t\t\t\t\t\t\t\t\t\t\t";
                // line 127
                if (($this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "pdv", array()) >= 50)) {
                    // line 128
                    echo "  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-success 
  \t\t\t\t\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 130
                    echo "  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-danger
  \t\t\t\t\t\t\t\t\t\t\t\t\t";
                }
                // line 131
                echo "\"
  \t\t\t\t\t\t\t\t\t\t\t role=\"progressbar\"
\t\t  \t\t\t\t\t\t\t\t\t aria-valuenow=\"";
                // line 133
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "pdv", array()), "html", null, true);
                echo "\"
\t\t  \t\t\t\t\t\t\t\t\t aria-valuemin=\"0\" 
\t\t  \t\t\t\t\t\t\t\t\t aria-valuemax=\"100\" 
\t\t  \t\t\t\t\t\t\t\t\t style=\"width:";
                // line 136
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "pdv", array()), "html", null, true);
                echo "%\"> 
\t\t  \t\t\t\t\t\t\t\t</div>
\t\t     \t\t\t\t\t\t\t<div class=\"progress-bar-title\">";
                // line 138
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "pdv", array()), "html", null, true);
                echo "</div>
\t\t     \t\t\t\t\t\t</div>
\t     \t\t\t\t\t\t</td>
\t     \t\t\t\t\t\t<td>";
                // line 141
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "gold", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 142
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "reputation", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td class=\"date_th\">";
                // line 143
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["missOver"] ?? $this->getContext($context, "missOver")), $context["i"], array(), "array"), "endDate", array()), "d/m/Y à H:i"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 146
            echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t\t";
        }
        // line 150
        echo "\t\t\t</div>
\t\t</div>
</div>


<div class=\"row\">
\t\t<div id=\"events\" class=\"col-sm-8 col-sm-push-2\"> 
\t\t\t<div id=\"events_header\">
\t\t\t\t<h4>Evènements en cours</h4>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t";
        // line 161
        if ((twig_length_filter($this->env, ($context["events"] ?? $this->getContext($context, "events"))) != 0)) {
            // line 162
            echo "\t\t\t\t\t<div class=\"panel-group\" id=\"accordion\">
\t\t\t\t\t";
            // line 163
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["events"] ?? $this->getContext($context, "events"))) - 1)));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 164
                echo "\t\t\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t\t    <div class=\"panel-heading\">
\t\t\t\t\t\t      <p class=\"panel-title\">
\t\t\t\t\t\t        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse";
                // line 167
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\">
\t\t\t\t\t\t        ";
                // line 168
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["events"] ?? $this->getContext($context, "events")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</a>
\t\t\t\t\t\t      </p>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    <div id=\"collapse";
                // line 171
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"panel-collapse collapse
\t\t\t\t\t\t    \t\t\t\t\t\t\t\t";
                // line 172
                if ($this->getAttribute($context["loop"], "first", array())) {
                    echo " in ";
                }
                echo "\">
\t\t\t\t\t\t      <div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t<p>Du <strong>";
                // line 174
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["events"] ?? $this->getContext($context, "events")), $context["i"], array(), "array"), "start", array()), "d/m/Y H:i"), "html", null, true);
                echo "</strong>
\t\t\t\t\t\t\t\t\t au <strong>";
                // line 175
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["events"] ?? $this->getContext($context, "events")), $context["i"], array(), "array"), "end", array()), "d/m/Y H:i"), "html", null, true);
                echo "</strong></p>
\t\t\t\t\t\t\t\t   <p>";
                // line 176
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["events"] ?? $this->getContext($context, "events")), $context["i"], array(), "array"), "description", array()), "html", null, true);
                echo "</p>
\t\t\t\t\t\t      </div>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 181
            echo "\t\t\t\t\t</div>
\t\t\t\t";
        } else {
            // line 183
            echo "\t\t\t\t\t<p>Aucun évènement en cours</p>
\t\t\t\t";
        }
        // line 185
        echo "\t\t\t</div>
\t\t</div>

\t</div>

<script> 
\t";
        // line 191
        if (twig_test_empty(($context["missOver"] ?? $this->getContext($context, "missOver")))) {
            // line 192
            echo "\t\t\$(\"#miss_not_over\").addClass(\"active\");\t
\t\t\$(\"#miss_not_over_pill\").addClass(\"active\");\t
\t";
        } else {
            // line 195
            echo "\t\t\$(\"#miss_over\").addClass(\"active\");\t
\t\t\$(\"#miss_over_pill\").addClass(\"active\");
\t";
        }
        // line 198
        echo "</script>
";
        
        $__internal_8ba3de45e516629b05c326fad594bd7e1346e9974e348fb1be997dc2a9815384->leave($__internal_8ba3de45e516629b05c326fad594bd7e1346e9974e348fb1be997dc2a9815384_prof);

        
        $__internal_725b8da6310452311eff34456cc81215d453cba9b6af6dbcf0d45b38ff874419->leave($__internal_725b8da6310452311eff34456cc81215d453cba9b6af6dbcf0d45b38ff874419_prof);

    }

    public function getTemplateName()
    {
        return "pension.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  485 => 198,  480 => 195,  475 => 192,  473 => 191,  465 => 185,  461 => 183,  457 => 181,  438 => 176,  434 => 175,  430 => 174,  423 => 172,  419 => 171,  413 => 168,  409 => 167,  404 => 164,  387 => 163,  384 => 162,  382 => 161,  369 => 150,  363 => 146,  354 => 143,  350 => 142,  346 => 141,  340 => 138,  335 => 136,  329 => 133,  325 => 131,  321 => 130,  317 => 128,  315 => 127,  308 => 123,  304 => 122,  300 => 121,  295 => 120,  291 => 119,  273 => 103,  271 => 102,  265 => 98,  262 => 97,  251 => 92,  246 => 90,  242 => 89,  238 => 87,  236 => 86,  231 => 84,  227 => 82,  221 => 81,  215 => 79,  213 => 78,  208 => 77,  206 => 76,  201 => 75,  198 => 74,  196 => 73,  189 => 69,  184 => 67,  180 => 66,  176 => 64,  172 => 63,  168 => 61,  166 => 60,  159 => 56,  154 => 54,  150 => 53,  144 => 51,  139 => 50,  137 => 49,  119 => 33,  113 => 29,  111 => 28,  100 => 19,  96 => 17,  93 => 16,  89 => 15,  84 => 14,  82 => 13,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/pension.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h2>Résumé de votre pension</h2>
\t<h4>
\t\t{% if missNotOver is not empty %}
\t\t\tVous avez actuellement {{ missNotOver|length }} 
\t\t\tpensionnaire{% if missNotOver|length > 1 %}s{% endif %}
\t\t{% else %}
\t\t\tVous n'avez aucun pensionnaire pour le moment!
\t\t{% endif %}
\t</h4>
</div>

<div class=\"row\">
\t\t<div class=\"list_miss  col-sm-10 col-sm-push-1\">
\t\t\t<ul class=\"nav nav-pills\">
\t\t  \t\t<li id=\"miss_not_over_pill\" class=\"miss_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#miss_not_over\">Missions en cours</a>
\t\t  \t\t</li>
\t\t  \t\t{% if missOver is not empty %}
\t\t  \t\t<li id=\"miss_over_pill\" class=\"miss_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#miss_over\">Missions terminées</a>
\t\t  \t\t</li>
\t\t  \t\t{% endif %}
\t\t\t</ul>
\t\t\t
\t\t\t<div class=\"tab-content\">
\t\t\t\t<div id=\"miss_not_over\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th class=\"img_th\"></th>
\t\t\t\t\t        <th>Nom</th>
\t\t\t\t\t        <th>Santé</th>
\t\t\t\t\t        <th>Repas</th>
\t\t\t\t\t        <th class='date_th'>Termine le</th>
\t\t\t\t\t        <th></th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    {% if missNotOver is not empty %}
\t\t\t\t\t     \t{% for i in 0..missNotOver|length -1 %}
\t\t\t\t\t\t\t<tr id=\"miss_not_over-{{ i }}\" class=\"mission_not_over active\">
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<img class=\"imgMission\" src=\"{{ asset('imgs/missions/' ~ missNotOver[i].image) }}\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\talt=\"Image de {{ missNotOver[i].name }}\"/>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>{{ missNotOver[i].name }}</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t<div class=\"progress\">
  \t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar
  \t\t\t\t\t\t\t\t\t\t\t\t\t{% if missNotOver[i].pdv >= 50 %}
  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-success 
  \t\t\t\t\t\t\t\t\t\t\t\t\t{% else %}
  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-danger
  \t\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}\"
  \t\t\t\t\t\t\t\t\t\t\t role=\"progressbar\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuenow=\"{{ missNotOver[i].pdv }}\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{ missNotOver[i].pdv }}%\"> 
  \t\t\t\t\t\t\t\t\t\t</div>
     \t\t\t\t\t\t\t\t\t<div class=\"progress-bar-title\">{{ missNotOver[i].pdv }}</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t{% set eaten = missNotOver[i].hasEaten %}
\t\t\t\t\t\t\t\t\t{% if eaten == 0 %}
\t\t\t\t\t\t\t\t\t\tAucun<br><a href={{ path('veterinary') }} class=\"btn btn-link\">Nourrir</a>
\t\t\t\t\t\t\t\t\t{% elseif eaten == 1 %}
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('imgs/croquette_bronze.png') }}\" height=\"30\" width=\"30\">
\t\t\t\t\t\t\t\t\t{% elseif eaten == 2 %}
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('imgs/croquette_argent.png') }}\" height=\"30\" width=\"30\">
\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\t<img src=\"{{ asset('imgs/croquette_or.png') }}\" height=\"30\" width=\"30\">
\t\t\t\t\t\t\t\t\t{% endif %}\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>{{ missNotOver[i].endDate|date(\"d/m/Y à H:i\") }}</td>
\t\t\t\t\t\t\t\t<td class=\"progress_td\">
\t\t\t\t\t\t\t\t\t{% set finishedAt = missNotOver[i].finishedAt|number_format %}
\t\t\t\t\t\t\t\t\t<div class=\"progress\">
  \t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar progress-bar-info progress-bar-striped active\" role=\"progressbar\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuenow=\"{{ finishedAt }}\"
  \t\t\t\t\t\t\t\t\t\t\t aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{ finishedAt }}%\"> 
  \t\t\t\t\t\t\t\t\t\t</div>
  \t\t\t\t\t\t\t\t\t\t <div class=\"progress-bar-title\">Terminée à {{ finishedAt }}%</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>

\t\t\t\t{% if missOver is not empty %}
\t\t\t\t<div id=\"miss_over\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th rowspan=\"2\"></th>
\t\t\t\t\t        <th rowspan=\"2\">Nom</th>
\t\t\t\t\t        <th rowspan=\"2\">Santé</th>
\t\t\t\t\t      \t<th colspan=\"2\">Récompenses</th>
\t\t\t\t\t      \t<th rowspan=\"2\" class=\"date_th\">Terminée le</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t      <tr>
\t\t\t\t\t      \t<th class=\"rewards_th\">Or</th>
\t\t\t\t\t        <th class=\"rewards_th\">Reputation</th>
\t\t\t\t\t      </tr>
\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t     \t{% for i in 0..missOver|length -1 %}
\t\t\t\t\t\t\t<tr id=\"miss_over-{{ i }}\" class=\"mission_over active\">
\t\t\t\t\t\t\t\t<td class=\"img_th\"><img class=\"imgMission\" src=\"{{ asset('imgs/missions/' ~ missOver[i].image) }}\"
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\talt=\"Image de {{ missOver[i].name }}\"</img></td>
\t\t\t\t\t\t\t\t<td>{{ missOver[i].name }}</td>
\t\t\t\t\t\t\t\t<td class=\"progress_td\">
\t\t\t\t\t\t\t\t\t<div class=\"progress\">
\t\t\t\t\t\t\t\t\t\t<div class=\"progress-bar
  \t\t\t\t\t\t\t\t\t\t\t\t\t{% if missOver[i].pdv >= 50 %}
  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-success 
  \t\t\t\t\t\t\t\t\t\t\t\t\t{% else %}
  \t\t\t\t\t\t\t\t\t\t\t\t\t\tprogress-bar-danger
  \t\t\t\t\t\t\t\t\t\t\t\t\t{% endif %}\"
  \t\t\t\t\t\t\t\t\t\t\t role=\"progressbar\"
\t\t  \t\t\t\t\t\t\t\t\t aria-valuenow=\"{{ missOver[i].pdv }}\"
\t\t  \t\t\t\t\t\t\t\t\t aria-valuemin=\"0\" 
\t\t  \t\t\t\t\t\t\t\t\t aria-valuemax=\"100\" 
\t\t  \t\t\t\t\t\t\t\t\t style=\"width:{{ missOver[i].pdv }}%\"> 
\t\t  \t\t\t\t\t\t\t\t</div>
\t\t     \t\t\t\t\t\t\t<div class=\"progress-bar-title\">{{ missOver[i].pdv }}</div>
\t\t     \t\t\t\t\t\t</div>
\t     \t\t\t\t\t\t</td>
\t     \t\t\t\t\t\t<td>{{ missOver[i].gold }}</td>
\t\t\t\t\t\t\t\t<td>{{ missOver[i].reputation }}</td>
\t\t\t\t\t\t\t\t<td class=\"date_th\">{{ missOver[i].endDate|date(\"d/m/Y à H:i\") }}</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t\t{% endif %}
\t\t\t</div>
\t\t</div>
</div>


<div class=\"row\">
\t\t<div id=\"events\" class=\"col-sm-8 col-sm-push-2\"> 
\t\t\t<div id=\"events_header\">
\t\t\t\t<h4>Evènements en cours</h4>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t{% if events|length != 0 %}
\t\t\t\t\t<div class=\"panel-group\" id=\"accordion\">
\t\t\t\t\t{% for i in 0..events|length-1 %}
\t\t\t\t\t\t<div class=\"panel panel-default\">
\t\t\t\t\t\t    <div class=\"panel-heading\">
\t\t\t\t\t\t      <p class=\"panel-title\">
\t\t\t\t\t\t        <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse{{ i }}\">
\t\t\t\t\t\t        {{ events[i].name }}</a>
\t\t\t\t\t\t      </p>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t    <div id=\"collapse{{ i }}\" class=\"panel-collapse collapse
\t\t\t\t\t\t    \t\t\t\t\t\t\t\t{% if loop.first %} in {% endif %}\">
\t\t\t\t\t\t      <div class=\"panel-body\">
\t\t\t\t\t\t\t\t\t<p>Du <strong>{{ events[i].start|date(\"d/m/Y H:i\") }}</strong>
\t\t\t\t\t\t\t\t\t au <strong>{{ events[i].end|date(\"d/m/Y H:i\") }}</strong></p>
\t\t\t\t\t\t\t\t   <p>{{ events[i].description }}</p>
\t\t\t\t\t\t      </div>
\t\t\t\t\t\t    </div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t</div>
\t\t\t\t{% else %}
\t\t\t\t\t<p>Aucun évènement en cours</p>
\t\t\t\t{% endif %}
\t\t\t</div>
\t\t</div>

\t</div>

<script> 
\t{% if missOver is empty %}
\t\t\$(\"#miss_not_over\").addClass(\"active\");\t
\t\t\$(\"#miss_not_over_pill\").addClass(\"active\");\t
\t{% else %}
\t\t\$(\"#miss_over\").addClass(\"active\");\t
\t\t\$(\"#miss_over_pill\").addClass(\"active\");
\t{% endif %}
</script>
{% endblock %}", "pension.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\pension.html.twig");
    }
}
