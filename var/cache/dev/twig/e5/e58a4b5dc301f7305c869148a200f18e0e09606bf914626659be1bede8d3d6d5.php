<?php

/* TwigBundle:Exception:error.xml.twig */
class __TwigTemplate_7cc0870a24199929c5327a483a7cb47707280eec64b9f2c2decebbf8fcf32121 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f57750906e990cff504952bab89f0bfa20dc62b0654f000a51b5e77113538992 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f57750906e990cff504952bab89f0bfa20dc62b0654f000a51b5e77113538992->enter($__internal_f57750906e990cff504952bab89f0bfa20dc62b0654f000a51b5e77113538992_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        $__internal_502046f18f649db10066bc7e6862b2763c0bade2822e1dd78d22135758048037 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_502046f18f649db10066bc7e6862b2763c0bade2822e1dd78d22135758048037->enter($__internal_502046f18f649db10066bc7e6862b2763c0bade2822e1dd78d22135758048037_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_f57750906e990cff504952bab89f0bfa20dc62b0654f000a51b5e77113538992->leave($__internal_f57750906e990cff504952bab89f0bfa20dc62b0654f000a51b5e77113538992_prof);

        
        $__internal_502046f18f649db10066bc7e6862b2763c0bade2822e1dd78d22135758048037->leave($__internal_502046f18f649db10066bc7e6862b2763c0bade2822e1dd78d22135758048037_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "TwigBundle:Exception:error.xml.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.xml.twig");
    }
}
