<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_3e6f28310adbc4d7f52f7a2cf1fb81b621c3bb2a519c471c826228f40a56d0cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1abe7b99fc3e0b8d31ab09a6ff3cfbbdcc6a10d35529f22bc6c585c83ee8aaff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1abe7b99fc3e0b8d31ab09a6ff3cfbbdcc6a10d35529f22bc6c585c83ee8aaff->enter($__internal_1abe7b99fc3e0b8d31ab09a6ff3cfbbdcc6a10d35529f22bc6c585c83ee8aaff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_074dfa7d72d5549893524fda404ffaa110817fde06208d6044aca85fa1aedb31 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_074dfa7d72d5549893524fda404ffaa110817fde06208d6044aca85fa1aedb31->enter($__internal_074dfa7d72d5549893524fda404ffaa110817fde06208d6044aca85fa1aedb31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/error.xml.twig", "TwigBundle:Exception:error.rdf.twig", 1)->display($context);
        
        $__internal_1abe7b99fc3e0b8d31ab09a6ff3cfbbdcc6a10d35529f22bc6c585c83ee8aaff->leave($__internal_1abe7b99fc3e0b8d31ab09a6ff3cfbbdcc6a10d35529f22bc6c585c83ee8aaff_prof);

        
        $__internal_074dfa7d72d5549893524fda404ffaa110817fde06208d6044aca85fa1aedb31->leave($__internal_074dfa7d72d5549893524fda404ffaa110817fde06208d6044aca85fa1aedb31_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/error.xml.twig' %}
", "TwigBundle:Exception:error.rdf.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
