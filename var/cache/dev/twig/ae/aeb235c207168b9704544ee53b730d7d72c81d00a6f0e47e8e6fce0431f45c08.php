<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_859f0b22e3e28305968376c3b9dbda370c2c0f707db15510c90bf548c4cb049e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41313767fb6951d3ae4b8f52dc9656354b3089501b95a94add80a65cbf88e225 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41313767fb6951d3ae4b8f52dc9656354b3089501b95a94add80a65cbf88e225->enter($__internal_41313767fb6951d3ae4b8f52dc9656354b3089501b95a94add80a65cbf88e225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_4b6484c73869a50aeece1348a3cb66043f6e8b552e462b1a4eb833d44e97e125 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b6484c73869a50aeece1348a3cb66043f6e8b552e462b1a4eb833d44e97e125->enter($__internal_4b6484c73869a50aeece1348a3cb66043f6e8b552e462b1a4eb833d44e97e125_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_41313767fb6951d3ae4b8f52dc9656354b3089501b95a94add80a65cbf88e225->leave($__internal_41313767fb6951d3ae4b8f52dc9656354b3089501b95a94add80a65cbf88e225_prof);

        
        $__internal_4b6484c73869a50aeece1348a3cb66043f6e8b552e462b1a4eb833d44e97e125->leave($__internal_4b6484c73869a50aeece1348a3cb66043f6e8b552e462b1a4eb833d44e97e125_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
