<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_97984f7cbd307ecd492bc706ee8a20d316cd77b4d7ddb8bd657bb2a9f5e08be9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0e67349d84d6a8f9b71fa0badf51907af3e1ebc83fcff8b710b51d3d016df4a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0e67349d84d6a8f9b71fa0badf51907af3e1ebc83fcff8b710b51d3d016df4a->enter($__internal_f0e67349d84d6a8f9b71fa0badf51907af3e1ebc83fcff8b710b51d3d016df4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_1efbde2dfd052c4bcfbd8786d29c12d5a08b3b16d055c42cbf0202d0f515c1b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1efbde2dfd052c4bcfbd8786d29c12d5a08b3b16d055c42cbf0202d0f515c1b9->enter($__internal_1efbde2dfd052c4bcfbd8786d29c12d5a08b3b16d055c42cbf0202d0f515c1b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_f0e67349d84d6a8f9b71fa0badf51907af3e1ebc83fcff8b710b51d3d016df4a->leave($__internal_f0e67349d84d6a8f9b71fa0badf51907af3e1ebc83fcff8b710b51d3d016df4a_prof);

        
        $__internal_1efbde2dfd052c4bcfbd8786d29c12d5a08b3b16d055c42cbf0202d0f515c1b9->leave($__internal_1efbde2dfd052c4bcfbd8786d29c12d5a08b3b16d055c42cbf0202d0f515c1b9_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
