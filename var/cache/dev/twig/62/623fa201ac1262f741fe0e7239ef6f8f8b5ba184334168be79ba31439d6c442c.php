<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_762a93268a56f27f40200fd7c3651e058eca5b9f7b583d66525805b15cc913af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_008b4fdeba16b9f7db055cb34c2a7cc5032e2c53b584a0fda8a75fddd55a845d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_008b4fdeba16b9f7db055cb34c2a7cc5032e2c53b584a0fda8a75fddd55a845d->enter($__internal_008b4fdeba16b9f7db055cb34c2a7cc5032e2c53b584a0fda8a75fddd55a845d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_e68a92d200c3bbab38ba3770a0179cd30f957a97659f8b29a2a82083f6658585 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e68a92d200c3bbab38ba3770a0179cd30f957a97659f8b29a2a82083f6658585->enter($__internal_e68a92d200c3bbab38ba3770a0179cd30f957a97659f8b29a2a82083f6658585_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_008b4fdeba16b9f7db055cb34c2a7cc5032e2c53b584a0fda8a75fddd55a845d->leave($__internal_008b4fdeba16b9f7db055cb34c2a7cc5032e2c53b584a0fda8a75fddd55a845d_prof);

        
        $__internal_e68a92d200c3bbab38ba3770a0179cd30f957a97659f8b29a2a82083f6658585->leave($__internal_e68a92d200c3bbab38ba3770a0179cd30f957a97659f8b29a2a82083f6658585_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
