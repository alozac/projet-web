<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_55de452d8b0179e832f487fd94f505ddb8c3005b997e0964d83add24b52ffcc9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_486a8806228e7a4cc49eb8087f9ea16717ffd7cdd799e34bd13e11206633b709 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_486a8806228e7a4cc49eb8087f9ea16717ffd7cdd799e34bd13e11206633b709->enter($__internal_486a8806228e7a4cc49eb8087f9ea16717ffd7cdd799e34bd13e11206633b709_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_b03431b5a915783db2ca9febfa809d3d742e3ddff93108cd4c944ca8ed05d723 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b03431b5a915783db2ca9febfa809d3d742e3ddff93108cd4c944ca8ed05d723->enter($__internal_b03431b5a915783db2ca9febfa809d3d742e3ddff93108cd4c944ca8ed05d723_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_486a8806228e7a4cc49eb8087f9ea16717ffd7cdd799e34bd13e11206633b709->leave($__internal_486a8806228e7a4cc49eb8087f9ea16717ffd7cdd799e34bd13e11206633b709_prof);

        
        $__internal_b03431b5a915783db2ca9febfa809d3d742e3ddff93108cd4c944ca8ed05d723->leave($__internal_b03431b5a915783db2ca9febfa809d3d742e3ddff93108cd4c944ca8ed05d723_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
