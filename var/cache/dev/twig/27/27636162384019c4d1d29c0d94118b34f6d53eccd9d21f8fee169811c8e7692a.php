<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_96c2ec46c11c02a7768ed9536cd1e5900e47430e64c419d39efdd5e9937d4617 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b367fc59c01a15a94a2d6c199a04bc4e422ebfd22d07dd46c7c375c9af55793 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7b367fc59c01a15a94a2d6c199a04bc4e422ebfd22d07dd46c7c375c9af55793->enter($__internal_7b367fc59c01a15a94a2d6c199a04bc4e422ebfd22d07dd46c7c375c9af55793_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_e2f5c6e06719663d792f10d2b45557b8fec24f2485dcc6e5a3fed9c99cdd0ee4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2f5c6e06719663d792f10d2b45557b8fec24f2485dcc6e5a3fed9c99cdd0ee4->enter($__internal_e2f5c6e06719663d792f10d2b45557b8fec24f2485dcc6e5a3fed9c99cdd0ee4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_7b367fc59c01a15a94a2d6c199a04bc4e422ebfd22d07dd46c7c375c9af55793->leave($__internal_7b367fc59c01a15a94a2d6c199a04bc4e422ebfd22d07dd46c7c375c9af55793_prof);

        
        $__internal_e2f5c6e06719663d792f10d2b45557b8fec24f2485dcc6e5a3fed9c99cdd0ee4->leave($__internal_e2f5c6e06719663d792f10d2b45557b8fec24f2485dcc6e5a3fed9c99cdd0ee4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
