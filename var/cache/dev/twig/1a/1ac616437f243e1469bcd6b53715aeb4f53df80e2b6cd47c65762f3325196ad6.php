<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_70611e36ef8af73fbdfb2e9a4ba9b0c9a85964ec781a1189ec87c16cf8963b29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b08a6590764b765ce050a0693bbce7c0600c7cdfea08f80b4a743cc78c45d6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b08a6590764b765ce050a0693bbce7c0600c7cdfea08f80b4a743cc78c45d6a->enter($__internal_5b08a6590764b765ce050a0693bbce7c0600c7cdfea08f80b4a743cc78c45d6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_f3dd67300dd6dff750c1e077e4e352d00643d5d94749d507ee642400e6677048 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3dd67300dd6dff750c1e077e4e352d00643d5d94749d507ee642400e6677048->enter($__internal_f3dd67300dd6dff750c1e077e4e352d00643d5d94749d507ee642400e6677048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b08a6590764b765ce050a0693bbce7c0600c7cdfea08f80b4a743cc78c45d6a->leave($__internal_5b08a6590764b765ce050a0693bbce7c0600c7cdfea08f80b4a743cc78c45d6a_prof);

        
        $__internal_f3dd67300dd6dff750c1e077e4e352d00643d5d94749d507ee642400e6677048->leave($__internal_f3dd67300dd6dff750c1e077e4e352d00643d5d94749d507ee642400e6677048_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3f669dfd729178ce8e00bec977f0b94051cacbb32c19bf29705420241319837d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f669dfd729178ce8e00bec977f0b94051cacbb32c19bf29705420241319837d->enter($__internal_3f669dfd729178ce8e00bec977f0b94051cacbb32c19bf29705420241319837d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_2c00b6697dba7529e838530e67acb995ac2e4611c33a08637d1aff41c175a322 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c00b6697dba7529e838530e67acb995ac2e4611c33a08637d1aff41c175a322->enter($__internal_2c00b6697dba7529e838530e67acb995ac2e4611c33a08637d1aff41c175a322_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_2c00b6697dba7529e838530e67acb995ac2e4611c33a08637d1aff41c175a322->leave($__internal_2c00b6697dba7529e838530e67acb995ac2e4611c33a08637d1aff41c175a322_prof);

        
        $__internal_3f669dfd729178ce8e00bec977f0b94051cacbb32c19bf29705420241319837d->leave($__internal_3f669dfd729178ce8e00bec977f0b94051cacbb32c19bf29705420241319837d_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_b8fda69e66a149fcbe87c8882380a42f53d7de6b585a32add7ac935e146cba31 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8fda69e66a149fcbe87c8882380a42f53d7de6b585a32add7ac935e146cba31->enter($__internal_b8fda69e66a149fcbe87c8882380a42f53d7de6b585a32add7ac935e146cba31_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_f6f1fbb5f623473ff6592f39754008d8ef115deeeb23cb5ac61faf7415a725c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f6f1fbb5f623473ff6592f39754008d8ef115deeeb23cb5ac61faf7415a725c6->enter($__internal_f6f1fbb5f623473ff6592f39754008d8ef115deeeb23cb5ac61faf7415a725c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["exception"] ?? $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_f6f1fbb5f623473ff6592f39754008d8ef115deeeb23cb5ac61faf7415a725c6->leave($__internal_f6f1fbb5f623473ff6592f39754008d8ef115deeeb23cb5ac61faf7415a725c6_prof);

        
        $__internal_b8fda69e66a149fcbe87c8882380a42f53d7de6b585a32add7ac935e146cba31->leave($__internal_b8fda69e66a149fcbe87c8882380a42f53d7de6b585a32add7ac935e146cba31_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_ad1ca7165aa1a2915f68ed267c7d63a9f804ce1de91038857bea694139785d0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad1ca7165aa1a2915f68ed267c7d63a9f804ce1de91038857bea694139785d0f->enter($__internal_ad1ca7165aa1a2915f68ed267c7d63a9f804ce1de91038857bea694139785d0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_884cfeae64c0343aae93d4f44f3943d7ec7ac3f525adc50498fd301deb45398a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_884cfeae64c0343aae93d4f44f3943d7ec7ac3f525adc50498fd301deb45398a->enter($__internal_884cfeae64c0343aae93d4f44f3943d7ec7ac3f525adc50498fd301deb45398a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_884cfeae64c0343aae93d4f44f3943d7ec7ac3f525adc50498fd301deb45398a->leave($__internal_884cfeae64c0343aae93d4f44f3943d7ec7ac3f525adc50498fd301deb45398a_prof);

        
        $__internal_ad1ca7165aa1a2915f68ed267c7d63a9f804ce1de91038857bea694139785d0f->leave($__internal_ad1ca7165aa1a2915f68ed267c7d63a9f804ce1de91038857bea694139785d0f_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
