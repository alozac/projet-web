<?php

/* post/post_consult.html.twig */
class __TwigTemplate_670c75efc4ce116600ffb190a6e7c2adef037aaee7c4b6c1b034dcf562051614 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "post/post_consult.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_772fc8eece8e77185372302acebe4a5f1e9a76589d79ee133937a44989626ef7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_772fc8eece8e77185372302acebe4a5f1e9a76589d79ee133937a44989626ef7->enter($__internal_772fc8eece8e77185372302acebe4a5f1e9a76589d79ee133937a44989626ef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_consult.html.twig"));

        $__internal_a1a50689a2bfd7ce462aeb5bb1dd129e620020bb00fed1d3167f586f233e6dd1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1a50689a2bfd7ce462aeb5bb1dd129e620020bb00fed1d3167f586f233e6dd1->enter($__internal_a1a50689a2bfd7ce462aeb5bb1dd129e620020bb00fed1d3167f586f233e6dd1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_consult.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_772fc8eece8e77185372302acebe4a5f1e9a76589d79ee133937a44989626ef7->leave($__internal_772fc8eece8e77185372302acebe4a5f1e9a76589d79ee133937a44989626ef7_prof);

        
        $__internal_a1a50689a2bfd7ce462aeb5bb1dd129e620020bb00fed1d3167f586f233e6dd1->leave($__internal_a1a50689a2bfd7ce462aeb5bb1dd129e620020bb00fed1d3167f586f233e6dd1_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_e998438359e2dcd4ff52cea0e6224dfb9815f05334c2ac9684784593065058f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e998438359e2dcd4ff52cea0e6224dfb9815f05334c2ac9684784593065058f4->enter($__internal_e998438359e2dcd4ff52cea0e6224dfb9815f05334c2ac9684784593065058f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_296d84a720eb684db39e497694963dde4ab2cc1ae03a6d27209fe69ebd780f36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_296d84a720eb684db39e497694963dde4ab2cc1ae03a6d27209fe69ebd780f36->enter($__internal_296d84a720eb684db39e497694963dde4ab2cc1ae03a6d27209fe69ebd780f36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_consult.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_296d84a720eb684db39e497694963dde4ab2cc1ae03a6d27209fe69ebd780f36->leave($__internal_296d84a720eb684db39e497694963dde4ab2cc1ae03a6d27209fe69ebd780f36_prof);

        
        $__internal_e998438359e2dcd4ff52cea0e6224dfb9815f05334c2ac9684784593065058f4->leave($__internal_e998438359e2dcd4ff52cea0e6224dfb9815f05334c2ac9684784593065058f4_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_f34caf1a9ce0faa3d04b1f3c3c6ef05cedfd2b9ca3fb8a529013993979d42590 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f34caf1a9ce0faa3d04b1f3c3c6ef05cedfd2b9ca3fb8a529013993979d42590->enter($__internal_f34caf1a9ce0faa3d04b1f3c3c6ef05cedfd2b9ca3fb8a529013993979d42590_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_e8b263f3e96c439729ab4088a698a880347cf5de869c1f10b11b36d09c88ab6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8b263f3e96c439729ab4088a698a880347cf5de869c1f10b11b36d09c88ab6c->enter($__internal_e8b263f3e96c439729ab4088a698a880347cf5de869c1f10b11b36d09c88ab6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h3>Messagerie</h3>
</div>


\t<div class=\"row\">
\t\t<div class=\"list_mess  col-sm-6\">
\t\t\t<ul id=\"mess_rec_pill\" class=\"nav nav-pills\">
\t\t  \t\t<li class=\"active mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_rec\">Recus</a>
\t\t  \t\t</li>
\t\t  \t\t<li id=\"mess_sent_pill\" class=\"mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_sent\">Envoyés</a>
\t\t  \t\t</li>
\t\t\t</ul>
\t\t\t
\t\t\t<div class=\"tab-content\">
\t\t\t\t<div id=\"list_mess_rec\" class=\"tab-pane fade in active\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Expéditeur</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    ";
        // line 37
        if ( !twig_test_empty(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")))) {
            // line 38
            echo "\t\t\t\t\t     ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["messagesRec"] ?? $this->getContext($context, "messagesRec"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 39
                echo "\t\t\t\t\t\t\t<tr id=\"mess_rec-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"message_rec link active\">
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t";
                // line 41
                if ( !(null === $this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "sender", array()))) {
                    // line 42
                    echo "\t\t\t\t\t\t\t\t\t\t";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "sender", array()), "name", array()), "html", null, true);
                    echo "
\t\t\t\t\t\t\t\t\t";
                } else {
                    // line 44
                    echo "\t\t\t\t\t\t\t\t\t\tUtilisateur supprimé
\t\t\t\t\t\t\t\t\t";
                }
                // line 46
                echo "\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>";
                // line 47
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "object", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 48
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")), $context["i"], array(), "array"), "date", array()), "d/m/Y à H:i"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "\t\t\t\t\t\t";
        }
        // line 52
        echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t\t<div id=\"list_mess_sent\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Destinataire</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    ";
        // line 65
        if ( !twig_test_empty(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")))) {
            // line 66
            echo "\t\t\t\t\t     ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["messagesSent"] ?? $this->getContext($context, "messagesSent"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 67
                echo "
\t\t\t\t\t     \t";
                // line 68
                $context["toAll"] = (( !(null === $this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "receiver", array())) && ($this->getAttribute($this->getAttribute($this->getAttribute(                // line 69
($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "receiver", array()), "name", array()) == $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "lastPension", array()), "name", array()))) && $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN"));
                // line 71
                echo "\t\t\t\t\t\t\t<tr id=\"mess_sent-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"message_sent link active\">
\t\t\t\t\t\t\t\t";
                // line 72
                if (($context["toAll"] ?? $this->getContext($context, "toAll"))) {
                    // line 73
                    echo "\t\t\t\t\t\t\t\t\t<td>Toutes les pensions</td>
\t\t\t\t\t\t\t\t";
                } elseif ( !(null === $this->getAttribute($this->getAttribute(                // line 74
($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "receiver", array()))) {
                    // line 75
                    echo "\t\t\t\t\t\t\t\t\t<td>";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "receiver", array()), "name", array()), "html", null, true);
                    echo "</td>
\t\t\t\t\t\t\t\t";
                } else {
                    // line 77
                    echo "\t\t\t\t\t\t\t\t\t<td>Utilisateur supprimé</td>
\t\t\t\t\t\t\t\t";
                }
                // line 79
                echo "\t\t\t\t\t\t\t\t<td>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "object", array()), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t\t<td>";
                // line 80
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($this->getAttribute(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")), $context["i"], array(), "array"), "date", array()), "d/m/Y à H:i"), "html", null, true);
                echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 83
            echo "\t\t\t\t\t\t";
        }
        // line 84
        echo "\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"mess_view\" class=\"col-sm-6\">
\t\t\t<p>Sélectionnez un message à lire</p>
\t\t</div>
\t</div>

\t<a href=\"";
        // line 95
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_write", array("id" => 0));
        echo "\" role=\"button\" id=\"answer\" class=\"chgBut btn btn-info btn-sm disabled\">Répondre</a>
\t<a href=\"";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_remove", array("id" => 0));
        echo "\" role=\"button\" id=\"remove\" class=\"chgBut btn btn-info btn-sm disabled\">Supprimer</a>
\t<a href=\"";
        // line 97
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_write");
        echo "\" role=\"button\" class=\"btn btn-info btn-sm\">Nouveau</a>

\t<script>

 \tvar messagesRec  = ";
        // line 101
        echo twig_jsonencode_filter(($context["messagesRec"] ?? $this->getContext($context, "messagesRec")));
        echo ";
  \tvar messagesSent = ";
        // line 102
        echo twig_jsonencode_filter(($context["messagesSent"] ?? $this->getContext($context, "messagesSent")));
        echo ";

\t\$(\".message_rec\").click(function(){
\t\t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t 
\t\tvar newMess = messagesRec[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefAns = \$(\"#answer\").attr(\"href\");
\t\t\$(\"#answer\").attr(\"href\", hrefAns.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".message_sent\").click(function(){
\t \t\$(\".message_sent\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t\t
\t\tvar newMess = messagesSent[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".mess_pill\").click(function(){
\t\t\$(\".chgBut\").addClass(\"disabled\");
\t \t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(\".message_sent\").removeClass(\"success\");
\t});
</script>
";
        
        $__internal_e8b263f3e96c439729ab4088a698a880347cf5de869c1f10b11b36d09c88ab6c->leave($__internal_e8b263f3e96c439729ab4088a698a880347cf5de869c1f10b11b36d09c88ab6c_prof);

        
        $__internal_f34caf1a9ce0faa3d04b1f3c3c6ef05cedfd2b9ca3fb8a529013993979d42590->leave($__internal_f34caf1a9ce0faa3d04b1f3c3c6ef05cedfd2b9ca3fb8a529013993979d42590_prof);

    }

    public function getTemplateName()
    {
        return "post/post_consult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 102,  245 => 101,  238 => 97,  234 => 96,  230 => 95,  217 => 84,  214 => 83,  205 => 80,  200 => 79,  196 => 77,  190 => 75,  188 => 74,  185 => 73,  183 => 72,  178 => 71,  176 => 69,  175 => 68,  172 => 67,  167 => 66,  165 => 65,  150 => 52,  147 => 51,  138 => 48,  134 => 47,  131 => 46,  127 => 44,  121 => 42,  119 => 41,  113 => 39,  108 => 38,  106 => 37,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/post_consult.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Messagerie</h3>
</div>


\t<div class=\"row\">
\t\t<div class=\"list_mess  col-sm-6\">
\t\t\t<ul id=\"mess_rec_pill\" class=\"nav nav-pills\">
\t\t  \t\t<li class=\"active mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_rec\">Recus</a>
\t\t  \t\t</li>
\t\t  \t\t<li id=\"mess_sent_pill\" class=\"mess_pill\">
\t\t  \t\t\t<a data-toggle=\"pill\" href=\"#list_mess_sent\">Envoyés</a>
\t\t  \t\t</li>
\t\t\t</ul>
\t\t\t
\t\t\t<div class=\"tab-content\">
\t\t\t\t<div id=\"list_mess_rec\" class=\"tab-pane fade in active\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Expéditeur</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    {% if messagesRec is not empty %}
\t\t\t\t\t     {% for i in 0..messagesRec|length -1 %}
\t\t\t\t\t\t\t<tr id=\"mess_rec-{{ i }}\" class=\"message_rec link active\">
\t\t\t\t\t\t\t\t<td>
\t\t\t\t\t\t\t\t\t{% if  messagesRec[i].sender is not null %}
\t\t\t\t\t\t\t\t\t\t{{ messagesRec[i].sender.name }}
\t\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t\tUtilisateur supprimé
\t\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>{{ messagesRec[i].object }}</td>
\t\t\t\t\t\t\t\t<td>{{ messagesRec[i].date|date(\"d/m/Y à H:i\") }}</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t\t<div id=\"list_mess_sent\" class=\"tab-pane fade in\">
\t\t\t\t\t<table class=\"table table-hover table-bordered\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t      <tr>
\t\t\t\t\t        <th>Destinataire</th>
\t\t\t\t\t        <th>Objet</th>
\t\t\t\t\t        <th>Date</th>
\t\t\t\t\t      </tr>
\t\t\t\t\t    </thead>
\t\t\t\t\t    <tbody>
\t\t\t\t\t    {% if messagesSent is not empty %}
\t\t\t\t\t     {% for i in 0..messagesSent|length -1 %}

\t\t\t\t\t     \t{% set toAll = messagesSent[i].receiver is not null 
\t\t\t\t\t     \t\t\tand messagesSent[i].receiver.name == app.user.lastPension.name
\t\t\t\t\t     \t\t\tand is_granted('ROLE_ADMIN') %}
\t\t\t\t\t\t\t<tr id=\"mess_sent-{{ i }}\" class=\"message_sent link active\">
\t\t\t\t\t\t\t\t{% if toAll %}
\t\t\t\t\t\t\t\t\t<td>Toutes les pensions</td>
\t\t\t\t\t\t\t\t{% elseif messagesSent[i].receiver is not null %}
\t\t\t\t\t\t\t\t\t<td>{{ messagesSent[i].receiver.name }}</td>
\t\t\t\t\t\t\t\t{% else %}
\t\t\t\t\t\t\t\t\t<td>Utilisateur supprimé</td>
\t\t\t\t\t\t\t\t{% endif %}
\t\t\t\t\t\t\t\t<td>{{ messagesSent[i].object }}</td>
\t\t\t\t\t\t\t\t<td>{{ messagesSent[i].date|date(\"d/m/Y à H:i\") }}</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t{% endfor %}
\t\t\t\t\t\t{% endif %}
\t\t\t\t\t    </tbody>
\t\t\t\t \t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"mess_view\" class=\"col-sm-6\">
\t\t\t<p>Sélectionnez un message à lire</p>
\t\t</div>
\t</div>

\t<a href=\"{{ path('post_write',  { 'id' : 0 }) }}\" role=\"button\" id=\"answer\" class=\"chgBut btn btn-info btn-sm disabled\">Répondre</a>
\t<a href=\"{{ path('post_remove', { 'id' : 0 }) }}\" role=\"button\" id=\"remove\" class=\"chgBut btn btn-info btn-sm disabled\">Supprimer</a>
\t<a href=\"{{ path('post_write') }}\" role=\"button\" class=\"btn btn-info btn-sm\">Nouveau</a>

\t<script>

 \tvar messagesRec  = {{ messagesRec|json_encode|raw }};
  \tvar messagesSent = {{ messagesSent|json_encode|raw }};

\t\$(\".message_rec\").click(function(){
\t\t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t 
\t\tvar newMess = messagesRec[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefAns = \$(\"#answer\").attr(\"href\");
\t\t\$(\"#answer\").attr(\"href\", hrefAns.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".message_sent\").click(function(){
\t \t\$(\".message_sent\").removeClass(\"success\");
\t\t\$(this).addClass(\"success\");
\t\t
\t\tvar newMess = messagesSent[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefRm = \$(\"#remove\").attr(\"href\");
\t\t\$(\"#remove\").attr(\"href\", hrefRm.replace(/[0-9]+/, newMess.id))
\t\t\t\t\t.removeClass(\"disabled\");

\t\t\$('#mess_view').html(\"<p>Message :</p><p>\" + newMess.message + \"</p>\");
\t});

\t\$(\".mess_pill\").click(function(){
\t\t\$(\".chgBut\").addClass(\"disabled\");
\t \t\$(\".message_rec\").removeClass(\"success\");
\t\t\$(\".message_sent\").removeClass(\"success\");
\t});
</script>
{% endblock %}", "post/post_consult.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\post\\post_consult.html.twig");
    }
}
