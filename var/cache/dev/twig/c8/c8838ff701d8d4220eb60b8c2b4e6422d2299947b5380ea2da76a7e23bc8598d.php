<?php

/* administration/modify_event.html.twig */
class __TwigTemplate_6b94102ffee470b39bd4ce3883cc958213e6d172f2054014f23c9d764fb9c7a2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("administration/homeAdmin.html.twig", "administration/modify_event.html.twig", 1);
        $this->blocks = array(
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "administration/homeAdmin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9298e3e10b1b9fce09e17171192d0b420ef7695716de5f051d483487d1dff8d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9298e3e10b1b9fce09e17171192d0b420ef7695716de5f051d483487d1dff8d4->enter($__internal_9298e3e10b1b9fce09e17171192d0b420ef7695716de5f051d483487d1dff8d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/modify_event.html.twig"));

        $__internal_593a4b897bd57bd58bdfb57c45aebcca70964920a57cb8c753599f2f88bfebee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_593a4b897bd57bd58bdfb57c45aebcca70964920a57cb8c753599f2f88bfebee->enter($__internal_593a4b897bd57bd58bdfb57c45aebcca70964920a57cb8c753599f2f88bfebee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "administration/modify_event.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9298e3e10b1b9fce09e17171192d0b420ef7695716de5f051d483487d1dff8d4->leave($__internal_9298e3e10b1b9fce09e17171192d0b420ef7695716de5f051d483487d1dff8d4_prof);

        
        $__internal_593a4b897bd57bd58bdfb57c45aebcca70964920a57cb8c753599f2f88bfebee->leave($__internal_593a4b897bd57bd58bdfb57c45aebcca70964920a57cb8c753599f2f88bfebee_prof);

    }

    // line 3
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_72ab48e7e9d96596a183360b881e8ec77c48c7b271640a55bf81192b08192f99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72ab48e7e9d96596a183360b881e8ec77c48c7b271640a55bf81192b08192f99->enter($__internal_72ab48e7e9d96596a183360b881e8ec77c48c7b271640a55bf81192b08192f99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_32a74e478d0e39513be33e938d9f7aff68258725857f973ea8dc0887fd57dbd4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32a74e478d0e39513be33e938d9f7aff68258725857f973ea8dc0887fd57dbd4->enter($__internal_32a74e478d0e39513be33e938d9f7aff68258725857f973ea8dc0887fd57dbd4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 4
        echo "\t";
        $this->displayParentBlock("main_screen", $context, $blocks);
        echo "
    
\t";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
\t";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "name", array()), 'row');
        echo "
\t";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "description", array()), 'row');
        echo "

\t<fieldset>
\t    <legend>Bonus</legend>
\t\t    <div class=\"form-inline\">
\t\t\t\t";
        // line 13
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "bonusGold", array()), 'row');
        echo "
\t\t\t\t";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "bonusReputation", array()), 'row');
        echo "
\t\t\t</div>
\t</fieldset>

\t<fieldset>
\t    <legend>Réductions</legend>
\t    \t<div class=\"form-inline\">
\t  \t\t  \t";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "salesGold", array()), 'row');
        echo "
\t    \t</div>
\t</fieldset>
\t
\t";
        // line 25
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "start", array()), 'row');
        echo "
\t";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "end", array()), 'row');
        echo "

\t";
        // line 28
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "create", array()), 'row', array("attr" => array("class" => "btn-primary")));
        echo "
\t";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "_token", array()), 'row');
        echo "
\t";
        // line 30
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "

";
        
        $__internal_32a74e478d0e39513be33e938d9f7aff68258725857f973ea8dc0887fd57dbd4->leave($__internal_32a74e478d0e39513be33e938d9f7aff68258725857f973ea8dc0887fd57dbd4_prof);

        
        $__internal_72ab48e7e9d96596a183360b881e8ec77c48c7b271640a55bf81192b08192f99->leave($__internal_72ab48e7e9d96596a183360b881e8ec77c48c7b271640a55bf81192b08192f99_prof);

    }

    public function getTemplateName()
    {
        return "administration/modify_event.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 30,  105 => 29,  101 => 28,  96 => 26,  92 => 25,  85 => 21,  75 => 14,  71 => 13,  63 => 8,  59 => 7,  55 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'administration/homeAdmin.html.twig' %}

{% block main_screen %}
\t{{ parent() }}
    
\t{{ form_start(form) }}
\t{{ form_row(form.name) }}
\t{{ form_row(form.description) }}

\t<fieldset>
\t    <legend>Bonus</legend>
\t\t    <div class=\"form-inline\">
\t\t\t\t{{ form_row(form.bonusGold) }}
\t\t\t\t{{ form_row(form.bonusReputation) }}
\t\t\t</div>
\t</fieldset>

\t<fieldset>
\t    <legend>Réductions</legend>
\t    \t<div class=\"form-inline\">
\t  \t\t  \t{{ form_row(form.salesGold) }}
\t    \t</div>
\t</fieldset>
\t
\t{{ form_row(form.start) }}
\t{{ form_row(form.end) }}

\t{{ form_row(form.create, { 'attr': {'class': 'btn-primary'} }) }}
\t{{ form_row(form._token) }}
\t{{ form_end(form) }}

{% endblock %}", "administration/modify_event.html.twig", "/var/www/projet-web/app/Resources/views/administration/modify_event.html.twig");
    }
}
