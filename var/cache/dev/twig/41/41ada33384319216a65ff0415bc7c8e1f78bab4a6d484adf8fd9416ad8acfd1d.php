<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_3992c0d06b1f3c27c0e930f39b2ab63feeb74dda7e73c314d7a6ca19a194889f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3a8ae822369cb7c04878f5c8e6595439e462e15a8985b9b895270fb77123d08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3a8ae822369cb7c04878f5c8e6595439e462e15a8985b9b895270fb77123d08->enter($__internal_b3a8ae822369cb7c04878f5c8e6595439e462e15a8985b9b895270fb77123d08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_2381986321d657574f0082b0e1cb217ba0e9e52e6c8c421c482421fffc4280ac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2381986321d657574f0082b0e1cb217ba0e9e52e6c8c421c482421fffc4280ac->enter($__internal_2381986321d657574f0082b0e1cb217ba0e9e52e6c8c421c482421fffc4280ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_b3a8ae822369cb7c04878f5c8e6595439e462e15a8985b9b895270fb77123d08->leave($__internal_b3a8ae822369cb7c04878f5c8e6595439e462e15a8985b9b895270fb77123d08_prof);

        
        $__internal_2381986321d657574f0082b0e1cb217ba0e9e52e6c8c421c482421fffc4280ac->leave($__internal_2381986321d657574f0082b0e1cb217ba0e9e52e6c8c421c482421fffc4280ac_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
