<?php

/* veterinaire.html.twig */
class __TwigTemplate_705566f123862beb4a219a355e3ffc05d7ef4871f4e97c7186b2e0b10f1c1196 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "veterinaire.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d0364afc6a5c831d98a0ac908cae0fd98320b13d51a7ce162ef87cdc24b54026 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d0364afc6a5c831d98a0ac908cae0fd98320b13d51a7ce162ef87cdc24b54026->enter($__internal_d0364afc6a5c831d98a0ac908cae0fd98320b13d51a7ce162ef87cdc24b54026_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "veterinaire.html.twig"));

        $__internal_1f55da62d81709961bd96a6e535905a1a5ce33a4323eb27e31d0ae15b38dd384 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f55da62d81709961bd96a6e535905a1a5ce33a4323eb27e31d0ae15b38dd384->enter($__internal_1f55da62d81709961bd96a6e535905a1a5ce33a4323eb27e31d0ae15b38dd384_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "veterinaire.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d0364afc6a5c831d98a0ac908cae0fd98320b13d51a7ce162ef87cdc24b54026->leave($__internal_d0364afc6a5c831d98a0ac908cae0fd98320b13d51a7ce162ef87cdc24b54026_prof);

        
        $__internal_1f55da62d81709961bd96a6e535905a1a5ce33a4323eb27e31d0ae15b38dd384->leave($__internal_1f55da62d81709961bd96a6e535905a1a5ce33a4323eb27e31d0ae15b38dd384_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_95d904d0250a10317bf3def77dbb2f600170de5982462f787aff636595b37c30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95d904d0250a10317bf3def77dbb2f600170de5982462f787aff636595b37c30->enter($__internal_95d904d0250a10317bf3def77dbb2f600170de5982462f787aff636595b37c30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_3e537fa7d3b00aec588e497f9d857b51bd7b9e41b2d50dc9f527f2870fca51ab = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e537fa7d3b00aec588e497f9d857b51bd7b9e41b2d50dc9f527f2870fca51ab->enter($__internal_3e537fa7d3b00aec588e497f9d857b51bd7b9e41b2d50dc9f527f2870fca51ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/veterinaire.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_3e537fa7d3b00aec588e497f9d857b51bd7b9e41b2d50dc9f527f2870fca51ab->leave($__internal_3e537fa7d3b00aec588e497f9d857b51bd7b9e41b2d50dc9f527f2870fca51ab_prof);

        
        $__internal_95d904d0250a10317bf3def77dbb2f600170de5982462f787aff636595b37c30->leave($__internal_95d904d0250a10317bf3def77dbb2f600170de5982462f787aff636595b37c30_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_fdc4fb1dfd08a57d6f3312dd1eb0ce303998bcc9785768e4c0611384dbfd6698 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fdc4fb1dfd08a57d6f3312dd1eb0ce303998bcc9785768e4c0611384dbfd6698->enter($__internal_fdc4fb1dfd08a57d6f3312dd1eb0ce303998bcc9785768e4c0611384dbfd6698_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_3037f1f7074427a9fedaab688b1e2a43b61dc8885fb7139df326e3c309968325 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3037f1f7074427a9fedaab688b1e2a43b61dc8885fb7139df326e3c309968325->enter($__internal_3037f1f7074427a9fedaab688b1e2a43b61dc8885fb7139df326e3c309968325_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h1>Bienvenue chez le vétérinaire</h1>
\t<h4> Ici vous pouvez soigner et nourrir vos pensionnaires. </h4>
\t<p>Soigner ou nourrir un animal vous coûtera 1
\t\t<img src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/paws.png"), "html", null, true);
        echo "\">
\t</p>
\t</div>

<div class=\"row\">
\t<div id=\"animal_list\" class=\"list-group col-sm-2 col-sm-push-1\">
\t";
        // line 20
        if ((twig_length_filter($this->env, ($context["animal_list"] ?? $this->getContext($context, "animal_list"))) != 0)) {
            // line 21
            echo "\t\t";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["animal_list"] ?? $this->getContext($context, "animal_list"))) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 22
                echo "\t\t<a id=\"ani-";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo "\" class=\"animalli list-group-item\" href='#'>";
                echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["animal_list"] ?? $this->getContext($context, "animal_list")), $context["i"], array(), "array"), "name", array()), "html", null, true);
                echo "</a>
\t\t";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "\t";
        } else {
            // line 25
            echo "\t\tAucun pensionnaire!
\t";
        }
        // line 27
        echo "\t</div>
\t
\t<div id=\"interactive_div\" class=\"col-sm-push-1 col-sm-8 hidden\">
\t\t<div id=\"div_heal\" class=\"col-sm-6\">
\t\t\t<p id=\"action_heal\"></p>
\t\t\t<div id=\"bar_life\" class=\"progress\">
  \t\t\t\t<div id=\"sub_bar_life\" class=\"progress-bar\" role=\"progressbar\"
  \t\t\t\t\t aria-valuemin=\"0\" aria-valuemax=\"100\"> 
  \t\t\t\t</div>
     \t\t\t<div id=\"progress-bar-title\" class=\"progress-bar-title\"></div>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t<a href=\"";
        // line 39
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cure", array("id" => 0));
        echo "\" class=\"action btn btn-info disabled\" role=\"button\" id=\"heal\" >
\t\t\t\t\tSoigner (<span id=\"cost_heal\"></span>
\t\t\t\t\t<img src=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
        echo "\">)
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"div_feed\" class=\"col-sm-6 col-sm-push-1\">
\t\t\t<p id=\"action_feed\"></p>
\t\t\t<p id=\"last_meal\">Dernier repas : <img src=\"\" class=\"meal_img\"></p>
\t\t\t<div>
\t\t\t\t<a href=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("feed", array("id" => 0, "typeFood" => 1)), "html", null, true);
        echo "\"
\t\t\t\t\tclass=\"action action_feed btn btn-info\" role=\"button\" id=\"typeFood-1\" >
\t\t\t\t\t<img src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_bronze.png"), "html", null, true);
        echo "\" class=\"meal_img\">
\t\t\t\t\t\t(";
        // line 53
        echo twig_escape_filter($this->env, $this->getAttribute(($context["costsFood"] ?? $this->getContext($context, "costsFood")), 1, array(), "array"), "html", null, true);
        echo "
\t\t\t\t\t\t<img src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
        echo "\">)
\t\t\t\t</a>
\t\t\t\t<a href=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("feed", array("id" => 0, "typeFood" => 2)), "html", null, true);
        echo "\"
\t\t\t\t\tclass=\"action action_feed btn btn-info\" role=\"button\" id=\"typeFood-2\" >
\t\t\t\t\t<img src=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_argent.png"), "html", null, true);
        echo "\" class=\"meal_img\">
\t\t\t\t\t\t(";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute(($context["costsFood"] ?? $this->getContext($context, "costsFood")), 2, array(), "array"), "html", null, true);
        echo "
\t\t\t\t\t\t<img src=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
        echo "\">)
\t\t\t\t</a>
\t\t\t\t<a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("feed", array("id" => 0, "typeFood" => 3)), "html", null, true);
        echo "\"
\t\t\t\t\tclass=\"action action_feed btn btn-info\" role=\"button\" id=\"typeFood-3\" >
\t\t\t\t\t<img src=\"";
        // line 64
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_or.png"), "html", null, true);
        echo "\" class=\"meal_img\">
\t\t\t\t\t\t(";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute(($context["costsFood"] ?? $this->getContext($context, "costsFood")), 3, array(), "array"), "html", null, true);
        echo "
\t\t\t\t\t\t<img src=\"";
        // line 66
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
        echo "\">)
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>

</div>

<script>

var animals = ";
        // line 76
        echo twig_jsonencode_filter(($context["animal_list"] ?? $this->getContext($context, "animal_list")));
        echo ";

";
        // line 78
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 3));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 79
            echo "\t";
            if (($this->getAttribute(($context["costsFood"] ?? $this->getContext($context, "costsFood")), $context["i"], array(), "array") > $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "lastPension", array()), "gold", array()))) {
                // line 80
                echo "\t\t\$(\"#typeFood-\" + ";
                echo twig_escape_filter($this->env, $context["i"], "html", null, true);
                echo ").addClass(\"disabled\");
\t";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "
\$(\".animalli\").click(function(){
\t\$(\"#interactive_div\").removeClass(\"hidden\");
\t\$(\".animalli\").css(\"font-weight\" , \"\");
\t\$(this).css(\"font-weight\" , \"bold\");

\tvar currAnimal = animals[\$(this).attr(\"id\").split(\"-\")[1]];

\tupdateHealDiv(currAnimal);

\tupdateFeedDiv(currAnimal);
});
\t
\tfunction updateHealDiv( currAnimal ){
\t\tif(currAnimal.pdv == 100){
\t\t\t\$(\"#action_heal\").html(currAnimal.name+\" n'a pas besoin d'être soigné.\");
\t\t\t\$(\"#heal\").addClass(\"hidden\");
\t\t\t\$(\"#bar_life\").addClass(\"hidden\");
\t\t} else {

\t\t\t\$(\"#bar_life\").removeClass(\"hidden\");
\t\t\t\$(\"#sub_bar_life\").attr(\"aria-valuenow\" , currAnimal.pdv)
\t\t\t\t\t\t  \t  .css(\"width\", currAnimal.pdv + \"%\")
\t\t\t\t\t\t  \t  .addClass(currAnimal.pdv < 50    ? \"progress-bar-danger\"
\t\t\t\t\t\t  \t  \t\t\t\t\t\t\t       : \"progress-bar-success\")
\t\t\t\t\t\t  \t  .removeClass(currAnimal.pdv < 50 ? \"progress-bar-success\"
\t\t\t\t\t\t  \t  \t\t\t\t\t\t\t\t   : \"progress-bar-danger\");

\t\t\t\$(\"#progress-bar-title\").html(\"Vie : \" + currAnimal.pdv + \" / 100\");
\t\t\t
\t\t\t\$(\"#cost_heal\").html( currAnimal.costHeal );

\t\t\tif (";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "lastPension", array()), "gold", array()), "html", null, true);
        echo " < currAnimal.costHeal) {
\t\t\t\t\$(\"#action_heal\").html(\"Vous n'avez pas assez d'argent pour soigner \"+ currAnimal.name + \".\");
\t\t\t\t\$(\"#heal\").addClass(\"disabled\")
\t\t\t\t\t\t  .removeClass(\"hidden\");
\t\t\t} else {
\t\t\t\t\$(\"#action_heal\").html(\"Voulez-vous soigner \"+ currAnimal.name + \" ?\");

\t\t\t\t\$(\"#heal\").removeClass(\"disabled hidden\");

\t\t\t\tvar href = \$(\"#heal\").attr(\"href\");
\t\t\t\t\$(\"#heal\").attr('href', href.replace(/[0-9]+/, currAnimal.id));
\t\t\t}
\t\t}
\t}

\tfunction updateFeedDiv( currAnimal ){
\t\tif(currAnimal.hasEaten == 0){
\t\t\t\$(\"#action_feed\").html(\"Quel repas donner à \"+ currAnimal.name + \" ?\");
\t\t\t\$(\".action_feed\").removeClass(\"hidden\");

\t\t\t\$(\"#last_meal\").addClass(\"hidden\");

\t\t\tfor (var i = 1; i <= 3; i++) {
\t\t\t\thref = \$(\"#typeFood-\" + i).attr(\"href\");
\t\t\t\t\$(\"#typeFood-\" + i).attr(\"href\", href.replace(/[0-9]+\\/[0-9]+/, currAnimal.id + \"/\" + i));
\t\t\t}
\t\t} else {
\t\t\t\$(\"#action_feed\").html(currAnimal.name + \" a déjà été nourri aujourdhui, revenez demain!\");

\t\t\t\$(\"#last_meal\").removeClass(\"hidden\");
\t\t\t\$(\"#last_meal > img\").attr(\"src\", currAnimal.hasEaten == 1 ? \"";
        // line 145
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_bronze.png"), "html", null, true);
        echo "\":
\t\t\t\t\t\t\t\t\t    \t  currAnimal.hasEaten == 2 ? \"";
        // line 146
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_argent.png"), "html", null, true);
        echo "\":
\t\t\t\t\t\t\t\t\t  \t\t\t\t\t\t\t   \t\t \"";
        // line 147
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/croquette_or.png"), "html", null, true);
        echo "\");
\t\t\t\$(\".action_feed\").addClass(\"hidden\");
\t\t}
\t}

</script>

";
        
        $__internal_3037f1f7074427a9fedaab688b1e2a43b61dc8885fb7139df326e3c309968325->leave($__internal_3037f1f7074427a9fedaab688b1e2a43b61dc8885fb7139df326e3c309968325_prof);

        
        $__internal_fdc4fb1dfd08a57d6f3312dd1eb0ce303998bcc9785768e4c0611384dbfd6698->leave($__internal_fdc4fb1dfd08a57d6f3312dd1eb0ce303998bcc9785768e4c0611384dbfd6698_prof);

    }

    public function getTemplateName()
    {
        return "veterinaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  307 => 147,  303 => 146,  299 => 145,  266 => 115,  232 => 83,  222 => 80,  219 => 79,  215 => 78,  210 => 76,  197 => 66,  193 => 65,  189 => 64,  184 => 62,  179 => 60,  175 => 59,  171 => 58,  166 => 56,  161 => 54,  157 => 53,  153 => 52,  148 => 50,  136 => 41,  131 => 39,  117 => 27,  113 => 25,  110 => 24,  99 => 22,  94 => 21,  92 => 20,  83 => 14,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/veterinaire.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h1>Bienvenue chez le vétérinaire</h1>
\t<h4> Ici vous pouvez soigner et nourrir vos pensionnaires. </h4>
\t<p>Soigner ou nourrir un animal vous coûtera 1
\t\t<img src=\"{{ asset('imgs/paws.png') }}\">
\t</p>
\t</div>

<div class=\"row\">
\t<div id=\"animal_list\" class=\"list-group col-sm-2 col-sm-push-1\">
\t{% if animal_list|length != 0 %}
\t\t{% for i in 0..animal_list|length-1 %}
\t\t<a id=\"ani-{{ i }}\" class=\"animalli list-group-item\" href='#'>{{ animal_list[i].name }}</a>
\t\t{% endfor %}
\t{% else %}
\t\tAucun pensionnaire!
\t{% endif %}
\t</div>
\t
\t<div id=\"interactive_div\" class=\"col-sm-push-1 col-sm-8 hidden\">
\t\t<div id=\"div_heal\" class=\"col-sm-6\">
\t\t\t<p id=\"action_heal\"></p>
\t\t\t<div id=\"bar_life\" class=\"progress\">
  \t\t\t\t<div id=\"sub_bar_life\" class=\"progress-bar\" role=\"progressbar\"
  \t\t\t\t\t aria-valuemin=\"0\" aria-valuemax=\"100\"> 
  \t\t\t\t</div>
     \t\t\t<div id=\"progress-bar-title\" class=\"progress-bar-title\"></div>
\t\t\t</div>
\t\t\t<div>
\t\t\t\t<a href=\"{{ path('cure', {'id' : 0}) }}\" class=\"action btn btn-info disabled\" role=\"button\" id=\"heal\" >
\t\t\t\t\tSoigner (<span id=\"cost_heal\"></span>
\t\t\t\t\t<img src=\"{{ asset('imgs/po.png') }}\">)
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>

\t\t<div id=\"div_feed\" class=\"col-sm-6 col-sm-push-1\">
\t\t\t<p id=\"action_feed\"></p>
\t\t\t<p id=\"last_meal\">Dernier repas : <img src=\"\" class=\"meal_img\"></p>
\t\t\t<div>
\t\t\t\t<a href=\"{{ path('feed', {'id' : 0, 'typeFood' : 1}) }}\"
\t\t\t\t\tclass=\"action action_feed btn btn-info\" role=\"button\" id=\"typeFood-1\" >
\t\t\t\t\t<img src=\"{{ asset('imgs/croquette_bronze.png') }}\" class=\"meal_img\">
\t\t\t\t\t\t({{ costsFood[1] }}
\t\t\t\t\t\t<img src=\"{{ asset('imgs/po.png') }}\">)
\t\t\t\t</a>
\t\t\t\t<a href=\"{{ path('feed', {'id' : 0, 'typeFood' : 2}) }}\"
\t\t\t\t\tclass=\"action action_feed btn btn-info\" role=\"button\" id=\"typeFood-2\" >
\t\t\t\t\t<img src=\"{{ asset('imgs/croquette_argent.png') }}\" class=\"meal_img\">
\t\t\t\t\t\t({{ costsFood[2] }}
\t\t\t\t\t\t<img src=\"{{ asset('imgs/po.png') }}\">)
\t\t\t\t</a>
\t\t\t\t<a href=\"{{ path('feed', {'id' : 0, 'typeFood' : 3}) }}\"
\t\t\t\t\tclass=\"action action_feed btn btn-info\" role=\"button\" id=\"typeFood-3\" >
\t\t\t\t\t<img src=\"{{ asset('imgs/croquette_or.png') }}\" class=\"meal_img\">
\t\t\t\t\t\t({{ costsFood[3] }}
\t\t\t\t\t\t<img src=\"{{ asset('imgs/po.png') }}\">)
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>

</div>

<script>

var animals = {{ animal_list|json_encode|raw }};

{% for i in 1..3 %}
\t{% if costsFood[i]  >  app.user.lastPension.gold %}
\t\t\$(\"#typeFood-\" + {{ i }}).addClass(\"disabled\");
\t{% endif %}
{% endfor %}

\$(\".animalli\").click(function(){
\t\$(\"#interactive_div\").removeClass(\"hidden\");
\t\$(\".animalli\").css(\"font-weight\" , \"\");
\t\$(this).css(\"font-weight\" , \"bold\");

\tvar currAnimal = animals[\$(this).attr(\"id\").split(\"-\")[1]];

\tupdateHealDiv(currAnimal);

\tupdateFeedDiv(currAnimal);
});
\t
\tfunction updateHealDiv( currAnimal ){
\t\tif(currAnimal.pdv == 100){
\t\t\t\$(\"#action_heal\").html(currAnimal.name+\" n'a pas besoin d'être soigné.\");
\t\t\t\$(\"#heal\").addClass(\"hidden\");
\t\t\t\$(\"#bar_life\").addClass(\"hidden\");
\t\t} else {

\t\t\t\$(\"#bar_life\").removeClass(\"hidden\");
\t\t\t\$(\"#sub_bar_life\").attr(\"aria-valuenow\" , currAnimal.pdv)
\t\t\t\t\t\t  \t  .css(\"width\", currAnimal.pdv + \"%\")
\t\t\t\t\t\t  \t  .addClass(currAnimal.pdv < 50    ? \"progress-bar-danger\"
\t\t\t\t\t\t  \t  \t\t\t\t\t\t\t       : \"progress-bar-success\")
\t\t\t\t\t\t  \t  .removeClass(currAnimal.pdv < 50 ? \"progress-bar-success\"
\t\t\t\t\t\t  \t  \t\t\t\t\t\t\t\t   : \"progress-bar-danger\");

\t\t\t\$(\"#progress-bar-title\").html(\"Vie : \" + currAnimal.pdv + \" / 100\");
\t\t\t
\t\t\t\$(\"#cost_heal\").html( currAnimal.costHeal );

\t\t\tif ({{ app.user.lastPension.gold }} < currAnimal.costHeal) {
\t\t\t\t\$(\"#action_heal\").html(\"Vous n'avez pas assez d'argent pour soigner \"+ currAnimal.name + \".\");
\t\t\t\t\$(\"#heal\").addClass(\"disabled\")
\t\t\t\t\t\t  .removeClass(\"hidden\");
\t\t\t} else {
\t\t\t\t\$(\"#action_heal\").html(\"Voulez-vous soigner \"+ currAnimal.name + \" ?\");

\t\t\t\t\$(\"#heal\").removeClass(\"disabled hidden\");

\t\t\t\tvar href = \$(\"#heal\").attr(\"href\");
\t\t\t\t\$(\"#heal\").attr('href', href.replace(/[0-9]+/, currAnimal.id));
\t\t\t}
\t\t}
\t}

\tfunction updateFeedDiv( currAnimal ){
\t\tif(currAnimal.hasEaten == 0){
\t\t\t\$(\"#action_feed\").html(\"Quel repas donner à \"+ currAnimal.name + \" ?\");
\t\t\t\$(\".action_feed\").removeClass(\"hidden\");

\t\t\t\$(\"#last_meal\").addClass(\"hidden\");

\t\t\tfor (var i = 1; i <= 3; i++) {
\t\t\t\thref = \$(\"#typeFood-\" + i).attr(\"href\");
\t\t\t\t\$(\"#typeFood-\" + i).attr(\"href\", href.replace(/[0-9]+\\/[0-9]+/, currAnimal.id + \"/\" + i));
\t\t\t}
\t\t} else {
\t\t\t\$(\"#action_feed\").html(currAnimal.name + \" a déjà été nourri aujourdhui, revenez demain!\");

\t\t\t\$(\"#last_meal\").removeClass(\"hidden\");
\t\t\t\$(\"#last_meal > img\").attr(\"src\", currAnimal.hasEaten == 1 ? \"{{ asset('imgs/croquette_bronze.png') }}\":
\t\t\t\t\t\t\t\t\t    \t  currAnimal.hasEaten == 2 ? \"{{ asset('imgs/croquette_argent.png') }}\":
\t\t\t\t\t\t\t\t\t  \t\t\t\t\t\t\t   \t\t \"{{ asset('imgs/croquette_or.png') }}\");
\t\t\t\$(\".action_feed\").addClass(\"hidden\");
\t\t}
\t}

</script>

{% endblock %}
", "veterinaire.html.twig", "/var/www/projet-web/app/Resources/views/veterinaire.html.twig");
    }
}
