<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_72ebf513eb9760eef7842b73f2815af11795bcfaa2919030a1f4fd47c00b4cb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c7a475b726a2d288ab0af339df306a4a67d6e0086403fed528114c472e00afe7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7a475b726a2d288ab0af339df306a4a67d6e0086403fed528114c472e00afe7->enter($__internal_c7a475b726a2d288ab0af339df306a4a67d6e0086403fed528114c472e00afe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_815c96dca7155db8158abba8bde9fad7924d5099af0946123a3e9195041f0734 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_815c96dca7155db8158abba8bde9fad7924d5099af0946123a3e9195041f0734->enter($__internal_815c96dca7155db8158abba8bde9fad7924d5099af0946123a3e9195041f0734_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        $this->loadTemplate("@Twig/Exception/exception.xml.twig", "TwigBundle:Exception:exception.rdf.twig", 1)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        
        $__internal_c7a475b726a2d288ab0af339df306a4a67d6e0086403fed528114c472e00afe7->leave($__internal_c7a475b726a2d288ab0af339df306a4a67d6e0086403fed528114c472e00afe7_prof);

        
        $__internal_815c96dca7155db8158abba8bde9fad7924d5099af0946123a3e9195041f0734->leave($__internal_815c96dca7155db8158abba8bde9fad7924d5099af0946123a3e9195041f0734_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include '@Twig/Exception/exception.xml.twig' with { 'exception': exception } %}
", "TwigBundle:Exception:exception.rdf.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
