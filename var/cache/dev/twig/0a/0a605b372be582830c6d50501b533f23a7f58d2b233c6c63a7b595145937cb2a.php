<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_14f86bde5cf92d4a090faec4392f30def772fd93a53c25c2ce9f8b9fecc32881 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4490a44632f4dbe608714580ccef870bf17d986fc1dcfe6ce52e339b5168ef9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4490a44632f4dbe608714580ccef870bf17d986fc1dcfe6ce52e339b5168ef9c->enter($__internal_4490a44632f4dbe608714580ccef870bf17d986fc1dcfe6ce52e339b5168ef9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_55d5423f2842794e4dda24031578214f50b5c43a7744e96d3d8441d255febbb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55d5423f2842794e4dda24031578214f50b5c43a7744e96d3d8441d255febbb8->enter($__internal_55d5423f2842794e4dda24031578214f50b5c43a7744e96d3d8441d255febbb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4490a44632f4dbe608714580ccef870bf17d986fc1dcfe6ce52e339b5168ef9c->leave($__internal_4490a44632f4dbe608714580ccef870bf17d986fc1dcfe6ce52e339b5168ef9c_prof);

        
        $__internal_55d5423f2842794e4dda24031578214f50b5c43a7744e96d3d8441d255febbb8->leave($__internal_55d5423f2842794e4dda24031578214f50b5c43a7744e96d3d8441d255febbb8_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_345707fbd2ba3a8074e692849fd184515ab053ae474a4b6cdc5eac468538a72c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_345707fbd2ba3a8074e692849fd184515ab053ae474a4b6cdc5eac468538a72c->enter($__internal_345707fbd2ba3a8074e692849fd184515ab053ae474a4b6cdc5eac468538a72c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_9e73354738627e1f127af47b13339d4f25ca6559587c5c3bee4176a6418a953b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e73354738627e1f127af47b13339d4f25ca6559587c5c3bee4176a6418a953b->enter($__internal_9e73354738627e1f127af47b13339d4f25ca6559587c5c3bee4176a6418a953b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_9e73354738627e1f127af47b13339d4f25ca6559587c5c3bee4176a6418a953b->leave($__internal_9e73354738627e1f127af47b13339d4f25ca6559587c5c3bee4176a6418a953b_prof);

        
        $__internal_345707fbd2ba3a8074e692849fd184515ab053ae474a4b6cdc5eac468538a72c->leave($__internal_345707fbd2ba3a8074e692849fd184515ab053ae474a4b6cdc5eac468538a72c_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_39d8ab131b622ea977946b41462054b7e5d1fc78f5776c40e3fe18cadd73a186 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39d8ab131b622ea977946b41462054b7e5d1fc78f5776c40e3fe18cadd73a186->enter($__internal_39d8ab131b622ea977946b41462054b7e5d1fc78f5776c40e3fe18cadd73a186_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_88b26fa509020117a670448b9b0e225e1eea8a438c48f4c7196387faab591712 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88b26fa509020117a670448b9b0e225e1eea8a438c48f4c7196387faab591712->enter($__internal_88b26fa509020117a670448b9b0e225e1eea8a438c48f4c7196387faab591712_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_88b26fa509020117a670448b9b0e225e1eea8a438c48f4c7196387faab591712->leave($__internal_88b26fa509020117a670448b9b0e225e1eea8a438c48f4c7196387faab591712_prof);

        
        $__internal_39d8ab131b622ea977946b41462054b7e5d1fc78f5776c40e3fe18cadd73a186->leave($__internal_39d8ab131b622ea977946b41462054b7e5d1fc78f5776c40e3fe18cadd73a186_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
