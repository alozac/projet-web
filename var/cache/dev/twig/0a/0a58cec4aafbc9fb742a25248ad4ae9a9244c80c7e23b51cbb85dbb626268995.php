<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_4f4c5c87f245675b203e5bd6969c583d3603b90f7416c4645f4b890601dee77c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_303912f7b8b43cf697218c35494c2dc874107f4d04e0dba44d515b5d80149024 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_303912f7b8b43cf697218c35494c2dc874107f4d04e0dba44d515b5d80149024->enter($__internal_303912f7b8b43cf697218c35494c2dc874107f4d04e0dba44d515b5d80149024_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_df4472749aef292d3f029a81a299a527d3d50068cf38b8452b7c5e8ec6b494b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df4472749aef292d3f029a81a299a527d3d50068cf38b8452b7c5e8ec6b494b3->enter($__internal_df4472749aef292d3f029a81a299a527d3d50068cf38b8452b7c5e8ec6b494b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_303912f7b8b43cf697218c35494c2dc874107f4d04e0dba44d515b5d80149024->leave($__internal_303912f7b8b43cf697218c35494c2dc874107f4d04e0dba44d515b5d80149024_prof);

        
        $__internal_df4472749aef292d3f029a81a299a527d3d50068cf38b8452b7c5e8ec6b494b3->leave($__internal_df4472749aef292d3f029a81a299a527d3d50068cf38b8452b7c5e8ec6b494b3_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
