<?php

/* bootstrap_3_layout.html.twig */
class __TwigTemplate_0aa1b8b4096c97dea9ea328e7dc3b878ff4e7b27f4f33a4ad518a118b542a524 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $_trait_0 = $this->loadTemplate("form_div_layout.html.twig", "bootstrap_3_layout.html.twig", 1);
        // line 1
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."form_div_layout.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'form_widget_simple' => array($this, 'block_form_widget_simple'),
                'textarea_widget' => array($this, 'block_textarea_widget'),
                'button_widget' => array($this, 'block_button_widget'),
                'money_widget' => array($this, 'block_money_widget'),
                'percent_widget' => array($this, 'block_percent_widget'),
                'datetime_widget' => array($this, 'block_datetime_widget'),
                'date_widget' => array($this, 'block_date_widget'),
                'time_widget' => array($this, 'block_time_widget'),
                'dateinterval_widget' => array($this, 'block_dateinterval_widget'),
                'choice_widget_collapsed' => array($this, 'block_choice_widget_collapsed'),
                'choice_widget_expanded' => array($this, 'block_choice_widget_expanded'),
                'checkbox_widget' => array($this, 'block_checkbox_widget'),
                'radio_widget' => array($this, 'block_radio_widget'),
                'form_label' => array($this, 'block_form_label'),
                'choice_label' => array($this, 'block_choice_label'),
                'checkbox_label' => array($this, 'block_checkbox_label'),
                'radio_label' => array($this, 'block_radio_label'),
                'checkbox_radio_label' => array($this, 'block_checkbox_radio_label'),
                'form_row' => array($this, 'block_form_row'),
                'button_row' => array($this, 'block_button_row'),
                'choice_row' => array($this, 'block_choice_row'),
                'date_row' => array($this, 'block_date_row'),
                'time_row' => array($this, 'block_time_row'),
                'datetime_row' => array($this, 'block_datetime_row'),
                'checkbox_row' => array($this, 'block_checkbox_row'),
                'radio_row' => array($this, 'block_radio_row'),
                'form_errors' => array($this, 'block_form_errors'),
            )
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2f63257aa1545353e29ca1f4c4b3ab05fde65cde09fe551fb50d0665f1c76754 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f63257aa1545353e29ca1f4c4b3ab05fde65cde09fe551fb50d0665f1c76754->enter($__internal_2f63257aa1545353e29ca1f4c4b3ab05fde65cde09fe551fb50d0665f1c76754_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        $__internal_14b44c611f3e90105a6985b417266968c1a05353e38a4b3de1a29aa899889437 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14b44c611f3e90105a6985b417266968c1a05353e38a4b3de1a29aa899889437->enter($__internal_14b44c611f3e90105a6985b417266968c1a05353e38a4b3de1a29aa899889437_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "bootstrap_3_layout.html.twig"));

        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('form_widget_simple', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('textarea_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('button_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('money_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('percent_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('datetime_widget', $context, $blocks);
        // line 55
        echo "
";
        // line 56
        $this->displayBlock('date_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('time_widget', $context, $blocks);
        // line 90
        $this->displayBlock('dateinterval_widget', $context, $blocks);
        // line 109
        $this->displayBlock('choice_widget_collapsed', $context, $blocks);
        // line 113
        echo "
";
        // line 114
        $this->displayBlock('choice_widget_expanded', $context, $blocks);
        // line 133
        echo "
";
        // line 134
        $this->displayBlock('checkbox_widget', $context, $blocks);
        // line 144
        echo "
";
        // line 145
        $this->displayBlock('radio_widget', $context, $blocks);
        // line 155
        echo "
";
        // line 157
        echo "
";
        // line 158
        $this->displayBlock('form_label', $context, $blocks);
        // line 162
        echo "
";
        // line 163
        $this->displayBlock('choice_label', $context, $blocks);
        // line 168
        echo "
";
        // line 169
        $this->displayBlock('checkbox_label', $context, $blocks);
        // line 172
        echo "
";
        // line 173
        $this->displayBlock('radio_label', $context, $blocks);
        // line 176
        echo "
";
        // line 177
        $this->displayBlock('checkbox_radio_label', $context, $blocks);
        // line 201
        echo "
";
        // line 203
        echo "
";
        // line 204
        $this->displayBlock('form_row', $context, $blocks);
        // line 211
        echo "
";
        // line 212
        $this->displayBlock('button_row', $context, $blocks);
        // line 217
        echo "
";
        // line 218
        $this->displayBlock('choice_row', $context, $blocks);
        // line 222
        echo "
";
        // line 223
        $this->displayBlock('date_row', $context, $blocks);
        // line 227
        echo "
";
        // line 228
        $this->displayBlock('time_row', $context, $blocks);
        // line 232
        echo "
";
        // line 233
        $this->displayBlock('datetime_row', $context, $blocks);
        // line 237
        echo "
";
        // line 238
        $this->displayBlock('checkbox_row', $context, $blocks);
        // line 244
        echo "
";
        // line 245
        $this->displayBlock('radio_row', $context, $blocks);
        // line 251
        echo "
";
        // line 253
        echo "
";
        // line 254
        $this->displayBlock('form_errors', $context, $blocks);
        
        $__internal_2f63257aa1545353e29ca1f4c4b3ab05fde65cde09fe551fb50d0665f1c76754->leave($__internal_2f63257aa1545353e29ca1f4c4b3ab05fde65cde09fe551fb50d0665f1c76754_prof);

        
        $__internal_14b44c611f3e90105a6985b417266968c1a05353e38a4b3de1a29aa899889437->leave($__internal_14b44c611f3e90105a6985b417266968c1a05353e38a4b3de1a29aa899889437_prof);

    }

    // line 5
    public function block_form_widget_simple($context, array $blocks = array())
    {
        $__internal_b88df3904debcb2832d42fe319070dc8cf32ecee6572eb5495360937d4ab8d84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b88df3904debcb2832d42fe319070dc8cf32ecee6572eb5495360937d4ab8d84->enter($__internal_b88df3904debcb2832d42fe319070dc8cf32ecee6572eb5495360937d4ab8d84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        $__internal_69d7bb88d986709602284df88157af957d56f70023bea3258f184d0de179d93a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69d7bb88d986709602284df88157af957d56f70023bea3258f184d0de179d93a->enter($__internal_69d7bb88d986709602284df88157af957d56f70023bea3258f184d0de179d93a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_widget_simple"));

        // line 6
        if (( !array_key_exists("type", $context) || !twig_in_filter(($context["type"] ?? $this->getContext($context, "type")), array(0 => "file", 1 => "hidden")))) {
            // line 7
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        }
        // line 9
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        
        $__internal_69d7bb88d986709602284df88157af957d56f70023bea3258f184d0de179d93a->leave($__internal_69d7bb88d986709602284df88157af957d56f70023bea3258f184d0de179d93a_prof);

        
        $__internal_b88df3904debcb2832d42fe319070dc8cf32ecee6572eb5495360937d4ab8d84->leave($__internal_b88df3904debcb2832d42fe319070dc8cf32ecee6572eb5495360937d4ab8d84_prof);

    }

    // line 12
    public function block_textarea_widget($context, array $blocks = array())
    {
        $__internal_aefd4baba1424876d645ffdeb38348b29fc775a6e28d4ee85d09f4e171cd23c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aefd4baba1424876d645ffdeb38348b29fc775a6e28d4ee85d09f4e171cd23c6->enter($__internal_aefd4baba1424876d645ffdeb38348b29fc775a6e28d4ee85d09f4e171cd23c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        $__internal_6d1c10d3f2dadbbecb945722e4adb77112ebe228e8fcb3efa4e2d166f5449b0b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d1c10d3f2dadbbecb945722e4adb77112ebe228e8fcb3efa4e2d166f5449b0b->enter($__internal_6d1c10d3f2dadbbecb945722e4adb77112ebe228e8fcb3efa4e2d166f5449b0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "textarea_widget"));

        // line 13
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        // line 14
        $this->displayParentBlock("textarea_widget", $context, $blocks);
        
        $__internal_6d1c10d3f2dadbbecb945722e4adb77112ebe228e8fcb3efa4e2d166f5449b0b->leave($__internal_6d1c10d3f2dadbbecb945722e4adb77112ebe228e8fcb3efa4e2d166f5449b0b_prof);

        
        $__internal_aefd4baba1424876d645ffdeb38348b29fc775a6e28d4ee85d09f4e171cd23c6->leave($__internal_aefd4baba1424876d645ffdeb38348b29fc775a6e28d4ee85d09f4e171cd23c6_prof);

    }

    // line 17
    public function block_button_widget($context, array $blocks = array())
    {
        $__internal_8e36ded17576ba9b8e398278c6e7f660f3e6fb3675c793de305cbb2d4d8f4d43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e36ded17576ba9b8e398278c6e7f660f3e6fb3675c793de305cbb2d4d8f4d43->enter($__internal_8e36ded17576ba9b8e398278c6e7f660f3e6fb3675c793de305cbb2d4d8f4d43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        $__internal_eb2ceb4b86eb5f581730274cbf9cd41ea3389cb562c21dc28cdcd77579d12a4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb2ceb4b86eb5f581730274cbf9cd41ea3389cb562c21dc28cdcd77579d12a4b->enter($__internal_eb2ceb4b86eb5f581730274cbf9cd41ea3389cb562c21dc28cdcd77579d12a4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_widget"));

        // line 18
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "btn-default")) : ("btn-default")) . " btn"))));
        // line 19
        $this->displayParentBlock("button_widget", $context, $blocks);
        
        $__internal_eb2ceb4b86eb5f581730274cbf9cd41ea3389cb562c21dc28cdcd77579d12a4b->leave($__internal_eb2ceb4b86eb5f581730274cbf9cd41ea3389cb562c21dc28cdcd77579d12a4b_prof);

        
        $__internal_8e36ded17576ba9b8e398278c6e7f660f3e6fb3675c793de305cbb2d4d8f4d43->leave($__internal_8e36ded17576ba9b8e398278c6e7f660f3e6fb3675c793de305cbb2d4d8f4d43_prof);

    }

    // line 22
    public function block_money_widget($context, array $blocks = array())
    {
        $__internal_4a03714803127ed7c40c7d743232fee61f74381e5a17c6811a805c55048e9ef7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a03714803127ed7c40c7d743232fee61f74381e5a17c6811a805c55048e9ef7->enter($__internal_4a03714803127ed7c40c7d743232fee61f74381e5a17c6811a805c55048e9ef7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        $__internal_5570c72ee1f5dcda508506088ab9e5097300d23a337b64c33e33bf2285f891cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5570c72ee1f5dcda508506088ab9e5097300d23a337b64c33e33bf2285f891cc->enter($__internal_5570c72ee1f5dcda508506088ab9e5097300d23a337b64c33e33bf2285f891cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "money_widget"));

        // line 23
        echo "<div class=\"input-group\">
        ";
        // line 24
        $context["append"] = (is_string($__internal_f0e126d13394ca5d463b4b38ded4b00ab4d7675c48dfd8aca637ba980dc767cf = ($context["money_pattern"] ?? $this->getContext($context, "money_pattern"))) && is_string($__internal_05d7fc3baecc6ad0c98ccacc0862d5a9f46489436c28f73889131a07a1e58315 = "{{") && ('' === $__internal_05d7fc3baecc6ad0c98ccacc0862d5a9f46489436c28f73889131a07a1e58315 || 0 === strpos($__internal_f0e126d13394ca5d463b4b38ded4b00ab4d7675c48dfd8aca637ba980dc767cf, $__internal_05d7fc3baecc6ad0c98ccacc0862d5a9f46489436c28f73889131a07a1e58315)));
        // line 25
        echo "        ";
        if ( !($context["append"] ?? $this->getContext($context, "append"))) {
            // line 26
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 28
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 29
        if (($context["append"] ?? $this->getContext($context, "append"))) {
            // line 30
            echo "            <span class=\"input-group-addon\">";
            echo twig_escape_filter($this->env, twig_replace_filter(($context["money_pattern"] ?? $this->getContext($context, "money_pattern")), array("{{ widget }}" => "")), "html", null, true);
            echo "</span>
        ";
        }
        // line 32
        echo "    </div>";
        
        $__internal_5570c72ee1f5dcda508506088ab9e5097300d23a337b64c33e33bf2285f891cc->leave($__internal_5570c72ee1f5dcda508506088ab9e5097300d23a337b64c33e33bf2285f891cc_prof);

        
        $__internal_4a03714803127ed7c40c7d743232fee61f74381e5a17c6811a805c55048e9ef7->leave($__internal_4a03714803127ed7c40c7d743232fee61f74381e5a17c6811a805c55048e9ef7_prof);

    }

    // line 35
    public function block_percent_widget($context, array $blocks = array())
    {
        $__internal_20abbe3101535aabc1ec1672db9fe1e5db06f69e4fc051c95de1d394d703bfb7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20abbe3101535aabc1ec1672db9fe1e5db06f69e4fc051c95de1d394d703bfb7->enter($__internal_20abbe3101535aabc1ec1672db9fe1e5db06f69e4fc051c95de1d394d703bfb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        $__internal_36b939d970b90f11fd1aa72fabdca86fc6df353a7bd4614c9ac7123f9b062d02 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36b939d970b90f11fd1aa72fabdca86fc6df353a7bd4614c9ac7123f9b062d02->enter($__internal_36b939d970b90f11fd1aa72fabdca86fc6df353a7bd4614c9ac7123f9b062d02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "percent_widget"));

        // line 36
        echo "<div class=\"input-group\">";
        // line 37
        $this->displayBlock("form_widget_simple", $context, $blocks);
        // line 38
        echo "<span class=\"input-group-addon\">%</span>
    </div>";
        
        $__internal_36b939d970b90f11fd1aa72fabdca86fc6df353a7bd4614c9ac7123f9b062d02->leave($__internal_36b939d970b90f11fd1aa72fabdca86fc6df353a7bd4614c9ac7123f9b062d02_prof);

        
        $__internal_20abbe3101535aabc1ec1672db9fe1e5db06f69e4fc051c95de1d394d703bfb7->leave($__internal_20abbe3101535aabc1ec1672db9fe1e5db06f69e4fc051c95de1d394d703bfb7_prof);

    }

    // line 42
    public function block_datetime_widget($context, array $blocks = array())
    {
        $__internal_baf08c357b13669682375ce59e2d08f56cc173ee24f4daacfcae0f4807aa0d07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_baf08c357b13669682375ce59e2d08f56cc173ee24f4daacfcae0f4807aa0d07->enter($__internal_baf08c357b13669682375ce59e2d08f56cc173ee24f4daacfcae0f4807aa0d07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        $__internal_580460820f91df4dd33a3c06c6a333abcf14dd60fc9fc39cce9da78c326897d3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_580460820f91df4dd33a3c06c6a333abcf14dd60fc9fc39cce9da78c326897d3->enter($__internal_580460820f91df4dd33a3c06c6a333abcf14dd60fc9fc39cce9da78c326897d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_widget"));

        // line 43
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 44
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 46
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 47
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 48
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'errors');
            // line 49
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'errors');
            // line 50
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "date", array()), 'widget', array("datetime" => true));
            // line 51
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "time", array()), 'widget', array("datetime" => true));
            // line 52
            echo "</div>";
        }
        
        $__internal_580460820f91df4dd33a3c06c6a333abcf14dd60fc9fc39cce9da78c326897d3->leave($__internal_580460820f91df4dd33a3c06c6a333abcf14dd60fc9fc39cce9da78c326897d3_prof);

        
        $__internal_baf08c357b13669682375ce59e2d08f56cc173ee24f4daacfcae0f4807aa0d07->leave($__internal_baf08c357b13669682375ce59e2d08f56cc173ee24f4daacfcae0f4807aa0d07_prof);

    }

    // line 56
    public function block_date_widget($context, array $blocks = array())
    {
        $__internal_2a10d52e2af82378df964b25542fc40f8411ccfac0696bbd8cb05ba7ab4e6493 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a10d52e2af82378df964b25542fc40f8411ccfac0696bbd8cb05ba7ab4e6493->enter($__internal_2a10d52e2af82378df964b25542fc40f8411ccfac0696bbd8cb05ba7ab4e6493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        $__internal_77fc1e04f7c0939860efa806f1122f4e71c54183c946c85a1f0d3f8ded21b036 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77fc1e04f7c0939860efa806f1122f4e71c54183c946c85a1f0d3f8ded21b036->enter($__internal_77fc1e04f7c0939860efa806f1122f4e71c54183c946c85a1f0d3f8ded21b036_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_widget"));

        // line 57
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 58
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 60
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 61
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 62
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 64
            echo twig_replace_filter(($context["date_pattern"] ?? $this->getContext($context, "date_pattern")), array("{{ year }}" =>             // line 65
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "year", array()), 'widget'), "{{ month }}" =>             // line 66
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "month", array()), 'widget'), "{{ day }}" =>             // line 67
$this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "day", array()), 'widget')));
            // line 69
            if (( !array_key_exists("datetime", $context) ||  !($context["datetime"] ?? $this->getContext($context, "datetime")))) {
                // line 70
                echo "</div>";
            }
        }
        
        $__internal_77fc1e04f7c0939860efa806f1122f4e71c54183c946c85a1f0d3f8ded21b036->leave($__internal_77fc1e04f7c0939860efa806f1122f4e71c54183c946c85a1f0d3f8ded21b036_prof);

        
        $__internal_2a10d52e2af82378df964b25542fc40f8411ccfac0696bbd8cb05ba7ab4e6493->leave($__internal_2a10d52e2af82378df964b25542fc40f8411ccfac0696bbd8cb05ba7ab4e6493_prof);

    }

    // line 75
    public function block_time_widget($context, array $blocks = array())
    {
        $__internal_2222c39f0ca81ac03cfe73f27c2a2d08565419985356379ff1a13b9d20778dc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2222c39f0ca81ac03cfe73f27c2a2d08565419985356379ff1a13b9d20778dc9->enter($__internal_2222c39f0ca81ac03cfe73f27c2a2d08565419985356379ff1a13b9d20778dc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        $__internal_974c0d1968bb026812c5f9abef71f8571172942f39e6434f1c2978d5f49ec6d9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_974c0d1968bb026812c5f9abef71f8571172942f39e6434f1c2978d5f49ec6d9->enter($__internal_974c0d1968bb026812c5f9abef71f8571172942f39e6434f1c2978d5f49ec6d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_widget"));

        // line 76
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 77
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 79
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 80
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 81
                echo "<div ";
                $this->displayBlock("widget_container_attributes", $context, $blocks);
                echo ">";
            }
            // line 83
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hour", array()), 'widget');
            if (($context["with_minutes"] ?? $this->getContext($context, "with_minutes"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minute", array()), 'widget');
            }
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                echo ":";
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "second", array()), 'widget');
            }
            // line 84
            echo "        ";
            if (( !array_key_exists("datetime", $context) || (false == ($context["datetime"] ?? $this->getContext($context, "datetime"))))) {
                // line 85
                echo "</div>";
            }
        }
        
        $__internal_974c0d1968bb026812c5f9abef71f8571172942f39e6434f1c2978d5f49ec6d9->leave($__internal_974c0d1968bb026812c5f9abef71f8571172942f39e6434f1c2978d5f49ec6d9_prof);

        
        $__internal_2222c39f0ca81ac03cfe73f27c2a2d08565419985356379ff1a13b9d20778dc9->leave($__internal_2222c39f0ca81ac03cfe73f27c2a2d08565419985356379ff1a13b9d20778dc9_prof);

    }

    // line 90
    public function block_dateinterval_widget($context, array $blocks = array())
    {
        $__internal_1e9bff05e79a26a9ab0ece8ec763ddc57ba8529c230966a61f77af4b1ca4e02c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e9bff05e79a26a9ab0ece8ec763ddc57ba8529c230966a61f77af4b1ca4e02c->enter($__internal_1e9bff05e79a26a9ab0ece8ec763ddc57ba8529c230966a61f77af4b1ca4e02c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        $__internal_92b552d2bc5e4f131d97684e71a6056b2a5605a1589f39e7fa4b7f3f49df3ea7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92b552d2bc5e4f131d97684e71a6056b2a5605a1589f39e7fa4b7f3f49df3ea7->enter($__internal_92b552d2bc5e4f131d97684e71a6056b2a5605a1589f39e7fa4b7f3f49df3ea7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "dateinterval_widget"));

        // line 91
        if ((($context["widget"] ?? $this->getContext($context, "widget")) == "single_text")) {
            // line 92
            $this->displayBlock("form_widget_simple", $context, $blocks);
        } else {
            // line 94
            $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-inline"))));
            // line 95
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 96
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
            // line 97
            if (($context["with_years"] ?? $this->getContext($context, "with_years"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "years", array()), 'widget');
            }
            // line 98
            if (($context["with_months"] ?? $this->getContext($context, "with_months"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "months", array()), 'widget');
            }
            // line 99
            if (($context["with_weeks"] ?? $this->getContext($context, "with_weeks"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "weeks", array()), 'widget');
            }
            // line 100
            if (($context["with_days"] ?? $this->getContext($context, "with_days"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "days", array()), 'widget');
            }
            // line 101
            if (($context["with_hours"] ?? $this->getContext($context, "with_hours"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "hours", array()), 'widget');
            }
            // line 102
            if (($context["with_minutes"] ?? $this->getContext($context, "with_minutes"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "minutes", array()), 'widget');
            }
            // line 103
            if (($context["with_seconds"] ?? $this->getContext($context, "with_seconds"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "seconds", array()), 'widget');
            }
            // line 104
            if (($context["with_invert"] ?? $this->getContext($context, "with_invert"))) {
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "invert", array()), 'widget');
            }
            // line 105
            echo "</div>";
        }
        
        $__internal_92b552d2bc5e4f131d97684e71a6056b2a5605a1589f39e7fa4b7f3f49df3ea7->leave($__internal_92b552d2bc5e4f131d97684e71a6056b2a5605a1589f39e7fa4b7f3f49df3ea7_prof);

        
        $__internal_1e9bff05e79a26a9ab0ece8ec763ddc57ba8529c230966a61f77af4b1ca4e02c->leave($__internal_1e9bff05e79a26a9ab0ece8ec763ddc57ba8529c230966a61f77af4b1ca4e02c_prof);

    }

    // line 109
    public function block_choice_widget_collapsed($context, array $blocks = array())
    {
        $__internal_96c99603911fb29cdac03d594f445a96b3eab5db4b9dbeebf4337da17a9bd95e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96c99603911fb29cdac03d594f445a96b3eab5db4b9dbeebf4337da17a9bd95e->enter($__internal_96c99603911fb29cdac03d594f445a96b3eab5db4b9dbeebf4337da17a9bd95e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        $__internal_2310f857f92e51bbbdfed74133f6fa0bc103b4552799eed748cd734b2e1d9a00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2310f857f92e51bbbdfed74133f6fa0bc103b4552799eed748cd734b2e1d9a00->enter($__internal_2310f857f92e51bbbdfed74133f6fa0bc103b4552799eed748cd734b2e1d9a00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_collapsed"));

        // line 110
        $context["attr"] = twig_array_merge(($context["attr"] ?? $this->getContext($context, "attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["attr"] ?? null), "class", array()), "")) : ("")) . " form-control"))));
        // line 111
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        
        $__internal_2310f857f92e51bbbdfed74133f6fa0bc103b4552799eed748cd734b2e1d9a00->leave($__internal_2310f857f92e51bbbdfed74133f6fa0bc103b4552799eed748cd734b2e1d9a00_prof);

        
        $__internal_96c99603911fb29cdac03d594f445a96b3eab5db4b9dbeebf4337da17a9bd95e->leave($__internal_96c99603911fb29cdac03d594f445a96b3eab5db4b9dbeebf4337da17a9bd95e_prof);

    }

    // line 114
    public function block_choice_widget_expanded($context, array $blocks = array())
    {
        $__internal_3fa1735e85c0cfdcca921324f932d55843c1740582f695545b429da8e5a0d0ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fa1735e85c0cfdcca921324f932d55843c1740582f695545b429da8e5a0d0ca->enter($__internal_3fa1735e85c0cfdcca921324f932d55843c1740582f695545b429da8e5a0d0ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        $__internal_74c6bcfb8778654ac1474d0eb17b077488e08c054014bc0370b82841f62e9c62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_74c6bcfb8778654ac1474d0eb17b077488e08c054014bc0370b82841f62e9c62->enter($__internal_74c6bcfb8778654ac1474d0eb17b077488e08c054014bc0370b82841f62e9c62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_widget_expanded"));

        // line 115
        if (twig_in_filter("-inline", (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) {
            // line 116
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 117
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 118
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 119
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        } else {
            // line 123
            echo "<div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">";
            // line 124
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? $this->getContext($context, "form")));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 125
                echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($context["child"], 'widget', array("parent_label_class" => (($this->getAttribute(                // line 126
($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), "translation_domain" =>                 // line 127
($context["choice_translation_domain"] ?? $this->getContext($context, "choice_translation_domain"))));
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "</div>";
        }
        
        $__internal_74c6bcfb8778654ac1474d0eb17b077488e08c054014bc0370b82841f62e9c62->leave($__internal_74c6bcfb8778654ac1474d0eb17b077488e08c054014bc0370b82841f62e9c62_prof);

        
        $__internal_3fa1735e85c0cfdcca921324f932d55843c1740582f695545b429da8e5a0d0ca->leave($__internal_3fa1735e85c0cfdcca921324f932d55843c1740582f695545b429da8e5a0d0ca_prof);

    }

    // line 134
    public function block_checkbox_widget($context, array $blocks = array())
    {
        $__internal_86b5af1cb6dfbd4d61308db044d204c7017772bfa94ad2f0ec5cbe457c90685d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86b5af1cb6dfbd4d61308db044d204c7017772bfa94ad2f0ec5cbe457c90685d->enter($__internal_86b5af1cb6dfbd4d61308db044d204c7017772bfa94ad2f0ec5cbe457c90685d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        $__internal_2b7ba52aa10d5ab1a93e8478db6b2d2823476228658a4080eca06470747a0b76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2b7ba52aa10d5ab1a93e8478db6b2d2823476228658a4080eca06470747a0b76->enter($__internal_2b7ba52aa10d5ab1a93e8478db6b2d2823476228658a4080eca06470747a0b76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_widget"));

        // line 135
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 136
        if (twig_in_filter("checkbox-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 137
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
        } else {
            // line 139
            echo "<div class=\"checkbox\">";
            // line 140
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("checkbox_widget", $context, $blocks)));
            // line 141
            echo "</div>";
        }
        
        $__internal_2b7ba52aa10d5ab1a93e8478db6b2d2823476228658a4080eca06470747a0b76->leave($__internal_2b7ba52aa10d5ab1a93e8478db6b2d2823476228658a4080eca06470747a0b76_prof);

        
        $__internal_86b5af1cb6dfbd4d61308db044d204c7017772bfa94ad2f0ec5cbe457c90685d->leave($__internal_86b5af1cb6dfbd4d61308db044d204c7017772bfa94ad2f0ec5cbe457c90685d_prof);

    }

    // line 145
    public function block_radio_widget($context, array $blocks = array())
    {
        $__internal_fe0ac13a49a26881e6cca37650aeb2f5009d5473274b404b4eb03c4af224498b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe0ac13a49a26881e6cca37650aeb2f5009d5473274b404b4eb03c4af224498b->enter($__internal_fe0ac13a49a26881e6cca37650aeb2f5009d5473274b404b4eb03c4af224498b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        $__internal_7ba8daea093964dee15a43b982cb247cc702d970bca98c741cb7314b0da25dca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7ba8daea093964dee15a43b982cb247cc702d970bca98c741cb7314b0da25dca->enter($__internal_7ba8daea093964dee15a43b982cb247cc702d970bca98c741cb7314b0da25dca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_widget"));

        // line 146
        $context["parent_label_class"] = ((array_key_exists("parent_label_class", $context)) ? (_twig_default_filter(($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")), (($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")))) : ((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : (""))));
        // line 147
        if (twig_in_filter("radio-inline", ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class")))) {
            // line 148
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
        } else {
            // line 150
            echo "<div class=\"radio\">";
            // line 151
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label', array("widget" => $this->renderParentBlock("radio_widget", $context, $blocks)));
            // line 152
            echo "</div>";
        }
        
        $__internal_7ba8daea093964dee15a43b982cb247cc702d970bca98c741cb7314b0da25dca->leave($__internal_7ba8daea093964dee15a43b982cb247cc702d970bca98c741cb7314b0da25dca_prof);

        
        $__internal_fe0ac13a49a26881e6cca37650aeb2f5009d5473274b404b4eb03c4af224498b->leave($__internal_fe0ac13a49a26881e6cca37650aeb2f5009d5473274b404b4eb03c4af224498b_prof);

    }

    // line 158
    public function block_form_label($context, array $blocks = array())
    {
        $__internal_519654016937471387b6553a1ded2639bfaf10d5b25c4be7e15cb72feb0e2924 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_519654016937471387b6553a1ded2639bfaf10d5b25c4be7e15cb72feb0e2924->enter($__internal_519654016937471387b6553a1ded2639bfaf10d5b25c4be7e15cb72feb0e2924_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        $__internal_4b493c9074cd13419c274bb5e58dd257e7e90e7431c107a3f41c8494419905e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4b493c9074cd13419c274bb5e58dd257e7e90e7431c107a3f41c8494419905e7->enter($__internal_4b493c9074cd13419c274bb5e58dd257e7e90e7431c107a3f41c8494419905e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_label"));

        // line 159
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " control-label"))));
        // line 160
        $this->displayParentBlock("form_label", $context, $blocks);
        
        $__internal_4b493c9074cd13419c274bb5e58dd257e7e90e7431c107a3f41c8494419905e7->leave($__internal_4b493c9074cd13419c274bb5e58dd257e7e90e7431c107a3f41c8494419905e7_prof);

        
        $__internal_519654016937471387b6553a1ded2639bfaf10d5b25c4be7e15cb72feb0e2924->leave($__internal_519654016937471387b6553a1ded2639bfaf10d5b25c4be7e15cb72feb0e2924_prof);

    }

    // line 163
    public function block_choice_label($context, array $blocks = array())
    {
        $__internal_d27870c3428e29d000e8b4308165c51805953c05b1e2cbdfc366bc4478f86c5d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d27870c3428e29d000e8b4308165c51805953c05b1e2cbdfc366bc4478f86c5d->enter($__internal_d27870c3428e29d000e8b4308165c51805953c05b1e2cbdfc366bc4478f86c5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        $__internal_9186bea93b47a221a3b69f7015dadea527c1299f2b74db3d104b6c2b9562b8cd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9186bea93b47a221a3b69f7015dadea527c1299f2b74db3d104b6c2b9562b8cd->enter($__internal_9186bea93b47a221a3b69f7015dadea527c1299f2b74db3d104b6c2b9562b8cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_label"));

        // line 165
        $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(twig_replace_filter((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")), array("checkbox-inline" => "", "radio-inline" => "")))));
        // line 166
        $this->displayBlock("form_label", $context, $blocks);
        
        $__internal_9186bea93b47a221a3b69f7015dadea527c1299f2b74db3d104b6c2b9562b8cd->leave($__internal_9186bea93b47a221a3b69f7015dadea527c1299f2b74db3d104b6c2b9562b8cd_prof);

        
        $__internal_d27870c3428e29d000e8b4308165c51805953c05b1e2cbdfc366bc4478f86c5d->leave($__internal_d27870c3428e29d000e8b4308165c51805953c05b1e2cbdfc366bc4478f86c5d_prof);

    }

    // line 169
    public function block_checkbox_label($context, array $blocks = array())
    {
        $__internal_0c8eca6e618e54f07d14ccd0c5a83dbcc6027cf99f9e48a4730423374a2a3cc0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0c8eca6e618e54f07d14ccd0c5a83dbcc6027cf99f9e48a4730423374a2a3cc0->enter($__internal_0c8eca6e618e54f07d14ccd0c5a83dbcc6027cf99f9e48a4730423374a2a3cc0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        $__internal_5d72c1e996680ba40ac0a05a516315c0f9594e1695597f1347b4f3148820bb1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d72c1e996680ba40ac0a05a516315c0f9594e1695597f1347b4f3148820bb1c->enter($__internal_5d72c1e996680ba40ac0a05a516315c0f9594e1695597f1347b4f3148820bb1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_label"));

        // line 170
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_5d72c1e996680ba40ac0a05a516315c0f9594e1695597f1347b4f3148820bb1c->leave($__internal_5d72c1e996680ba40ac0a05a516315c0f9594e1695597f1347b4f3148820bb1c_prof);

        
        $__internal_0c8eca6e618e54f07d14ccd0c5a83dbcc6027cf99f9e48a4730423374a2a3cc0->leave($__internal_0c8eca6e618e54f07d14ccd0c5a83dbcc6027cf99f9e48a4730423374a2a3cc0_prof);

    }

    // line 173
    public function block_radio_label($context, array $blocks = array())
    {
        $__internal_88153b19e94ccacadc9e7b507b78643a4b7c85e1d83ed5217b3c1b8f2b3853cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_88153b19e94ccacadc9e7b507b78643a4b7c85e1d83ed5217b3c1b8f2b3853cd->enter($__internal_88153b19e94ccacadc9e7b507b78643a4b7c85e1d83ed5217b3c1b8f2b3853cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        $__internal_93b1803c425d77ef38d74a6c423d9648eeaa4062d0bd998629d3e5289e4eb274 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93b1803c425d77ef38d74a6c423d9648eeaa4062d0bd998629d3e5289e4eb274->enter($__internal_93b1803c425d77ef38d74a6c423d9648eeaa4062d0bd998629d3e5289e4eb274_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_label"));

        // line 174
        $this->displayBlock("checkbox_radio_label", $context, $blocks);
        
        $__internal_93b1803c425d77ef38d74a6c423d9648eeaa4062d0bd998629d3e5289e4eb274->leave($__internal_93b1803c425d77ef38d74a6c423d9648eeaa4062d0bd998629d3e5289e4eb274_prof);

        
        $__internal_88153b19e94ccacadc9e7b507b78643a4b7c85e1d83ed5217b3c1b8f2b3853cd->leave($__internal_88153b19e94ccacadc9e7b507b78643a4b7c85e1d83ed5217b3c1b8f2b3853cd_prof);

    }

    // line 177
    public function block_checkbox_radio_label($context, array $blocks = array())
    {
        $__internal_b77010c49fe41f1d1c021e6f51c93d6a8bb3185ffe6f4eea88421d7f1e21f1be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b77010c49fe41f1d1c021e6f51c93d6a8bb3185ffe6f4eea88421d7f1e21f1be->enter($__internal_b77010c49fe41f1d1c021e6f51c93d6a8bb3185ffe6f4eea88421d7f1e21f1be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        $__internal_5733246a0e6e8b656d74abaef93ecf6bc83324797e59caf6687a801ef6254185 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5733246a0e6e8b656d74abaef93ecf6bc83324797e59caf6687a801ef6254185->enter($__internal_5733246a0e6e8b656d74abaef93ecf6bc83324797e59caf6687a801ef6254185_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_radio_label"));

        // line 178
        echo "    ";
        // line 179
        echo "    ";
        if (array_key_exists("widget", $context)) {
            // line 180
            echo "        ";
            if (($context["required"] ?? $this->getContext($context, "required"))) {
                // line 181
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter(((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " required"))));
                // line 182
                echo "        ";
            }
            // line 183
            echo "        ";
            if (array_key_exists("parent_label_class", $context)) {
                // line 184
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? $this->getContext($context, "label_attr")), array("class" => twig_trim_filter((((($this->getAttribute(($context["label_attr"] ?? null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute(($context["label_attr"] ?? null), "class", array()), "")) : ("")) . " ") . ($context["parent_label_class"] ?? $this->getContext($context, "parent_label_class"))))));
                // line 185
                echo "        ";
            }
            // line 186
            echo "        ";
            if (( !(($context["label"] ?? $this->getContext($context, "label")) === false) && twig_test_empty(($context["label"] ?? $this->getContext($context, "label"))))) {
                // line 187
                if ( !twig_test_empty(($context["label_format"] ?? $this->getContext($context, "label_format")))) {
                    // line 188
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? $this->getContext($context, "label_format")), array("%name%" =>                     // line 189
($context["name"] ?? $this->getContext($context, "name")), "%id%" =>                     // line 190
($context["id"] ?? $this->getContext($context, "id"))));
                } else {
                    // line 193
                    $context["label"] = $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->humanize(($context["name"] ?? $this->getContext($context, "name")));
                }
            }
            // line 196
            echo "        <label";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? $this->getContext($context, "label_attr")));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 197
            echo ($context["widget"] ?? $this->getContext($context, "widget"));
            echo " ";
            echo twig_escape_filter($this->env, (( !(($context["label"] ?? $this->getContext($context, "label")) === false)) ? ((((($context["translation_domain"] ?? $this->getContext($context, "translation_domain")) === false)) ? (($context["label"] ?? $this->getContext($context, "label"))) : ($this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(($context["label"] ?? $this->getContext($context, "label")), array(), ($context["translation_domain"] ?? $this->getContext($context, "translation_domain")))))) : ("")), "html", null, true);
            // line 198
            echo "</label>
    ";
        }
        
        $__internal_5733246a0e6e8b656d74abaef93ecf6bc83324797e59caf6687a801ef6254185->leave($__internal_5733246a0e6e8b656d74abaef93ecf6bc83324797e59caf6687a801ef6254185_prof);

        
        $__internal_b77010c49fe41f1d1c021e6f51c93d6a8bb3185ffe6f4eea88421d7f1e21f1be->leave($__internal_b77010c49fe41f1d1c021e6f51c93d6a8bb3185ffe6f4eea88421d7f1e21f1be_prof);

    }

    // line 204
    public function block_form_row($context, array $blocks = array())
    {
        $__internal_e4d558dad11e74a6af9c2a66ac5ffeda69f51de6efdf44c7a74ed363d4e20ade = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e4d558dad11e74a6af9c2a66ac5ffeda69f51de6efdf44c7a74ed363d4e20ade->enter($__internal_e4d558dad11e74a6af9c2a66ac5ffeda69f51de6efdf44c7a74ed363d4e20ade_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        $__internal_34fe6f8c29cd2378074cef526d94f3d6bf962cb1780c8ce4a8a4682e3f9ef86b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_34fe6f8c29cd2378074cef526d94f3d6bf962cb1780c8ce4a8a4682e3f9ef86b->enter($__internal_34fe6f8c29cd2378074cef526d94f3d6bf962cb1780c8ce4a8a4682e3f9ef86b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_row"));

        // line 205
        echo "<div class=\"form-group";
        if ((( !($context["compound"] ?? $this->getContext($context, "compound")) || ((array_key_exists("force_error", $context)) ? (_twig_default_filter(($context["force_error"] ?? $this->getContext($context, "force_error")), false)) : (false))) &&  !($context["valid"] ?? $this->getContext($context, "valid")))) {
            echo " has-error";
        }
        echo "\">";
        // line 206
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'label');
        // line 207
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 208
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 209
        echo "</div>";
        
        $__internal_34fe6f8c29cd2378074cef526d94f3d6bf962cb1780c8ce4a8a4682e3f9ef86b->leave($__internal_34fe6f8c29cd2378074cef526d94f3d6bf962cb1780c8ce4a8a4682e3f9ef86b_prof);

        
        $__internal_e4d558dad11e74a6af9c2a66ac5ffeda69f51de6efdf44c7a74ed363d4e20ade->leave($__internal_e4d558dad11e74a6af9c2a66ac5ffeda69f51de6efdf44c7a74ed363d4e20ade_prof);

    }

    // line 212
    public function block_button_row($context, array $blocks = array())
    {
        $__internal_bb5475de15d8455c680953bd0a377c2d913ee24a9c90c56a9ca59ba8f87f3fea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb5475de15d8455c680953bd0a377c2d913ee24a9c90c56a9ca59ba8f87f3fea->enter($__internal_bb5475de15d8455c680953bd0a377c2d913ee24a9c90c56a9ca59ba8f87f3fea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        $__internal_335a15e8a2f618c0773cdb5379b61b38ca43df9909f9b8f6cf7f2ee303156cc4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_335a15e8a2f618c0773cdb5379b61b38ca43df9909f9b8f6cf7f2ee303156cc4->enter($__internal_335a15e8a2f618c0773cdb5379b61b38ca43df9909f9b8f6cf7f2ee303156cc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "button_row"));

        // line 213
        echo "<div class=\"form-group\">";
        // line 214
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 215
        echo "</div>";
        
        $__internal_335a15e8a2f618c0773cdb5379b61b38ca43df9909f9b8f6cf7f2ee303156cc4->leave($__internal_335a15e8a2f618c0773cdb5379b61b38ca43df9909f9b8f6cf7f2ee303156cc4_prof);

        
        $__internal_bb5475de15d8455c680953bd0a377c2d913ee24a9c90c56a9ca59ba8f87f3fea->leave($__internal_bb5475de15d8455c680953bd0a377c2d913ee24a9c90c56a9ca59ba8f87f3fea_prof);

    }

    // line 218
    public function block_choice_row($context, array $blocks = array())
    {
        $__internal_2e165c50be84cd18e20fedcdd667625c868c46ecd2d29bbec7c76c29d70ec971 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e165c50be84cd18e20fedcdd667625c868c46ecd2d29bbec7c76c29d70ec971->enter($__internal_2e165c50be84cd18e20fedcdd667625c868c46ecd2d29bbec7c76c29d70ec971_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        $__internal_3e113cd6a2eea4cdf5e2b6626e6c82886486fbb29375a8836b2bb7f842f056e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e113cd6a2eea4cdf5e2b6626e6c82886486fbb29375a8836b2bb7f842f056e4->enter($__internal_3e113cd6a2eea4cdf5e2b6626e6c82886486fbb29375a8836b2bb7f842f056e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "choice_row"));

        // line 219
        $context["force_error"] = true;
        // line 220
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_3e113cd6a2eea4cdf5e2b6626e6c82886486fbb29375a8836b2bb7f842f056e4->leave($__internal_3e113cd6a2eea4cdf5e2b6626e6c82886486fbb29375a8836b2bb7f842f056e4_prof);

        
        $__internal_2e165c50be84cd18e20fedcdd667625c868c46ecd2d29bbec7c76c29d70ec971->leave($__internal_2e165c50be84cd18e20fedcdd667625c868c46ecd2d29bbec7c76c29d70ec971_prof);

    }

    // line 223
    public function block_date_row($context, array $blocks = array())
    {
        $__internal_75040f5bc42be57ddac7ec698d6a169d444390245feb9d689543d566b5d906d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75040f5bc42be57ddac7ec698d6a169d444390245feb9d689543d566b5d906d4->enter($__internal_75040f5bc42be57ddac7ec698d6a169d444390245feb9d689543d566b5d906d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        $__internal_635efaccbd4907cbea539bc1547b1a60a2b082a4f3ccf978a53ddb3b2c08140d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_635efaccbd4907cbea539bc1547b1a60a2b082a4f3ccf978a53ddb3b2c08140d->enter($__internal_635efaccbd4907cbea539bc1547b1a60a2b082a4f3ccf978a53ddb3b2c08140d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "date_row"));

        // line 224
        $context["force_error"] = true;
        // line 225
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_635efaccbd4907cbea539bc1547b1a60a2b082a4f3ccf978a53ddb3b2c08140d->leave($__internal_635efaccbd4907cbea539bc1547b1a60a2b082a4f3ccf978a53ddb3b2c08140d_prof);

        
        $__internal_75040f5bc42be57ddac7ec698d6a169d444390245feb9d689543d566b5d906d4->leave($__internal_75040f5bc42be57ddac7ec698d6a169d444390245feb9d689543d566b5d906d4_prof);

    }

    // line 228
    public function block_time_row($context, array $blocks = array())
    {
        $__internal_2126bad5ec385572d17a9cec6d7c6aa2630d7c1c6e63e2e792cd8d5d1d198fd2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2126bad5ec385572d17a9cec6d7c6aa2630d7c1c6e63e2e792cd8d5d1d198fd2->enter($__internal_2126bad5ec385572d17a9cec6d7c6aa2630d7c1c6e63e2e792cd8d5d1d198fd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        $__internal_40d9761ef3648819f860529f2d6f3a0d71444ccd48f28f977ba03f49ce6e73a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40d9761ef3648819f860529f2d6f3a0d71444ccd48f28f977ba03f49ce6e73a7->enter($__internal_40d9761ef3648819f860529f2d6f3a0d71444ccd48f28f977ba03f49ce6e73a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "time_row"));

        // line 229
        $context["force_error"] = true;
        // line 230
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_40d9761ef3648819f860529f2d6f3a0d71444ccd48f28f977ba03f49ce6e73a7->leave($__internal_40d9761ef3648819f860529f2d6f3a0d71444ccd48f28f977ba03f49ce6e73a7_prof);

        
        $__internal_2126bad5ec385572d17a9cec6d7c6aa2630d7c1c6e63e2e792cd8d5d1d198fd2->leave($__internal_2126bad5ec385572d17a9cec6d7c6aa2630d7c1c6e63e2e792cd8d5d1d198fd2_prof);

    }

    // line 233
    public function block_datetime_row($context, array $blocks = array())
    {
        $__internal_51e31a6f75fa95d384c84664fe33d0be109b2f35f18e0e4a4ebd5afdde9953fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51e31a6f75fa95d384c84664fe33d0be109b2f35f18e0e4a4ebd5afdde9953fe->enter($__internal_51e31a6f75fa95d384c84664fe33d0be109b2f35f18e0e4a4ebd5afdde9953fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        $__internal_aac4e7ab171b6b2a19da48fa03db51995aa992375d7e44ae7f5882d75ba7f068 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_aac4e7ab171b6b2a19da48fa03db51995aa992375d7e44ae7f5882d75ba7f068->enter($__internal_aac4e7ab171b6b2a19da48fa03db51995aa992375d7e44ae7f5882d75ba7f068_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "datetime_row"));

        // line 234
        $context["force_error"] = true;
        // line 235
        $this->displayBlock("form_row", $context, $blocks);
        
        $__internal_aac4e7ab171b6b2a19da48fa03db51995aa992375d7e44ae7f5882d75ba7f068->leave($__internal_aac4e7ab171b6b2a19da48fa03db51995aa992375d7e44ae7f5882d75ba7f068_prof);

        
        $__internal_51e31a6f75fa95d384c84664fe33d0be109b2f35f18e0e4a4ebd5afdde9953fe->leave($__internal_51e31a6f75fa95d384c84664fe33d0be109b2f35f18e0e4a4ebd5afdde9953fe_prof);

    }

    // line 238
    public function block_checkbox_row($context, array $blocks = array())
    {
        $__internal_c1f315f36a573af853f5e59c2c91be4e3ef4089065a8280a9ce96799af4b8840 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1f315f36a573af853f5e59c2c91be4e3ef4089065a8280a9ce96799af4b8840->enter($__internal_c1f315f36a573af853f5e59c2c91be4e3ef4089065a8280a9ce96799af4b8840_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        $__internal_db193a15532bbe3f1a80640c06146f0fcbedffa1efd5c9837399148f53342891 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db193a15532bbe3f1a80640c06146f0fcbedffa1efd5c9837399148f53342891->enter($__internal_db193a15532bbe3f1a80640c06146f0fcbedffa1efd5c9837399148f53342891_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "checkbox_row"));

        // line 239
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 240
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 241
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 242
        echo "</div>";
        
        $__internal_db193a15532bbe3f1a80640c06146f0fcbedffa1efd5c9837399148f53342891->leave($__internal_db193a15532bbe3f1a80640c06146f0fcbedffa1efd5c9837399148f53342891_prof);

        
        $__internal_c1f315f36a573af853f5e59c2c91be4e3ef4089065a8280a9ce96799af4b8840->leave($__internal_c1f315f36a573af853f5e59c2c91be4e3ef4089065a8280a9ce96799af4b8840_prof);

    }

    // line 245
    public function block_radio_row($context, array $blocks = array())
    {
        $__internal_3562c35c359f2703895aa095ae4acd14427caf498f741095fddd6ae3ff9d6f1e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3562c35c359f2703895aa095ae4acd14427caf498f741095fddd6ae3ff9d6f1e->enter($__internal_3562c35c359f2703895aa095ae4acd14427caf498f741095fddd6ae3ff9d6f1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        $__internal_cf091e6b113b9f7b63ad112d21c1bb71b5995760e30ce33a26d30bbfd04704a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf091e6b113b9f7b63ad112d21c1bb71b5995760e30ce33a26d30bbfd04704a3->enter($__internal_cf091e6b113b9f7b63ad112d21c1bb71b5995760e30ce33a26d30bbfd04704a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "radio_row"));

        // line 246
        echo "<div class=\"form-group";
        if ( !($context["valid"] ?? $this->getContext($context, "valid"))) {
            echo " has-error";
        }
        echo "\">";
        // line 247
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'widget');
        // line 248
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["form"] ?? $this->getContext($context, "form")), 'errors');
        // line 249
        echo "</div>";
        
        $__internal_cf091e6b113b9f7b63ad112d21c1bb71b5995760e30ce33a26d30bbfd04704a3->leave($__internal_cf091e6b113b9f7b63ad112d21c1bb71b5995760e30ce33a26d30bbfd04704a3_prof);

        
        $__internal_3562c35c359f2703895aa095ae4acd14427caf498f741095fddd6ae3ff9d6f1e->leave($__internal_3562c35c359f2703895aa095ae4acd14427caf498f741095fddd6ae3ff9d6f1e_prof);

    }

    // line 254
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_70ff51a23663c623cac696199c13ceeada9c65ca497ebf20c9f1974967a40efb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70ff51a23663c623cac696199c13ceeada9c65ca497ebf20c9f1974967a40efb->enter($__internal_70ff51a23663c623cac696199c13ceeada9c65ca497ebf20c9f1974967a40efb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_9f11977c411d7f5e6b956de095025c917903023c19cb7e7ea022a3ed1ea8d4e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f11977c411d7f5e6b956de095025c917903023c19cb7e7ea022a3ed1ea8d4e2->enter($__internal_9f11977c411d7f5e6b956de095025c917903023c19cb7e7ea022a3ed1ea8d4e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 255
        if ((twig_length_filter($this->env, ($context["errors"] ?? $this->getContext($context, "errors"))) > 0)) {
            // line 256
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 257
            echo "    <ul class=\"list-unstyled\">";
            // line 258
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 259
                echo "<li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 261
            echo "</ul>
    ";
            // line 262
            if ($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_9f11977c411d7f5e6b956de095025c917903023c19cb7e7ea022a3ed1ea8d4e2->leave($__internal_9f11977c411d7f5e6b956de095025c917903023c19cb7e7ea022a3ed1ea8d4e2_prof);

        
        $__internal_70ff51a23663c623cac696199c13ceeada9c65ca497ebf20c9f1974967a40efb->leave($__internal_70ff51a23663c623cac696199c13ceeada9c65ca497ebf20c9f1974967a40efb_prof);

    }

    public function getTemplateName()
    {
        return "bootstrap_3_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  1061 => 262,  1058 => 261,  1050 => 259,  1046 => 258,  1044 => 257,  1038 => 256,  1036 => 255,  1027 => 254,  1017 => 249,  1015 => 248,  1013 => 247,  1007 => 246,  998 => 245,  988 => 242,  986 => 241,  984 => 240,  978 => 239,  969 => 238,  959 => 235,  957 => 234,  948 => 233,  938 => 230,  936 => 229,  927 => 228,  917 => 225,  915 => 224,  906 => 223,  896 => 220,  894 => 219,  885 => 218,  875 => 215,  873 => 214,  871 => 213,  862 => 212,  852 => 209,  850 => 208,  848 => 207,  846 => 206,  840 => 205,  831 => 204,  819 => 198,  815 => 197,  800 => 196,  796 => 193,  793 => 190,  792 => 189,  791 => 188,  789 => 187,  786 => 186,  783 => 185,  780 => 184,  777 => 183,  774 => 182,  771 => 181,  768 => 180,  765 => 179,  763 => 178,  754 => 177,  744 => 174,  735 => 173,  725 => 170,  716 => 169,  706 => 166,  704 => 165,  695 => 163,  685 => 160,  683 => 159,  674 => 158,  663 => 152,  661 => 151,  659 => 150,  656 => 148,  654 => 147,  652 => 146,  643 => 145,  632 => 141,  630 => 140,  628 => 139,  625 => 137,  623 => 136,  621 => 135,  612 => 134,  601 => 130,  595 => 127,  594 => 126,  593 => 125,  589 => 124,  585 => 123,  578 => 119,  577 => 118,  576 => 117,  572 => 116,  570 => 115,  561 => 114,  551 => 111,  549 => 110,  540 => 109,  529 => 105,  525 => 104,  521 => 103,  517 => 102,  513 => 101,  509 => 100,  505 => 99,  501 => 98,  497 => 97,  495 => 96,  491 => 95,  489 => 94,  486 => 92,  484 => 91,  475 => 90,  463 => 85,  460 => 84,  450 => 83,  445 => 81,  443 => 80,  441 => 79,  438 => 77,  436 => 76,  427 => 75,  415 => 70,  413 => 69,  411 => 67,  410 => 66,  409 => 65,  408 => 64,  403 => 62,  401 => 61,  399 => 60,  396 => 58,  394 => 57,  385 => 56,  374 => 52,  372 => 51,  370 => 50,  368 => 49,  366 => 48,  362 => 47,  360 => 46,  357 => 44,  355 => 43,  346 => 42,  335 => 38,  333 => 37,  331 => 36,  322 => 35,  312 => 32,  306 => 30,  304 => 29,  302 => 28,  296 => 26,  293 => 25,  291 => 24,  288 => 23,  279 => 22,  269 => 19,  267 => 18,  258 => 17,  248 => 14,  246 => 13,  237 => 12,  227 => 9,  224 => 7,  222 => 6,  213 => 5,  203 => 254,  200 => 253,  197 => 251,  195 => 245,  192 => 244,  190 => 238,  187 => 237,  185 => 233,  182 => 232,  180 => 228,  177 => 227,  175 => 223,  172 => 222,  170 => 218,  167 => 217,  165 => 212,  162 => 211,  160 => 204,  157 => 203,  154 => 201,  152 => 177,  149 => 176,  147 => 173,  144 => 172,  142 => 169,  139 => 168,  137 => 163,  134 => 162,  132 => 158,  129 => 157,  126 => 155,  124 => 145,  121 => 144,  119 => 134,  116 => 133,  114 => 114,  111 => 113,  109 => 109,  107 => 90,  105 => 75,  102 => 74,  100 => 56,  97 => 55,  95 => 42,  92 => 41,  90 => 35,  87 => 34,  85 => 22,  82 => 21,  80 => 17,  77 => 16,  75 => 12,  72 => 11,  70 => 5,  67 => 4,  64 => 2,  14 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% use \"form_div_layout.html.twig\" %}

{# Widgets #}

{% block form_widget_simple -%}
    {% if type is not defined or type not in ['file', 'hidden'] %}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) -%}
    {% endif %}
    {{- parent() -}}
{%- endblock form_widget_simple %}

{% block textarea_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock textarea_widget %}

{% block button_widget -%}
    {% set attr = attr|merge({class: (attr.class|default('btn-default') ~ ' btn')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block money_widget -%}
    <div class=\"input-group\">
        {% set append = money_pattern starts with '{{' %}
        {% if not append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
        {{- block('form_widget_simple') -}}
        {% if append %}
            <span class=\"input-group-addon\">{{ money_pattern|replace({ '{{ widget }}':''}) }}</span>
        {% endif %}
    </div>
{%- endblock money_widget %}

{% block percent_widget -%}
    <div class=\"input-group\">
        {{- block('form_widget_simple') -}}
        <span class=\"input-group-addon\">%</span>
    </div>
{%- endblock percent_widget %}

{% block datetime_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form.date) -}}
            {{- form_errors(form.time) -}}
            {{- form_widget(form.date, { datetime: true } ) -}}
            {{- form_widget(form.time, { datetime: true } ) -}}
        </div>
    {%- endif %}
{%- endblock datetime_widget %}

{% block date_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or not datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif %}
            {{- date_pattern|replace({
                '{{ year }}': form_widget(form.year),
                '{{ month }}': form_widget(form.month),
                '{{ day }}': form_widget(form.day),
            })|raw -}}
        {% if datetime is not defined or not datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock date_widget %}

{% block time_widget -%}
    {% if widget == 'single_text' %}
        {{- block('form_widget_simple') -}}
    {% else -%}
        {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        {% if datetime is not defined or false == datetime -%}
            <div {{ block('widget_container_attributes') -}}>
        {%- endif -%}
        {{- form_widget(form.hour) }}{% if with_minutes %}:{{ form_widget(form.minute) }}{% endif %}{% if with_seconds %}:{{ form_widget(form.second) }}{% endif %}
        {% if datetime is not defined or false == datetime -%}
            </div>
        {%- endif -%}
    {% endif %}
{%- endblock time_widget %}

{%- block dateinterval_widget -%}
    {%- if widget == 'single_text' -%}
        {{- block('form_widget_simple') -}}
    {%- else -%}
        {%- set attr = attr|merge({class: (attr.class|default('') ~ ' form-inline')|trim}) -%}
        <div {{ block('widget_container_attributes') }}>
            {{- form_errors(form) -}}
            {%- if with_years %}{{ form_widget(form.years) }}{% endif -%}
            {%- if with_months %}{{ form_widget(form.months) }}{% endif -%}
            {%- if with_weeks %}{{ form_widget(form.weeks) }}{% endif -%}
            {%- if with_days %}{{ form_widget(form.days) }}{% endif -%}
            {%- if with_hours %}{{ form_widget(form.hours) }}{% endif -%}
            {%- if with_minutes %}{{ form_widget(form.minutes) }}{% endif -%}
            {%- if with_seconds %}{{ form_widget(form.seconds) }}{% endif -%}
            {%- if with_invert %}{{ form_widget(form.invert) }}{% endif -%}
        </div>
    {%- endif -%}
{%- endblock dateinterval_widget -%}

{% block choice_widget_collapsed -%}
    {% set attr = attr|merge({class: (attr.class|default('') ~ ' form-control')|trim}) %}
    {{- parent() -}}
{%- endblock %}

{% block choice_widget_expanded -%}
    {% if '-inline' in label_attr.class|default('') -%}
        {%- for child in form %}
            {{- form_widget(child, {
                parent_label_class: label_attr.class|default(''),
                translation_domain: choice_translation_domain,
            }) -}}
        {% endfor -%}
    {%- else -%}
        <div {{ block('widget_container_attributes') }}>
            {%- for child in form %}
                {{- form_widget(child, {
                    parent_label_class: label_attr.class|default(''),
                    translation_domain: choice_translation_domain,
                }) -}}
            {% endfor -%}
        </div>
    {%- endif %}
{%- endblock choice_widget_expanded %}

{% block checkbox_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'checkbox-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"checkbox\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock checkbox_widget %}

{% block radio_widget -%}
    {%- set parent_label_class = parent_label_class|default(label_attr.class|default('')) -%}
    {% if 'radio-inline' in parent_label_class %}
        {{- form_label(form, null, { widget: parent() }) -}}
    {% else -%}
        <div class=\"radio\">
            {{- form_label(form, null, { widget: parent() }) -}}
        </div>
    {%- endif %}
{%- endblock radio_widget %}

{# Labels #}

{% block form_label -%}
    {%- set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' control-label')|trim}) -%}
    {{- parent() -}}
{%- endblock form_label %}

{% block choice_label -%}
    {# remove the checkbox-inline and radio-inline class, it's only useful for embed labels #}
    {%- set label_attr = label_attr|merge({class: label_attr.class|default('')|replace({'checkbox-inline': '', 'radio-inline': ''})|trim}) -%}
    {{- block('form_label') -}}
{% endblock %}

{% block checkbox_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock checkbox_label %}

{% block radio_label -%}
    {{- block('checkbox_radio_label') -}}
{%- endblock radio_label %}

{% block checkbox_radio_label %}
    {# Do not display the label if widget is not defined in order to prevent double label rendering #}
    {% if widget is defined %}
        {% if required %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' required')|trim}) %}
        {% endif %}
        {% if parent_label_class is defined %}
            {% set label_attr = label_attr|merge({class: (label_attr.class|default('') ~ ' ' ~ parent_label_class)|trim}) %}
        {% endif %}
        {% if label is not same as(false) and label is empty %}
            {%- if label_format is not empty -%}
                {% set label = label_format|replace({
                    '%name%': name,
                    '%id%': id,
                }) %}
            {%- else -%}
                {% set label = name|humanize %}
            {%- endif -%}
        {% endif %}
        <label{% for attrname, attrvalue in label_attr %} {{ attrname }}=\"{{ attrvalue }}\"{% endfor %}>
            {{- widget|raw }} {{ label is not same as(false) ? (translation_domain is same as(false) ? label : label|trans({}, translation_domain)) -}}
        </label>
    {% endif %}
{% endblock checkbox_radio_label %}

{# Rows #}

{% block form_row -%}
    <div class=\"form-group{% if (not compound or force_error|default(false)) and not valid %} has-error{% endif %}\">
        {{- form_label(form) -}}
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock form_row %}

{% block button_row -%}
    <div class=\"form-group\">
        {{- form_widget(form) -}}
    </div>
{%- endblock button_row %}

{% block choice_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock choice_row %}

{% block date_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock date_row %}

{% block time_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock time_row %}

{% block datetime_row -%}
    {% set force_error = true %}
    {{- block('form_row') }}
{%- endblock datetime_row %}

{% block checkbox_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock checkbox_row %}

{% block radio_row -%}
    <div class=\"form-group{% if not valid %} has-error{% endif %}\">
        {{- form_widget(form) -}}
        {{- form_errors(form) -}}
    </div>
{%- endblock radio_row %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
    {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
    <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            <li><span class=\"glyphicon glyphicon-exclamation-sign\"></span> {{ error.message }}</li>
        {%- endfor -%}
    </ul>
    {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", "bootstrap_3_layout.html.twig", "C:\\wamp64\\www\\projet-web\\vendor\\symfony\\symfony\\src\\Symfony\\Bridge\\Twig\\Resources\\views\\Form\\bootstrap_3_layout.html.twig");
    }
}
