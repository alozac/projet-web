<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_5d3f0c8eec27eb6ec7c3a06069109b7bc85987b4c7915b2fdf4d4204ab30bc12 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99b9c1ab540e005033d5dd1d5ee75c0a116cf9494d7a6d7ed6302ae023d1955b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99b9c1ab540e005033d5dd1d5ee75c0a116cf9494d7a6d7ed6302ae023d1955b->enter($__internal_99b9c1ab540e005033d5dd1d5ee75c0a116cf9494d7a6d7ed6302ae023d1955b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_cc9c1bbd634ccc0869f636b6e601fa9d56ae1e636af4af2115328e32fb96387d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cc9c1bbd634ccc0869f636b6e601fa9d56ae1e636af4af2115328e32fb96387d->enter($__internal_cc9c1bbd634ccc0869f636b6e601fa9d56ae1e636af4af2115328e32fb96387d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_99b9c1ab540e005033d5dd1d5ee75c0a116cf9494d7a6d7ed6302ae023d1955b->leave($__internal_99b9c1ab540e005033d5dd1d5ee75c0a116cf9494d7a6d7ed6302ae023d1955b_prof);

        
        $__internal_cc9c1bbd634ccc0869f636b6e601fa9d56ae1e636af4af2115328e32fb96387d->leave($__internal_cc9c1bbd634ccc0869f636b6e601fa9d56ae1e636af4af2115328e32fb96387d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
