<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_3625f97c24411b9a7e21ea46121c0df632b11585270e7999303f7e12c0ac42b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6207c2cd92494170487b866a0f44287b9e559666895638acb17647b12bf917fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6207c2cd92494170487b866a0f44287b9e559666895638acb17647b12bf917fe->enter($__internal_6207c2cd92494170487b866a0f44287b9e559666895638acb17647b12bf917fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        $__internal_d8b8c079a9d6e47e3c36739867d1d9bfa0b7e25a5e387a54917f3f586543b065 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8b8c079a9d6e47e3c36739867d1d9bfa0b7e25a5e387a54917f3f586543b065->enter($__internal_d8b8c079a9d6e47e3c36739867d1d9bfa0b7e25a5e387a54917f3f586543b065_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_6207c2cd92494170487b866a0f44287b9e559666895638acb17647b12bf917fe->leave($__internal_6207c2cd92494170487b866a0f44287b9e559666895638acb17647b12bf917fe_prof);

        
        $__internal_d8b8c079a9d6e47e3c36739867d1d9bfa0b7e25a5e387a54917f3f586543b065->leave($__internal_d8b8c079a9d6e47e3c36739867d1d9bfa0b7e25a5e387a54917f3f586543b065_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/attributes.html.php");
    }
}
