<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b390fd88ae5b2e7c2545545e6a46f5954eb0f4397f65c77bded42c013e54790a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7fbe1d227648fa8a4a20de5627c72beb61cd6b1af3a72ce174f24c3c162291fb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fbe1d227648fa8a4a20de5627c72beb61cd6b1af3a72ce174f24c3c162291fb->enter($__internal_7fbe1d227648fa8a4a20de5627c72beb61cd6b1af3a72ce174f24c3c162291fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_ecb04a8743241ab7fb4342f589643a10e6e662fd69bba19fd3b91ff7abbaf9e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ecb04a8743241ab7fb4342f589643a10e6e662fd69bba19fd3b91ff7abbaf9e5->enter($__internal_ecb04a8743241ab7fb4342f589643a10e6e662fd69bba19fd3b91ff7abbaf9e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7fbe1d227648fa8a4a20de5627c72beb61cd6b1af3a72ce174f24c3c162291fb->leave($__internal_7fbe1d227648fa8a4a20de5627c72beb61cd6b1af3a72ce174f24c3c162291fb_prof);

        
        $__internal_ecb04a8743241ab7fb4342f589643a10e6e662fd69bba19fd3b91ff7abbaf9e5->leave($__internal_ecb04a8743241ab7fb4342f589643a10e6e662fd69bba19fd3b91ff7abbaf9e5_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9b3cb3d452c2bb73f2ca0c2d8984e1095c920169bb6ca9f594f7a79699c600fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b3cb3d452c2bb73f2ca0c2d8984e1095c920169bb6ca9f594f7a79699c600fa->enter($__internal_9b3cb3d452c2bb73f2ca0c2d8984e1095c920169bb6ca9f594f7a79699c600fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_25a3d3965a0cef20e7da9c08fe771e1516982281fe648d4eb9b3e47c3243f7c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25a3d3965a0cef20e7da9c08fe771e1516982281fe648d4eb9b3e47c3243f7c4->enter($__internal_25a3d3965a0cef20e7da9c08fe771e1516982281fe648d4eb9b3e47c3243f7c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_25a3d3965a0cef20e7da9c08fe771e1516982281fe648d4eb9b3e47c3243f7c4->leave($__internal_25a3d3965a0cef20e7da9c08fe771e1516982281fe648d4eb9b3e47c3243f7c4_prof);

        
        $__internal_9b3cb3d452c2bb73f2ca0c2d8984e1095c920169bb6ca9f594f7a79699c600fa->leave($__internal_9b3cb3d452c2bb73f2ca0c2d8984e1095c920169bb6ca9f594f7a79699c600fa_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ffe8b405c6cadea14a89393d29354b37d3158a9c2b779a9405dbfbdbedff1371 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffe8b405c6cadea14a89393d29354b37d3158a9c2b779a9405dbfbdbedff1371->enter($__internal_ffe8b405c6cadea14a89393d29354b37d3158a9c2b779a9405dbfbdbedff1371_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a20326b3762def3170ce51d841e8e90543fb9350a498acf9dd0e50efe1ce9bdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a20326b3762def3170ce51d841e8e90543fb9350a498acf9dd0e50efe1ce9bdf->enter($__internal_a20326b3762def3170ce51d841e8e90543fb9350a498acf9dd0e50efe1ce9bdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_a20326b3762def3170ce51d841e8e90543fb9350a498acf9dd0e50efe1ce9bdf->leave($__internal_a20326b3762def3170ce51d841e8e90543fb9350a498acf9dd0e50efe1ce9bdf_prof);

        
        $__internal_ffe8b405c6cadea14a89393d29354b37d3158a9c2b779a9405dbfbdbedff1371->leave($__internal_ffe8b405c6cadea14a89393d29354b37d3158a9c2b779a9405dbfbdbedff1371_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7ab256b351cf54e2f1da7397f5c6eaf68b95e4f277a67fcf5fabbe9000bd9041 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ab256b351cf54e2f1da7397f5c6eaf68b95e4f277a67fcf5fabbe9000bd9041->enter($__internal_7ab256b351cf54e2f1da7397f5c6eaf68b95e4f277a67fcf5fabbe9000bd9041_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8f5d1488312ae6da1bbe8ae6765edf2c3afe4b2f7af054c4810e64d28ab14247 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8f5d1488312ae6da1bbe8ae6765edf2c3afe4b2f7af054c4810e64d28ab14247->enter($__internal_8f5d1488312ae6da1bbe8ae6765edf2c3afe4b2f7af054c4810e64d28ab14247_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_8f5d1488312ae6da1bbe8ae6765edf2c3afe4b2f7af054c4810e64d28ab14247->leave($__internal_8f5d1488312ae6da1bbe8ae6765edf2c3afe4b2f7af054c4810e64d28ab14247_prof);

        
        $__internal_7ab256b351cf54e2f1da7397f5c6eaf68b95e4f277a67fcf5fabbe9000bd9041->leave($__internal_7ab256b351cf54e2f1da7397f5c6eaf68b95e4f277a67fcf5fabbe9000bd9041_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
