<?php

/* /connect/connect.html.twig */
class __TwigTemplate_e7e99542313bc9c6f7eb6aae32c72047ef77e5da8f217b5b8903a23fdb8567a3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "/connect/connect.html.twig", 1);
        $this->blocks = array(
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e0d547df88fea03f78fc8113db69696f64c161dec6c6ad785d3e0b10779d38b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0d547df88fea03f78fc8113db69696f64c161dec6c6ad785d3e0b10779d38b2->enter($__internal_e0d547df88fea03f78fc8113db69696f64c161dec6c6ad785d3e0b10779d38b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/connect/connect.html.twig"));

        $__internal_5f6c05457a2c96e93d4185dec6400c32045cc78a022483cc1ed7424eb8c6df03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f6c05457a2c96e93d4185dec6400c32045cc78a022483cc1ed7424eb8c6df03->enter($__internal_5f6c05457a2c96e93d4185dec6400c32045cc78a022483cc1ed7424eb8c6df03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "/connect/connect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e0d547df88fea03f78fc8113db69696f64c161dec6c6ad785d3e0b10779d38b2->leave($__internal_e0d547df88fea03f78fc8113db69696f64c161dec6c6ad785d3e0b10779d38b2_prof);

        
        $__internal_5f6c05457a2c96e93d4185dec6400c32045cc78a022483cc1ed7424eb8c6df03->leave($__internal_5f6c05457a2c96e93d4185dec6400c32045cc78a022483cc1ed7424eb8c6df03_prof);

    }

    // line 2
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_78ddf8d0a81007944b242f7a244b83dd6733161fe8996205918d5b01adfe21c7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78ddf8d0a81007944b242f7a244b83dd6733161fe8996205918d5b01adfe21c7->enter($__internal_78ddf8d0a81007944b242f7a244b83dd6733161fe8996205918d5b01adfe21c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_9f500d2955079404d0a4f4f8b9dda4f23f7b65e1ce5d8fff0bbe73a4895a4595 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f500d2955079404d0a4f4f8b9dda4f23f7b65e1ce5d8fff0bbe73a4895a4595->enter($__internal_9f500d2955079404d0a4f4f8b9dda4f23f7b65e1ce5d8fff0bbe73a4895a4595_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 3
        echo "    <h1>Connexion</h1>

    ";
        // line 5
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 6
            echo "\t\t<div class=\"alert alert-danger error_connect\">";
            echo twig_escape_filter($this->env, $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "message", array()), "html", null, true);
            echo "</div>
\t";
        }
        // line 8
        echo "
";
        // line 16
        echo "
  <form action=\"";
        // line 17
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
  \t<div class=\"form-group\">
    \t<label for=\"username\">Pseudo :</label>
    \t<input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, ($context["last_username"] ?? $this->getContext($context, "last_username")), "html", null, true);
        echo "\" />
    </div>
    
    <div class=\"form-group\">
    \t<label for=\"password\">Mot de passe :</label>
    \t<input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\"  />
    </div>
    <button type=\"submit\" class=\"btn btn-primary\">Connexion</button>
  </form>


\t<button type=\"button\" class=\"btn btn-link\">
\t\t<a href=\"";
        // line 32
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("signup");
        echo "\">Inscription</a>
\t</button>

";
        
        $__internal_9f500d2955079404d0a4f4f8b9dda4f23f7b65e1ce5d8fff0bbe73a4895a4595->leave($__internal_9f500d2955079404d0a4f4f8b9dda4f23f7b65e1ce5d8fff0bbe73a4895a4595_prof);

        
        $__internal_78ddf8d0a81007944b242f7a244b83dd6733161fe8996205918d5b01adfe21c7->leave($__internal_78ddf8d0a81007944b242f7a244b83dd6733161fe8996205918d5b01adfe21c7_prof);

    }

    public function getTemplateName()
    {
        return "/connect/connect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  88 => 32,  73 => 20,  67 => 17,  64 => 16,  61 => 8,  55 => 6,  53 => 5,  49 => 3,  40 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block main_screen %}
    <h1>Connexion</h1>

    {% if error %}
\t\t<div class=\"alert alert-danger error_connect\">{{ error.message }}</div>
\t{% endif %}

{#
\t{{ form_start(form) }}
\t{{ form_row(form.username) }}
\t{{ form_row(form.plainPwd) }}
\t{{ form_row(form.connect, { 'attr': {'class': 'btn-primary'} }) }}
\t{{ form_row(form._token) }}
\t{{ form_end(form) }} #}

  <form action=\"{{ path('login') }}\" method=\"post\">
  \t<div class=\"form-group\">
    \t<label for=\"username\">Pseudo :</label>
    \t<input type=\"text\" id=\"username\" class=\"form-control\" name=\"_username\" value=\"{{ last_username }}\" />
    </div>
    
    <div class=\"form-group\">
    \t<label for=\"password\">Mot de passe :</label>
    \t<input type=\"password\" id=\"password\" class=\"form-control\" name=\"_password\"  />
    </div>
    <button type=\"submit\" class=\"btn btn-primary\">Connexion</button>
  </form>


\t<button type=\"button\" class=\"btn btn-link\">
\t\t<a href=\"{{ path('signup') }}\">Inscription</a>
\t</button>

{% endblock %}", "/connect/connect.html.twig", "/var/www/projet-web/app/Resources/views/connect/connect.html.twig");
    }
}
