<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_681238283f81bad1081c8bf6f2cfe6f4b88996d7cdd14e050bd04f753872c941 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3fa59051b79a139be5cce5dc95c1b7580ce4238f9d23bcf374d34471b3be5fa0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fa59051b79a139be5cce5dc95c1b7580ce4238f9d23bcf374d34471b3be5fa0->enter($__internal_3fa59051b79a139be5cce5dc95c1b7580ce4238f9d23bcf374d34471b3be5fa0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_2006c55563b0670b695550908adc912ca22a8589f2d0cf61ea9f06a4196af898 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2006c55563b0670b695550908adc912ca22a8589f2d0cf61ea9f06a4196af898->enter($__internal_2006c55563b0670b695550908adc912ca22a8589f2d0cf61ea9f06a4196af898_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_3fa59051b79a139be5cce5dc95c1b7580ce4238f9d23bcf374d34471b3be5fa0->leave($__internal_3fa59051b79a139be5cce5dc95c1b7580ce4238f9d23bcf374d34471b3be5fa0_prof);

        
        $__internal_2006c55563b0670b695550908adc912ca22a8589f2d0cf61ea9f06a4196af898->leave($__internal_2006c55563b0670b695550908adc912ca22a8589f2d0cf61ea9f06a4196af898_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
