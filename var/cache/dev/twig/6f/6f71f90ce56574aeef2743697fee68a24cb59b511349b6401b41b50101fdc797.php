<?php

/* persos.html.twig */
class __TwigTemplate_4f7f48d7fd5447ef5c1a13477d3b8210af9e616900afb038dc1a84896f2e02f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "persos.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f2463a393c85d394005824908c9e3317ff95c2744021c9a8e02c09050c4f2182 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2463a393c85d394005824908c9e3317ff95c2744021c9a8e02c09050c4f2182->enter($__internal_f2463a393c85d394005824908c9e3317ff95c2744021c9a8e02c09050c4f2182_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "persos.html.twig"));

        $__internal_706fe023e70a65abb0c40aad9cdbc442d47108e8b35cd618decc69b5ad075b76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_706fe023e70a65abb0c40aad9cdbc442d47108e8b35cd618decc69b5ad075b76->enter($__internal_706fe023e70a65abb0c40aad9cdbc442d47108e8b35cd618decc69b5ad075b76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "persos.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f2463a393c85d394005824908c9e3317ff95c2744021c9a8e02c09050c4f2182->leave($__internal_f2463a393c85d394005824908c9e3317ff95c2744021c9a8e02c09050c4f2182_prof);

        
        $__internal_706fe023e70a65abb0c40aad9cdbc442d47108e8b35cd618decc69b5ad075b76->leave($__internal_706fe023e70a65abb0c40aad9cdbc442d47108e8b35cd618decc69b5ad075b76_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a99482e94cf8aa74170ad329a2ad597c3167536cde311b33276fcc68be2cfe61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a99482e94cf8aa74170ad329a2ad597c3167536cde311b33276fcc68be2cfe61->enter($__internal_a99482e94cf8aa74170ad329a2ad597c3167536cde311b33276fcc68be2cfe61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_bf71957fbebcba457a860e105a9e85a8f63cfdff0dcca671bff8a5496a1528cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf71957fbebcba457a860e105a9e85a8f63cfdff0dcca671bff8a5496a1528cf->enter($__internal_bf71957fbebcba457a860e105a9e85a8f63cfdff0dcca671bff8a5496a1528cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/persos.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_bf71957fbebcba457a860e105a9e85a8f63cfdff0dcca671bff8a5496a1528cf->leave($__internal_bf71957fbebcba457a860e105a9e85a8f63cfdff0dcca671bff8a5496a1528cf_prof);

        
        $__internal_a99482e94cf8aa74170ad329a2ad597c3167536cde311b33276fcc68be2cfe61->leave($__internal_a99482e94cf8aa74170ad329a2ad597c3167536cde311b33276fcc68be2cfe61_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_f5efdd311bd0adb88ebec4dde48d8a0c156550ed8ac29c8e162162744624e949 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f5efdd311bd0adb88ebec4dde48d8a0c156550ed8ac29c8e162162744624e949->enter($__internal_f5efdd311bd0adb88ebec4dde48d8a0c156550ed8ac29c8e162162744624e949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_3530aa948301a25b32c5fa4c142e5caa373323e2febd4439029f5de577c030a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3530aa948301a25b32c5fa4c142e5caa373323e2febd4439029f5de577c030a5->enter($__internal_3530aa948301a25b32c5fa4c142e5caa373323e2febd4439029f5de577c030a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h3>Vos pensions</h3>
</div>

\t
\t<div class=\"row\">
\t\t<div id=\"otherPensions\" class=\"col-sm-4\">
\t\t\t<ul id=\"list_pensions\" class=\"list-group\">
\t\t\t\t";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, (twig_length_filter($this->env, ($context["allPensions"] ?? $this->getContext($context, "allPensions"))) - 1)));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 19
            echo "\t\t\t\t\t<li id=\"pension-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"pension list-group-item
\t\t\t\t\t\t\t";
            // line 20
            if (($this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), $context["i"], array(), "array"), "name", array()) == $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "lastPension", array()), "name", array()))) {
                // line 21
                echo "\t\t\t\t\t\t\t\tactive
\t\t\t\t\t\t\t";
            }
            // line 22
            echo "\"
\t\t\t\t\t>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["allPensions"] ?? $this->getContext($context, "allPensions")), $context["i"], array(), "array"), "name", array()), "html", null, true);
            echo "</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "\t\t\t</ul>
\t\t</div>

\t\t<div id=\"currPension\" class=\"col-sm-7 col-sm-push-1\">

\t\t\t<div id=\"removePension\">
\t\t\t\t<a href='";
        // line 31
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("removePension", array("id" => $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "LastPension", array()), "id", array()))), "html", null, true);
        echo "'>
\t\t\t\t\t<button type=\"button\" id=\"rmPensionBut\" disabled class=\" upBut btn btn-default\">Supprimer</button>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t<div id=\"changePension\">
\t\t\t\t<a href='";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("changePension", array("id" => $this->getAttribute($this->getAttribute(($context["auth"] ?? $this->getContext($context, "auth")), "LastPension", array()), "id", array()))), "html", null, true);
        echo "'>
\t\t\t\t\t<button type=\"button\" id=\"changePensionBut\" disabled class=\"upBut btn btn-default\">Changer de pension</button>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t<div class=\"row\">
\t\t<div id=\"div_create_pension\" class=\"col-sm-4\">
\t\t\t<button data-toggle=\"collapse\" data-target=\"#createPension\" class=\"btn btn-default\">Nouvelle pension</button>

\t\t\t<div id=\"createPension\" class=\"collapse\">
\t\t\t\t";
        // line 48
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_start');
        echo "
\t\t\t\t";
        // line 49
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'widget');
        echo "
\t\t\t\t";
        // line 50
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_end');
        echo "
\t\t\t</div>
\t\t</div>
\t</div>

<script>
\tvar pensions = ";
        // line 56
        echo twig_jsonencode_filter(($context["allPensions"] ?? $this->getContext($context, "allPensions")));
        echo ";

\t\$(\".pension\").click(function(){
\t\t\$(\".pension\").removeClass(\"active\");
\t\t\$(this).addClass(\"active\");
\t 
\t\tvar newPension = pensions[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefRm = \$(\"#removePension > a\").attr(\"href\");
\t\t\$(\"#removePension > a\").attr(\"href\", hrefRm.replace(/[0-9]+/, newPension.id));

\t\tvar hrefChg = \$(\"#changePension > a\").attr(\"href\");
\t\t\$(\"#changePension > a\").attr(\"href\", hrefChg.replace(/[0-9]+/, newPension.id));

\t\tif (newPension.id == ";
        // line 70
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "lastPension", array()), "id", array()), "html", null, true);
        echo ")
\t\t\t\$(\".upBut\").prop(\"disabled\", true);
\t\telse \$(\".upBut\").prop(\"disabled\", false);

\t\t/** Mettre a jour div \"currPension\" **/
\t});
</script>

";
        
        $__internal_3530aa948301a25b32c5fa4c142e5caa373323e2febd4439029f5de577c030a5->leave($__internal_3530aa948301a25b32c5fa4c142e5caa373323e2febd4439029f5de577c030a5_prof);

        
        $__internal_f5efdd311bd0adb88ebec4dde48d8a0c156550ed8ac29c8e162162744624e949->leave($__internal_f5efdd311bd0adb88ebec4dde48d8a0c156550ed8ac29c8e162162744624e949_prof);

    }

    public function getTemplateName()
    {
        return "persos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 70,  161 => 56,  152 => 50,  148 => 49,  144 => 48,  129 => 36,  121 => 31,  113 => 25,  105 => 23,  102 => 22,  98 => 21,  96 => 20,  91 => 19,  87 => 18,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/persos.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Vos pensions</h3>
</div>

\t
\t<div class=\"row\">
\t\t<div id=\"otherPensions\" class=\"col-sm-4\">
\t\t\t<ul id=\"list_pensions\" class=\"list-group\">
\t\t\t\t{% for i in 0..allPensions|length -1 %}
\t\t\t\t\t<li id=\"pension-{{ i }}\" class=\"pension list-group-item
\t\t\t\t\t\t\t{% if allPensions[i].name == auth.lastPension.name %}
\t\t\t\t\t\t\t\tactive
\t\t\t\t\t\t\t{% endif %}\"
\t\t\t\t\t>{{ allPensions[i].name }}</li>
\t\t\t\t{% endfor %}
\t\t\t</ul>
\t\t</div>

\t\t<div id=\"currPension\" class=\"col-sm-7 col-sm-push-1\">

\t\t\t<div id=\"removePension\">
\t\t\t\t<a href='{{ path(\"removePension\", { \"id\" : auth.LastPension.id }) }}'>
\t\t\t\t\t<button type=\"button\" id=\"rmPensionBut\" disabled class=\" upBut btn btn-default\">Supprimer</button>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t<div id=\"changePension\">
\t\t\t\t<a href='{{ path(\"changePension\", { \"id\" : auth.LastPension.id }) }}'>
\t\t\t\t\t<button type=\"button\" id=\"changePensionBut\" disabled class=\"upBut btn btn-default\">Changer de pension</button>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t</div>
\t
\t<div class=\"row\">
\t\t<div id=\"div_create_pension\" class=\"col-sm-4\">
\t\t\t<button data-toggle=\"collapse\" data-target=\"#createPension\" class=\"btn btn-default\">Nouvelle pension</button>

\t\t\t<div id=\"createPension\" class=\"collapse\">
\t\t\t\t{{ form_start(formNew) }}
\t\t\t\t{{ form_widget(formNew) }}
\t\t\t\t{{ form_end(formNew) }}
\t\t\t</div>
\t\t</div>
\t</div>

<script>
\tvar pensions = {{ allPensions|json_encode|raw }};

\t\$(\".pension\").click(function(){
\t\t\$(\".pension\").removeClass(\"active\");
\t\t\$(this).addClass(\"active\");
\t 
\t\tvar newPension = pensions[\$(this).attr(\"id\").split(\"-\")[1]];

\t\tvar hrefRm = \$(\"#removePension > a\").attr(\"href\");
\t\t\$(\"#removePension > a\").attr(\"href\", hrefRm.replace(/[0-9]+/, newPension.id));

\t\tvar hrefChg = \$(\"#changePension > a\").attr(\"href\");
\t\t\$(\"#changePension > a\").attr(\"href\", hrefChg.replace(/[0-9]+/, newPension.id));

\t\tif (newPension.id == {{ app.user.lastPension.id }})
\t\t\t\$(\".upBut\").prop(\"disabled\", true);
\t\telse \$(\".upBut\").prop(\"disabled\", false);

\t\t/** Mettre a jour div \"currPension\" **/
\t});
</script>

{% endblock %}", "persos.html.twig", "/var/www/projet-web/app/Resources/views/persos.html.twig");
    }
}
