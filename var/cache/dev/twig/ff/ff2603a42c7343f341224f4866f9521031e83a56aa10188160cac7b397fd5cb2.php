<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_b03481cb9dd7e7237be076eb647ee577030a397ea305f277cc5f0e3ce43a874f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_54cf7c798a63616a640fc795a9dc50de807ceae325f0d2dbc893796513bed9e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_54cf7c798a63616a640fc795a9dc50de807ceae325f0d2dbc893796513bed9e7->enter($__internal_54cf7c798a63616a640fc795a9dc50de807ceae325f0d2dbc893796513bed9e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_a150f51701f723c056463e6f8a18ce564f21a3dfc12f754728b4b12abc5a1210 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a150f51701f723c056463e6f8a18ce564f21a3dfc12f754728b4b12abc5a1210->enter($__internal_a150f51701f723c056463e6f8a18ce564f21a3dfc12f754728b4b12abc5a1210_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_54cf7c798a63616a640fc795a9dc50de807ceae325f0d2dbc893796513bed9e7->leave($__internal_54cf7c798a63616a640fc795a9dc50de807ceae325f0d2dbc893796513bed9e7_prof);

        
        $__internal_a150f51701f723c056463e6f8a18ce564f21a3dfc12f754728b4b12abc5a1210->leave($__internal_a150f51701f723c056463e6f8a18ce564f21a3dfc12f754728b4b12abc5a1210_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_4669c31a98a39b93df088b524d2197254c174eebe9d5ba80173ad79e7af56359 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4669c31a98a39b93df088b524d2197254c174eebe9d5ba80173ad79e7af56359->enter($__internal_4669c31a98a39b93df088b524d2197254c174eebe9d5ba80173ad79e7af56359_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_cd9d8bea6fb46171a4538b2a8a354128f7de65889c2c299883caa7ad1bc73d2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd9d8bea6fb46171a4538b2a8a354128f7de65889c2c299883caa7ad1bc73d2b->enter($__internal_cd9d8bea6fb46171a4538b2a8a354128f7de65889c2c299883caa7ad1bc73d2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_cd9d8bea6fb46171a4538b2a8a354128f7de65889c2c299883caa7ad1bc73d2b->leave($__internal_cd9d8bea6fb46171a4538b2a8a354128f7de65889c2c299883caa7ad1bc73d2b_prof);

        
        $__internal_4669c31a98a39b93df088b524d2197254c174eebe9d5ba80173ad79e7af56359->leave($__internal_4669c31a98a39b93df088b524d2197254c174eebe9d5ba80173ad79e7af56359_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_8ac64d72abb178ed6f851c23cd0af2e2e79dcabb301c4474d6f8e1bff4ea2574 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8ac64d72abb178ed6f851c23cd0af2e2e79dcabb301c4474d6f8e1bff4ea2574->enter($__internal_8ac64d72abb178ed6f851c23cd0af2e2e79dcabb301c4474d6f8e1bff4ea2574_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1e76e0c785578f72590db750e3287d2a1b60b7551904d0ec227ddad231413090 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e76e0c785578f72590db750e3287d2a1b60b7551904d0ec227ddad231413090->enter($__internal_1e76e0c785578f72590db750e3287d2a1b60b7551904d0ec227ddad231413090_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["location"] ?? $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_1e76e0c785578f72590db750e3287d2a1b60b7551904d0ec227ddad231413090->leave($__internal_1e76e0c785578f72590db750e3287d2a1b60b7551904d0ec227ddad231413090_prof);

        
        $__internal_8ac64d72abb178ed6f851c23cd0af2e2e79dcabb301c4474d6f8e1bff4ea2574->leave($__internal_8ac64d72abb178ed6f851c23cd0af2e2e79dcabb301c4474d6f8e1bff4ea2574_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
