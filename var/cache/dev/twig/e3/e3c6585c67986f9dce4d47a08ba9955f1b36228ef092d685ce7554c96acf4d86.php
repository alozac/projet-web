<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_dcf22dd1a355cdba0088913c9abbb78af886c63f1c81f1aa459e1bf2337c85dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_acada573f50fed4ae4bde231eaed19100db9b3ae4eb62af2ef5f73965fa32675 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_acada573f50fed4ae4bde231eaed19100db9b3ae4eb62af2ef5f73965fa32675->enter($__internal_acada573f50fed4ae4bde231eaed19100db9b3ae4eb62af2ef5f73965fa32675_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_65c68c20a8352cfc6d442754db7c7d673aba9633ae5761e02da83d904729a86b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65c68c20a8352cfc6d442754db7c7d673aba9633ae5761e02da83d904729a86b->enter($__internal_65c68c20a8352cfc6d442754db7c7d673aba9633ae5761e02da83d904729a86b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_acada573f50fed4ae4bde231eaed19100db9b3ae4eb62af2ef5f73965fa32675->leave($__internal_acada573f50fed4ae4bde231eaed19100db9b3ae4eb62af2ef5f73965fa32675_prof);

        
        $__internal_65c68c20a8352cfc6d442754db7c7d673aba9633ae5761e02da83d904729a86b->leave($__internal_65c68c20a8352cfc6d442754db7c7d673aba9633ae5761e02da83d904729a86b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6f59b5c180d4d55bcd1d3282617924420e9c779677dab30bb11677f1d280cd4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f59b5c180d4d55bcd1d3282617924420e9c779677dab30bb11677f1d280cd4e->enter($__internal_6f59b5c180d4d55bcd1d3282617924420e9c779677dab30bb11677f1d280cd4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_498a05a87d26fc9db11d7350d4e69b0459a04167228a60b50b4974aee6f7afc6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_498a05a87d26fc9db11d7350d4e69b0459a04167228a60b50b4974aee6f7afc6->enter($__internal_498a05a87d26fc9db11d7350d4e69b0459a04167228a60b50b4974aee6f7afc6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_498a05a87d26fc9db11d7350d4e69b0459a04167228a60b50b4974aee6f7afc6->leave($__internal_498a05a87d26fc9db11d7350d4e69b0459a04167228a60b50b4974aee6f7afc6_prof);

        
        $__internal_6f59b5c180d4d55bcd1d3282617924420e9c779677dab30bb11677f1d280cd4e->leave($__internal_6f59b5c180d4d55bcd1d3282617924420e9c779677dab30bb11677f1d280cd4e_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_390dee4dec55abfb224ebc6e5e7934db264fea0b406d6d5c310c903176a7817a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_390dee4dec55abfb224ebc6e5e7934db264fea0b406d6d5c310c903176a7817a->enter($__internal_390dee4dec55abfb224ebc6e5e7934db264fea0b406d6d5c310c903176a7817a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_829b0e378ee535cb44c4664115cc8642de0171f7c42ef6b3085b36f69a5d82e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_829b0e378ee535cb44c4664115cc8642de0171f7c42ef6b3085b36f69a5d82e4->enter($__internal_829b0e378ee535cb44c4664115cc8642de0171f7c42ef6b3085b36f69a5d82e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_829b0e378ee535cb44c4664115cc8642de0171f7c42ef6b3085b36f69a5d82e4->leave($__internal_829b0e378ee535cb44c4664115cc8642de0171f7c42ef6b3085b36f69a5d82e4_prof);

        
        $__internal_390dee4dec55abfb224ebc6e5e7934db264fea0b406d6d5c310c903176a7817a->leave($__internal_390dee4dec55abfb224ebc6e5e7934db264fea0b406d6d5c310c903176a7817a_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_907f2a017bcb27d5230c709348932e3fb2c8f2904ea2c12cf35154dc904762cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_907f2a017bcb27d5230c709348932e3fb2c8f2904ea2c12cf35154dc904762cf->enter($__internal_907f2a017bcb27d5230c709348932e3fb2c8f2904ea2c12cf35154dc904762cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_71a565d5abe2c53336284ad6b14d2f72eaac050c76dacd099473800026800ceb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71a565d5abe2c53336284ad6b14d2f72eaac050c76dacd099473800026800ceb->enter($__internal_71a565d5abe2c53336284ad6b14d2f72eaac050c76dacd099473800026800ceb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_71a565d5abe2c53336284ad6b14d2f72eaac050c76dacd099473800026800ceb->leave($__internal_71a565d5abe2c53336284ad6b14d2f72eaac050c76dacd099473800026800ceb_prof);

        
        $__internal_907f2a017bcb27d5230c709348932e3fb2c8f2904ea2c12cf35154dc904762cf->leave($__internal_907f2a017bcb27d5230c709348932e3fb2c8f2904ea2c12cf35154dc904762cf_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
