<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_d69a53e4b74858860d7271b8fa047330c508ff6e71f4164011ebb8ff6714ac3b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_570a00b76143bcb367dd8f7c7a687838c03ce2ddd493872c72b6c2abd32d1dfa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_570a00b76143bcb367dd8f7c7a687838c03ce2ddd493872c72b6c2abd32d1dfa->enter($__internal_570a00b76143bcb367dd8f7c7a687838c03ce2ddd493872c72b6c2abd32d1dfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_9adaa985436563366e68694ee41003f5362277aa66a30e36f4e76e7227e2d60c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9adaa985436563366e68694ee41003f5362277aa66a30e36f4e76e7227e2d60c->enter($__internal_9adaa985436563366e68694ee41003f5362277aa66a30e36f4e76e7227e2d60c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_570a00b76143bcb367dd8f7c7a687838c03ce2ddd493872c72b6c2abd32d1dfa->leave($__internal_570a00b76143bcb367dd8f7c7a687838c03ce2ddd493872c72b6c2abd32d1dfa_prof);

        
        $__internal_9adaa985436563366e68694ee41003f5362277aa66a30e36f4e76e7227e2d60c->leave($__internal_9adaa985436563366e68694ee41003f5362277aa66a30e36f4e76e7227e2d60c_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_0ef3ab66b0b280358c42230345c3d49ce4a8148121c399e88101de8523fd8658 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ef3ab66b0b280358c42230345c3d49ce4a8148121c399e88101de8523fd8658->enter($__internal_0ef3ab66b0b280358c42230345c3d49ce4a8148121c399e88101de8523fd8658_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ef9e75ccb7d0302ae6e2e293ee18faf2fd49c02514e4c66df2e5f6af3344a692 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef9e75ccb7d0302ae6e2e293ee18faf2fd49c02514e4c66df2e5f6af3344a692->enter($__internal_ef9e75ccb7d0302ae6e2e293ee18faf2fd49c02514e4c66df2e5f6af3344a692_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_ef9e75ccb7d0302ae6e2e293ee18faf2fd49c02514e4c66df2e5f6af3344a692->leave($__internal_ef9e75ccb7d0302ae6e2e293ee18faf2fd49c02514e4c66df2e5f6af3344a692_prof);

        
        $__internal_0ef3ab66b0b280358c42230345c3d49ce4a8148121c399e88101de8523fd8658->leave($__internal_0ef3ab66b0b280358c42230345c3d49ce4a8148121c399e88101de8523fd8658_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_bb63e9bda14de163e956b22a81cd61f2a21e134d2964378db6bda01ee5e91ed8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb63e9bda14de163e956b22a81cd61f2a21e134d2964378db6bda01ee5e91ed8->enter($__internal_bb63e9bda14de163e956b22a81cd61f2a21e134d2964378db6bda01ee5e91ed8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a1338f8e783207c1f985ca6493cbd25f49d7372759edd1976ab94c71476f5249 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1338f8e783207c1f985ca6493cbd25f49d7372759edd1976ab94c71476f5249->enter($__internal_a1338f8e783207c1f985ca6493cbd25f49d7372759edd1976ab94c71476f5249_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_a1338f8e783207c1f985ca6493cbd25f49d7372759edd1976ab94c71476f5249->leave($__internal_a1338f8e783207c1f985ca6493cbd25f49d7372759edd1976ab94c71476f5249_prof);

        
        $__internal_bb63e9bda14de163e956b22a81cd61f2a21e134d2964378db6bda01ee5e91ed8->leave($__internal_bb63e9bda14de163e956b22a81cd61f2a21e134d2964378db6bda01ee5e91ed8_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_a01ab000d195cacc18c98b68aa66e1e5df51567d735cafca366b59aa0a18e6ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a01ab000d195cacc18c98b68aa66e1e5df51567d735cafca366b59aa0a18e6ac->enter($__internal_a01ab000d195cacc18c98b68aa66e1e5df51567d735cafca366b59aa0a18e6ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_24b011b40a9531e0d1036da3f4e900e9b3be2cc1d8541eb3f8f415c511be53a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24b011b40a9531e0d1036da3f4e900e9b3be2cc1d8541eb3f8f415c511be53a1->enter($__internal_24b011b40a9531e0d1036da3f4e900e9b3be2cc1d8541eb3f8f415c511be53a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_24b011b40a9531e0d1036da3f4e900e9b3be2cc1d8541eb3f8f415c511be53a1->leave($__internal_24b011b40a9531e0d1036da3f4e900e9b3be2cc1d8541eb3f8f415c511be53a1_prof);

        
        $__internal_a01ab000d195cacc18c98b68aa66e1e5df51567d735cafca366b59aa0a18e6ac->leave($__internal_a01ab000d195cacc18c98b68aa66e1e5df51567d735cafca366b59aa0a18e6ac_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "C:\\wamp64\\www\\projet-web\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
