<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_b6d56098bc91a629e8a0ec271b83420f6bc7b332c679e3c6c1a274634ee90af5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ec1978949b861fb4a601a63cf5a6f32a1dade4d24d24bcdf102cc97f1705ef47 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ec1978949b861fb4a601a63cf5a6f32a1dade4d24d24bcdf102cc97f1705ef47->enter($__internal_ec1978949b861fb4a601a63cf5a6f32a1dade4d24d24bcdf102cc97f1705ef47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        $__internal_ffd2daab961de5cfab433e5b9f3ecd9b0c1c8d0866c484da95f3f3cc5571742d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ffd2daab961de5cfab433e5b9f3ecd9b0c1c8d0866c484da95f3f3cc5571742d->enter($__internal_ffd2daab961de5cfab433e5b9f3ecd9b0c1c8d0866c484da95f3f3cc5571742d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, ($context["status_code"] ?? $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_ec1978949b861fb4a601a63cf5a6f32a1dade4d24d24bcdf102cc97f1705ef47->leave($__internal_ec1978949b861fb4a601a63cf5a6f32a1dade4d24d24bcdf102cc97f1705ef47_prof);

        
        $__internal_ffd2daab961de5cfab433e5b9f3ecd9b0c1c8d0866c484da95f3f3cc5571742d->leave($__internal_ffd2daab961de5cfab433e5b9f3ecd9b0c1c8d0866c484da95f3f3cc5571742d_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.js.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
