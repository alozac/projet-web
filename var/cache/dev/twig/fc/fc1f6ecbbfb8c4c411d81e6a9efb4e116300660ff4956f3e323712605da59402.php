<?php

/* post/post_write.html.twig */
class __TwigTemplate_1c64977cbb50d6f8bb686abb1622474359026cebdb95172d8f92585b40e453eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "post/post_write.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01790655c000d0611565df2cebffa71941a39753cbcf77790ffafa521a6238ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01790655c000d0611565df2cebffa71941a39753cbcf77790ffafa521a6238ec->enter($__internal_01790655c000d0611565df2cebffa71941a39753cbcf77790ffafa521a6238ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_write.html.twig"));

        $__internal_c9f6ca929141e7b72becc388f2c52ee682cc57cf42548430efdb3fdf3422a6fb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9f6ca929141e7b72becc388f2c52ee682cc57cf42548430efdb3fdf3422a6fb->enter($__internal_c9f6ca929141e7b72becc388f2c52ee682cc57cf42548430efdb3fdf3422a6fb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "post/post_write.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_01790655c000d0611565df2cebffa71941a39753cbcf77790ffafa521a6238ec->leave($__internal_01790655c000d0611565df2cebffa71941a39753cbcf77790ffafa521a6238ec_prof);

        
        $__internal_c9f6ca929141e7b72becc388f2c52ee682cc57cf42548430efdb3fdf3422a6fb->leave($__internal_c9f6ca929141e7b72becc388f2c52ee682cc57cf42548430efdb3fdf3422a6fb_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9972641b2985c788844e49487dc13235741c90d106c5a9c02aef679056d93e84 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9972641b2985c788844e49487dc13235741c90d106c5a9c02aef679056d93e84->enter($__internal_9972641b2985c788844e49487dc13235741c90d106c5a9c02aef679056d93e84_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_e884a6dc5d74d5bbf356ba79cc6957e2861ee05ff3cfe8bb81ea32a52bd2c9a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e884a6dc5d74d5bbf356ba79cc6957e2861ee05ff3cfe8bb81ea32a52bd2c9a4->enter($__internal_e884a6dc5d74d5bbf356ba79cc6957e2861ee05ff3cfe8bb81ea32a52bd2c9a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/post_new.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_e884a6dc5d74d5bbf356ba79cc6957e2861ee05ff3cfe8bb81ea32a52bd2c9a4->leave($__internal_e884a6dc5d74d5bbf356ba79cc6957e2861ee05ff3cfe8bb81ea32a52bd2c9a4_prof);

        
        $__internal_9972641b2985c788844e49487dc13235741c90d106c5a9c02aef679056d93e84->leave($__internal_9972641b2985c788844e49487dc13235741c90d106c5a9c02aef679056d93e84_prof);

    }

    // line 8
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_8fae48d5466bb92e7aae09859023b2ca7318b43c0a9655181a7b2fa9daf94069 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8fae48d5466bb92e7aae09859023b2ca7318b43c0a9655181a7b2fa9daf94069->enter($__internal_8fae48d5466bb92e7aae09859023b2ca7318b43c0a9655181a7b2fa9daf94069_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_c66074ab2f2cba786ded0082d15593b0e5240f923b7dd0fc33810ef319654e32 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c66074ab2f2cba786ded0082d15593b0e5240f923b7dd0fc33810ef319654e32->enter($__internal_c66074ab2f2cba786ded0082d15593b0e5240f923b7dd0fc33810ef319654e32_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 9
        echo "
<div class=\"page-header\">
\t<h3>Nouveau message</h3>
</div>

\t";
        // line 14
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_start');
        echo "
\t";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "receiver", array()), 'row');
        echo "
\t";
        // line 16
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
            // line 17
            echo "\t\t";
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "toAllPensions", array()), 'row');
            echo "\t";
        }
        // line 18
        echo "\t";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "object", array()), 'row');
        echo "
\t";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "message", array()), 'row');
        echo "
\t";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "send", array()), 'row', array("attr" => array("class" => "btn-primary")));
        echo "
\t";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute(($context["formNew"] ?? $this->getContext($context, "formNew")), "_token", array()), 'row');
        echo "
\t";
        // line 22
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock(($context["formNew"] ?? $this->getContext($context, "formNew")), 'form_end', array("render_rest" => false));
        echo "
<script>
\t\$(\"#message_toAllPensions\").click(function(){
\t\tif (\$(this).is(\":checked\"))
\t\t\t\$(\"#message_receiver\").prop('disabled', true);
\t\telse 
\t\t\t\$(\"#message_receiver\").prop('disabled', false);
\t});
</script>
";
        
        $__internal_c66074ab2f2cba786ded0082d15593b0e5240f923b7dd0fc33810ef319654e32->leave($__internal_c66074ab2f2cba786ded0082d15593b0e5240f923b7dd0fc33810ef319654e32_prof);

        
        $__internal_8fae48d5466bb92e7aae09859023b2ca7318b43c0a9655181a7b2fa9daf94069->leave($__internal_8fae48d5466bb92e7aae09859023b2ca7318b43c0a9655181a7b2fa9daf94069_prof);

    }

    public function getTemplateName()
    {
        return "post/post_write.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 22,  111 => 21,  107 => 20,  103 => 19,  98 => 18,  93 => 17,  91 => 16,  87 => 15,  83 => 14,  76 => 9,  67 => 8,  55 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/post_new.css')}}\"/>
{% endblock %}

{% block main_screen %}

<div class=\"page-header\">
\t<h3>Nouveau message</h3>
</div>

\t{{ form_start(formNew) }}
\t{{ form_row(formNew.receiver)  }}
\t{% if is_granted('ROLE_ADMIN') %}
\t\t{{ form_row(formNew.toAllPensions) }}\t{% endif %}
\t{{ form_row(formNew.object) }}
\t{{ form_row(formNew.message) }}
\t{{ form_row(formNew.send, { 'attr': {'class': 'btn-primary'} }) }}
\t{{ form_row(formNew._token) }}
\t{{ form_end(formNew, {'render_rest': false}) }}
<script>
\t\$(\"#message_toAllPensions\").click(function(){
\t\tif (\$(this).is(\":checked\"))
\t\t\t\$(\"#message_receiver\").prop('disabled', true);
\t\telse 
\t\t\t\$(\"#message_receiver\").prop('disabled', false);
\t});
</script>
{% endblock %}", "post/post_write.html.twig", "C:\\wamp64\\www\\projet-web\\app\\Resources\\views\\post\\post_write.html.twig");
    }
}
