<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_278ad2f69e54ceabdca392d33c4489a8ea6e2564bd5c686f5ac8d70a12df54b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_75c6666eb3498cea8613ca6993d8ea1ab3803ff3b5d55da7e87eee278ce56cde = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75c6666eb3498cea8613ca6993d8ea1ab3803ff3b5d55da7e87eee278ce56cde->enter($__internal_75c6666eb3498cea8613ca6993d8ea1ab3803ff3b5d55da7e87eee278ce56cde_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_f95c5b3c2489eb98b2ab1c03fd1499087a74acd4cb8feb1f94d8077c6cd9f758 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f95c5b3c2489eb98b2ab1c03fd1499087a74acd4cb8feb1f94d8077c6cd9f758->enter($__internal_f95c5b3c2489eb98b2ab1c03fd1499087a74acd4cb8feb1f94d8077c6cd9f758_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_75c6666eb3498cea8613ca6993d8ea1ab3803ff3b5d55da7e87eee278ce56cde->leave($__internal_75c6666eb3498cea8613ca6993d8ea1ab3803ff3b5d55da7e87eee278ce56cde_prof);

        
        $__internal_f95c5b3c2489eb98b2ab1c03fd1499087a74acd4cb8feb1f94d8077c6cd9f758->leave($__internal_f95c5b3c2489eb98b2ab1c03fd1499087a74acd4cb8feb1f94d8077c6cd9f758_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
