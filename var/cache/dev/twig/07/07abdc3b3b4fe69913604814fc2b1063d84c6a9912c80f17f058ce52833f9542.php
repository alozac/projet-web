<?php

/* missions.html.twig */
class __TwigTemplate_8973753529fd7ba1728e8e7d5a645233573b1fb62b1b224a132b9eaf04beb6cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "missions.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'main_screen' => array($this, 'block_main_screen'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b31d33541eb31edc2e6fa93c7bdec84a886177acc656ffbf411721fab6002467 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b31d33541eb31edc2e6fa93c7bdec84a886177acc656ffbf411721fab6002467->enter($__internal_b31d33541eb31edc2e6fa93c7bdec84a886177acc656ffbf411721fab6002467_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "missions.html.twig"));

        $__internal_86997731f3af5843cf8d65872195cc9bd59289706290634f6ae469d98083ed48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_86997731f3af5843cf8d65872195cc9bd59289706290634f6ae469d98083ed48->enter($__internal_86997731f3af5843cf8d65872195cc9bd59289706290634f6ae469d98083ed48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "missions.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b31d33541eb31edc2e6fa93c7bdec84a886177acc656ffbf411721fab6002467->leave($__internal_b31d33541eb31edc2e6fa93c7bdec84a886177acc656ffbf411721fab6002467_prof);

        
        $__internal_86997731f3af5843cf8d65872195cc9bd59289706290634f6ae469d98083ed48->leave($__internal_86997731f3af5843cf8d65872195cc9bd59289706290634f6ae469d98083ed48_prof);

    }

    // line 3
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_b8ab0fcf2b2462d50705100a7f28ba9034941c82029c58fb779d56259d2f2b8a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8ab0fcf2b2462d50705100a7f28ba9034941c82029c58fb779d56259d2f2b8a->enter($__internal_b8ab0fcf2b2462d50705100a7f28ba9034941c82029c58fb779d56259d2f2b8a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_f348ce020a4fe8320d2ff481b35dbe0de591b56996c352b665499f74872a2a86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f348ce020a4fe8320d2ff481b35dbe0de591b56996c352b665499f74872a2a86->enter($__internal_f348ce020a4fe8320d2ff481b35dbe0de591b56996c352b665499f74872a2a86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 4
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/missions.css"), "html", null, true);
        echo "\"/>
";
        
        $__internal_f348ce020a4fe8320d2ff481b35dbe0de591b56996c352b665499f74872a2a86->leave($__internal_f348ce020a4fe8320d2ff481b35dbe0de591b56996c352b665499f74872a2a86_prof);

        
        $__internal_b8ab0fcf2b2462d50705100a7f28ba9034941c82029c58fb779d56259d2f2b8a->leave($__internal_b8ab0fcf2b2462d50705100a7f28ba9034941c82029c58fb779d56259d2f2b8a_prof);

    }

    // line 9
    public function block_main_screen($context, array $blocks = array())
    {
        $__internal_76fe17275cebdb084b53e7da6ac5bc54fa08a9a185a9a639feb250a413cd0fe0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76fe17275cebdb084b53e7da6ac5bc54fa08a9a185a9a639feb250a413cd0fe0->enter($__internal_76fe17275cebdb084b53e7da6ac5bc54fa08a9a185a9a639feb250a413cd0fe0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        $__internal_88964657397aa53fa70c8adc551ed7ba0d2cd2b810642191929daa88be093de9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88964657397aa53fa70c8adc551ed7ba0d2cd2b810642191929daa88be093de9->enter($__internal_88964657397aa53fa70c8adc551ed7ba0d2cd2b810642191929daa88be093de9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main_screen"));

        // line 10
        echo "
 ";
        // line 11
        if ((($context["new_possible"] ?? $this->getContext($context, "new_possible")) == 1)) {
            // line 12
            echo " \t<div class=\"alert alert-danger\">
 \t\tNombre de Paws insuffisant, revenez demain!
 \t</div>
 ";
        } elseif ((        // line 15
($context["new_possible"] ?? $this->getContext($context, "new_possible")) == 2)) {
            // line 16
            echo "  \t<div class=\"alert alert-danger\">
 \t\tNombre de missions maximal atteint! Finissez une mission pour pouvoir en accepter une autre.
 \t</div>
 ";
        }
        // line 20
        echo "
\t<div id=\"currMissDescr\" class=\"clearfix\">
\t\t<h2></h2>
\t\t<img class = \"pull-left img-responsive\"/>
\t\t<p></p>
\t</div>

\t<ul id=\"listM\" class=\"col-sm-2\">
\t";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(range(0, 2));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 29
            echo "\t\t<li id=\"mission-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"link nameMission\">";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["missions"] ?? $this->getContext($context, "missions")), $context["i"], array(), "array"), "name", array()), "html", null, true);
            echo "</li>
\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "\t</ul>

\t<div id=\"currMissRewards\" class=\"col-sm-5 col-sm-push-1\">
\t\t<div id=\"rewards\">
\t\t\t<h4>Récompenses :</h4>

\t\t\t<ul>
\t\t\t\t<li>
\t\t\t\t\t<span id=\"gold\"></span>
\t\t\t\t\t<img src=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/po.png"), "html", null, true);
        echo "\">
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\tRéputation : <span id=\"reputation\"></span>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\tDurée : <span id=\"duration\"></span>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>
\t\t<a href='";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("acceptedMiss", array("id" => 0));
        echo "' id=\"acceptBut\"
\t\t\ttype=\"button\" class=\"btn btn-default ";
        // line 51
        if ((($context["new_possible"] ?? $this->getContext($context, "new_possible")) != 0)) {
            echo " disabled ";
        }
        echo "\">
\t\t\t\tAccepter (1 <img src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("imgs/paws.png"), "html", null, true);
        echo "\">)
\t\t</a>
\t</div>

";
        
        $__internal_88964657397aa53fa70c8adc551ed7ba0d2cd2b810642191929daa88be093de9->leave($__internal_88964657397aa53fa70c8adc551ed7ba0d2cd2b810642191929daa88be093de9_prof);

        
        $__internal_76fe17275cebdb084b53e7da6ac5bc54fa08a9a185a9a639feb250a413cd0fe0->leave($__internal_76fe17275cebdb084b53e7da6ac5bc54fa08a9a185a9a639feb250a413cd0fe0_prof);

    }

    // line 58
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_4bb222c324ab0d898e0d72989d0d7915a6efc22b0b22bfdd709bfbaff50eb78e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4bb222c324ab0d898e0d72989d0d7915a6efc22b0b22bfdd709bfbaff50eb78e->enter($__internal_4bb222c324ab0d898e0d72989d0d7915a6efc22b0b22bfdd709bfbaff50eb78e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_a5e9d1e6d34231ab3a9731d320b434b37481962591b1973cfaad262cf8ca4960 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a5e9d1e6d34231ab3a9731d320b434b37481962591b1973cfaad262cf8ca4960->enter($__internal_a5e9d1e6d34231ab3a9731d320b434b37481962591b1973cfaad262cf8ca4960_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 59
        echo " <script>

 var missions = ";
        // line 61
        echo twig_jsonencode_filter(($context["missions"] ?? $this->getContext($context, "missions")));
        echo ";

\$(\".nameMission\").click(function(){
\t\$(\".nameMission\").css(\"font-weight\" , \"\");
\t\$(this).css(\"font-weight\" , \"bold\");
 
\tvar newMiss = missions[\$(this).attr(\"id\").split(\"-\")[1]];

\tvar href = \$(\"#currMissRewards > a\").attr(\"href\");
\t\$(\"#currMissRewards > a\").attr(\"href\", href.replace(/[0-9]+/, newMiss.id));

\t\$(\"#currMissDescr > h2\").html(newMiss.name);
\t\$(\"#currMissDescr > img\").attr({\"src\" : \"imgs/missions/\" + newMiss.image,
\t\t\t\t\t\t\t\t\t\"alt\" : \"Image de \" + newMiss.name
\t\t\t\t\t\t\t\t\t});
\t\$(\"#currMissDescr > p\").html(newMiss.descr);

\tvar durMiss = newMiss.duration;

\tvar duration = (Math.floor(durMiss/24) > 0 ? (Math.floor(durMiss/24)+\"j \") : \"\") + 
\t\t\t\t   (durMiss%24 > 0 ? (durMiss%24+\"h\") : \"\"); 

\t\$(\"#gold\").html(newMiss.gold);
\t\$(\"#reputation\").html(newMiss.reputation);
\t\$(\"#duration\").html(duration);

});
\$(\"#mission-0\").click();

</script>

";
        
        $__internal_a5e9d1e6d34231ab3a9731d320b434b37481962591b1973cfaad262cf8ca4960->leave($__internal_a5e9d1e6d34231ab3a9731d320b434b37481962591b1973cfaad262cf8ca4960_prof);

        
        $__internal_4bb222c324ab0d898e0d72989d0d7915a6efc22b0b22bfdd709bfbaff50eb78e->leave($__internal_4bb222c324ab0d898e0d72989d0d7915a6efc22b0b22bfdd709bfbaff50eb78e_prof);

    }

    public function getTemplateName()
    {
        return "missions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 61,  178 => 59,  169 => 58,  154 => 52,  148 => 51,  144 => 50,  131 => 40,  120 => 31,  109 => 29,  105 => 28,  95 => 20,  89 => 16,  87 => 15,  82 => 12,  80 => 11,  77 => 10,  68 => 9,  56 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block stylesheets %}
\t{{ parent() }}
    <link rel=\"stylesheet\" href=\"{{asset('css/missions.css')}}\"/>
{% endblock %}


{% block main_screen %}

 {% if new_possible == 1 %}
 \t<div class=\"alert alert-danger\">
 \t\tNombre de Paws insuffisant, revenez demain!
 \t</div>
 {% elseif new_possible == 2 %}
  \t<div class=\"alert alert-danger\">
 \t\tNombre de missions maximal atteint! Finissez une mission pour pouvoir en accepter une autre.
 \t</div>
 {% endif %}

\t<div id=\"currMissDescr\" class=\"clearfix\">
\t\t<h2></h2>
\t\t<img class = \"pull-left img-responsive\"/>
\t\t<p></p>
\t</div>

\t<ul id=\"listM\" class=\"col-sm-2\">
\t{% for i in 0..2 %}
\t\t<li id=\"mission-{{ i }}\" class=\"link nameMission\">{{ missions[i].name }}</li>
\t{% endfor %}
\t</ul>

\t<div id=\"currMissRewards\" class=\"col-sm-5 col-sm-push-1\">
\t\t<div id=\"rewards\">
\t\t\t<h4>Récompenses :</h4>

\t\t\t<ul>
\t\t\t\t<li>
\t\t\t\t\t<span id=\"gold\"></span>
\t\t\t\t\t<img src=\"{{ asset('imgs/po.png') }}\">
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\tRéputation : <span id=\"reputation\"></span>
\t\t\t\t</li>
\t\t\t\t<li>
\t\t\t\t\tDurée : <span id=\"duration\"></span>
\t\t\t\t</li>
\t\t\t</ul>
\t\t</div>
\t\t<a href='{{ path(\"acceptedMiss\", { \"id\" : 0 }) }}' id=\"acceptBut\"
\t\t\ttype=\"button\" class=\"btn btn-default {% if new_possible != 0 %} disabled {% endif %}\">
\t\t\t\tAccepter (1 <img src=\"{{ asset('imgs/paws.png') }}\">)
\t\t</a>
\t</div>

{% endblock %}

 {% block javascripts %}
 <script>

 var missions = {{ missions|json_encode|raw }};

\$(\".nameMission\").click(function(){
\t\$(\".nameMission\").css(\"font-weight\" , \"\");
\t\$(this).css(\"font-weight\" , \"bold\");
 
\tvar newMiss = missions[\$(this).attr(\"id\").split(\"-\")[1]];

\tvar href = \$(\"#currMissRewards > a\").attr(\"href\");
\t\$(\"#currMissRewards > a\").attr(\"href\", href.replace(/[0-9]+/, newMiss.id));

\t\$(\"#currMissDescr > h2\").html(newMiss.name);
\t\$(\"#currMissDescr > img\").attr({\"src\" : \"imgs/missions/\" + newMiss.image,
\t\t\t\t\t\t\t\t\t\"alt\" : \"Image de \" + newMiss.name
\t\t\t\t\t\t\t\t\t});
\t\$(\"#currMissDescr > p\").html(newMiss.descr);

\tvar durMiss = newMiss.duration;

\tvar duration = (Math.floor(durMiss/24) > 0 ? (Math.floor(durMiss/24)+\"j \") : \"\") + 
\t\t\t\t   (durMiss%24 > 0 ? (durMiss%24+\"h\") : \"\"); 

\t\$(\"#gold\").html(newMiss.gold);
\t\$(\"#reputation\").html(newMiss.reputation);
\t\$(\"#duration\").html(duration);

});
\$(\"#mission-0\").click();

</script>

{% endblock %}", "missions.html.twig", "/var/www/projet-web/app/Resources/views/missions.html.twig");
    }
}
