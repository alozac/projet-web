<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_adb526847cdf210a36c75ba1504e3e95fa45a85033a35d1d7a9d2bc2ae61ea5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_883f9f399f732df18dbecc817f0188588245b59b847765d30c52d10817dc2733 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_883f9f399f732df18dbecc817f0188588245b59b847765d30c52d10817dc2733->enter($__internal_883f9f399f732df18dbecc817f0188588245b59b847765d30c52d10817dc2733_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_13b006fe7368cfec0ada397abddab03858265e128b6e973ef499dd2a5808b7dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13b006fe7368cfec0ada397abddab03858265e128b6e973ef499dd2a5808b7dc->enter($__internal_13b006fe7368cfec0ada397abddab03858265e128b6e973ef499dd2a5808b7dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_883f9f399f732df18dbecc817f0188588245b59b847765d30c52d10817dc2733->leave($__internal_883f9f399f732df18dbecc817f0188588245b59b847765d30c52d10817dc2733_prof);

        
        $__internal_13b006fe7368cfec0ada397abddab03858265e128b6e973ef499dd2a5808b7dc->leave($__internal_13b006fe7368cfec0ada397abddab03858265e128b6e973ef499dd2a5808b7dc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_row.html.php");
    }
}
