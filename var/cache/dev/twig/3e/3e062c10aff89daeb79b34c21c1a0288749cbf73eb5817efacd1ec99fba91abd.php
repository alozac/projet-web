<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_c0f4eb0aa1f57f9b5c201a58718dfe29da11a80e4eff179f9560942617f82b39 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09b757cefd82ab6df7d403c1043e07e1d0cae7cd0cb4c4e8f3e6a6a43ee59927 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09b757cefd82ab6df7d403c1043e07e1d0cae7cd0cb4c4e8f3e6a6a43ee59927->enter($__internal_09b757cefd82ab6df7d403c1043e07e1d0cae7cd0cb4c4e8f3e6a6a43ee59927_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_77da7424de7a81cedaed96a3b45854a7bfe6e2b36b2720d816d341714da09877 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77da7424de7a81cedaed96a3b45854a7bfe6e2b36b2720d816d341714da09877->enter($__internal_77da7424de7a81cedaed96a3b45854a7bfe6e2b36b2720d816d341714da09877_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_09b757cefd82ab6df7d403c1043e07e1d0cae7cd0cb4c4e8f3e6a6a43ee59927->leave($__internal_09b757cefd82ab6df7d403c1043e07e1d0cae7cd0cb4c4e8f3e6a6a43ee59927_prof);

        
        $__internal_77da7424de7a81cedaed96a3b45854a7bfe6e2b36b2720d816d341714da09877->leave($__internal_77da7424de7a81cedaed96a3b45854a7bfe6e2b36b2720d816d341714da09877_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
