<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_38e037e47066014282d8f1c2202b8c84ee2349cdad23d29482e1aa51dfc9b85f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df1ebbbc8f5f3c43c4c1b5893af23246af1ee9cbd2c72bd16f90012515dba083 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df1ebbbc8f5f3c43c4c1b5893af23246af1ee9cbd2c72bd16f90012515dba083->enter($__internal_df1ebbbc8f5f3c43c4c1b5893af23246af1ee9cbd2c72bd16f90012515dba083_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_69010e0877e6ed47183d388b060bf42b338da1617e51c54403f90e889cfcf8cc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69010e0877e6ed47183d388b060bf42b338da1617e51c54403f90e889cfcf8cc->enter($__internal_69010e0877e6ed47183d388b060bf42b338da1617e51c54403f90e889cfcf8cc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_df1ebbbc8f5f3c43c4c1b5893af23246af1ee9cbd2c72bd16f90012515dba083->leave($__internal_df1ebbbc8f5f3c43c4c1b5893af23246af1ee9cbd2c72bd16f90012515dba083_prof);

        
        $__internal_69010e0877e6ed47183d388b060bf42b338da1617e51c54403f90e889cfcf8cc->leave($__internal_69010e0877e6ed47183d388b060bf42b338da1617e51c54403f90e889cfcf8cc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
