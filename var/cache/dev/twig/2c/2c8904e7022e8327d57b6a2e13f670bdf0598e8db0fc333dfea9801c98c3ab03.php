<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_35257767f5a8030d00b165b9d61374d99daa315600bb3ccedec2595b30197068 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_97bf13ea627e3a9a325045a1201ccae1d706236b399b4e0868889dde94b1f22a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_97bf13ea627e3a9a325045a1201ccae1d706236b399b4e0868889dde94b1f22a->enter($__internal_97bf13ea627e3a9a325045a1201ccae1d706236b399b4e0868889dde94b1f22a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_43d92601b0a7d3824bf9410452452f82effa8071cb9975e252b36f8d140c08b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43d92601b0a7d3824bf9410452452f82effa8071cb9975e252b36f8d140c08b7->enter($__internal_43d92601b0a7d3824bf9410452452f82effa8071cb9975e252b36f8d140c08b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        $this->loadTemplate("@Twig/Exception/exception.txt.twig", "TwigBundle:Exception:exception.css.twig", 2)->display(array_merge($context, array("exception" => ($context["exception"] ?? $this->getContext($context, "exception")))));
        // line 3
        echo "*/
";
        
        $__internal_97bf13ea627e3a9a325045a1201ccae1d706236b399b4e0868889dde94b1f22a->leave($__internal_97bf13ea627e3a9a325045a1201ccae1d706236b399b4e0868889dde94b1f22a_prof);

        
        $__internal_43d92601b0a7d3824bf9410452452f82effa8071cb9975e252b36f8d140c08b7->leave($__internal_43d92601b0a7d3824bf9410452452f82effa8071cb9975e252b36f8d140c08b7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{% include '@Twig/Exception/exception.txt.twig' with { 'exception': exception } %}
*/
", "TwigBundle:Exception:exception.css.twig", "/var/www/projet-web/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
