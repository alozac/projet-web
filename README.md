Jeu de gestion de pensions animalières réalisé avec le framework Symfony 2 dans le cadre d'un projet universitaire.
Le joueur est amené à gérer une pension animalière en acceptant des missions lui rapportant diverses ressources.
Il doit alors prendre soin de s'occuper des animaux recueillis, en les nourissant et les soignant si besoin.